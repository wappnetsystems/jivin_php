<?php

use Illuminate\Database\Seeder;

class PackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert([
        	[
            'package_name' => 'Basic Plan',
            'package_price' =>0,
            'package_duretion' =>null,
            'is_unlimited' =>1,
            'is_free' =>1,
            'is_active' => 1,        	
        	],
        	[
            'package_name' => 'Pro Plan 1',
            'package_price' =>10,
            'package_duretion' =>1, //Month
            'is_unlimited' =>0,
            'is_free' =>0,
            'is_active' => 1,        	
        	],        	
        	[
            'package_name' => 'Pro Plan 2',
            'package_price' =>25,
            'package_duretion' =>3, //Month
            'is_unlimited' =>0,
            'is_free' =>0,
            'is_active' => 1,        	
        	],
        	[
            'package_name' => 'Pro Plan 3',
            'package_price' =>50,
            'package_duretion' =>6, //Month
            'is_unlimited' =>0,
            'is_free' =>0,
            'is_active' => 1,        	
        	],
        	[
            'package_name' => 'Pro Plan 4',
            'package_price' =>100,
            'package_duretion' =>12, //Month
            'is_unlimited' =>0,
            'is_free' =>0,
            'is_active' => 1,        	
        	],        	        	
        	[
            'package_name' => 'Pro Plan 5',
            'package_price' =>1000,
            'package_duretion' =>null, //Month
            'is_unlimited' =>1,
            'is_free' =>0,
            'is_active' => 1,        	
        	],        	        	
        ]);
    }
}
