<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	[
            'name' => 'Jivin User1',
            'email' => 'user1@jivin.com',
            'username'=>'user1',
            'password' => bcrypt('123456'),        	
        	],
        	[
            'name' => 'Jivin User2',
            'email' => 'user2@jivin.com',
            'username'=>'user2',
            'password' => bcrypt('123456'),        	
        	],
        	[
            'name' => 'Jivin User3',
            'email' => 'user3@jivin.com',
            'username'=>'user3',
            'password' => bcrypt('123456'),        	
        	],
        	[
            'name' => 'Jivin User4',
            'email' => 'user4@jivin.com',
            'username'=>'user4',
            'password' => bcrypt('123456'),        	
        	],
        	[
            'name' => 'Jivin User5',
            'email' => 'user5@jivin.com',
            'username'=>'user5',
            'password' => bcrypt('123456'),        	
        	], 
        	[
            'name' => 'Jivin User6',
            'email' => 'user6@jivin.com',
            'username'=>'user6',
            'password' => bcrypt('123456'),        	
        	],
        	[
            'name' => 'Jivin User7',
            'email' => 'user7@jivin.com',
            'username'=>'user7',
            'password' => bcrypt('123456'),        	
        	],
        	[
            'name' => 'Jivin User8',
            'email' => 'user8@jivin.com',
            'username'=>'user8',
            'password' => bcrypt('123456'),        	
        	],
        	[
            'name' => 'Jivin User9',
            'email' => 'user9@jivin.com',
            'username'=>'user9',
            'password' => bcrypt('123456'),        	
        	],
        	[
            'name' => 'Jivin User10',
            'email' => 'user10@jivin.com',
            'username'=>'user10',
            'password' => bcrypt('123456'),        	
        	],         	       	        	        	        	
        ]);
    }
}
