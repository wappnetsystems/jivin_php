<?php

use Illuminate\Database\Seeder;

class InterestCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('intereste_category')->insert([
        	[
            'name' => 'Music',
            'is_active' => 1,        	
        	],
        	[
            'name' => 'Dance',
            'is_active' => 1,        	
        	],
        	[
            'name' => 'Instuments',
            'is_active' => 1,        	
        	]        	        	
        ]);
    }
}
