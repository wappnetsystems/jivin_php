<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	 $this->call(AdminsTableSeeder::class);
    	 //$this->call(UsersTableSeeder::class);
         $this->call(InterestCategorySeeder::class);
         $this->call(SubInterestCategorySeeder::class);
         //$this->call(PackagesTableSeeder::class);
         //$this->call(SubscriptionTableSeeder::class);
         //$this->call(CountryTableSeeder::class);
         //$this->call(RegionTableSeeder::class);
         //$this->call(DivisionTableSeeder::class);
         //$this->call(StateTableSeeder::class);
    }
}
