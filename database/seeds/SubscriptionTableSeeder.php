<?php

use Illuminate\Database\Seeder;

class SubscriptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscription')->insert([
        	[
            'user_id' => 1,
            'package_id' =>1, //Basic user
            'package_valide_from' =>null,
            'package_valide_to' =>null,
            'is_active' => 1,        	
        	],
        	[
            'user_id' => 2, //Basic user
            'package_id' =>1,
            'package_valide_from' =>null,
            'package_valide_to' =>null,
            'is_active' => 1,        	
        	],
        	[
            'user_id' => 3, //Basic user
            'package_id' =>1,
            'package_valide_from' =>null,
            'package_valide_to' =>null,
            'is_active' => 1,        	
        	],
        	[
            'user_id' => 4, //Basic user
            'package_id' =>1,
            'package_valide_from' =>null,
            'package_valide_to' =>null,
            'is_active' => 1,        	
        	],
        	[
            'user_id' => 5, //Basic user
            'package_id' =>1,
            'package_valide_from' =>null,
            'package_valide_to' =>null,
            'is_active' => 1,        	
        	],
        	[
            'user_id' => 6, //Pro user
            'package_id' =>2,
            'package_valide_from' =>'2017-03-14',
            'package_valide_to' =>'2017-04-14',
            'is_active' => 1,        	
        	],
        	[
            'user_id' => 7, //Pro user
            'package_id' =>2,
            'package_valide_from' =>'2017-03-14',
            'package_valide_to' =>'2017-04-14',
            'is_active' => 1,        	
        	],
        	[
            'user_id' => 8, //Pro user
            'package_id' =>2,
            'package_valide_from' =>'2017-03-14',
            'package_valide_to' =>'2017-04-14',
            'is_active' => 1,        	
        	],
        	[
            'user_id' => 9, //Pro user
            'package_id' =>2,
            'package_valide_from' =>'2017-03-14',
            'package_valide_to' =>'2017-04-14',
            'is_active' => 1,        	
        	],
        	[
            'user_id' => 10, //Pro user
            'package_id' =>2,
            'package_valide_from' =>'2017-03-14',
            'package_valide_to' =>'2017-04-14',
            'is_active' => 1,        	
        	],        	        	        	        	        	        	        	        	        	      	        	
        ]);
    }
}
