<?php

use Illuminate\Database\Seeder;

class SubInterestCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_intereste_category')->insert([
        	[
            'name' => 'Alternative',
            'interest_category_id' => 1,
            'is_active' => 1,        	
        	],
        	[
            'name' => 'Blues',
            'interest_category_id' => 1,
            'is_active' => 1,        	
        	],
        	[
            'name' => 'Classical',
            'interest_category_id' => 1,
            'is_active' => 1,        	
        	],
        	[
            'name' => 'Ballroom',
            'interest_category_id' => 2,
            'is_active' => 1,        	
        	],
        	[
            'name' => 'Cha-Cha',
            'interest_category_id' => 2,
            'is_active' => 1,        	
        	],
        	[
            'name' => 'Fox Trot',
            'interest_category_id' => 2,
            'is_active' => 1,        	
        	],
        	[
            'name' => 'Bowed Strings',
            'interest_category_id' => 3,
            'is_active' => 1,        	
        	],
        	[
            'name' => 'Woodwinds',
            'interest_category_id' => 3,
            'is_active' => 1,        	
        	],
        	[
            'name' => 'Brass Instruments',
            'interest_category_id' => 3,
            'is_active' => 1,        	
        	],        	        	        	        	
        ]);
    }
}
