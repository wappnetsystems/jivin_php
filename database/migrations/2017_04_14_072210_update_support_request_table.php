<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSupportRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('support_request', function($table) {
         $table->boolean('is_seen')->default(0)->after('is_active');
         $table->string('ticket_no')->nullable()->after('id');
         $table->boolean('is_attachments')->default(0)->after('is_seen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('support_request', function($table) {
            $table->dropColumn('is_seen');
            $table->dropColumn('ticket_no');
            $table->dropColumn('is_attachments');
        });
    }
}
