<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAccountSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_account_settings', function($table) {
            $table->dropColumn('background_position');
        });        
        Schema::table('user_account_settings', function($table) {
          $table->text('background_position')->after('show_tile_background');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_account_settings', function($table) {
            $table->dropColumn('background_position');
        }); 
    }
}
