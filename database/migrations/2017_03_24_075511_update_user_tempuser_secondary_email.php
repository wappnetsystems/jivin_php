<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTempuserSecondaryEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
          $table->text('secondary_email')->after('email');
        });
        Schema::table('temp_users', function($table) {
          $table->text('secondary_email')->after('email');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('secondary_email');
        });
        Schema::table('temp_users', function($table) {
            $table->dropColumn('secondary_email');
        });        
    }
}
