<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTempUserTableWithLocationInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_users', function($table) {
            $table->integer('country_id')->unsigned()->nullable()->after('user_image');
            $table->integer('region_id')->unsigned()->nullable()->after('country_id');
            $table->integer('division_id')->unsigned()->nullable()->after('region_id');
            $table->integer('state_id')->unsigned()->nullable()->after('division_id'); 
              
            $table->foreign('country_id')->references('id')->on('country')->onDelete('cascade');
            $table->foreign('region_id')->references('id')->on('region')->onDelete('cascade');
            $table->foreign('division_id')->references('id')->on('division')->onDelete('cascade');
            $table->foreign('state_id')->references('id')->on('state')->onDelete('cascade');                    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temp_users', function($table) {
            $table->dropColumn('country_id');
            $table->dropColumn('region_id');
            $table->dropColumn('division_id');
            $table->dropColumn('state_id');
        });
    }
}
