<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->enum('post_type', array('shared','self'))->default('self');
            $table->integer('visible_by')->unsigned();
            $table->integer('created_by_user_id')->unsigned();
            $table->integer('shared_by_user_id')->unsigned()->nullable();
            $table->text('display_text')->nullable();
            $table->text('text_details')->nullable();
            $table->string('audio_name')->nullable();
            $table->string('video_name')->nullable();
            $table->integer('boost_order');
            $table->boolean('is_adult')->default(0);
            $table->boolean('is_active')->default(1);
            $table->nullableTimestamps();
            $table->softDeletes();
            
            $table->foreign('created_by_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('shared_by_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('visible_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_logs');
    }
}
