<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserdetailsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('username')->after('email');
            $table->string('birth_month')->after('username');
            $table->string('birth_year')->after('birth_month');
            $table->string('user_image')->default('default.jpg')->after('birth_year');
            $table->boolean('is_login')->default(0)->after('user_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('username');
            $table->dropColumn('birth_month');
            $table->dropColumn('birth_year');
            $table->dropColumn('user_image');
            $table->dropColumn('is_login');
        });
    }
}
