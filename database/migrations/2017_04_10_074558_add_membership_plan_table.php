<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMembershipPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plan_name');
            $table->enum('type', array('Paid', 'Free'));
            $table->string('duration')->nullable();
            $table->boolean('allow_boost')->default(0);
            $table->integer('allow_boost_count')->default(0);
            $table->enum('allow_boost_count_per_month_day', array('Monthly', 'Daily'))->nullable();
            $table->boolean('allow_advertisement')->default(0);
            $table->string('price')->nullable();
            $table->boolean('is_active')->default(1);
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plans');
    }
}
