<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubInterestCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_intereste_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('interest_category_id')->unsigned();
            $table->string('name', 100);
            $table->boolean('is_active');
            $table->nullableTimestamps();
            $table->softDeletes();

            // unique key
            $table->unique('name');
            // Uncomment the line below if you want to link user ids to your users table
            $table->foreign('interest_category_id')->references('id')->on('intereste_category')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_intereste_category');
    }
}
