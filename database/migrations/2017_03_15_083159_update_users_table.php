<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->integer('total_follows')->after('is_login');
            $table->integer('total_followed_by')->after('total_follows');
            $table->integer('total_videos')->after('total_followed_by');
            $table->integer('total_audios')->after('total_videos');
            $table->integer('total_votes')->after('total_audios');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('total_follows');
            $table->dropColumn('total_followed_by');
            $table->dropColumn('total_videos');
            $table->dropColumn('total_audios');
            $table->dropColumn('total_votes');
        });
    }
}
