<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAccountSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('user_account_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('region_id')->unsigned()->nullable();
            $table->integer('division_id')->unsigned()->nullable();
            $table->integer('state_id')->unsigned()->nullable(); 
            $table->string('language')->nullable();
            $table->string('time_zone')->nullable(); 
            $table->string('social_link_facebook')->nullable();          
            $table->string('social_link_twitter')->nullable();
            $table->string('social_link_linkedin')->nullable();
            $table->string('social_link_youtube')->nullable();
            $table->string('social_link_pinterest')->nullable();
            $table->boolean('show_sensitive_media')->default(0);
            $table->boolean('my_media_is_sensitive')->default(0);

            //Design Settings
            $table->string('background_image')->nullable();
            $table->boolean('show_tile_background')->default(1);
            $table->enum('background_position',['left,center,right'])->nullable();
            $table->string('background_color_code')->nullable();

            $table->nullableTimestamps();
            $table->softDeletes();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('country_id')->references('id')->on('country')->onDelete('cascade');
            $table->foreign('region_id')->references('id')->on('region')->onDelete('cascade');
            $table->foreign('division_id')->references('id')->on('division')->onDelete('cascade');
            $table->foreign('state_id')->references('id')->on('state')->onDelete('cascade');            
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_account_settings');
    }
}
