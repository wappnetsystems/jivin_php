<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_post', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_by')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->string('title');
            $table->text('details');
            $table->boolean('is_active')->default(1);
            $table->nullableTimestamps();

            $table->foreign('report_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_post');
    }
}
