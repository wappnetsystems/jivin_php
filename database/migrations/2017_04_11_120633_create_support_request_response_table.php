<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportRequestResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_request_reaply_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('support_request_id')->unsigned();
            $table->integer('reaply_to')->unsigned();
            $table->text('admin_reaply')->nullable();
            $table->nullableTimestamps();

            $table->foreign('support_request_id')->references('id')->on('support_request')->onDelete('cascade');
            $table->foreign('reaply_to')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('support_request_reaply_log');
    }
}
