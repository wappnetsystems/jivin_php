<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activity_by')->unsigned();
            $table->integer('visible_by')->unsigned();
            $table->enum('activity_type', ['user_follow','post_like', 'post_share','profile_update','add_post','leader_board','wall_of_fame','bid','advertisement','post_boost']);
            $table->integer('related_id')->nullable();
            $table->string('notification_text')->nullable();
            $table->boolean('is_read')->default(0); //Unread = 0 // Read = 1
            $table->nullableTimestamps();
            $table->foreign('activity_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('visible_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notification_log');
    }
}
