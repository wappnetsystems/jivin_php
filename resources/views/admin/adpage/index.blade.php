@extends('admin.layout.authdashboard')
@section('content')
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Ad Page Management</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->


                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ url('/admin/advertise/list') }}">Ad Page Management</a></li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->


        

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                            <div class="panel">
                        <div class="panel-body demo-nifty-alert" style="padding: 15px 20px 0px;">
                            <div class="row">
                                <div class="col-sm-12">
                    
                                    <!-- Success Alert -->
                                    <!--===================================================-->
                                    @if (session('alert-success'))
                                    <div class="alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done!</strong> {{ session('alert-success') }}
                                    </div>
                                    @endif                                    
                    
                    
                                    <!-- Danger Alert -->
                                    <?php //print_r($validator);die;?>
                                    <!--===================================================-->
                                    @if (!empty($errors->all()))

                                    
                                        <div>
                                        
                                        <div class="alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oh snap!</strong> {{$errors->all()[0]}}
                                        </div>
                                        </div>
                                   
                                    <?php /*?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oh snap!</strong> {{ session('alert-danger') }}
                                    </div> <?php */?>
                                    @endif                                    
                    
                                </div>
                            </div>
                        </div>                            
                                <div class="panel-heading">
                                    <h3 class="panel-title advertise_page_title">Ad Page Add</h3>
                                </div>
                                 
                                <form id="interest-category-add-update-form" method="Post" action="{{ url('/admin/adpage/add') }}" class="form-horizontal">
                                {{ csrf_field() }}
                                    <div class="panel-body">
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Ad Page URL</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="pageurl" class="form-control" name="pageurl" placeholder="Ad Page URL">
                                                    <input type="hidden" class="form-control" id="hiddenid" name="id">                                                    
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Ad Page Name</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="page_name" class="form-control" name="page_name" placeholder="Ad Page Name">                                                 
                                                </div>
                                            </div>

                                            <div class="form-group has-feedback">
                                                <label class="col-lg-3 control-label">Ad Region</label>
                                                <div class="col-lg-7">
                                                    <select id="region" name="region" class="form-control" data-bv-field="region_id">

                                                    <option value="">Please Select Region</option>
                                                    @if(!empty($data['allregion']))
                                                        @foreach($data['allregion'] as $region)
                                                            <option value="{{$region['id']}}">{{$region['name']}}</option>
                                                        @endforeach
                                                    @endif
                                                  
                                                    </select>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Min Bid ammount</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="min_bid" class="form-control" name="min_bid" placeholder="Min Bid ammount">                                         
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Buy Now Price</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="buy_now_price" class="form-control" name="buy_now_price" placeholder="Buy Now Price">                                         
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Status</label>
                                                <div class="col-lg-7">
                                                    <div class="radio">
                                                        <input id="demo-radio-7" class="demoradio7 advertise_status_active magic-radio" type="radio" name="is_active" value="1" checked="checked">
                                                        <label for="demo-radio-7">Active</label>
                    
                                                        <input id="demo-radio-8" class=" demoradio8 advertise_status_deactive magic-radio" type="radio" name="is_active" value="0">
                                                        <label for="demo-radio-8">Deactive</label>
                                                    </div>
                                                </div>
                                            </div> 


                                        </fieldset>
                    
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-7 col-sm-offset-3">
                                                <button class="btn btn-mint" type="submit">Save</button>
                                                <button type="reset" id="reset" class="btn btn-mint" value="Reset">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>                    
                    <!--  Data Tables -->
                    <!--===================================================-->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Ad Page List</h3>
                        </div>
                        <div class="panel-body">
                                    @if(session()->has('success'))
                                    <div class="status-update-msg alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done!</strong> {{ session()->get('success') }}
                                    </div> 

                                    @endif
                                    @if(session()->has('error'))
                                    <div class="status-delete alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oops! </strong> {{ session()->get('error') }}
                                    </div> 
                                     @endif                                                          
                            <table id="advertise-list-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        
                                        <th class="min-tablet">Region Name</th>
                                        <th class="min-tablet">Page Name</th>
                                        <th class="min-tablet">Page URL</th>
                                        <th class="min-tablet">Min bid</th>
                                        <th class="min-tablet">Buy Now Price</th>
                                        <th class="min-desktop">Status</th>
                                        <th class="min-desktop">Actions</th>
                                    </tr>
                                    @if(!empty($data))
                                    <?php  //echo "<pre>";print_r($data['list']);die;?>
                                        @foreach($data['list'] as $record)
                                            <tr>
                                                
                                                <td>{{$record->getadregion->name}}</td>
                                                {{-- <td>{{$record['ad_region_id']}}</td> --}}
                                                <td>{{$record['page_name']}}</td>
                                                <td>{{$record['page_url']}}</td>
                                                <td>{{$record['min_bid']}}</td>
                                                <td>{{$record['buy_now_price']}}</td>
                                                <td>
                                                <?php
                                                if($record['status'] == 0){
                                                    $sts = "Deactive";
                                                }else{
                                                    $sts = "Active";
                                                }?>
                                                <strong>{{$sts}}</strong>
                                                <?php /*
                                                ?>
                                                <div class="toggle btn btn-default {{$sts}}" data-toggle="toggle" style="width: 92px; height: 32px;"><input class="toggle-3" type="checkbox" data-toggle="toggle"><div class="toggle-group"><label class="btn btn-primary toggle-on">Enabled</label><label class="btn btn-default active toggle-off">Disabled</label><span class="toggle-handle btn btn-default"></span></div></div><?php*/?>

                                                </td>
                                                <td>

                                                <button class="btn-edit btn btn-mint btn-icon editbtn" data-ad_region_id="{{$record['ad_region_id']}}"  data-id="{{$record['id']}}" data-page_url="{{$record['page_url']}}" data-page_name="{{$record['page_name']}}" data-min_bid="{{$record['min_bid']}}" data-buy_now_price="{{$record['buy_now_price']}}" data-status="{{$record['status']}}"><i class="demo-psi-pen-5 icon-lg"></i></button>
                                                <a href="{{ url('/admin/adpage/details').'/'.$record["id"] }}"><button>Details</button></a>

                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                    Record not found
                                    @endif
                                </thead>
                                <tbody></tbody>
                            </table>
                  <div class="paginatediv" style="float: right;">
                    {!!$data['list']->links()!!}
                  </div>                        </div>
                         

                    </div>
                    <!--===================================================-->
                    <!-- End Striped Table -->
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
<script type="text/javascript">
$( document ).ready(function() {

    $('.editbtn').click(function(){
        //alert($(this).data("id"));
        $('#hiddenid').val($(this).data("id"));
        $('#region').val($(this).data("ad_region_id"));
        $('#pageurl').val($(this).data("page_url"));
        $('#page_name').val($(this).data("page_name"));
        $('#min_bid').val($(this).data("min_bid"));
        $('#buy_now_price').val($(this).data("buy_now_price"));

        var vstatus = $(this).data("status");
            console.log("status "+vstatus);

        //$('input[class=radio][value=vstatus]').prop("checked",true);
       if(vstatus == 1){
            $(".demoradio7").attr('checked', 'checked');
            //$("#demo-radio-8").attr('checked', '');
            $(".demoradio8").removeAttr("checked");
        }else{
            $(".demoradio8").attr('checked', 'checked');
            //$("#demo-radio-7").attr('checked', '');
            $(".demoradio7").removeAttr("checked");
            
        }

    });
    });
</script>
@stop