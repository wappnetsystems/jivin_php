@extends('admin.layout.authdashboard')
@section('content')
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">

                <?php //echo "<pre>";print_r($data['country']);die;?>
                @if(!empty($data['allpage']))
                    @foreach($data['allpage'] as $allpage)
                        @if($allpage['id'] == Request::segment(4))
                            <?php $staticurl = $allpage['page_url'];?>
                            <div id="page-title">
                                <h1 class="page-header text-overflow">Ad Price Master for {{$allpage['page_url']}} URL</h1>
                            </div>
                        @endif
                    @endforeach
                @endif

                <?php // print_r($data['stateArray']); die;?>
                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ url('/admin/advertise/list') }}">Ad Page Management</a></li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->


        

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                            <div class="panel">
                        <div class="panel-body demo-nifty-alert" style="padding: 15px 20px 0px;">
                            <div class="row">
                                <div class="col-sm-12">
                    
                                    <!-- Success Alert -->
                                    <!--===================================================-->
                                    @if (session('alert-success'))
                                    <div class="alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done!</strong> {{ session('alert-success') }}
                                    </div>
                                    @endif                                    
                    
                    
                                    <!-- Danger Alert -->
                                    <?php //print_r($validator);die;?>
                                    <!--===================================================-->
                                    @if (!empty($errors->all()))

                                    
                                        <div>
                                        
                                        <div class="alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oh snap!</strong> {{$errors->all()[0]}}
                                        </div>
                                        </div>
                                   
                                    <?php /*?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oh snap!</strong> {{ session('alert-danger') }}
                                    </div> <?php */?>
                                    @endif                                    
                    
                                </div>
                            </div>
                        </div>                            
                                <div class="panel-heading">
                                    <h3 class="panel-title advertise_page_title">Ad Page Add</h3>
                                </div>
                                 
                                <form id="interest-category-add-update-form" method="Post" action="{{ url('/admin/ad_price_details/add') }}" class="form-horizontal">
                                {{ csrf_field() }}
                                    <div class="panel-body">
                                        <fieldset>

                                            
                                            <div class="form-group has-feedback">
                                                <label class="col-lg-3 control-label">Country</label>
                                                <div class="col-lg-7">
                                                    <select id="country" name="country" class="form-control" data-bv-field="region_id">

                                                    <option value="1">USA</option>
                                                  
                                                    </select>
                                                    </div>
                                            </div>

                                        <?php /* ?>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Ad Page URL</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="pageurl" class="form-control" value="{{$staticurl}}" name="pageurl" placeholder="Ad Page URL" required="true">
                                                               
                                                </div>
                                            </div>
                                            <?php */ ?>
                                                    <input type="hidden" class="form-control" id="hiddenid" name="id">
                                                    <input type="hidden" class="form-control" id="hiddenid" name="ad_page_id" value="{{Request::segment(4)}}">  
                                            <div class="form-group has-feedback">
                                                <label class="col-lg-3 control-label">State</label>
                                                <div class="col-lg-7">
                                                    <select id="state" name="state" class="form-control" data-bv-field="region_id">

                                                    <option value="">Please Select State</option>
                                                    @if(!empty($data['state']))
                                                        @foreach($data['state'] as $state)
                                                            <option value="{{$state['id']}}">{{$state['name']}}</option>
                                                        @endforeach
                                                    @endif
                                                  
                                                    </select>
                                                    </div>
                                            </div>

                                            <?php /*<div class="form-group has-feedback">
                                                <label class="col-lg-3 control-label">Ad Region</label>
                                                <div class="col-lg-7">
                                                    <select id="region" name="region" class="form-control" data-bv-field="region_id">

                                                    <option value="">Please Select Region</option>
                                                    @if(!empty($data['allpage']))
                                                        @foreach($data['allpage'] as $region)
                                                            <option value="{{$region['id']}}">{{$region['page_url']}}</option>
                                                        @endforeach
                                                    @endif
                                                  
                                                    </select>
                                                    </div>
                                            </div>*/?>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">To Time</label>
                                                <div class="col-lg-7">
                                                    <input type="time" id="to_time" class="form-control" name="to_time" placeholder="To Time">                                         
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">From Time</label>
                                                <div class="col-lg-7">
                                                    <input type="time" id="from_time" class="form-control" name="from_time" placeholder="From Time">                                         
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Min Bid ammount</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="min_bid" class="form-control" name="min_bid" placeholder="Min Bid ammount">                                         
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Buy Now Price</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="buy_now_price" class="form-control" name="buy_now_price" placeholder="Buy Now Price">                                         
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Status</label>
                                                <div class="col-lg-7">
                                                    <div class="radio">
                                                        <input id="demo-radio-7" class="demoradio7 advertise_status_active magic-radio" type="radio" name="is_active" value="1" checked="checked">
                                                        <label for="demo-radio-7">Active</label>
                    
                                                        <input id="demo-radio-8" class=" demoradio8 advertise_status_deactive magic-radio" type="radio" name="is_active" value="0">
                                                        <label for="demo-radio-8">Deactive</label>
                                                    </div>
                                                </div>
                                            </div> 


                                        </fieldset>
                    
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-7 col-sm-offset-3">
                                                <button class="btn btn-mint" type="submit">Save</button>
                                                <button type="reset" id="reset" class="btn btn-mint" value="Reset">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>                    
                    <!--  Data Tables -->
                    <!--===================================================-->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Ad Page List</h3>
                        </div>
                        <div class="panel-body">
                                    @if(session()->has('success'))
                                    <div class="status-update-msg alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done!</strong> {{ session()->get('success') }}
                                    </div> 

                                    @endif
                                    @if(session()->has('error'))
                                    <div class="status-delete alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oops! </strong> {{ session()->get('error') }}
                                    </div> 
                                     @endif                                                          
                            <table id="advertise-list-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="min-tablet">State</th>
                                        <th class="min-tablet">To Time</th>
                                        <th class="min-tablet">From TIme</th>
                                        <th class="min-tablet">Min bid</th>
                                        <th class="min-tablet">Buy Now Price</th>
                                        <th class="min-desktop">Status</th>
                                        <th class="min-desktop">Actions</th>
                                    </tr>
                                    @if(!empty($data))
                                    <?php  // echo "<pre>";print_r($data['list']);die;?>
                                        @foreach($data['list'] as $record)
                                            <tr>
                                                <td>{{$record->substate['name']}}</td>
                                                <?php // print_r($record->substate);?>
                                                <td>{{$record['to_time']}}</td>
                                                <td>{{$record['from_time']}}</td>
                                                <td>{{$record['min_bid']}}</td>
                                                <td>{{$record['buy_now_price']}}</td>
                                                <td>
                                                <?php
                                                if($record['status'] == 0){
                                                    $sts = "Deactive";
                                                }else{
                                                    $sts = "Active";
                                                }?>
                                                <strong>{{$sts}}</strong>

                                                </td>
                                                <td>

                                                <button class="btn-edit btn btn-mint btn-icon editbtn" data-state="{{$record['state_id']}}"  data-id="{{$record['id']}}" data-to_time="{{$record['to_time']}}" data-from_time="{{$record['from_time']}}" data-min_bid="{{$record['min_bid']}}" data-buy_now_price="{{$record['buy_now_price']}}" data-status="{{$record['status']}}"><i class="demo-psi-pen-5 icon-lg"></i></button>

                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                    Record not found
                                    @endif
                                </thead>
                                <tbody></tbody>
                            </table>
                  <div class="paginatediv" style="float: right;">
                    {!!$data['list']->links()!!}
                  </div>                        </div>
                         

                    </div>
                    <!--===================================================-->
                    <!-- End Striped Table -->
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
<script type="text/javascript">
$( document ).ready(function() {

    $('.editbtn').click(function(){
        //alert($(this).data("id"));
        $('#hiddenid').val($(this).data("id"));
        $('#state').val($(this).data("state"));
        $('#to_time').val($(this).data("to_time"));
        $('#from_time').val($(this).data("from_time"));
        $('#min_bid').val($(this).data("min_bid"));

        $('#buy_now_price').val($(this).data("buy_now_price"));

        var vstatus = $(this).data("status");
            console.log("status "+vstatus);

        //$('input[class=radio][value=vstatus]').prop("checked",true);
       if(vstatus == 1){
            $(".demoradio7").attr('checked', 'checked');
            //$("#demo-radio-8").attr('checked', '');
            $(".demoradio8").removeAttr("checked");
        }else{
            $(".demoradio8").attr('checked', 'checked');
            //$("#demo-radio-7").attr('checked', '');
            $(".demoradio7").removeAttr("checked");
            
        }

    });
    });
</script>
@stop