@extends('admin.layout.authdashboard')
@section('content')
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">

    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Jivin Video Management</h1>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->


    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
        <li><a href="#">Jivin Video Management</a></li>
    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
   
    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">

        <!--  Data Tables -->
        <!--===================================================-->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Video List</h3>
            </div>
            <div class="panel-body">
                @if(session('success'))
                <div class="status-update-msg alert alert-success">
                    <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                    <strong>Success!</strong> {{ session('success') }}
                </div>
                @endif
                @if(session('error'))
                <div class="status-update-msg alert alert-danger">
                    <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                    <strong>Success!</strong> {{ session('error') }}
                </div>
                @endif
                <div class="">
                    <table id="video-list-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="min-tablet">Title</th>
                                <th class="min-tablet">Video</th>
                                <th class="min-tablet">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $video_detail[0]->video_title }}</td>
                                <td><a href="{{ url('uploads/'.$video_detail[0]->video_file) }}" target="_blank"><i class="fa fa-eye"></i></a></td>
                                <td><a href="{{ route('edit_jivin_video',['id'=>$video_detail[0]->id]) }}"><i class="fa fa-edit"></i></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--===================================================-->
        <!-- End Striped Table -->

    </div>
    <!--===================================================-->
    <!--End page content-->


</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->

@endsection
@section('scripts')
@stop
