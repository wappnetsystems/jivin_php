@extends('admin.layout.authdashboard')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">

    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Jivin Video Edit</h1>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->


    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
        <li><a href="#">Jivin Video Edit</a></li>
    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->




    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="panel">
            <div class="panel-body demo-nifty-alert" style="padding: 15px 20px 0px;">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- Success Alert -->
                        <!--===================================================-->
                        @if (session('success'))
                        <div class="alert alert-success">
                            <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                            <strong>Success!</strong> {{ session('success') }}
                        </div>
                        @endif                                    


                        <!-- Danger Alert -->
                        <!--===================================================-->
                        @if (session('alert-danger'))
                        <div class="alert alert-danger">
                            <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                            <strong>Error!</strong> {{ session('alert-danger') }}
                        </div>
                        @endif                                    

                    </div>
                </div>
            </div>                            
            <div class="panel-heading">
                <h3 class="panel-title plan_page_title">Jivin Video Edit</h3>
            </div>

            <form id="video_edit_frm" method="Post" action="{{ route('update_jivin_video') }}" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
                <input type="hidden" name="id" id="id" value="{{$id}}" />
                <div class="panel-body">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Video Title</label>
                            <div class="col-lg-7">
                                <input type="text" value="{{ $video_detail[0]->video_title }}" id="video_title" class="form-control" name="video_title" placeholder="Video Title">

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Video</label>
                            <div class="col-lg-7">
                                <input type="file" id="video_file" class="form-control" name="video_file" placeholder="Video File">

                            </div>
                        </div> 

                    </fieldset>

                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-3">
                            <button class="btn btn-mint" type="submit">Save</button>
                            <button type="reset" id="reset" class="btn btn-mint" value="Reset">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>                    


    </div>
    <!--===================================================-->
    <!--End page content-->


</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
<script>
    $(document).ready(function () {
        $('#video_edit_frm').bootstrapValidator({
            message: 'This value is not valid',

            fields: {
                video_title: {
                    message: 'video title is required',
                    validators: {
                        notEmpty: {
                            message: 'video title is required'
                        }
                    }
                },
                video_file:{
                     message: 'Only mp4 files are allowed.',
                    validators:{
                        file: {
                            type: 'video/mp4',
                        }
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
        });
    });
</script>
@stop