@extends('admin.layout.authdashboard')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
<style type="text/css">
    #plan-list-table_wrapper {
        overflow-x: scroll;
    }
</style>
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Membership Plans Management</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->


                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ url('/admin/memership_plans/list') }}">Membership Plans Management</a></li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->


        

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    <div class="panel">
                        <div class="panel-body demo-nifty-alert" style="padding: 15px 20px 0px;">
                            <div class="row">
                                <div class="col-sm-12">
                    
                                    <!-- Success Alert -->
                                    <!--===================================================-->
                                    @if (session('alert-success'))
                                    <div class="alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done!</strong> {{ session('alert-success') }}
                                    </div>
                                    @endif                                    
                    
                    
                                    <!-- Danger Alert -->
                                    <!--===================================================-->
                                    @if (session('alert-danger'))
                                    <div class="alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oh snap!</strong> {{ session('alert-danger') }}
                                    </div>
                                    @endif                                    
                    
                                </div>
                            </div>
                        </div>                            
                        <div class="panel-heading">
                            <h3 class="panel-title plan_page_title">Membership Plan Add</h3>
                        </div>
                         
                        <form id="plan-add-update-form" method="Post" action="{{ url('/admin/memership_plan/add') }}" class="form-horizontal">
                        {{ csrf_field() }}
                            <div class="panel-body">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Plan Name</label>
                                        <div class="col-lg-7">
                                            <input type="text" id="plan_name" class="form-control" name="plan_name" placeholder="Plan Name">
                                            <input id="plan_id" type="hidden" class="form-control" name="id">                                                    
                                        </div>
                                    </div>                                            
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Plan Type</label>
                                        <div class="col-lg-7">
                                            <select name="plan_type" class="form-control" id="plan_type">
                                                <option value="">Please select plan type</option>
                                                <option value="Free">Free</option>
                                                <option value="Paid">Paid</option>
                                            </select>                                                   
                                        </div>
                                    </div>                                                                                        
                                    <div class="form-group paid_section">
                                        <label class="col-lg-3 control-label">Allow Boost</label>
                                        <div class="col-lg-7">
                                            <div class="radio">
                                                <input id="demo-radio-1" class="boost_allowed_active magic-radio" type="radio" name="boost_allowed" value="1" checked="checked">
                                                <label for="demo-radio-1">Yes</label>
            
                                                <input id="demo-radio-2" class="boost_allowed_deactive magic-radio" type="radio" name="boost_allowed" value="0" checked>
                                                <label for="demo-radio-2">No</label>
                                            </div>
                                        </div>
                                    </div>                                            
                                    <div class="boost_section">

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Boost Duration</label>
                                        <div class="col-lg-7">
                                            <div class="radio">
                                                <input id="demo-radio-11" class="magic-radio" type="radio" name="boost_count_duration_type" value="1" checked>
                                                <label for="demo-radio-11">Monthly</label>
                                    
                                                <input id="demo-radio-12" class="magic-radio" type="radio" name="boost_count_duration_type" value="0">
                                                <label for="demo-radio-12">Daily</label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                     <div class="form-group first_form_group">
                                        <div class="row">
                                        <label class="col-lg-3 control-label">Allowed Boost Count</label>
                                        <div class="col-lg-7">
                                            <div class="input-group mar-btm">
                                                <input type="number" value="0" min="1" name="boost_count[]" id="boost_count" placeholder="Allowed Boost Count" class="form-control">
                                                <!-- <div class="input-group-btn">
                                                    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle boost_count_duration_type" type="button" aria-expanded="false">
                                                        Monthly <i class="dropdown-caret"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a id="daily">Daily</a></li>
                                                        <li><a id="monthly">Monthly</a></li>
                                                    </ul>

                                                    <input type="hidden" id="boost_count_duration_type_hidden" name="boost_count_duration_type" value="Monthly">
                                                </div> -->

                                                <div class="input-group-btn">
                                                    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle boost_for_location" type="button" aria-expanded="false">
                                                        Country <i class="dropdown-caret"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right select-boost-location">
                                                        <li><a>Country</a></li>
                                                        <li><a>Region</a></li>
                                                        <li><a>Division</a></li>
                                                        <li><a>State</a></li>
                                                    </ul>
                                                    <input type="hidden" class="boost_for_location_hidden" name=" boost_for_location[]" value="Country">
                                                </div>
                                                <button type="button" class="btn btn-success pull-right add-more-boost"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                            </div> 

                                        </div>
                                    </div>
                                    </div>
                                    </div>
                                       
                                    <div class="form-group paid_section">
                                        <label class="col-lg-3 control-label">Allow Post Advertisement</label>
                                        <div class="col-lg-7">
                                            <div class="radio">
                                                <input id="demo-radio-3" class="ap_allowed_active magic-radio" type="radio" name="add_post_allowed" value="1" checked="checked">
                                                <label for="demo-radio-3">Yes</label>
            
                                                <input id="demo-radio-4" class="ap_allowed_deactive magic-radio" type="radio" name="add_post_allowed" value="0" checked>
                                                <label for="demo-radio-4">No</label>
                                            </div>
                                        </div>
                                    </div>                                                  
                                    <div class="form-group paid_section">
                                        <label class="col-lg-3 control-label">Plan Duration (Days)</label>
                                        <div class="col-lg-7">
                                            <input type="number" id="plan_duration" class="form-control" value="0" min="1" name="plan_duration" placeholder="Plan Duration">                                                  
                                        </div>
                                    </div>
                                    <div class="form-group paid_section">
                                        <label class="col-lg-3 control-label">Plan Price ($)</label>
                                        <div class="col-lg-7">
                                            <input type="number" value="0" min="1" step="any" id="plan_price" class="form-control" name="plan_price" placeholder="Plan Price">                                                 
                                        </div>
                                    </div>                                  
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Status</label>
                                        <div class="col-lg-7">
                                            <div class="radio">
                                                <input id="demo-radio-7" class="plan_status_active magic-radio" type="radio" name="is_active" value="1" checked="checked">
                                                <label for="demo-radio-7">Active</label>
            
                                                <input id="demo-radio-8" class="plan_status_deactive magic-radio" type="radio" name="is_active" value="0">
                                                <label for="demo-radio-8">Deactive</label>
                                            </div>
                                        </div>
                                    </div>                                            
                                </fieldset>
            
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-sm-7 col-sm-offset-3">
                                        <button class="btn btn-mint" type="submit">Save</button>
                                        <button type="reset" id="reset" class="btn btn-mint" value="Reset">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>                    
                    <!--  Data Tables -->
                    <!--===================================================-->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Memebership Plan List</h3>
                        </div>
                        <div class="panel-body">
                                    <div class="status-update-msg alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done!</strong> Status Updated!
                                    </div> 
                                    <div class="status-delete alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done! </strong> Plan Deleted!
                                    </div>   
                                    <div class="status-not-update-msg alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Error! </strong> You cannot disable this plan because currently users are sunscribed to this plan!
                                    </div>

                            <table id="plan-list-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="min-tablet">Id</th>
                                        <th class="min-tablet">Plan Name</th>
                                        <th class="min-tablet">Plan Type</th>
                                        <th class="min-tablet">Plan Duration (Days)</th>
                                        <th class="min-tablet">Plan Price ($)</th>
                                        <th class="min-tablet">Country Boost</th>
                                        <th class="min-tablet">Region Boost</th>
                                        <th class="min-tablet">Division Boost</th>
                                        <th class="min-tablet">State Boost</th>
                                        <th class="min-tablet">Allow Boost</th>
                                        
                                        <th class="min-tablet">Boost Montly/Daily</th>
                                        <!-- <th class="min-tablet">Boost For Location</th> -->
                                        <th class="min-tablet">Allow Post Ad</th>
                                        <th class="min-desktop">Status</th>
                                        <th class="min-desktop">Actions</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <!--===================================================-->
                    <!-- End Striped Table -->
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
  <script src="{{ asset('js/plan.js') }}"></script>
@stop