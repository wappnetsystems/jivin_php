@extends('admin.layout.authdashboard')
@section('content')
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Dashboard</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->


                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--                 <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Pages</a></li>
                    <li class="active">Blank page</li>
                </ol> -->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->


        

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
                    <hr class="new-section-sm bord-no">
                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3">
                            <div class="panel panel-trans text-center">
                            <div class="panel-heading">
                                <h3 class="panel-title">You are logged in as Admin!</h3>
                            </div>
                            <div class="panel-body">
<!--                                 <p>Start putting content on grids or panels, you can also use different combinations of grids.
                                <br>Please check out the dashboard and other pages.</p>
 -->                            </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection