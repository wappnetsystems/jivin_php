@extends('admin.layout.authdashboard')
@section('content')
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Site Setting Management</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->


                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ url('/admin/sitesetting') }}">Site Setting Management</a></li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->


        

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                            <div class="panel">
                        <div class="panel-body demo-nifty-alert" style="padding: 15px 20px 0px;">
                            <div class="row">
                                <div class="col-sm-12">
                    
                                    <!-- Success Alert -->
                                    <!--===================================================-->
                                    @if (session('alert-success'))
                                    <div class="alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done!</strong> {{ session('alert-success') }}
                                    </div>
                                    @endif                                    
                    
                    
                                    <!-- Danger Alert -->
                                    <!--===================================================-->
                                    @if (session('alert-danger'))
                                    <div class="alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oh snap!</strong> {{ session('alert-danger') }}
                                    </div>
                                    @endif                                    
                    
                                </div>
                            </div>
                        </div>                            
                                <div class="panel-heading">
                                    <h3 class="panel-title cms_page_title">Site Setting Edit</h3>
                                </div>
                                 
                                <form id="cms-add-update-form" method="Post" action="{{ url('/admin/sitesetting/update') }}" class="form-horizontal">
                                {{ csrf_field() }}
                                    <div class="panel-body">
                                        <fieldset>
                                            <!-- <div class="form-group">
                                                <label class="col-lg-3 control-label">Boost Text</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="page_title" class="form-control" value="{{$boost_text}}" name="page_title" placeholder="CMS Page Title">
                                                    <input id="cms_id" type="hidden" class="form-control" name="id">                                                    
                                                </div>
                                            </div>                                             -->
                                            <input id="id" type="hidden" value="{{$id}}" class="form-control" name="id"> 
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Boost Text</label>
                                                <div class="col-lg-7">
                                                    <textarea name="boost_text" id="boost_text" required="true" rows="4" cols="70">{{$boost_text}}</textarea>                                             
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Ad Space Text</label>
                                                <div class="col-lg-7">
                                                    <textarea name="adspace_text" id="adspace_text" required="true" rows="4" cols="70">{{$adspace_text}}</textarea>                                             
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Creative Text</label>
                                                <div class="col-lg-7">
                                                    <textarea name="creative_text" id="creative_text" required="true" rows="4" cols="70">{{$creative_text}}</textarea>                                             
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Bid Slot Text</label>
                                                <div class="col-lg-7">
                                                    <textarea name="bidslot_text" id="bidslot_text" required="true" rows="4" cols="70">{{$bidslot_text}}</textarea>                                             
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Bid Review Text</label>
                                                <div class="col-lg-7">
                                                    <textarea name="bidreview_text" id="bidreview_text" required="true" rows="4" cols="70">{{$bidreview_text}}</textarea>                                             
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Your Bid Text</label>
                                                <div class="col-lg-7">
                                                    <textarea name="yourbid_text" id="yourbid_text" required="true" rows="4" cols="70">{{$yourbid_text}}</textarea>                                             
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Ad Location Text</label>
                                                <div class="col-lg-7">
                                                    <textarea name="adlocation_text" id="adlocation_text" required="true" rows="4" cols="70">{{$adlocation_text}}</textarea>                                             
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Jackpot Text</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="jackpot_text" value="{{ $jackpot_text }}" name="jackpot_text" maxlength="30" class="form-control" required="true" />
                                                </div>
                                            </div>
                                            <!-- <div class="form-group">
                                                <label class="col-lg-3 control-label">Status</label>
                                                <div class="col-lg-7">
                                                    <div class="radio">
                                                        <input id="demo-radio-7" class="cms_status_active magic-radio" type="radio" name="is_active" value="1" checked="checked">
                                                        <label for="demo-radio-7">Active</label>
                    
                                                        <input id="demo-radio-8" class="cms_status_deactive magic-radio" type="radio" name="is_active" value="0">
                                                        <label for="demo-radio-8">Deactive</label>
                                                    </div>
                                                </div>
                                            </div>                                             -->
                                        </fieldset>
                    
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-7 col-sm-offset-3">
                                                <button class="btn btn-mint" type="submit">Save</button>
                                                <!-- <button type="reset" id="reset" class="btn btn-mint" value="Reset">Reset</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>                    
                    <!--  Data Tables -->
                    <!--===================================================-->

                    <!--===================================================-->
                    <!-- End Striped Table -->
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
  <script src="{{ asset('js/cms.js') }}"></script>
  <script type="text/javascript" src="https://cdn.tinymce.com/4/tinymce.min.js"></script>

  <script type="text/javascript">
  $(function(){
      tinymce.init({ selector:'textarea' });
  })
  </script>
@stop