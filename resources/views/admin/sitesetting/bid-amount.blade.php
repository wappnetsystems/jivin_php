@extends('admin.layout.authdashboard')
@section('content')
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Change Bid Amount</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->


                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ url('/admin/sitesetting/change-bid-amount') }}">Change Bid Amount</a></li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->


        

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                            <div class="panel">
                        <div class="panel-body demo-nifty-alert" style="padding: 15px 20px 0px;">
                            <div class="row">
                                <div class="col-sm-12">
                    
                                    <!-- Success Alert -->
                                    <!--===================================================-->
                                    @if (session('alert-success'))
                                    <div class="alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done!</strong> {{ session('alert-success') }}
                                    </div>
                                    @endif                                    
                    
                    
                                    <!-- Danger Alert -->
                                    <!--===================================================-->
                                    @if (session('alert-danger'))
                                    <div class="alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oh snap!</strong> {{ session('alert-danger') }}
                                    </div>
                                    @endif                                    
                    
                                </div>
                            </div>
                        </div>                            
                                <div class="panel-heading">
                                    <h3 class="panel-title cms_page_title">Change Bid Amount </h3>
                                </div>
                                 
                                <form id="bid-amount-update-form" method="Post" action="{{ url('/admin/sitesetting/change-bid-amount') }}" class="form-horizontal">
                                {{ csrf_field() }}
                                    <div class="panel-body">
                                        <fieldset>                                            
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Select Position</label>
                                                <div class="col-lg-7">
                                                    <select required="" name="region_id" class="form-control region-select">
                                                        <option value="">Select Any Position</option>
                                                        @foreach($regionList as $eachRegion)
                                                        <option value="{{$eachRegion->id}}">
                                                            {{$eachRegion->name}}
                                                        </option>
                                                        @endforeach
                                                    </select>                                             
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Select Page</label>
                                                <div class="col-lg-7">
                                                    <select required="" name="page_id" class="form-control page-id">
                                                        <option value="">Select Page</option>
                                                    </select>                                             
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Buy Now Price ($)</label>
                                                <div class="col-lg-7">
                                                    <input required="" class="form-control bid-amount" type="text" name="bid_amount">                                             
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Base Price ($)</label>
                                                <div class="col-lg-7">
                                                    <input required="" class="form-control min-bid" type="text" name="min_bid">                                             
                                                </div>
                                            </div>

                                        </fieldset>
                    
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-7 col-sm-offset-3">
                                                <button class="btn btn-mint" type="submit">Save</button>
                                                <!-- <button type="reset" id="reset" class="btn btn-mint" value="Reset">Reset</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>                    
                    <!--  Data Tables -->
                    <!--===================================================-->

                    <!--===================================================-->
                    <!-- End Striped Table -->
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
  <script src="{{ asset('js/cms.js') }}"></script>
  <script type="text/javascript">
      
      $(document).on('click','.region-select',function() {

            var id = this.value;
            $.ajax({

                'type': 'GET',
                'url' : '{{ url("admin/sitesetting/get-page-by-region") }}/'+id,
                success: function(res) {

                    $('.page-id').html(res.html);
                }
            })
      })

      $(document).on('click','.page-id',function() {
            var bid_amount = $(this).find(':selected').data('bid-amount');
            var min_bid = $(this).find(':selected').data('min-bid');
            // console.log(data)
            $('.bid-amount').val(bid_amount);
            $('.min-bid').val(min_bid);
      })

      /*$(document).on('submit','#bid-amount-update-form',function(e){

            var chk = $('.bid-amount').val();
            if(chk) {
                return true;
            }
            alert('Please enter bid amount!');
            e.preventDefault();
            return false;
      })*/


  </script>
@stop