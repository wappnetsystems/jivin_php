@extends('admin.layout.authdashboard')
@section('content')


<?php // echo "<pre>";print_r($boostplans);die;?>
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">

    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Revenue Plans Management</h1>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->


    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
        <li><a href="{{ url('/admin/revenue/index') }}">Revenue Management</a></li>
    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->
    <?php
    if (isset($_GET['year'])) {
        $year = $_GET['year'];
    } else {
        $year = date("Y");
    }

    if (isset($_GET['phase'])) {
        $phase = $_GET['phase'];
    } else {
        if (in_array(date("m"), array("01", "02", "03", "04"))) {
            $phase = '3';
        } elseif (in_array(date("m"), array("05", "06", "07", "08"))) {
            $phase = '1';
        } elseif (in_array(date("m"), array("09", "10", "11", "12"))) {
            $phase = '2';
        }
    }
    ?>

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="panel">
            <div class="panel-body demo-nifty-alert">
                <div class="row">
                    <div class="col-sm-12">
                        <!--===================================================-->
                        @if (session('alert-success'))
                        <div class="alert alert-success">
                            <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                            <strong>Well done!</strong> {{ session('alert-success') }}
                        </div>
                        @endif                                    


                        <!--===================================================-->
                        @if (!empty($errors->all()))


                        <div>

                            <div class="alert alert-danger">
                                <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                <strong>Oh snap!</strong> {{$errors->all()[0]}}
                            </div>
                        </div>
                        @endif            
                    </div>
                </div>
            </div>                         

            <form id="fillterrevenue" method="Get">

                <input type="hidden" id="year" name="year" value="<?php echo $year; ?>">
                <input type="hidden" id="phase" name="phase" value="<?php echo $phase; ?>">
            </form>   



        </div>                    
        <!--  Data Tables -->
        <!--===================================================-->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Revenue List</h3>
            </div>
            <div class="panel-body">

                <div style="padding-bottom:32px;">
                    <div class="col-sm-4">
                        <h5>Filtered By</h5>
                    </div>

                    <div class="col-sm-1"> <h5>Year</h5></div>

                    <div class="col-sm-2">
                        <select id="select_year" name="cat_main_id" class="form-control select_year" data-bv-field="cat_main_id">

                            <option value="">Please Select Year</option>

                            <option value="2016" <?php if ($year == '2016') echo "selected"; ?> >2016</option>

                            <option value="2017" <?php if ($year == '2017') echo "selected"; ?> >2017</option>

                            <option value="2018" <?php if ($year == '2018') echo "selected"; ?> >2018</option>

                        </select>
                    </div>
                    <div class="col-sm-1"><h5>Span</h5></div>

                    <div class="col-sm-2">
                        <select id="select_phase" name="cat_main_id" class="form-control select_phase" data-bv-field="cat_main_id">

                            <option value="">Please Select Span</option>

                            <option value="1" <?php if ($phase == '1') echo "selected"; ?> >January - April</option>

                            <option value="2" <?php if ($phase == '2') echo "selected"; ?> >May - August</option>

                            <option value="3" <?php if ($phase == '3') echo "selected"; ?> >September - December</option>

                        </select>
                    </div>
                    <?php // echo "<pre>";print_r($boostplans);echo "</pre>";//die; ?>
                    <br>
                </div>


                <table id="plan-list-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="min-tablet">Revenue For</th>
                            <th class="min-tablet">Revenue Amount ($)</th>


                        </tr>
                        <tr>
                            <td>
                                Advertisement/Bid
                            </td>
                            <td>


                                @if(!empty($boostplans))
                                @foreach($boostplans as $record)
                                @if($record->tfor == "Bid Payment")
                                {{$record->totalRevenue}}
                                @endif

                                @endforeach
                                @else
                                Record not found
                                @endif



                            </td>
                        </tr>

                        <tr>
                            <td>
                                Boost Membership Plan
                            </td>
                            <td>
                                @if(!empty($boostplans))
                                @foreach($boostplans as $record)
                                @if($record->tfor == "Boost Plan Subscription")
                                {{$record->totalRevenue}}

                                @endif

                                @endforeach
                                @else
                                Record not found
                                @endif
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Membership Plan
                            </td>
                            <td>
                                @if(!empty($boostplans))
                                @foreach($boostplans as $record)
                                @if($record->tfor == "Plan Subscription")
                                {{$record->totalRevenue}}

                                @endif

                                @endforeach
                                @else
                                Record not found
                                @endif
                            </td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <!--===================================================-->
        <!-- End Striped Table -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Jackpot Setting</h3>
            </div>
            <form action="#" id="" method="post">
                <div class="panel-body">

                    {{ csrf_field() }}

                    <div class="form-group col-sm-12">
                        <label class="col-lg-2 control-label"> Jackpot calculate method</label>

                        <input type="radio" checked="" name="jackpot_method" value="Manual" /> Manual &nbsp; &nbsp;&nbsp;
                        <input type="radio" name="jackpot_method" value="Percentage" /> Percentage
                    </div>


                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-3">
                            <button class="btn btn-mint" type="submit">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!--Form for manual jackpot amount start-->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Manual Jackpot Amount</h3>
            </div>
            <form action="{{ route('update_jackpot') }}" id="update_jackpot_frm" method="post">
                <div class="panel-body">

                    {{ csrf_field() }}

                    <div class="form-group col-sm-12">
                        <label class="col-lg-2 control-label"> Country</label>
                        <!--                        <div class="col-lg-3">
                                                    <select class="form-control" name="region" id="region" onchange="get_region_amount()">
                                                        <option value="">Select Region</option>
                                                        @foreach($region as $region_data)
                                                        <option value="{{ $region_data->id }}">{{ $region_data->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>-->
                        <div class="col-sm-3">
                            <input type="hidden" name="country" id="country" value="1" />
                            <input type="text" value="{{ $jackpot_amt }}" id="country_amount" class="form-control" name="country_amount" placeholder="Amount in USD">
                        </div>
                    </div>

                    <div class="form-group col-sm-12">
                        <label class="col-lg-2 control-label"> Region</label>
                        <div class="col-lg-3">
                            <select class="form-control" name="region" id="region" onchange="get_region_amount()">
                                <option value="">Select Region</option>
                                @foreach($region as $region_data)
                                <option value="{{ $region_data->id }}">{{ $region_data->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" value="" id="region_amount" class="form-control" name="region_amount" placeholder="Amount in USD">
                        </div>
                    </div>
                    <br>
                    <div class="form-group col-sm-12">
                        <label class="col-lg-2 control-label"> Division</label>
                        <div class="col-lg-3">
                            <select class="form-control" name="division" id="division" onchange="get_division_amount()">
                                <option value="">Select Division</option>
                                @foreach($division as $division_data)
                                <option value="{{ $division_data->id }}">{{ $division_data->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" value="" id="division_amount" class="form-control" name="division_amount" placeholder="Amount in USD">
                        </div>
                    </div>
                    <br>
                    <div class="form-group col-sm-12">
                        <label class="col-lg-2 control-label"> State</label>
                        <div class="col-lg-3">
                            <select class="form-control" name="state" id="state" onchange="get_state_amount()">
                                <option value="">Select State</option>
                                @foreach($state as $state_data)
                                <option value="{{ $state_data->id }}">{{ $state_data->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" value="" id="state_amount" class="form-control" name="state_amount" placeholder="Amount in USD">
                        </div>
                    </div>

                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-3">
                            <button class="btn btn-mint" type="submit">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Percentage Jackpot Amount</h3>
            </div>
            <form action="#" id="" method="post">
                <div class="panel-body">

                    {{ csrf_field() }}

                    <div class="form-group col-sm-12">
                        <label class="col-lg-2 control-label"> Country</label>
                        <!--                        <div class="col-lg-3">
                                                    <select class="form-control" name="region" id="region" onchange="get_region_amount()">
                                                        <option value="">Select Region</option>
                                                        @foreach($region as $region_data)
                                                        <option value="{{ $region_data->id }}">{{ $region_data->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>-->
                        <div class="col-sm-2">
                            <input type="hidden" name="country" id="country" value="1" />
                            <input type="text" value="" id="country_amount" class="form-control" name="country_amount" placeholder="Membership Revenue Percentage Amount">
                        </div>
                        <div class="col-sm-2">
                            <input type="hidden" name="country" id="country" value="1" />
                            <input type="text" value="" id="country_amount" class="form-control" name="country_amount" placeholder="Boost Revenue Percentage Amount">
                        </div>
                        <div class="col-sm-2">
                            <input type="hidden" name="country" id="country" value="1" />
                            <input type="text" value="" id="country_amount" class="form-control" name="country_amount" placeholder="Advertisement Revenue Percentage Amount">
                        </div>
                        <div class="col-sm-2">
                            Total Percentage: 100%
                        </div>
                    </div>

                    <div class="form-group col-sm-12">
                        <label class="col-lg-2 control-label"> Region</label>
                        <!--                        <div class="col-lg-3">
                                                    <select class="form-control" name="region" id="region" onchange="get_region_amount()">
                                                        <option value="">Select Region</option>
                                                        @foreach($region as $region_data)
                                                        <option value="{{ $region_data->id }}">{{ $region_data->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>-->
                        <div class="col-sm-2">
                            <input type="text" value="" id="region_amount" class="form-control" name="region_amount" placeholder="Membership Revenue Percentage Amount">
                        </div>
                        <div class="col-sm-2">
                            <input type="hidden" name="country" id="country" value="1" />
                            <input type="text" value="" id="country_amount" class="form-control" name="country_amount" placeholder="Boost Revenue Percentage Amount">
                        </div>
                        <div class="col-sm-2">
                            <input type="hidden" name="country" id="country" value="1" />
                            <input type="text" value="" id="country_amount" class="form-control" name="country_amount" placeholder="Advertisement Revenue Percentage Amount">
                        </div>
                        <div class="col-sm-2">
                            Total Percentage: 100%
                        </div>
                    </div>
                    <br>
                    <div class="form-group col-sm-12">
                        <label class="col-lg-2 control-label"> Division</label>
                        <!--                        <div class="col-lg-3">
                                                    <select class="form-control" name="division" id="division" onchange="get_division_amount()">
                                                        <option value="">Select Division</option>
                                                        @foreach($division as $division_data)
                                                        <option value="{{ $division_data->id }}">{{ $division_data->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>-->
                        <div class="col-sm-2">
                            <input type="text" value="" id="division_amount" class="form-control" name="division_amount" placeholder="Membership Revenue Percentage Amount">
                        </div>
                        <div class="col-sm-2">
                            <input type="hidden" name="country" id="country" value="1" />
                            <input type="text" value="" id="country_amount" class="form-control" name="country_amount" placeholder="Boost Revenue Percentage Amount">
                        </div>
                        <div class="col-sm-2">
                            <input type="hidden" name="country" id="country" value="1" />
                            <input type="text" value="" id="country_amount" class="form-control" name="country_amount" placeholder="Advertisement Revenue Percentage Amount">
                        </div>
                        <div class="col-sm-2">
                            Total Percentage: 100%
                        </div>
                    </div>
                    <br>
                    <div class="form-group col-sm-12">
                        <label class="col-lg-2 control-label"> State</label>
                        <!--                        <div class="col-lg-3">
                                                    <select class="form-control" name="state" id="state" onchange="get_state_amount()">
                                                        <option value="">Select State</option>
                                                        @foreach($state as $state_data)
                                                        <option value="{{ $state_data->id }}">{{ $state_data->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>-->
                        <div class="col-sm-2">
                            <input type="text" value="" id="state_amount" class="form-control" name="state_amount" placeholder="Membership Revenue Percentage Amount">
                        </div>
                        <div class="col-sm-2">
                            <input type="hidden" name="country" id="country" value="1" />
                            <input type="text" value="" id="country_amount" class="form-control" name="country_amount" placeholder="Boost Revenue Percentage Amount">
                        </div>
                        <div class="col-sm-2">
                            <input type="hidden" name="country" id="country" value="1" />
                            <input type="text" value="" id="country_amount" class="form-control" name="country_amount" placeholder="Advertisement Revenue Percentage Amount">
                        </div>
                        <div class="col-sm-2">
                            Total Percentage: 100%
                        </div>
                    </div>

                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-3">
                            <button class="btn btn-mint" type="submit">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>


        <!--Form for manual jackpot amount end-->

    </div>
    <!--===================================================-->
    <!--End page content-->


</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
                                function get_region_amount() {
                                    $.ajax({
                                        url: "{{ route('get_jackpot_price') }}",
                                        type: "post",
                                        dataType: "json",
                                        data: {
                                            'area_type': "Region", location_id: $('#region').val(),
                                            "_token": "{{ csrf_token() }}",
                                        },
                                        success: function (data) {
                                            if (data.status) {
                                                $('#region_amount').val(data.amount);
                                            } else {
                                                $('#region_amount').val('0.00');
                                            }
                                        }
                                    })
                                }
                                function get_division_amount() {
                                    $.ajax({
                                        url: "{{ route('get_jackpot_price') }}",
                                        type: "post",
                                        dataType: "json",
                                        data: {
                                            'area_type': "Division", location_id: $('#division').val(),
                                            "_token": "{{ csrf_token() }}",
                                        },
                                        success: function (data) {
                                            if (data.status) {
                                                $('#division_amount').val(data.amount);
                                            } else {
                                                $('#division_amount').val('0.00');
                                            }
                                        }
                                    })
                                }
                                function get_state_amount() {
                                    $.ajax({
                                        url: "{{ route('get_jackpot_price') }}",
                                        type: "post",
                                        dataType: "json",
                                        data: {
                                            'area_type': "State", location_id: $('#state').val(),
                                            "_token": "{{ csrf_token() }}",
                                        },
                                        success: function (data) {
                                            if (data.status) {
                                                $('#state_amount').val(data.amount);
                                            } else {
                                                $('#state_amount').val('0.00');
                                            }
                                        }
                                    })
                                }
                                $(document).ready(function () {

                                    $('#region').select2();
                                    $('#division').select2();
                                    $('#state').select2();

                                    var faIcon = {
                                        valid: 'fa fa-check-circle fa-lg text-success',
                                        invalid: 'fa fa-times-circle fa-lg',
                                        validating: 'fa fa-refresh'
                                    }
                                    $('#update_jackpot_frm').bootstrapValidator({
                                        message: 'This value is not valid',
                                        feedbackIcons: faIcon,
                                        fields: {
                                            country_amount: {
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Region is required.'
                                                    }
                                                }
                                            },
                                            country: {
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Amount is required.'
                                                    },
                                                    numeric: {
                                                        message: "Please enter valid amount."
                                                    }
                                                }
                                            },
                                            region: {
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Region is required.'
                                                    }
                                                }
                                            },
                                            division: {
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Division is required.'
                                                    }
                                                }
                                            },
                                            state: {
                                                validators: {
                                                    notEmpty: {
                                                        message: 'State is required.'
                                                    }
                                                }
                                            },
                                            region_amount: {
                                                message: 'Amount is not valid',
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Amount is required.'
                                                    },
                                                    numeric: {
                                                        message: "Please enter valid amount."
                                                    }
                                                }
                                            },
                                            division_amount: {
                                                message: 'Amount is not valid',
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Amount is required.'
                                                    },
                                                    numeric: {
                                                        message: "Please enter valid amount."
                                                    }
                                                }
                                            },
                                            state_amount: {
                                                message: 'Amount is not valid',
                                                validators: {
                                                    notEmpty: {
                                                        message: 'Amount is required.'
                                                    },
                                                    numeric: {
                                                        message: "Please enter valid amount."
                                                    }
                                                }
                                            },

                                        }
                                    }).on('success.field.bv', function (e, data) {
                                        var $parent = data.element.parents('.form-group');
                                        $parent.removeClass('has-success');
                                    });

                                    // $('.editbtn').click(function(){
                                    //     $('#amount').val($(this).data("amount"));
                                    //     $('#id').val($(this).data("id"));
                                    //     $('#planname').val($(this).data("planname"));
                                    //     $('#country_boost_count').val($(this).data("countryboost"));
                                    //     $('#region_boost_count').val($(this).data("regionboost"));
                                    //     $('#division_boost_count').val($(this).data("divisionboost"));
                                    //     $('#state_boost_count').val($(this).data("stateboost"));
                                    //     var vstatus = $(this).data("status");
                                    //         console.log("status "+vstatus);
                                    //    if(vstatus == 1){
                                    //         $(".demoradio7").attr('checked', 'checked');
                                    //         $(".demoradio8").removeAttr("checked");
                                    //     }else{
                                    //         $(".demoradio8").attr('checked', 'checked');
                                    //         $(".demoradio7").removeAttr("checked");
                                    //     }
                                    // });


                                    //fillterrevenue
                                    $('.select_year').change(function () {
                                        var phase = $(this).val();
                                        $('#year').val($(this).val());
                                        if ($('.select_phase').val()) {
                                            $('#fillterrevenue').submit();
                                        } else {
                                            alert('Please choose month span for searching!');
                                        }

                                        //alert( phase);
                                    });
                                    $('.select_phase').change(function () {
                                        var phase = $(this).val();
                                        $('#phase').val($(this).val());

                                        if ($('.select_year').val()) {
                                            $('#fillterrevenue').submit();
                                        } else {
                                            alert('Please choose year for searching!');
                                        }

                                        //alert( phase);
                                    });
                                });
</script>
@stop