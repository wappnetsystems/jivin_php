@extends('admin.layout.authdashboard')
@section('content')
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
          <div id="content-container">
                

                <!--Page content-->
                <!--===================================================-->
               <div id="page-content">
                    
                    <!-- MAIL INBOX -->
                    <!--===================================================-->
                    
                    <div class="fixed-fluid">
                        @include('admin.layout.support_left')
                         <div class="fluid">
                    
                            <!-- VIEW MESSAGE -->
                            <!--===================================================-->
                               <div class="panel">
                                  <div class="panel-body">
                                   
        



          <?php $example =$replies[0]->post_type; ?>

          <!-- <div class="btn-group btn-group"> -->

          <form action="{{ url('/admin/support_request/details')}}/{{$ticket_no}}/{{$user_id}}" method="get">

            <div class="row">
              <div class="col-lg-2 col-sm-2">
                <button title="Print This" type="button" class="btn btn-sm btn-default"><i class="demo-pli-printer" onclick="myFunction()"></i></button>
              </div>
              <div class="col-lg-4 col-sm-2">
                <select class="form-control" name="status" id="status">              
                    <option <?php if (isset($example) && $example=="Open") echo "selected";?>>Open</option>
                    <option <?php if (isset($example) && $example=="In Process") echo "selected";?>>In Process</option>
                    <option <?php if (isset($example) && $example=="Close") echo "selected";?>>Close</option>
                </select>
              </div>
              <div class="col-sm-2">
                <input class="form-control btn btn-primary" type="submit" value="submit">
              </div>
            </div>

          </form>
        <!-- </div> -->

      <div class="clearfix"></div>
      
  <ul>
        @foreach($username as $usernames)
    <li>

    <div class="row">
        <div class="col-sm-7" style="margin-bottom: 5px;">

                    <!--Sender Information-->
                    <hr>
            <div class="media">
                   
             <span class="media-left">
                  <img src="{{ URL::to('/images/users/')}}/{{$image}}" class="img-circle img-sm" alt="Profile Picture">
                    </span>
                   <div class="media-body">
                        <div class="text-bold">{{$user->name}}</div>
                        <small class="text-muted">{{$user->email}}</small>
                    </div>
            </div>
        </div>
        <hr >
        <div class="col-sm-5 clearfix">

            <!--Details Information-->
                 <div class="pull-right text-right">
                <p class="mar-no"><small class="text-muted">{{date('jS M, Y h:i a',strtotime($usernames->created_at))}}</small></p>
               
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
                                         
                                           <!--Message-->
                     <!--===================================================-->
      <div class="pad-ver bord-ver">
   
          {{$usernames->user_asked}}
      </div>
   </li>
   @endforeach
  
   <div class="pad-ver mar-ver bord-btm">
                      <p class="text-main text-bold box-inline"><i class="demo-psi-paperclip icon-fw"></i> Attachments <span> {{$countusercomment}}- </span></p>
                                       

                     <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">View All Files</button>

                     </div>
                            <!-- Modal -->
                  <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">
    
                   <!-- Modal content-->
                  <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 <h4 class="modal-title">All Files</h4>
                               </div>
                            <div class="modal-body">
      
              <ul class="mail-attach-list list-ov">
                   
                   @foreach($userask as $val)
                      
                  <li>{{$val->file_name}}
                            <a href="{{ URL::to('/uploads/')}}/{{$val->file_name}}" class="thumbnail" download>
                                      <div class="mail-file-img">
                                              <?php 
                                           if(preg_match("/\.(gif|png|jpg)$/",$val->file_name))
                                                    {
                                                   ?>
                                      <img class="image-responsive" src="{{ URL::to('/uploads/')}}/{{$val->file_name}}" alt="image">

                                                    <?php }else
                                                        {?>

                                       <img class="image-responsive" src="{{ URL::to('/sitehtml/img/')}}/fileicon.png" alt="image">
                                                     <?php
                                                     }?>
                                               </div>
                                                   
                                        
                                
                                                </a>     
                      </li>
                                            
                           @endforeach
                    
                                            
                                          
              </ul>

        </div>

        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
         </div>
      
    </div>
  </div>
    <ul class="mail-attach-list list-ov">
                   
                   @foreach($userask as $val)
                      
                  <li>{{$val->file_name}}
                            <a href="{{ URL::to('/uploads/')}}/{{$val->file_name}}" class="thumbnail" download>
                                      <div class="mail-file-img">
                                              <?php 
                                           if(preg_match("/\.(gif|png|jpg)$/",$val->file_name))
                                                    {
                                                   ?>
                                      <img class="image-responsive" src="{{ URL::to('/uploads/')}}/{{$val->file_name}}" alt="image">

                                                    <?php }else
                                                        {?>

                                       <img class="image-responsive" src="{{ URL::to('/sitehtml/img/')}}/fileicon.png" alt="image">
                                                     <?php
                                                     }?>
                                               </div>
                                                   
                                        
                                
                                                </a>     
                      </li>
                                            
                           @endforeach
                    
                                            
                                          
              </ul>
@foreach($bothcomment as  $bothcomments)
   <li>

     <div class="row">
                                <div class="col-sm-7">
                    
                                            <!--Sender Information-->
                                      <div class="media">
                                               
                                         <span class="media-left">
                                              <img src="{{ URL::to('/images/users/')}}/{{$image}}" class="img-circle img-sm" alt="Profile Picture">
                                                </span>
                                               <div class="media-body">
                                                    <div class="text-bold">{{$user->name}}</div>
                                                    <small class="text-muted">{{$user->email}}</small>
                                                </div>
                                            </div>
                                       </div>
                                        <hr class="hr-sm visible-xs">
                                            <div class="col-sm-5 clearfix">
                    
                                            <!--Details Information-->
                                                 <div class="pull-right text-right">
                                                <p class="mar-no"><small class="text-muted">{{ $bothcomments->created_at}}</small></p>
                                               
                                            </div>
                                        </div>
                                    </div>
                                         
                                           <!--Message-->
                     <!--===================================================-->
                                    <div class="pad-ver bord-ver">
                                 
                                        {{ $bothcomments->user_asked}}
     

      <div class="pad-ver mar-ver bord-btm">
                      <p class="text-main text-bold box-inline"><i class="demo-psi-paperclip icon-fw"></i> Attachments <span> {{count($bothcomments->attachments)}} </span></p>
                                       

                     <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">View All Files</button>

                     </div>                              
                      </div>


                                  <!-- Modal -->
                  <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">
    
                   <!-- Modal content-->
                  <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 <h4 class="modal-title">All Files</h4>
                               </div>
                            <div class="modal-body">
      
              <ul class="mail-attach-list list-ov">
                   
          


      
                   @foreach($bothcomments->attachments as $value)
                     
                  <li>
                            <a href="{{ URL::to('/uploads/')}}<?php echo $value['file_name'];?>" class="thumbnail" download>
                                      <div class="mail-file-img">
                                              <?php 
                                           if(preg_match("/\.(gif|png|jpg)$/",$value['file_name']))
                                                    {
                                                   ?>
                                      <img class="image-responsive" src="{{ URL::to('/uploads/')}}/<?php echo $value['file_name'];?>" alt="image">

                                                    <?php }else
                                                        {?>

                                       <img class="image-responsive" src="{{ URL::to('/sitehtml/img/')}}/fileicon.png" alt="image">
                                                     <?php
                                                     }?>
                                               </div>
                                                   
                                        
                                
                                                </a>     
                      </li>
                                            
                           @endforeach
                    
                                            
                                          
              </ul>

        </div>

        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
         </div>
      
    </div>
  </div>

<ul class="mail-attach-list list-ov">
                   
          


      
                   @foreach($bothcomments->attachments as $value)
                     
                  <li>
                            <a href="{{ URL::to('/uploads/')}}<?php echo $value['file_name'];?>" class="thumbnail" download>
                                      <div class="mail-file-img">
                                              <?php 
                                           if(preg_match("/\.(gif|png|jpg)$/",$value['file_name']))
                                                    {
                                                   ?>
                                      <img class="image-responsive" src="{{ URL::to('/uploads/')}}/<?php echo $value['file_name'];?>" alt="image">

                                                    <?php }else
                                                        {?>

                                       <img class="image-responsive" src="{{ URL::to('/sitehtml/img/')}}/fileicon.png" alt="image">
                                                     <?php
                                                     }?>
                                               </div>
                                                   
                                        
                                
                                                </a>     
                      </li>
                                            
                           @endforeach
                    
                                            
                                          
              </ul>

   </li>
   @endforeach


</ul>


                 <strong><div id="reply">Reply</div></strong> 
                                     
                                    
            <div class="panel panel-default panel-left" id="myfie">
               <div class="panel-body">
                                

                 <form action="{{ url('/admin/support_request/updatesupport/reply')}}/{{$ticket_no}}" class="form-horizontal" method="post" enctype="multipart/form-data" id="form">
                  
                   <input type="hidden" name="_token" value="{{ csrf_token() }}">                    

                          <div class="form-group">
                                          
                            <div class="col-lg-11">
                                 <input type="hidden" id="ticket" name="ticket" class="form-control" value="<?php echo $d=rand();?>">
                                             
                            </div>

                          </div>

                      

              


                  <div class="form-group">

                    <div class="row">
                      <label class="col-lg-2 control-label text-left" for="inputBody">Message</label>

                      <div class="col-lg-10">
                        <textarea id="inputBody" name="inputBody" class="form-control" ></textarea>
                      </div>
                    </div>
                    <div class="row"> 
                      <label class="col-lg-2 control-label text-left" >Attachments</label>
                      <div class="col-lg-6">
                          <input class="form-control" type="file" name="files[]" multiple>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-lg-2 control-label text-left">&nbsp;</label>
                      <div class="col-lg-2">
                        <input class="form-control btn btn-primary" type="submit" value="submit">
                      </div>
                    </div>
                                   
                  </form>
             
                                 
                                </div>
                         
                    
            
                                    </div> 
                                </div>
                            </div>
                            
                      <!--===================================================-->
                            <!-- END VIEW MESSAGE -->
                    
                        </div>
                    </div>
                    
                    <!--===================================================-->
                    <!-- END OF MAIL INBOX -->
                    
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
  <script src="{{ asset('js/support.js') }}"></script>
    <script src="{{ asset('js/print.js') }}"></script>
    <script src="{{ asset('js/hide-show.js') }}"></script>
    <script src="{{ asset('js/autosave.js') }}"></script>

@stop