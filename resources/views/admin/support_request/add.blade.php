@extends('admin.layout.authdashboard')
@section('content')
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
                    <!-- MAIL INBOX -->
                    <!--===================================================-->
                    
                    <div class="fixed-fluid">
                        @include('admin.layout.support_left')
                        <div class="fluid">
                          <!-- COMPOSE EMAIL -->
                            <!--===================================================-->
                            <div class="panel panel-default panel-left">
                                <div class="panel-body">
                                    <div class="mar-btm pad-btm clearfix">
                                        <!--Cc & bcc toggle buttons-->
                                      <!--   <div class="pull-right pad-btm">
                                            <div class="btn-group">
                                                <button id="demo-toggle-cc" data-toggle="button" type="button" class="btn btn-sm btn-default btn-active-info">Cc</button>
                                                <button id="demo-toggle-bcc" data-toggle="button" type="button" class="btn btn-sm btn-default btn-active-info">Bcc</button>
                                            </div>
                                        </div> -->
                    
                                        <h1 class="page-header text-overflow">
                                            Compose Message
                                        </h1>
                                    </div>
                    
                    
                    
                                    <!--Input form-->
                                    <form action="addsupport/inbox" class="form-horizontal" method="post" enctype="multipart/form-data">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                              <div class="form-group">
                                          
                                            <div class="col-lg-11">
                                                <input type="hidden" id="ticket" name="ticket" class="form-control" value="<?php echo $d=rand();?>">
                                             
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-1 control-label text-left" for="inputEmail">To</label>
                                            <div class="col-lg-11">
                                                <input type="email" id="inputEmail" name="inputEmail" class="form-control">
                                            </div>
                                        </div>
                                      <!--   <div id="demo-cc-input" class="hide form-group">
                                            <label class="col-lg-1 control-label text-left" for="inputCc">Cc</label>
                                            <div class="col-lg-11">
                                                <input type="text" id="inputCc" class="form-control">
                                            </div>
                                        </div>
                                        <div id="demo-bcc-input" class="hide form-group">
                                            <label class="col-lg-1 control-label text-left" for="inputBcc">Bcc</label>
                                            <div class="col-lg-11">
                                                <input type="text" id="inputBcc" class="form-control">
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <label class="col-lg-1 control-label text-left" for="inputSubject">Subject</label>
                                            <div class="col-lg-11">
                                                <input type="text" id="inputSubject" name="inputSubject" class="form-control">
                                            </div>
                                        </div>


                                           <div class="form-group">
                                            <label class="col-lg-1 control-label text-left" for="inputSubject">Message</label>
                                            <div class="col-lg-11">
                                                

                                                <textarea id="inputBody" name="inputBody" class="form-control">
                                                    
                                                </textarea>
                                                  



                                                <!--   <div id="demo-mail-compose"></div>
                    
                                                 <div class="pad-ver"> -->
                                            </div>
                                        </div>
                                    
                    
                    
                                    <!--Attact file button-->
                                  <!--   <div class="media pad-btm">
                                        <div class="media-left">
                                            <span class="btn btn-default btn-file">
                                                Attachment 
                                            </span>
                                        </div>
                                        <div id="demo-attach-file" class="media-body"></div>
                                    </div> -->
                    <input type="file" name="files[]" multiple>
                    
                                    <!--Wysiwyg editor : Summernote placeholder-->
                                   <!--  <div id="demo-mail-compose"></div>
                    
                                    <div class="pad-ver"> -->

                    
                                        <!--Send button-->
                                <!--         <button id="mail-send-btn" type="button" class="btn btn-primary"> -->
                         <div id="mail-save-btn" type="button" class="btn btn-default">
                             <input type="submit" value="submit"></div>
                                </form>
                                   
                    
                             </div>
                          </div>
                    
                    
                            <!--===================================================-->
                            <!-- END VIEW MESSAGE -->
                    </div>
                        </div>
                    
                    
                    <!--===================================================-->
                    <!-- END OF MAIL INBOX -->
                    
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
  <script src="{{ asset('js/support.js') }}"></script>
@stop