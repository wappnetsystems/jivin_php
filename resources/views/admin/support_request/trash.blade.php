@extends('admin.layout.authdashboard')
@section('content')
<style type="text/css">
.pagination {
    padding-left: 0;
      margin-top: auto;
    text-align: center;
    list-style: none;
}    
</style>
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    
                    <!-- MAIL INBOX -->
                    <!--===================================================-->
                    
                    <div class="fixed-fluid">
                        @include('admin.layout.support_left')
                        <div class="fluid">
                            <div class="panel">
                                <div id="demo-email-list" class="panel-body">
                                    <h1 class="page-header text-overflow pad-btm">Trash ({{$getSupportRequestDeletedCount}})</h1>
                    
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="input-group mar-btm">
                                             <form action="searchtrashbox" method="get">
                                              <input type="text" placeholder="Search" id="Search" name="search" class="form-control">
                                             <!--  <span class="input-group-btn"> -->
                                            <!--     <button class="btn btn-mint" type="button">Search</button> -->
                                            <input type="submit"  value="submit">
                                            </form>
                                            <!--   </span> -->
                                            </div>                    

                                        </div>
                                        <hr class="hr-sm visible-xs">
                                        <div class="col-sm-8 clearfix">

                                            <div class="pull-right">
                                               {{ $getSupportRequests->links() }}   
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="hr-sm">
                    
                                    <!--Mail list group-->
                                    <ul id="demo-mail-list" class="mail-list">
                                        <?php

                                          if(!$getSupportRequests->count()){
                                            echo '<li class="mail-list-unread mail-attach">No Support Request Found</li>';
                                          }
                                         ?>
                                        @foreach ($getSupportRequests as $getSupportRequest)
                                        <!--Mail list item-->
                                        <?php if ($getSupportRequest->is_seen == 0) {
                                            $seenClass = "mail-list-unread";
                                        }else{
                                             $seenClass = "mail-list-read";
                                            } ?>
                                        <li class="{{$seenClass}} mail-attach">
                                            <div class="mail-from" style="width: 150px;"><a href="#">{{$getSupportRequest->ticket_no}} 
                                            <?php if ($getSupportRequest->post_type == "Open") {
                                                echo ' <span style="margin-left: 7px;" class="badge badge-danger pull-right">Open</span>';
                                            }elseif($getSupportRequest->post_type == "Close"){
                                                echo ' <span style="margin-left: 7px;" class="badge badge-success pull-right">Close</span>';
                                            }else{
                                               echo ' <span style="margin-left: 7px;" class="badge badge-warning pull-right">In Process</span>';
                                            }  ?>

                                            </a></div>                                        
                                            <div class="mail-from"style="width: 70px;"><a href="#"><img class="img-circle img-sm img-border" src="{{$getSupportRequest->user_img_url}}" alt="Profile Picture"></a></div>                                        
                                            <div class="mail-from" style="width: 150px;"><a href="{{ url('/admin/support_request/details')}}/{{$getSupportRequest->ticket_no}}/{{$getSupportRequest->user_id}}">{{$getSupportRequest->user_full_name}}</a></div>
                                            <div class="mail-time" style="width: 146px;">{{$getSupportRequest->time}}</div>

                                             <div class="mail-attach-icon" style="width: 50px;">
                                                
                                     <?php if ($getSupportRequest->is_attachments == 1) {
                                                echo '   <a href="#">
                                                 
                                                    <i class="demo-psi-paperclip icon-fw"></i>
                                                </a>';
                                         
                                            }
                                            else{
                                               echo '';
                                            }  ?>


                                            </div>
                                  <div class="mail-subject">
                         <a href="{{ url('/admin/support_request/details')}}/{{$getSupportRequest->ticket_no}}/{{$getSupportRequest->user_id}}">{{ $getSupportRequest->subject}}</a>
                                                                                 </div>

                                                    <div style="width: 100px;">

                                                    <a href="{{ url('/admin/support_request/delete')}}/{{$getSupportRequest->ticket_no}}" class="list-group-item">
                                        <i class="demo-pli-recycling icon-lg icon-fw" id="{{ $getSupportRequest->id}}"></i>
                                       
                                    </a></div>

                                        </li>
                                        @endforeach
                          
                                    </ul>
                                </div>
                    
                    
                                <!--Mail footer-->
                                <div class="panel-footer clearfix">
                                    <div class="pull-right">
                                      {{ $getSupportRequests->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--===================================================-->
                    <!-- END OF MAIL INBOX -->
                    
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
  <script src="{{ asset('js/support.js') }}"></script>
     

@stop