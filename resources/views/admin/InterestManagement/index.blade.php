@extends('admin.layout.authdashboard')
@section('content')
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Interest Management</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->


                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ url('/admin/interest-category/list') }}">Interest Category Management</a></li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->


        

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                            <div class="panel">
                        <div class="panel-body demo-nifty-alert" style="padding: 15px 20px 0px;">
                            <div class="row">
                                <div class="col-sm-12">
                    
                                    <!-- Success Alert -->
                                    <!--===================================================-->
                                    @if (session('alert-success'))
                                    <div class="alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done!</strong> {{ session('alert-success') }}
                                    </div>
                                    @endif                                    
                    
                    
                                    <!-- Danger Alert -->
                                    <!--===================================================-->
                                    @if (session('alert-danger'))
                                    <div class="alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oh snap!</strong> {{ session('alert-danger') }}
                                    </div>
                                    @endif                                    
                    
                                </div>
                            </div>
                        </div>                            
                                <div class="panel-heading">
                                    <h3 class="panel-title cat_page_title">Interest Category Add</h3>
                                </div>
                                 
                                <form id="interest-category-add-update-form" method="Post" action="{{ url('/admin/interest-category/add') }}" class="form-horizontal">
                                {{ csrf_field() }}
                                    <div class="panel-body">
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Category Name</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="cat_name" class="form-control" name="name" placeholder="Category Name">
                                                    <input id="cat_id" type="hidden" class="form-control" name="id">                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Status</label>
                                                <div class="col-lg-7">
                                                    <div class="radio">
                                                        <input id="demo-radio-7" class="cat_status_active magic-radio" type="radio" name="is_active" value="1" checked="checked">
                                                        <label for="demo-radio-7">Active</label>
                    
                                                        <input id="demo-radio-8" class="cat_status_deactive magic-radio" type="radio" name="is_active" value="0">
                                                        <label for="demo-radio-8">Deactive</label>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </fieldset>
                    
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-7 col-sm-offset-3">
                                                <button class="btn btn-mint" type="submit">Save</button>
                                                <button type="reset" id="reset" class="btn btn-mint" value="Reset">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>                    
                    <!--  Data Tables -->
                    <!--===================================================-->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Interest Category List</h3>
                        </div>
                        <div class="panel-body">
                                    <div class="status-update-msg alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done!</strong> Status Updated!
                                    </div> 
                                    <div class="status-delete alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done! </strong> Interest Category Deleted!
                                    </div>                                                           
                            <table id="interest-category-list-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="min-tablet">Id</th>
                                        <th class="min-tablet">Name</th>
                                        <th class="min-desktop">Status</th>
                                        <th class="min-desktop">Actions</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <!--===================================================-->
                    <!-- End Striped Table -->
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
  <script src="{{ asset('js/interest.js') }}"></script>
@stop