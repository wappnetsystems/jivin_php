@extends('admin.layout.authdashboard')
@section('content')
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Ad Region Management</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->


                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ url('/admin/advertise/list') }}">Ad Region Management</a></li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->


        

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                            <div class="panel">
                        <div class="panel-body demo-nifty-alert" style="padding: 15px 20px 0px;">
                            <div class="row">
                                <div class="col-sm-12">
                    
                                    <!-- Success Alert -->
                                    <!--===================================================-->
                                    @if (session('alert-success'))
                                    <div class="alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done!</strong> {{ session('alert-success') }}
                                    </div>
                                    @endif                                    
                    
                    
                                    <!-- Danger Alert -->
                                    <?php //print_r($validator);die;?>
                                    <!--===================================================-->
                                    @if (!empty($errors->all()))

                                    
                                        <div>
                                        
                                        <div class="alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oh snap!</strong> {{$errors->all()[0]}}
                                        </div>
                                        </div>
                                   
                                    <?php /*?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oh snap!</strong> {{ session('alert-danger') }}
                                    </div> <?php */?>
                                    @endif                                    
                    
                                </div>
                            </div>
                        </div>                            
                                <div class="panel-heading">
                                    <h3 class="panel-title advertise_page_title">Ad Region Add</h3>
                                </div>
                                 
                                <form id="interest-category-add-update-form" method="Post" action="{{ url('/admin/adregion/add') }}" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                    <div class="panel-body">
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Ad Region Name</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="adregionname" class="form-control" name="name" placeholder="Ad Region Name">
                                                    <input id="adregion_id" type="hidden" class="form-control" name="id">                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Ad Region Height</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="height" class="form-control" name="height" placeholder="Ad Region Hight">                                         
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Ad Region Width</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="width" class="form-control" name="width" placeholder="Ad Region Width">                                         
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Ad Region Class</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="css_class" class="form-control" name="css_class" placeholder="Ad Region Calss">                                         
                                                </div>
                                            </div>
                                            <div class="form-group imagesrcdiv">
                                                <label class="col-lg-3 control-label"> </label>
                                                <div class="col-lg-7">
                                                    <img id="imagesample" src="" alt="Smiley face" height="60" width="90">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Ad Region Sample Image</label>
                                                <div class="col-lg-7">
                                                    <input type="file" id="image" class="form-control" name="image" placeholder="Ad Region Sample Image">                                         
                                                </div>
                                            </div>

                                            
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Status</label>
                                                <div class="col-lg-7">
                                                    <div class="radio">
                                                        <input id="demo-radio-7" class="demoradio7 advertise_status_active magic-radio" type="radio" name="is_active" value="1" checked="checked">
                                                        <label for="demo-radio-7">Active</label>
                    
                                                        <input id="demo-radio-8" class=" demoradio8 advertise_status_deactive magic-radio" type="radio" name="is_active" value="0">
                                                        <label for="demo-radio-8">Deactive</label>
                                                    </div>
                                                </div>
                                            </div> 


                                        </fieldset>
                    
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-7 col-sm-offset-3">
                                                <button class="btn btn-mint" type="submit">Save</button>
                                                <button type="reset" id="reset" class="btn btn-mint" value="Reset">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>                    
                    <!--  Data Tables -->
                    <!--===================================================-->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Ad Region List</h3>
                        </div>
                        <div class="panel-body">
                                    @if(session()->has('success'))
                                    <div class="status-update-msg alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done!</strong> {{ session()->get('success') }}
                                    </div> 

                                    @endif
                                    @if(session()->has('error'))
                                    <div class="status-delete alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oops! </strong> {{ session()->get('error') }}
                                    </div> 
                                     @endif                                                          
                            <table id="advertise-list-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        
                                        <th class="min-tablet">Name</th>
                                        <th class="min-tablet">Height</th>
                                        <th class="min-tablet">Width</th>
                                        <th class="min-tablet">Image</th>
                                        <th class="min-tablet">CSS Class</th>
                                        <th class="min-desktop">Status</th>
                                        <th class="min-desktop">Actions</th>
                                    </tr>
                                    @if(!empty($data))
                                    <?php // echo "<pre>";print_r($data);die;?>
                                        @foreach($data as $record)
                                            <tr>
                                                
                                                <td>{{$record['name']}}</td>
                                                <td>{{$record['height']}}</td>
                                                <td>{{$record['width']}}</td>
                                                
                                                <td><img src="{{ asset('ad_region').'/'.$record['image']}}" alt="Smiley face" height="60" width="90"></td>
                                                <td>{{$record['css_class']}}</td>
                                                <td>
                                                <?php
                                                if($record['status'] == 0){
                                                    $sts = "Deactive";
                                                }else{
                                                    $sts = "Active";
                                                }?>
                                                <strong>{{$sts}}</strong>
                                                <?php /*
                                                ?>
                                                <div class="toggle btn btn-default {{$sts}}" data-toggle="toggle" style="width: 92px; height: 32px;"><input class="toggle-3" type="checkbox" data-toggle="toggle"><div class="toggle-group"><label class="btn btn-primary toggle-on">Enabled</label><label class="btn btn-default active toggle-off">Disabled</label><span class="toggle-handle btn btn-default"></span></div></div><?php*/?>

                                                </td>
                                                <td>

                                                <button class="btn-edit btn btn-mint btn-icon editbtn" data-cssclass="{{$record['css_class']}}" data-image="{{ asset('ad_region').'/'.$record['image']}}" data-id="{{$record['id']}}" data-name="{{$record['name']}}" data-height="{{$record['height']}}" data-width="{{$record['width']}}" data-status="{{$record['status']}}"><i class="demo-psi-pen-5 icon-lg"></i></button>
                                                <a href="{{ url('/admin/adregion/delete').'/'.$record['id'] }}" onclick="return confirm('Are you sure you want to delete this item?');"><button>Delete</button></a>
                                                </td>
                                                
                                            </tr>
                                        @endforeach
                                    @else
                                    Record not found
                                    @endif
                                </thead>
                                <tbody></tbody>
                            </table>
                  <div class="paginatediv" style="float: right;">
                    {!!$data->links()!!}
                  </div>                        </div>
                         

                    </div>
                    <!--===================================================-->
                    <!-- End Striped Table -->
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
<script type="text/javascript">
$( document ).ready(function() {

    $('.imagesrcdiv').css("display","none");

    $('.editbtn').click(function(){

        $('.imagesrcdiv').css("display","block");
        $('#adregionname').val($(this).data("name"));
        $('#adregion_id').val($(this).data("id"));
        $('#height').val($(this).data("height"));
        $('#width').val($(this).data("width"));
        $('#css_class').val($(this).data("cssclass"));

        var imagesrc = $(this).data("image");

        $('#imagesample').attr("src",imagesrc);
        var vstatus = $(this).data("status");
            console.log("status "+vstatus);

        //$('input[class=radio][value=vstatus]').prop("checked",true);
       if(vstatus == 1){
            $(".demoradio7").attr('checked', 'checked');
            //$("#demo-radio-8").attr('checked', '');
            $(".demoradio8").removeAttr("checked");
        }else{
            $(".demoradio8").attr('checked', 'checked');
            //$("#demo-radio-7").attr('checked', '');
            $(".demoradio7").removeAttr("checked");
            
        }

    });
    });
</script>
@stop