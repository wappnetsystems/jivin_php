<!--Profile Widget-->
<!--================================-->
<div id="mainnav-profile" class="mainnav-profile">
    <div class="profile-wrap">
        <div class="pad-btm">
            <img class="img-circle img-sm img-border" src="{{ asset('adminhtml/img/profile-photos/1.png')}}" alt="Profile Picture">
        </div>
        <a href="#profile-nav" class="box-block" data-toggle="collapse" aria-expanded="false">
            <span class="pull-right dropdown-toggle">
                <i class="dropdown-caret"></i>
            </span>
            <p class="mnp-name">{{ Auth::guard('admin')->user()->name }}</p>
            <span class="mnp-desc">{{ Auth::guard('admin')->user()->email }}</span>
        </a>
    </div>
    <div id="profile-nav" class="collapse list-group bg-trans">
       <!--  <a href="#" class="list-group-item">
            <i class="demo-pli-male icon-lg icon-fw"></i> View Profile
        </a> -->
        <a href="{{ url('/admin/settings') }}" class="list-group-item">
            <i class="demo-pli-gear icon-lg icon-fw"></i> Settings
        </a>
        <a href="{{ url('/admin/logout') }}"  class="list-group-item">
            <i class="demo-pli-unlock icon-lg icon-fw"></i> Logout
        </a>                                     
    </div>
</div>


<ul id="mainnav-menu" class="list-group">

<!--Category name-->
<li class="list-header">Navigation</li>

<!--Menu list item-->
<li>
    <a href="{{ url('/admin/dashboard') }}">
        <i class="demo-psi-home"></i>
        <span class="menu-title">
            <strong>Dashboard</strong>
        </span>
    </a>
</li>
<li>
    <a href="{{ url('/admin/revenue/index') }}">
        <i class="demo-psi-split-vertical-2"></i>
        <span class="menu-title">
            <strong>Jivin Revenue</strong>
        </span>
    </a>
</li>
<!--Menu list item-->
<li>
    <a href="{{ url('/admin/user/list') }}">
        <i class="demo-psi-male"></i>
        <span class="menu-title">
            <strong>Jivin Users</strong>
        </span>
    </a>
</li>
<li>
    <a href="{{ url('/admin/price/management/1') }}">
        <i class="demo-psi-male"></i>
        <span class="menu-title">
            <strong>Price Management</strong>
        </span>
    </a>
</li>
<li>
    <a href="#">
        <i class="demo-psi-split-vertical-2"></i>
        <span class="menu-title">
            <strong>Plans</strong>
        </span>
        <i class="arrow"></i>
    </a>

    <!--Submenu-->
    <ul class="collapse">
        <li><a href="{{ url('/admin/memership_plans/list') }}">Membership Plans</a></li>                                            
    </ul>
    <ul class="collapse">
        <li><a href="{{ url('/admin/boost_plans/list') }}">Boost Plans</a></li>                                            
    </ul>
</li>
<!--Menu list item-->
<li>
    <a href="#">
        <i class="demo-psi-split-vertical-2"></i>
        <span class="menu-title">
            <strong>Manage Interest</strong>
        </span>
        <i class="arrow"></i>
    </a>

    <!--Submenu-->
    <ul class="collapse">
        <li><a href="{{ url('/admin/interest-category/list') }}">Interest Category</a></li>                                            
    </ul>
    <ul class="collapse">
        <li><a href="{{ url('/admin/interest-sub-category/list') }}">Interests</a></li>                                            
    </ul>    
</li>


   <!--Menu list item-->

{{-- <li>
    <a href="#">
        <i class="demo-psi-split-vertical-2"></i>
        <span class="menu-title">
            <strong>Manage Advertise</strong>
        </span>
        <i class="arrow"></i>
    </a>--}}

    <!--Submenu-->
    {{-- <ul class="collapse">
        <li><a href="{{ url('/admin/advertise/list') }}">Advertise Page</a></li>                                            
    </ul>
    <ul class="collapse">
        <li><a href="{{ url('/admin/advertise-settings/dependent-dropdown') }}">Advertise Settings</a></li>                                            
    </ul>      --}}
    {{--<ul class="collapse">
        <li><a href="{{ url('/admin/adregion/list') }}">Advertise Region</a></li>                                            
    </ul>
    <ul class="collapse">
        <li><a href="{{ url('/admin/adpage/list') }}">Advertise Page</a></li>                                            
    </ul>
</li>--}}

<!--Menu list item-->

<li>
    <a href="#">
        <i class="demo-psi-split-vertical-2"></i>
        <span class="menu-title">
            <strong>Manage Location</strong>
        </span>
        <i class="arrow"></i>
    </a>

    <!--Submenu-->
    <ul class="collapse">
        <li><a href="{{ url('/admin/country/list') }}">Country</a></li>                                            
    </ul>
    <ul class="collapse">
        <li><a href="{{ url('/admin/region/list') }}">Region</a></li>                                            
    </ul>     
    <ul class="collapse">
        <li><a href="{{ url('/admin/devision/list') }}">Division</a></li>                                            
    </ul>     
    <ul class="collapse">
        <li><a href="{{ url('/admin/state/list') }}">State</a></li>                                            
    </ul>    
</li>
<li>
    <a href="#">
        <i class="demo-psi-split-vertical-2"></i>
        <span class="menu-title">
            <strong>Manage Site</strong>
        </span>
        <i class="arrow"></i>
    </a>

    <!--Submenu-->
    <!-- <ul class="collapse">
        <li><a href="{{ url('/admin/cms/list') }}">CMS</a></li>                                            
    </ul>  -->
    <ul class="collapse">
        <li><a href="{{ url('/admin/sitesetting') }}">Site Setting</a></li>                                            
    </ul> 
    <ul class="collapse">
        <li><a href="{{ url('/admin/sitesetting/change-bid-amount') }}">Change Bid Amount</a></li>                                            
    </ul>  
    <ul class="collapse">
        <li><a href="{{ route('jivin_video') }}">Jivin Video</a></li>                                            
    </ul>  
</li>
<li>
    <a href="#">
        <i class="demo-psi-split-vertical-2"></i>
        <span class="menu-title">
            <strong>Support Request </strong>
        </span>
        <i class="arrow"></i>
    </a>

    <!--Submenu-->
    <ul class="collapse">
        <li><a href="{{ url('/admin/support_request/list/inbox') }}">Inbox</a></li>                                            
    </ul>   
</li>
<li>
    <a href="#">
        <i class="demo-psi-split-vertical-2"></i>
        <span class="menu-title">
            <strong>Report Post </strong>
        </span>
        <i class="arrow"></i>
    </a>

    <!--Submenu-->
    <ul class="collapse">
        <li><a href="{{ url('/admin/post/details') }}">Post List</a></li>                                            
    </ul>   
</li>
<li class="list-divider"></li>                                
</ul>