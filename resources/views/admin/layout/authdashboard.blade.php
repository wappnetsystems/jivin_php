<!DOCTYPE html>
<html lang="en">



<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">    
    <title>{{ config('app.name', 'Jivin Admin') }}</title>


    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>


    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('adminhtml/css/bootstrap.min.css') }}" rel="stylesheet">


    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('adminhtml/css/nifty.min.css') }}" rel="stylesheet">


    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="{{ asset('adminhtml/css/demo/nifty-demo-icons.min.css') }}" rel="stylesheet">


    <!--Demo [ DEMONSTRATION ]-->
    <link href="{{ asset('adminhtml/css/demo/nifty-demo.min.css') }}" rel="stylesheet">

        
    <!--Morris.js [ OPTIONAL ]-->
    <link href="{{ asset('adminhtml/plugins/morris-js/morris.min.css') }}" rel="stylesheet">


    <!--Magic Checkbox [ OPTIONAL ]-->
    <link href="{{ asset('adminhtml/plugins/magic-check/css/magic-check.min.css') }}" rel="stylesheet">

    <!--DataTables [ OPTIONAL ]-->
    <link href="{{ asset('adminhtml/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('adminhtml/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}" rel="stylesheet">

        <!--Bootstrap Validator [ OPTIONAL ]-->
    <link href="{{ asset('adminhtml/plugins/bootstrap-validator/bootstrapValidator.min.css') }}" rel="stylesheet">

    <!--Switchery [ OPTIONAL ]-->
    <link href="{{ asset('adminhtml/plugins/switchery/switchery.min.css') }}" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!--Summernote [ OPTIONAL ]-->
    <link href="{{ asset('adminhtml/plugins/summernote/summernote.min.css') }}" rel="stylesheet">
    
    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="{{ asset('adminhtml/plugins/pace/pace.min.css') }}" rel="stylesheet">
    
    <link href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" rel="stylesheet" />
    
    <script src="{{ asset('adminhtml/plugins/pace/pace.min.js') }}"></script>


    <!--jQuery [ REQUIRED ]-->
    <script src="{{ asset('adminhtml/js/jquery-2.2.4.min.js') }}"></script>


    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="{{ asset('adminhtml/js/bootstrap.min.js') }}"></script>


    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="{{ asset('adminhtml/js/nifty.min.js') }}"></script>
    
    <!--DataTables [ OPTIONAL ]-->
    <script src="{{ asset('adminhtml/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('adminhtml/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('adminhtml/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!--DataTables Sample [ SAMPLE ]-->
<!--     <script src="{{ asset('adminhtml/js/demo/tables-datatables.js') }}"></script> -->

    <!--Bootstrap Validator [ OPTIONAL ]-->
    <script src="{{ asset('adminhtml/plugins/bootstrap-validator/bootstrapValidator.min.js') }}"></script>


    <!--Masked Input [ OPTIONAL ]-->
    <script src="{{ asset('adminhtml/plugins/masked-input/jquery.maskedinput.min.js') }}"></script>


    <!--Form validation [ SAMPLE ]-->
    <script src="{{ asset('adminhtml/js/demo/form-validation.js') }}"></script> 
    <!-- <script src="{{ asset('adminhtml/js/demo/form-component.js') }}"></script>  -->

    <!--Switchery [ OPTIONAL ]-->
    <script src="{{ asset('adminhtml/plugins/switchery/switchery.min.js') }}"></script>

    <!--Summernote [ OPTIONAL ]-->
    <script src="{{ asset('adminhtml/plugins/summernote/summernote.min.js') }}"></script>


    <!--Form File Upload [ SAMPLE ]-->
    <script src="{{ asset('adminhtml/js/demo/form-text-editor.js') }}"></script>
        
    <!--Custome JS-->
    @yield ('scripts')   

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script> 
    <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
    </script>         
    <!-- Scripts -->
</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->
<body>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">
        
        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">

                <!--Brand logo & name-->
                <!--================================-->
                <div class="navbar-header">
                    <a href="{{ url('/admin') }}" class="navbar-brand">
                        <img src="{{ asset('adminhtml/img/jivinlogo.png') }}" alt="Jivin Logo" class="brand-icon">
                        <div class="brand-title">
                            <span class="brand-text">Jivin</span>
                        </div>
                    </a>
                </div>
                <!--================================-->
                <!--End brand logo & name-->


                <!--Navbar Dropdown-->
                <!--================================-->
                <div class="navbar-content clearfix">
                    <ul class="nav navbar-top-links pull-left">

                        <!--Navigation toogle button-->
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <li class="tgl-menu-btn">
                            <a class="mainnav-toggle" href="#">
                                <i class="demo-pli-view-list"></i>
                            </a>
                        </li>
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <!--End Navigation toogle button-->

                    </ul>
                    <ul class="nav navbar-top-links pull-right">

                        @include('admin.layout.authtoprightmenu')
                    </ul>
                </div>
                <!--================================-->
                <!--End Navbar Dropdown-->

            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->

        <div class="boxed">
           @yield('content')
            
            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">

                    <!--Menu-->
                    <!--================================-->
                    <div id="mainnav-menu-wrap">
                        <div class="nano">
                            <div class="nano-content">
                              @include('admin.layout.authleftmenu')
                            </div>
                        </div>
                    </div>
                    <!--================================-->
                    <!--End menu-->

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->

        </div>

        

        <!-- FOOTER -->
        <!--===================================================-->
        <footer id="footer">
            <p class="pad-lft">&#0169; 2017 Jivin</p>
        </footer>
        <!--===================================================-->
        <!-- END FOOTER -->


        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->



    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->


</body>
</html>
