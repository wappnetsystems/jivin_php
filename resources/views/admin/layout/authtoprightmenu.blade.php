<!--User dropdown-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<li id="dropdown-user" class="dropdown">
    <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
        <span class="pull-right">
            <!--<img class="img-circle img-user media-object" src="img/profile-photos/1.png" alt="Profile Picture">-->
            <i class="demo-pli-male ic-user"></i>
        </span>
        <div class="username hidden-xs">{{ Auth::guard('admin')->user()->name }}</div>
    </a>


    <div class="dropdown-menu dropdown-menu-md dropdown-menu-right panel-default">

        <!-- User dropdown menu -->
        <ul class="head-list">
            <!-- <li>
                <a href="#">
                    <i class="demo-pli-male icon-lg icon-fw"></i> Profile
                </a>
            </li> -->
            <li>
                <a href="{{ url('/admin/settings') }}">
                    <i class="demo-pli-gear icon-lg icon-fw"></i> Settings
                </a>
            </li>
        </ul>

        <!-- Dropdown footer -->
        <div class="pad-all text-right">
            <a href="{{ url('/admin/logout') }}" class="btn btn-primary">
                <i class="demo-pli-unlock"></i> Logout
            </a>
        </div>
    </div>
</li>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End user dropdown-->