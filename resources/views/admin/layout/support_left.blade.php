<div class="fixed-sm-200 fixed-md-250 pull-sm-left">
    <div class="panel">
        <div class="pad-all bord-btm">
            <div class="pad-all">
                <!-- <a href="{{ url('/admin/support_request/add') }}" class="btn btn-block btn-success">Add New Support ticket</a> -->
            </div>                                
        </div>

        <p class="pad-hor mar-top text-main text-bold">Folders</p>
        <div class="list-group bg-trans pad-btm bord-btm">
            <a href="{{ url('/admin/support_request/list/inbox') }}" class="list-group-item text-semibold text-main">
                <span class="badge badge-success pull-right">{{$getSupportRequestInboxCount}}</span>
                <span class="text-main"><i class="demo-pli-inbox-full icon-lg icon-fw"></i> Inbox</span>
            </a>

            <a href="{{ url('/admin/support_request/list/deleted') }}" class="list-group-item text-semibold">
                <span class="badge badge-danger pull-right">{{$getSupportRequestDeletedCount}}</span>
                <span class="text-main"><i class="demo-pli-recycling icon-lg icon-fw"></i> Trash</span>
            </a>
        </div>


    </div>
</div>