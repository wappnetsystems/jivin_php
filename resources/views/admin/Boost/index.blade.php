@extends('admin.layout.authdashboard')
@section('content')
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Boost Plans Management</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->


                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ url('/admin/boost_plans/list') }}">Boost Plans Management</a></li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->


        

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                            <div class="panel">
                        <div class="panel-body demo-nifty-alert" style="padding: 15px 20px 0px;">
                            <div class="row">
                                <div class="col-sm-12">
<!-- Success Alert -->
                                    <!--===================================================-->
                                    @if (session('alert-success'))
                                    <div class="alert alert-success">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Well done!</strong> {{ session('alert-success') }}
                                    </div>
                                    @endif                                    
                    
                    
                                    <!-- Danger Alert -->
                                    <?php //print_r($validator);die;?>
                                    <!--===================================================-->
                                    @if (!empty($errors->all()))

                                    
                                        <div>
                                        
                                        <div class="alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oh snap!</strong> {{$errors->all()[0]}}
                                        </div>
                                        </div>
                                   
                                    {{-- <div class="alert alert-danger">
                                        <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                        <strong>Oh snap!</strong> {{ session('alert-danger') }}
                                    </div> --}}
                                    @endif            
                                </div>
                            </div>
                        </div>                            
                                <div class="panel-heading">
                                    <h3 id="boostplan-title" class="panel-title plan_page_title">Boost Plan Add</h3>
                                </div>
                                 
                                <form id="plan-add-update-form" method="Post" action="{{ url('/admin/boost_plan/add') }}" class="form-horizontal">
                                {{ csrf_field() }}
                                    <div class="panel-body">
                                        <fieldset>
                                           <div class="form-group">
                                                <label class="col-lg-3 control-label">Plan Name</label>
                                                <div class="col-lg-7">
                                                    <input value="{{old('planname')}}" type="text" id="planname" class="form-control" name="planname" placeholder="Plan Name">
                                                    <input value="{{old('id')}}" id="id" type="hidden" class="form-control" name="id">                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Plan Price($)</label>
                                                <div class="col-lg-7">
                                                    <input value="{{old('amount')}}" type="text" id="amount" class="form-control" name="amount" placeholder="Amount">                                         
                                                </div>
                                            </div>
                                            {{-- <div class="form-group">
                                                <label class="col-lg-3 control-label">Num of boost</label>
                                                <div class="col-lg-7">
                                                    <input value="{{old('numofboost')}}" type="text" id="numofboost" class="form-control" name="numofboost" placeholder="Num of boost">                                         
                                                </div>
                                            </div> --}}
                                           <div class="form-group">
                                                <label class="col-lg-3 control-label">Num of country boost</label>
                                                <div class="col-lg-7">
                                                    <input value="{{old('country_boost_count')}}" type="text" id="country_boost_count" class="form-control" name="country_boost_count" placeholder="Num of boost">                                         
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Num of region boost</label>
                                                <div class="col-lg-7">
                                                    <input value="{{old('region_boost_count')}}" type="text" id="region_boost_count" class="form-control" name="region_boost_count" placeholder="Num of boost">                                         
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Num of division boost</label>
                                                <div class="col-lg-7">
                                                    <input value="{{old('division_boost_count')}}" type="text" id="division_boost_count" class="form-control" name="division_boost_count" placeholder="Num of boost">                                         
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Num of state boost</label>
                                                <div class="col-lg-7">
                                                    <input value="{{old('state_boost_count')}}" type="text" id="state_boost_count" class="form-control" name="state_boost_count" placeholder="Num of boost">                                         
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Status</label>
                                                <div class="col-lg-7">
                                                    <div class="radio">

                                                        <input id="demo-radio-7" class="demoradio7 advertise_status_active magic-radio" type="radio" name="is_active" value="1" @if(null!==old('is_active')) @if(old('is_active')==1) checked="checked" @endif  @else checked="checked" @endif >
                                                        <label for="demo-radio-7">Active</label>
                    
                                                        <input id="demo-radio-8" class="demoradio8 advertise_status_deactive magic-radio" type="radio" name="is_active" value="0" @if(null!==old('is_active')) && old('is_active')==0) checked="checked" @endif >
                                                        <label for="demo-radio-8">Deactive</label>
                                                    </div>
                                                </div>
                                            </div> 
                                           
                                        </fieldset>
                    
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-7 col-sm-offset-3">
                                                <button class="btn btn-mint" type="submit">Save</button>
                                                <button type="reset" id="reset" class="btn btn-mint" value="Reset">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>                    
                    <!--  Data Tables -->
                    <!--===================================================-->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Boost Plan List</h3>
                        </div>
                        <div class="panel-body">
                                    <div class="col-sm-12">
                                    <?php ?>
                                                             
                                    <?php ?>
                                </div>

                            <table id="plan-list-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        {{-- <th class="min-tablet">Id</th> --}}
                                        <th class="min-tablet">Plan Name</th>
                                        <th class="min-tablet">Country Boost Count</th>
                                        <th class="min-tablet">Region Boost Count</th>
                                        <th class="min-tablet">Division Boost Count</th>
                                        <th class="min-tablet">State Boost Count</th>
                                        {{-- <th class="min-tablet">Boost Count</th> --}}
                                        <th class="min-tablet">Plan Price($)</th>
                                        <th class="min-tablet">Status</th>
                                        <th class="min-desktop">Actions</th>

                                    </tr>
                                    @if(!empty($boostplans))
                                    <?php // echo "<pre>";print_r($boostplans);die;?>
                                        @foreach($boostplans as $record)
                                            <tr>
                                                
                                                <td>{{$record['plan_name']}}</td>

                                                <td>{{$record['country_boost_count']}}</td>
                                                <td>{{$record['region_boost_count']}}</td>
                                                <td>{{$record['division_boost_count']}}</td>
                                                <td>{{$record['state_boost_count']}}</td>

                                                {{-- <td>{{$record['num_of_boost']}}</td> --}}
                                                <td>{{$record['amount']}}</td>
                                                <td>
                                                <?php
                                                if($record['is_active'] == 0){
                                                    $sts = "Deactive";
                                                    ?>
                                                        <input class="toggle-3" id="toogle-3-{{$record['id']}}" type="checkbox" data-toggle="toggle" data-id="{{$record['id']}}">
                                                    <?php
                                                } else {
                                                    ?>
                                                        <input class="toggle-3" id="toogle-3-{{$record['id']}}" type="checkbox" data-toggle="toggle" checked data-id="{{$record['id']}}">
                                                    <?php
                                                    $sts = "Active";
                                                }
                                                ?>
                                                <!-- <strong>{{$sts}}</strong> -->
                                               
                                                <!-- <div class="toggle btn btn-default {{$sts}}" data-toggle="toggle" style="width: 92px; height: 32px;">
                                                    <input class="toggle-3" type="checkbox" data-toggle="toggle">
                                                    <div class="toggle-group">
                                                        <label class="btn btn-primary toggle-on">Enabled</label>
                                                        <label class="btn btn-default active toggle-off">Disabled</label>
                                                        <span class="toggle-handle btn btn-default"></span>
                                                    </div> 
                                                </div>-->

                                                </td>
                                                <td>

                                                <button class="btn-edit btn btn-mint btn-icon editbtn" data-countryboost="{{$record['country_boost_count']}}" data-regionboost="{{$record['region_boost_count']}}" data-divisionboost="{{$record['division_boost_count']}}" data-stateboost="{{$record['state_boost_count']}}" data-status="{{$record['is_active']}}" data-id="{{$record['id']}}" data-numofboost="{{$record['num_of_boost']}}" data-amount="{{$record['amount']}}" data-planname="{{$record['plan_name']}}"><i class="demo-psi-pen-5 icon-lg"></i></button></td>
                                            </tr>
                                        @endforeach
                                    @else
                                    Record not found
                                    @endif
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <!--===================================================-->
                    <!-- End Striped Table -->
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
<script type="text/javascript">
$( document ).ready(function() {

    $('.editbtn').click(function(){
        //alert($(this).data("id"));
        $('#amount').val($(this).data("amount"));
        $('#id').val($(this).data("id"));
        $('#planname').val($(this).data("planname"));
        //$('#numofboost').val($(this).data("numofboost"));
        $('#boostplan-title').html('Edit Boost Plan : '+ $(this).data("planname"));

        $('#country_boost_count').val($(this).data("countryboost"));
        $('#region_boost_count').val($(this).data("regionboost"));
        $('#division_boost_count').val($(this).data("divisionboost"));
        $('#state_boost_count').val($(this).data("stateboost"));
        var vstatus = $(this).data("status");
            console.log("status "+typeof vstatus);

        //$('input[class=radio][value=vstatus]').prop("checked",true);
       if(vstatus == 1){
            //$("#demo-radio-8").attr('checked', '');
            $("#demo-radio-7").attr('checked', 'checked');
            $("#demo-radio-8").removeAttr("checked");
        }else{
            $("#demo-radio-8").attr('checked', 'checked');
            $("#demo-radio-7").removeAttr("checked");
            //$("#demo-radio-7").attr('checked', '');
            
        }

        $("html, body").animate({ scrollTop: 0 }, "slow");

    });

    

    $('#plan-list-table').on( 'change', '.toggle-3', function ($event) {
        console.log($($event.target).attr('data-id'));
        var id = $($event.target).attr('data-id');
        if($(this).prop("checked") == true){
            //Enable
            updateStatus(id,1)
        }else{
            //Disable
            updateStatus(id,0)
        }        
    });

    });

    function updateStatus(id,status){
        $.ajax({
            type    :   "POST",
            cache   :   false,
            url     :   APP_URL+'/boost-plan-ajax-status-update',
            data    :   {'id':id,'status':status}, 
            dataType:   "JSON",
            xhrFields: {
                withCredentials: true
            },
            success: function(data){
            console.log(data);
                if (data) {
                    $('.status-update-msg').show();
                    $('.status-update-msg').delay(2000).fadeOut('slow');
                    //listPlans();
                }else{                  
                    $('.status-not-update-msg').show();
                    $('.status-not-update-msg').delay(2000).fadeOut('slow');
                    //listPlans();
                }
               //listInterestCategory();
            },          
            error: function(xhr){       
                alert("ERROR","<p>An error has occurred. Please try again.</p>");
            }
        });
    }
</script>
@stop