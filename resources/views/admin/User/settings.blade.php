@extends('admin.layout.authdashboard')
@section('content')
<style type="text/css">
    .with-errors {
        color: red;
    }
</style>
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Settings</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->


                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
                    <li><a href="{{ url('/admin/settings') }}">Settings</a></li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->


        

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                   
                    <!--  Data Tables -->
                    <!--===================================================-->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Change Password</h3>
                        </div>
                        <div class="panel-body">
                            @if(\Session::has('success'))

                            <div class="alert alert-success fade in">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success!</strong> {{\Session::get('success')}}.
                            </div>
                            @endif

                            <form id="form-change-password" role="form" method="POST" action="{{ url('/admin/save_settings') }}" novalidate class="form-horizontal">
                              <div class="col-md-9">             
                                <label for="current-password" class="col-sm-4 control-label">Current Password</label>
                                <div class="col-sm-8">
                                  <div class="form-group">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                    <input type="password" class="form-control" id="current-password" name="current_password" placeholder="Password">
                                  </div>
                                  
                                  <div class="help-block with-errors">
                                     @if ($errors->has('current_password'))
                                        {{ $errors->first('current_password') }}
                                     @endif
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                                <label for="password" class="col-sm-4 control-label">New Password</label>
                                <div class="col-sm-8">
                                  <div class="form-group">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                  </div>
                                  <div class="help-block with-errors">
                                     @if ($errors->has('password'))
                                        {{ $errors->first('password') }}
                                     @endif
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <label for="password_confirmation" class="col-sm-4 control-label">Confirm New Password</label>
                                <div class="col-sm-8">
                                  <div class="form-group">
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Re-enter Password">
                                  </div>
                                  <div class="help-block with-errors">
                                     @if ($errors->has('password_confirmation'))
                                         {{ $errors->first('password_confirmation') }}
                                     @endif
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                                
                              </div>
                              <div class="form-group">
                                <div class="col-sm-offset-5 col-sm-6">
                                  <button type="submit" class="btn btn-danger">Submit</button>
                                </div>
                              </div>
                            </form>
                        </div>
                    </div>
                    <!--===================================================-->
                    <!-- End Striped Table -->
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection
@section('scripts')
  
@stop