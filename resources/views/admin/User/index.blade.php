@extends('admin.layout.authdashboard')
@section('content')
<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">

    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Users Management</h1>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->


    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
        <li><a href="{{ url('/admin/memership_plans/list') }}">Users Management</a></li>
    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->




    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">

        <!--  Data Tables -->
        <!--===================================================-->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Users List</h3>
            </div>
            <div class="panel-body">
                <div class="status-update-msg alert alert-success">
                    <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                    <strong>Well done!</strong> Status Updated!
                </div>    
                <div class="">
                    <table id="user-list-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="min-tablet">Id</th>
                                <th class="min-tablet">Profile Image</th>
                                <th class="min-tablet">Name</th>
                                <th class="min-tablet">Email</th>
                                <th class="min-tablet">Username</th>
                                <!-- <th class="min-desktop">Status</th> -->
                                <th class="min-desktop">Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--===================================================-->
        <!-- End Striped Table -->

    </div>
    <!--===================================================-->
    <!--End page content-->


</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<div class="modal fade" id="wallet_view" tabindex="-1" role="dialog"
     aria-labelledby="edit" aria-hidden="true">
    <div class="displaytable">
        <div class="modal-dialog" style=" margin-top:-50% !important;" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">View Wallet
                        <span class="close pull-right" data-dismiss="modal">x</span>
                    </h4>
                </div>
                <div class="modal-body" id="wallet_body">
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('js/user.js') }}"></script>
<script>
    function get_wallet_detail(id){
        
        $('#wallet_body').html('');
        $.ajax({
           url:"{{ route('get_wallet_list') }}" ,
           type:"post",
           dataType:"html",
           data:{'user_id':id},
           success:function(data){
               $('#wallet_body').html(data);
           }
        });
    }
    
</script>
@stop