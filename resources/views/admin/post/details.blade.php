@extends('admin.layout.authdashboard')
@section('content')
            <!--CONTENT CONTAINER-->
            <!--===================================================-->


          <div id="content-container">
                
                <div id="page-title">
                    <h1 class="page-header text-overflow">Posts Report Management</h1>
                </div>

              <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
                    <li><a href="#">Posts Report Management</a></li>
                </ol>
                <!--Page content-->
                <!--===================================================-->
               <div id="page-content">
                    
                    <!-- MAIL INBOX -->
                    <!--===================================================-->
              <div class="panel">
                    <div class="panel-body">
                          <div class="table-responsive">
                              <table class="table table-striped">
                                  <thead>
                                      <tr>
                                          <th>Image</th>
                                          <th>Reporte To</th>
                                          <th>Email</th>
                                          <th>Report By</th>
                                          <th>Title</th>
                                          <th>Details</th>
                                          <th>Post Details</th>
                                          <th>Action</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                  <?php //print_r($post);die;?>
                                  @if(count($post)>0)
                                  @foreach($post as $posts)
                                   <?php //echo "<pre>"; print_r($posts->posts['userposts']['name']);?>
                                   
                                  <tr id="reposrtpost{{$posts->id}}">
                                      <td><img class="img-circle img-sm img-border" src="{{$posts->posts['userposts']['user_image_url']}}" width="70px" height="70px" ></td>
                                      <td>{{$posts->posts['userposts']['name']}}</td>
                                      <td>{{$posts->posts['userposts']['email']}}</td>
                                      <td>{{$posts->users[0]['email']}}</td>
                                      <td>{{$posts->title}}</td>
                                      <td>{{$posts->details}}</td>
                                      <th><input type="button" class="showpost" name="show" data-val="{{$posts->posts['text_details']}}" data-daudio ="{{$posts->posts['audio_url']}}" data-video ="{{$posts->posts['video_url']}}"  data-mediatype ="{{$posts->posts['media_type']}}" value="Show"></th>
                                      <th><select class="selectaction" data-post_id="{{$posts->post_id}}" data-id="{{$posts->id}}">
                                        <?php
                                        if($posts->is_active == 0){
                                            ?>
                                            <option value="" selected>Select</option>
                                            <?php
                                        }else{
                                            ?>
                                            <option value="" selected>Select</option>
                                            <option value="remove">Remove</option>
                                            <?php
                                        }
                                        ?>
                                           
                                          </select></th>                             
                                  </tr>
                                  
                                  @endforeach
                                  @else
                                  <td>Record Not found</td>
                                  
                                  @endif
                              </tbody>
                          </table>
                      </div>
                  </div>

                  <div class="paginatediv" style="float: right;">
                    {!!$post->links()!!}
                  </div>
                  
                  </div>

                            
                      <!--===================================================-->
                            <!-- END VIEW MESSAGE -->
                    
                      
                    
                    <!--===================================================-->
                    <!-- END OF MAIL INBOX -->
                                 
                    
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->



            <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="myModalLabel">Post Details</h4>

            </div>
            <div class="modal-body contectdiv"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
  <script src="{{ asset('js/support.js') }}"></script>
    <script src="{{ asset('js/print.js') }}"></script>
    <script src="{{ asset('js/hide-show.js') }}"></script>
    <script src="{{ asset('js/autosave.js') }}"></script>
<script type="text/javascript">
$( document ).ready(function() {
var adminurl = "{{url('/admin')}}";
$(".selectaction").change(function(){
      var reportId = $(this).val();
      
      if (reportId!='') {
            if (reportId == 'remove') {

                  //removepostfun($(this).data("post_id"),$(this).data("id"));
                  
                  var result = confirm("Want to remove this post?");
                  if (result) {
                      removepostfun($(this).data("post_id"),$(this).data("id"));
                  }
            }else if (reportId == 'block') {
                  deleteuserfun();
                  //alert(adminurl+" "+$(this).data("id")+" => "+reportId);
            }
            
      }
      
});
function removepostfun(postid,id) { 
      //alert(postid+" "+id);
            
      
      		$.ajax({
			type	: 	"POST",
			//cache	:	false,
			url		: 	adminurl+'/post/delete',
			data	:	{'postid':postid,'id':id}, 
			//dataType:	"JSON",
			//xhrFields: {
			//	withCredentials: true
			//},
			success: function(data){
            //console.log(data);
            if (data == 1) {
                  console.log(data);
                  var dynmictd = "#reposrtpost"+id;
                  //$(dynmictd).slideUp("slow", function() { $(dynmictd).remove();});
                  //$(dynmictd).remove();

                  alert("Post removed successfully");
                  setTimeout(function(){location.reload(); }, 2000);

                  
            }
			
			},			
			error: function(xhr){ 		
				alert("ERROR","<p>An error has occurred. Please try again.</p>");
			}
		});
}
  $( ".showpost" ).click(function() {
    var data = $(this).data("val");
    var audio = $(this).data("daudio");

    var video = $(this).data("video");
    var mediatype = $(this).data("mediatype"); 
    var html = "";
    //alert(mediatype); 
        if (mediatype == "audio") {
            html = html+'<audio controls><source src="'+audio+'" type="audio/mpeg"></audio>';
            //html = audio;
        }else if (mediatype == "video") {
            html = html+'<video width="320" height="240" controls><source src="'+video+'" type="video/mp4"></video>';
            //html = video;
        }else{
            html = html+data;
        }
        console.log(html);

    $('.contectdiv').html(html);
    $('#myModal').modal('show');
  });
});
</script>
@stop