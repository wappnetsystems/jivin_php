@extends('admin.layout.authdashboard')
@section('content')
<style type="text/css">
.table-responsive{width: 100%; height: 500px; overflow: auto;}
table.bird-place-list thead th{white-space: nowrap; background: #e1e9f3; color: #1c3350!important;font-weight: normal!important; border-color: #bdd0e8!important; padding-top: 11px; padding-bottom: 11px;}
.hidden {display: none;}
</style>
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">Bid Price Management</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->


                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
                    <li><a href="javascript:;">Bid Price Management</a></li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->


        

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                   
                    <!--  Data Tables -->
                    <!--===================================================-->
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="col-lg-6 col-sm-6">
                                <h3 class="panel-title">Bid Price List</h3>
                            </div>
                            <div class="col-lg-6 col-sm-6">
                                <select class="form-control go-to-page">
                                    @foreach($all_ad_regions as $eachAdreg)
                                        <optgroup label="{{$eachAdreg->name}}">
                                            @foreach($eachAdreg->getalladpages as $eachAdpage) 
                                            <option value="{{$eachAdpage->id}}">{{$eachAdpage->page_name}}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                            
                        </div>
                        <div class="panel-body">

                            <div class="status-update-msg alert alert-success hidden">
                                <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                <strong>Well done!</strong> Value successfully changed!
                            </div> 
                            <div class="status-update-msg-error alert alert-danger hidden">
                                <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                                <strong>Oh snap!</strong> Data not saved. Please try again!
                            </div> 

                            <div class="table-responsive">                                                
                                <table class="table table-striped table-bordered bird-place-list" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="min-tablet">State</th>
                                            <?php $hour = 0;

                                            while($hour++ < 24) {

                                                $timetoprint = date('ga',mktime($hour,0,0,1,1,2011));
                                            ?>
                                            <th class="min-tablet">{{$timetoprint}}</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($all_state as $eachState)
                                            <tr>
                                            <td style="background: #e1e9f3;" title="{{$eachState->name}}">{{$eachState->slug}}</td>
                                            @foreach($data->where('state_id',$eachState->id)->all() as $eachPriceData)
                                                <td class="edit-td" data-page_id="{{$eachPriceData->ad_page_id}}" data-state_id="{{$eachPriceData->state_id}}"
                                                data-min_bid="{{$eachPriceData->min_bid}}" data-buy_now="{{$eachPriceData->buy_now_price}}" data-from_time="{{$eachPriceData->from_time}}" data-to_time="{{$eachPriceData->to_time}}">
                                                    <p>
                                                        Min Bid:&nbsp;{{$eachPriceData->min_bid}}
                                                    </p>
                                                    <p>
                                                        Buy Price:&nbsp;{{$eachPriceData->buy_now_price}}
                                                    </p> 
                                                </td>
                                            @endforeach
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--===================================================-->
                    <!-- End Striped Table -->
                    
                </div>
                <!--===================================================-->
                <!--End page content-->


            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
@endsection

@section('scripts')

    <script type="text/javascript">
        
        $(document).ready(function() {

            $('body,html').on('change','table.bird-place-list tbody tr td.open-td input',function(event) {

                event.stopPropagation();
                var min_bid = $(this).parents().parents().find('p:first-child input').val();
                var buy_now = $(this).parents().parents().find('p:last-child input').val();
                $(this).parents().parents().data('min_bid',min_bid);
                $(this).parents().parents().data('buy_now',buy_now);
                var data_val = $(this).parents().parents().data();
                var state_id = $(this).parents().parents().data('state_id');

                $.ajax({

                    type:"POST",
                    url: '{{route("ajax.save-price")}}',
                    data: {state_id:data_val.state_id , page_id:data_val.page_id,from_time:data_val.from_time,to_time:data_val.to_time, buy_now_price:buy_now, min_bid:min_bid,
                            _token:'{{csrf_token()}}'},
                    success: function(res) {

                        if(!res.success) {
                            
                            $('.status-update-msg-error').removeClass('hidden');
                            if(!$('.status-update-msg').hasClass('hidden')) {
                                $('.status-update-msg').addClass('hidden');
                            }
                            setTimeout(function(){
                                $('.status-update-msg-error').addClass('hidden');
                            },2000);
                        }else{

                            $('.status-update-msg').removeClass('hidden');
                            if(!$('.status-update-msg-error').hasClass('hidden')) {
                                $('.status-update-msg-error').addClass('hidden');
                            }
                            setTimeout(function(){
                                $('.status-update-msg').addClass('hidden');
                            },2000);
                        }
                        
                    }
                })

            });
            $('body,html').on('click','table.bird-place-list tbody tr td.edit-td',function(event) {

                $('body table.bird-place-list tbody tr td.open-td').each(function() {

                    var data_inner = $(this).data();
                    $(this).find('p:first-child').html('Min Bid: '+data_inner.min_bid);
                    $(this).find('p:last-child').html('Buy Price: '+data_inner.buy_now);
                    $(this).removeClass('open-td');
                    $(this).addClass('edit-td');
                    event.stopPropagation();
                })
                
                var data = $(this).data();
                $(this).addClass('open-td');
                $(this).removeClass('edit-td');
                $(this).find('p:first-child').html('Min Bid: <input style="width:100%;" type="text" value="'+data.min_bid+'">')
                $(this).find('p:last-child').html('Buy Price: <input style="width:100%;" type="text" value="'+data.buy_now+'">')
            })

            $('body').click(function(event) {
                // console.log( event.target.tagName.toUpperCase());
                if(event.target.tagName.toUpperCase() != 'INPUT') {

                    $('body table.bird-place-list tbody tr td').each(function(event) {
                        
                        var getclass = $(this).attr('class');
                        if(getclass == "open-td"){
                            var data_inner = $(this).data();
                        $(this).find('p:first-child').html('Min Bid: '+data_inner.min_bid);
                        $(this).find('p:last-child').html('Buy Price: '+data_inner.buy_now);
                        $(this).removeClass('open-td');
                        $(this).addClass('edit-td');
                        }
                    });
                }
                
            });

            $('body,html').on('change','.go-to-page',function() {
                location.href = '{{url("admin/price/management")}}/'+$(this).val();
            })
        })

    </script>
@endsection