<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="ico/favicon.png">

<title>JIVIN</title>

<!-- Bootstrap core CSS -->
<link href="{{ asset('sitehtml/css/bootstrap.css') }}" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<!-- Custom styles for this template -->
<link href="{{ asset('sitehtml/css/carousel.css') }}" rel="stylesheet">
<link href="{{ asset('sitehtml/style.css') }}" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,800' rel='stylesheet' type='text/css'>
</head>
<!-- NAVBAR
================================================== -->
<body>

<div class="navigation">

<div class="container">

<div class="navbar navbar-inverse navbar-static-top" role="navigation">
<div class="container">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" style="display:none;">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="#"><img src="{{ asset('sitehtml/img/j3profile.png') }}" width="50" /></a>
</div>
<div class="navbar-collapse collapse hidden">
<ul class="nav navbar-nav">
<li><a href="#">Home</a></li>
<li><a href="#about">About</a></li>
<li><a href="#contact">Contact</a></li>
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
<ul class="dropdown-menu">
<li><a href="#">Action</a></li>
<li><a href="#">Another action</a></li>
<li><a href="#">Something else here</a></li>
<li class="divider"></li>
<li class="dropdown-header">Nav header</li>
<li><a href="#">Separated link</a></li>
<li><a href="#">One more separated link</a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>

</div>
</div>




<div class="homemainbody">
<div class="container">
<div class="row">

<div class="col-lg-8">
<h1>Welcome to JIVIN</h1>
<p>
Let The Competition Begin!
</p>
</div>


<div class="col-lg-4">
<div class="loginbox">

<div class="row">
<div class="col-lg-12">
<input type="text" placeholder="Enter your username" />
</div>

<div class="col-lg-8">
<input type="text" placeholder="Password" />
<a href="#" class="forgot">Forgot your password?</a>
</div>

<div class="col-lg-4">
<input type="submit" value="Log in" />
</div>

</div>

</div>




<div class="loginbox">

<h2><img src="{{ asset('sitehtml/img/logo.png') }}" /> New Here? Sign up</h2>



<!-- <form action="/"> -->
<div class="row">
<div class="col-lg-12">
<input type="text" placeholder="Full Name" />
</div>

<div class="col-lg-12">
<input type="text" placeholder="Email" />
</div>

<div class="col-lg-4">
<input type="submit" value="Register" />
</div>

<div class="col-lg-8">
&nbsp;
</div>

<div class="col-lg-12 socialregister">
<br>
<strong>OR</strong>
<br>
<a href="#"><img src="{{ asset('sitehtml/img/rfb.jpg') }}"></a>
<a href="#"><img src="{{ asset('sitehtml/img/rtw.jpg') }}"></a>
<a href="#"><img src="{{ asset('sitehtml/img/rin.jpg') }}"></a>
<a href="#"><img src="{{ asset('sitehtml/img/rgp.jpg') }}"></a>
</div>

</div>
<!-- </form> -->







</div>


</div>



</div>
</div>




<div class="container footer">
<div class="row">
<div class="col-lg-12">
<a href="#">Quick Link</a>
<a href="#">Quick Link</a>
<a href="#">Quick Link</a>
<a href="#">Quick Link</a>
<a href="#">Quick Link</a>
<a href="#">Quick Link</a>
<a href="#">Quick Link</a>


</div>
</div>
</div>





</div>








<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="{{ asset('sitehtml/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('sitehtml/js/holder.js') }}"></script>
<script src="{{ asset('sitehtml/js/jquery.backstretch.js') }}"></script>
<script>
$.backstretch([
"{{ asset('sitehtml/img/1.jpg') }}",
"{{ asset('sitehtml/img/2.jpg') }}",
"{{ asset('sitehtml/img/3.jpg') }}",
"{{ asset('sitehtml/img/4.jpg') }}",
"{{ asset('sitehtml/img/5.jpg') }}",
"{{ asset('sitehtml/img/6.jpg') }}",
"{{ asset('sitehtml/img/7.jpg') }}"
], {
fade: 750,      //Speed of Fade
duration: 4000  //Time of image display
});
</script>
<script>
$('input').on('focus', function () {
$(this).parent().parent().parent().addClass('opfull');
});

$('input').on('blur', function () {
$(this).parent().parent().parent().removeClass('opfull');
});

</script>
</body>
</html>
