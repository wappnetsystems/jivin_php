<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    protected $table = 'plans';
    protected $hidden = [
        'updated_at', 'created_at'
    ];
    protected $appends = ['subscription_count'];
    public function plansubs()
    {
        return $this->belongsTo('App\Subscription','package_id','id');
    }

    public function getSubscriptionCountAttribute()
    {
        return Subscription::where('package_id','=',$this->id)->count();
    }
}
