<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportRequest extends Model
{
    protected $table = 'support_request';
    protected $hidden = [
       'time'
    ];  
    protected $fillable=['ticket_no','user_id','subject','user_asked','post_type','is_attachments','is_main'];
    protected $appends = ['user_full_name','user_img_url'];
    protected $primaryKey = 'id';


   public function attachments()
   {
         return $this->hasMany('App\SupportRequestAttachment', 'request_id', 'id');
        //return $this->hasMany('App\SupportRequestAttachment','id','request_id');
    
   }

    public static function getCommets($ticket_no){
        return $comments = SupportRequest::with('attachments')->where('is_main','=',0)
        ->where('ticket_no','=',$ticket_no)->get()->toArray();
    }   
    public function getUserFullNameAttribute(){
      $name = User::where('id','=',$this->user_id)->first();
      if ($name) {
         return $name->name;
      }else{
        return "";
      }
      
    }
    public function getUserImgUrlAttribute(){
      $imgUrl = User::where('id','=',$this->user_id)->first()->user_image_url;
      return $imgUrl;
    }
    public function getTimeAttribute()
    {  
        return date('d F Y', strtotime($this->created_at)).' at '.date('h:i A', strtotime($this->created_at));
    } 

    public function supportrequestattachement()
    {
return $this->hasMany('App\SupportRequestAttachment');
    } 
    public function getSupportImageUrlAttribute()
    {
        // $attachedUrl[]=SupportRequestAttachment::where('request_id','=',$this->id)->file_name;
        // if( $attachedUrl)
        // {
        //   return env('IMG_URL').'uploads/'. $this->file_name;
           
        // }else{
        //     return "";
        // }
    }       
}
