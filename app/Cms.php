<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    protected $table = 'cms_pages';
    protected $hidden = [
        'updated_at', 'created_at'
    ];
}
