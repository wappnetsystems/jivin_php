<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdCreative extends Model
{
	protected $table = 'ad_creative';
	
	protected $appends = ['image_url'];

	public function getImageUrlAttribute()
    {  
        return env('IMG_URL').'adcreative/'. $this->content;
    }

	public function getAdRegions()
	{
	    return $this->belongsTo('App\AdRegions','ad_region_id','id'); //hasMany belongsTo
	}
	
	/*public function getAdpages()
	{
	    return $this->belongsTo('App\AdPages','ad_page_id','id'); //hasMany belongsTo
	}*/

	public function getAdBid()
	{
	    return $this->hasMany('App\AdBid','advertisement_id','id'); //hasMany belongsTo
	}
}
