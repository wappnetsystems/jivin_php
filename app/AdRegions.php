<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdRegions extends Model
{
	protected $table = 'ad_regions'; 
	public function getadpages()
	{
	    return $this->hasMany('App\AdPages','ad_region_id','id'); //hasMany belongsTo
	}
	protected $appends = ['image_url'];

	public function getImageUrlAttribute()
    {  
        return env('IMG_URL').'ad_region/'. $this->image;
    }

    public function getalladpages()
	{
	    return $this->hasMany('App\AdPages','ad_region_id','id')->where('status',1); //hasMany belongsTo
	}
}
