<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscription';
    protected $hidden = [
        'updated_at', 'created_at'
    ];

   public function subsplan()
   {
         return $this->hasMany('App\Plans','package_id','id');
   } 

   public function getsubsplan()
   {
         return $this->hasMany('App\Plans','id','package_id');
   } 
}
