<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportRequestReaplyLog extends Model
{
    protected $table = 'support_request_reaply_log';
    protected $hidden = [
        'updated_at', 'created_at'
    ];  
}
