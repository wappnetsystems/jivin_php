<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertise extends Model
{
    //
        protected $table = 'advertise';
         protected $hidden = [
        'updated_at', 'created_at'
    ];

}
