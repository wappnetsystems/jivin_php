<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoostPlans extends Model
{
    protected $table = 'boost_plan';
    protected $hidden = [
        'updated_at', 'created_at','deleted_at'
    ];

    // protected $appends = ['subscription_count'];
    // public function plansubs()
    // {
    //     return $this->belongsTo('App\Subscription','package_id','id');
    // }

    // public function getSubscriptionCountAttribute()
    // {
    //     return Subscription::where('package_id','=',$this->id)->count();
    // }
}
