<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::group(['middleware' => ['web']], function(){
// 	Route::auth();
// 	Route::get('/home', 'HomeController@index');
// });
//route for crypto currency prices
Route::get('update_crypto_price', 'CryptopriceController@update_crypto_price')->name('update_crypto_price');

Route::group(['prefix' => 'social/', 'middleware' => ['web']], function() {
    //Twitter login
    Route::get('twitter_login', 'ApiUserController@redirectToProvider');
    Route::get('twitter_login/callback', 'ApiUserController@handleProviderCallback');
});

Route::get("linkedin_login/{flag?}", 'SocialloginController@linkedin_login')->name("linkedin_login");
Route::any("linkedin_auth/", 'SocialloginController@linkedin_auth')->name("linkedin_auth");
Route::any("login_linkedin_auth/", 'SocialloginController@login_linkedin_auth')->name("login_linkedin_auth");

Route::get("instagram_login/{flag?}", 'SocialloginController@instagram_login')->name("instagram_login");
Route::any("instagram_auth/", 'SocialloginController@instagram_auth')->name("instagram_auth");
Route::any("login_instagram_auth/", 'SocialloginController@login_instagram_auth')->name("login_instagram_auth");
Route::get('twitter_auth/','SocialloginController@twitter_auth')->name('twitter_auth');
Route::get('login_twitter_auth/','SocialloginController@login_twitter_auth')->name('login_twitter_auth');

Route::group(['middleware' => ['web']], function () {
    //Login Routes...
    Route::get('/', 'AdminAuth\AuthController@showLoginForm');
    Route::get('admin', 'AdminAuth\AuthController@showLoginForm');
    Route::get('admin/login', 'AdminAuth\AuthController@showLoginForm');
    Route::post('admin/login', 'AdminAuth\AuthController@login');
    Route::get('admin/logout', 'AdminAuth\AuthController@logout');

    Route::get('/check-leaderboard', function() {

        \App\LeaderboardLog::addLog(7);
    });
    Route::post('admin/get_wallet_list', 'AdminUserController@get_wallet_list')->name('get_wallet_list');
    //Dash Board
    Route::get('admin/dashboard', 'AdminController@index');
    //Ajax Call
    Route::get('interest-category-ajax-list', 'AdminInterestManagementController@getList');
    Route::post('interest-category-ajax-status-update', 'AdminInterestManagementController@updateStatus');
    Route::post('interest-category-ajax-delete', 'AdminInterestManagementController@delete');

    Route::get('interest-category-sub-ajax-list', 'AdminSubInterestManagementController@getList');
    Route::post('interest-category-sub-ajax-status-update', 'AdminSubInterestManagementController@updateStatus');
    Route::post('interest-category-sub-ajax-delete', 'AdminSubInterestManagementController@delete');
    Route::post('interest-category-sub-ajax-order', 'AdminSubInterestManagementController@order');

    Route::get('user_ajax_list', 'AdminUserController@getList');
    Route::post('user-ajax-status-update', 'AdminUserController@updateStatus');

    Route::get('membership_plan_ajax_list', 'AdminPlanController@getList');
    Route::post('plan-ajax-status-update', 'AdminPlanController@updateStatus');

    Route::post('boost-plan-ajax-status-update', 'AdminBoostController@updateStatus');

    Route::get('country_list', 'AdminCountryController@getList');
    Route::post('country-ajax-status-update', 'AdminCountryController@updateStatus');

    Route::get('region_list', 'AdminRegionController@getList');
    Route::post('region-ajax-status-update', 'AdminRegionController@updateStatus');

    Route::get('devision_list', 'AdminDevisionController@getList');
    Route::post('devision-ajax-status-update', 'AdminDevisionController@updateStatus');

    Route::get('state_list', 'AdminStateController@getList');
    Route::post('state-ajax-status-update', 'AdminStateController@updateStatus');
    Route::post('state-ajax-state-compete-update', 'AdminStateController@updateStateComplete');

    Route::get('cms_list', 'AdminCMSController@getList');
    Route::post('cms-ajax-status-update', 'AdminCMSController@updateStatus');

    Route::get('advertise_list', 'AdminAdvertiseController@getList');
    Route::post('advertise-ajax-status-update', 'AdminAdvertiseController@updateStatus');
    Route::post('advertiseagain-ajax-status-update', 'AdminAdvertiseController@updateStatusagain');
});

//Mobile Api

Route::group(['prefix' => 'mobileapi','namespace'=>'Mobileapi'], function () {

     Route::post('mobile_authenticate','UserController@mobile_authenticate')->name('mobile_authenticate');
    Route::post('mobile_logout', 'UserController@mobile_logout');
    Route::post('mobile_registerUser','UserController@mobile_registerUser')->name('mobile_registerUser');
    Route::get('mobile_interest_category','UserController@mobile_interest_category')->name('mobile_interest_category');
    Route::post('mobile_interest_category_by_id','UserController@mobile_interest_category_by_id')->name('mobile_interest_category_by_id');
    
    Route::get('mobile_country_list/', 'UserController@mobile_country_list');
    Route::get('mobile_language_list/', 'UserController@mobile_language_list');
    Route::get('mobile_all_country_list/', 'UserController@mobile_all_country_list');
    
    Route::post('mobile_country_regions/', 'UserController@mobile_country_regions');
    Route::post('mobile_country_divisions/', 'UserController@mobile_country_divisions');
    Route::post('mobile_country_states/', 'UserController@mobile_country_states');

        Route::post('get_country_regions/', 'UserController@get_country_regions');
        Route::post('get_country_divisions/', 'UserController@get_country_divisions');
        Route::post('get_country_states/', 'UserController@get_country_states');

    Route::post('mobile_forget_password/', 'UserController@mobile_forget_password'); // Get link By Email
    Route::post('mobile_authenticate_user_key/', 'UserController@mobile_authenticate_user_key');  // Authenticate User
    Route::post('mobile_change_password/', 'UserController@mobile_change_password'); // Change Password
    Route::post('mobile_followed_user_list/', 'UserController@mobile_followed_user_list'); //mobile_followed_user_list
    Route::post('mobile_following_user_list/', 'UserController@mobile_following_user_list');//mobile_following_user_list
    Route::post('mobile_following_user_details/', 'UserController@mobile_following_user_details');
    Route::post('mobile_add_user_profile_image/', 'UserController@mobile_add_user_profile_image');
    Route::get('mobile_map_location/', 'UserController@mobile_map_location');
    Route::post('mobile_user_profile_details/', 'UserController@mobile_user_profile_details');
    Route::post('mobile_notification_details/', 'MobileNotificationController@mobile_notification_details');
    Route::post('mobile_notification/', 'MobileNotificationController@mobile_notification');
    Route::post('mobile_numberone/', 'ApiLeaderboardLogController@mobile_numberone');
    Route::post('mobile_get_address/', 'UserController@mobile_get_address');
    Route::post('mobile_get_user_image/', 'UserController@mobile_get_user_image');
    Route::post('mobile_update_user_account_settings/', 'UserController@mobile_update_user_account_settings');
    Route::post('update_user_design_settings/', 'UserController@update_user_design_settings');
    Route::post('mobile_update_address/', 'UserController@mobile_update_address')->name('mobile_update_address');
    Route::post('mobile_verify_shuftipro/', 'UserController@mobile_verify_shuftipro')->name('mobile_verify_shuftipro');
    Route::post('mobile_saveboostsetting/', 'UserController@mobile_saveboostsetting');
    Route::get('mobile_get_all_plan/', 'ApiPlanController@mobile_get_all_plan');
    Route::post('create_customer/', 'MobileApiStripePayment@create_customer')->name('create_customer');
    Route::post('mobile_get_card_list/', 'MobileApiStripePayment@mobile_get_card_list')->name('mobile_get_card_list');
    Route::post('mobile_profile_posts/', 'UserController@mobile_profile_posts');
    Route::post('mobile_insert_card/', 'MobileApiStripePayment@mobile_insert_card')->name('mobile_insert_card');
    
    Route::post('get_currency_list/', 'UserController@get_currency_list')->name('get_currency_list'); //nishit
    Route::post('my_wallet_setting/', 'UserController@my_wallet_setting')->name('my_wallet_setting');
    Route::post('mobile_get_address_list/', 'UserController@mobile_get_address_list')->name('mobile_get_address_list');
    
    Route::post('mobile_current_boosted_location/', 'ApiPlanController@mobile_current_boosted_location')->name('mobile_current_boosted_location');
    Route::get('mobile_all_boost_plan/', 'ApiPlanController@mobile_all_boost_plan');
    
    Route::post('mobile_support_requests/', 'ApiPlanController@mobile_support_requests');
    
    Route::post('mobile_add_support/', 'ApiPlanController@mobile_add_support'); //02/06/2020 nish

    Route::post('mobile_check_user_state', 'ApiPlanController@mobile_check_user_state');
    Route::get('mobileMapLocationLeaderboard/', 'ApiPlanController@mobileMapLocationLeaderboard');
    Route::post('mobileLeaderboardList/', 'ApiPlanController@mobileLeaderboardList');
    Route::post('mobile_jackpot_amount/', 'ApiPlanController@mobile_jackpot_amount')->name('mobile_jackpot_amount');
    Route::post('check_email_exits/', 'UserController@check_email_exits')->name('check_email_exits');
    Route::post('get_user_follower_list/', 'UserController@get_user_follower_list')->name('get_user_follower_list');
    Route::post('delete_user/','UserController@delete_user')->name('delete_user');
    
    Route::post('get_state_by_country/','UserController@get_state_by_country')->name('get_state_by_country');
    Route::post('get_division_by_country/','UserController@get_division_by_country')->name('get_division_by_country');

    Route::post('get_compete_states/','UserController@get_compete_states')->name('get_compete_states');
    //secure routes
    Route::group(['middleware' => ['api_auth']], function() {
        Route::post('create_video_post/','PostController@create_video_post')->name('create_video_post');
        Route::post('get_wall_post_list/','PostController@get_wall_post_list')->name('get_wall_post_list');
        Route::post('get_post_details_by_id/','PostController@get_post_details_by_id')->name('get_post_details_by_id');
        Route::post('create_audio_post/','PostController@create_audio_post')->name('create_audio_post');
        Route::post('create_normal_post/','PostController@create_normal_post')->name('create_normal_post');
        Route::post('report_post/','PostController@report_post')->name('report_post');
        Route::post('vote_on_post/','PostController@vote_on_post')->name('vote_on_post');
        Route::post('jivi_it/','PostController@jivi_it')->name('jivi_it');
        Route::post('boost_post/','PostController@boost_post')->name('boost_post');
        Route::post('add_post_comment/','PostController@add_post_comment')->name('add_post_comment');
        Route::post('get_ranking_on_wall/','PostController@get_ranking_on_wall')->name('get_ranking_on_wall');
        Route::post('edit_post_comment/','PostController@edit_post_comment')->name('edit_post_comment');
        Route::post('comment_report/','PostController@comment_report')->name('comment_report');
        Route::post('delete_comment/','PostController@delete_comment')->name('delete_comment');
        
        //NotificationController
        Route::post('get_notification_list/','NotificationController@get_notification_list')->name('get_notification_list');
        Route::post('mark_notification_read/','NotificationController@mark_notification_read')->name('mark_notification_read');
        
        //ApiPlanController
        Route::post('mobile_all_boost_count/', 'ApiPlanController@mobile_all_boost_count')->name('mobile_all_boost_count');
        Route::post('boost_purchase_confirm/','ApiPlanController@boost_purchase_confirm')->name('boost_purchase_confirm');
        Route::post('get_user_boost_counts/','ApiPlanController@get_user_boost_counts')->name('get_user_boost_counts');
        
        //UserController
        Route::post('mobile_follow_user/','UserController@mobile_follow_user')->name('mobile_follow_user');
        Route::post('mobile_unfollow_user/','UserController@mobile_unfollow_user')->name('mobile_unfollow_user');
        Route::post('deactive_user_account/','UserController@deactive_user_account')->name('deactive_user_account');
        Route::post('get_user_boost_location_details/','UserController@get_user_boost_location_details')->name('get_user_boost_location_details');
        
        //GallaryController
        Route::post('get_video_files_by_user/','GallaryController@get_video_files_by_user')->name('get_video_files_by_user');
        Route::post('get_audio_files_by_user/','GallaryController@get_audio_files_by_user')->name('get_audio_files_by_user');
        Route::post('get_images_list/','GallaryController@get_images_list')->name('get_images_list');
        Route::post('gallery_upload_image/','GallaryController@gallery_upload_image')->name('gallery_upload_image');
        
        //WallOfFameController
        Route::post('get_competition_winners/','WallOfFameController@get_competition_winners')->name('get_competition_winners');
        Route::post('mobile_competition_winners/','WallOfFameController@mobile_competition_winners')->name('mobile_competition_winners');
        
        //ApiLeaderboardLogController
        Route::post('get_leaderboard_list/','ApiLeaderboardLogController@get_leaderboard_list')->name('get_leaderboard_list');

        Route::post('mobile_leaderboard_list/','ApiLeaderboardLogController@mobile_leaderboard_list')->name('mobile_leaderboard_list');

        Route::post('month_interval_list/','ApiLeaderboardLogController@month_interval_list')->name('month_interval_list');
        
        //EventController methods
        Route::post('mobile_add_event/', 'EventController@mobile_add_event');
        Route::post('mobile_edit_event/', 'EventController@mobile_edit_event');
        Route::post('mobile_delete_event/', 'EventController@mobile_delete_event');
        Route::post('get_my_events/', 'EventController@get_my_events');
        Route::post('get_all_events_list/', 'EventController@get_all_events_list');
        Route::post('mobile_event_apply/', 'EventController@mobile_event_apply');
        Route::post('get_event_by_id/', 'EventController@get_event_by_id');
   
        //AdvertismentController methods 02/06/2020
        Route::post('mobile_create_advertisment/', 'AdvertismentController@mobile_create_advertisment');
        Route::post('mobile_update_advertisment/', 'AdvertismentController@mobile_update_advertisment');
        Route::post('mobile_advertisment_types/', 'AdvertismentController@mobile_advertisment_types');
        Route::post('mobile_my_advertisment/', 'AdvertismentController@mobile_my_advertisment');
        Route::post('mobile_get_advertisment_by_type/', 'AdvertismentController@mobile_get_advertisment_by_type');
        
           //ChatControlller methods 02/06/2020
           Route::post('mobile_send_message/', 'ChatController@mobile_send_message');
           Route::post('mobile_get_all_message/', 'ChatController@mobile_get_all_message');
        
    });
    
});

Route::group(['prefix' => 'admin/'], function() {

    //Price master script
    Route::get('price-master-script', 'AdminController@priceMasterScript');
    Route::get('price/management/{page_id?}', 'AdminController@priceManagement');
    Route::post('ajax/save-price', 'AdminController@ajaxSavePrice')->name('ajax.save-price');

    //Interest Category Management
    Route::get('interest-category/list', 'AdminInterestManagementController@listInterestCategory');
    Route::post('interest-category/add', 'AdminInterestManagementController@postAddNewCategory');

    //Interest Sub Category Management
    Route::get('interest-sub-category/list', 'AdminSubInterestManagementController@listInterestCategory');
    Route::post('interest-sub-category/add', 'AdminSubInterestManagementController@postAddNewCategory');

    //Advertise Management
    Route::get('advertise/list', 'AdminAdvertiseController@listAdvertise');
    Route::post('advertise/add', 'AdminAdvertiseController@postAddNewAdvertise');
    Route::get('advertise-settings/list', 'AdminAdvertiseController@settingsAdvertise');

    Route::get('post/details', 'AdminPostController@details');
    Route::post('post/delete', 'AdminPostController@delete');
    //Country Management
    Route::get('country/list', 'AdminCountryController@listCountry');
    Route::post('country/add', 'AdminCountryController@postAddNewCountry');

    //Region Management
    Route::get('region/list', 'AdminRegionController@listRegion');
    Route::post('region/add', 'AdminRegionController@postAddNewRegion');

    //Devision Management
    Route::get('devision/list', 'AdminDevisionController@listDevision');
    Route::post('devision/add', 'AdminDevisionController@postAddNewDevision');

    //State Management
    Route::get('state/list', 'AdminStateController@listState');
    Route::post('state/add', 'AdminStateController@postAddNewState');


    //Manage Plans   
    Route::get('memership_plans/list', 'AdminPlanController@listPlans');
    Route::post('memership_plan/add', 'AdminPlanController@postAddNewPlan');

    //Manage Plans   
    Route::get('boost_plans/list', 'AdminBoostController@listPlans');
    Route::post('boost_plan/add', 'AdminBoostController@postAddNewPlan');

    //Manage User
    Route::get('user/list', 'AdminUserController@listUsers');

    //CMS Management
    Route::get('cms/list', 'AdminCMSController@listCms');
    Route::post('cms/add', 'AdminCMSController@postAddNewCms');

    //Support Request   
    Route::get('support_request/list/inbox', 'AdminSupportRequestController@listSupportyRequest');


    Route::get('support_request/list/searchinbox', 'AdminSupportRequestController@searchinbox');
    Route::get('support_request/list/searchtrashbox', 'AdminSupportRequestController@searchtrashbox');

    Route::get('support_request/list/deleted', 'AdminSupportRequestController@listSupportRequest');

    Route::get('support_request/details/{ticket_no}/{user_id}', 'AdminSupportRequestController@DetailsSupportyRequest');
    Route::get('support_request/add', 'AdminSupportRequestController@AddSupportyRequest');

    Route::post('support_request/addsupport/inbox', 'AdminSupportRequestController@addsupport');
    Route::get('support_request/delete/{id}', 'AdminSupportRequestController@delete');
    // Route::get('support_request/list/search','AdminSupportRequestController@search');
    Route::post('support_request/updatesupport/reply/{ticket_no}', 'AdminSupportRequestController@addreply');


    Route::post('support_request/details/addsupport/inbox', 'AdminSupportRequestController@addsupport');

    //Route::get('post/details','AdminPostController@details');
    Route::get('adregion/list', 'AdminAdRegionController@listadregion');
    Route::post('adregion/add', 'AdminAdRegionController@postAdd');
    Route::get('adregion/delete/{id}', 'AdminAdRegionController@delete');


    Route::get('adpage/list', 'AdminAdPagesController@listadpage');
    Route::post('adpage/add', 'AdminAdPagesController@postAdd');

    Route::get('adpage/details/{id}', 'AdminAdPagesController@details');
    Route::post('ad_price_details/add', 'AdminAdPagesController@ad_price_details');
    //Route::get('adpage/add', 'AdminAdPagesController@ad_price_details');
    //Site Setting Management
    Route::get('sitesetting', 'AdminSiteSettingController@index');
    Route::post('sitesetting/update', 'AdminSiteSettingController@update');

    Route::match(['get', 'post'], 'sitesetting/change-bid-amount', 'AdminSiteSettingController@updateBidAmount');
    Route::get('sitesetting/get-page-by-region/{id}', 'AdminSiteSettingController@getPageByRegion');

    Route::get('revenue/index', 'AdminRevenueController@index');
    //Route::post('sitesetting/update','AdminSiteSettingController@update');

    Route::get('settings', 'AdminUserController@settings');
    Route::post('save_settings', 'AdminUserController@save_settings');

    //route for update manual jackpot amount
    Route::post('update_jackpot', 'AdminRevenueController@update_jackpot')->name('update_jackpot');
    Route::post('get_jackpot_price', 'AdminRevenueController@get_jackpot_price')->name('get_jackpot_price');

    Route::get('jivin_video', 'AdminJivinVideoController@index')->name('jivin_video');
    Route::get('edit_jivin_video/{id}', 'AdminJivinVideoController@edit_jivin_video')->name('edit_jivin_video');
    Route::post('update_jivin_video', 'AdminJivinVideoController@update_jivin_video')->name('update_jivin_video');
});


Route::get('insert_old_interest_data/', 'ApiController@insert_old_interest_data');
//All Api Part
Route::group(['prefix' => 'api/v1/', 'middleware' => ['web']], function() {
    //User Auth Part
    Route::get('sitesetiing/', 'ApiController@sitesetiing');
    Route::post('logout/', 'ApiController@logoutUser');
    Route::post('login/', 'ApiController@loginUser');
    Route::post('register/', 'ApiController@registerUser');
    Route::post('check_auth_key/', 'ApiController@keyIsValide');
    Route::post('check_user/', 'ApiController@checkUserState');
    Route::get('getallinterestcategory/', 'ApiController@getInterestCategory');
    Route::get('getinterestcategorybyid/{id}', 'ApiController@getInterestCategoryById');

    Route::post('deactiveaccount/', 'ApiController@deactiveaccount');
    Route::post('userdetails/', 'ApiController@userdetails');

    //Event Part
    Route::post('add-event/', 'ApiEventController@addEvent');
    Route::post('get-event-list/', 'ApiEventController@getEventList');
    Route::post('get_event_date_list/', 'ApiEventController@getEventDateList');
    Route::get('get_date/', 'ApiEventController@getDate');

    //Location Part
    Route::get('get-country-list/', 'ApiLocationController@getCountryList');
    Route::post('get-country-regions/', 'ApiLocationController@getRegions');
    Route::post('get-country-divisions/', 'ApiLocationController@getDivisions');
    Route::post('get-country-states/', 'ApiLocationController@getStates');
    Route::get('save-all-location-us/', 'ApiLocationController@saveUsCountry');
    Route::get('get-map-location/', 'ApiLocationController@getMapLocations');
    Route::get('getMapLocationLeaderboard/', 'ApiLocationController@getMapLocationLeaderboard');
    //User Wall Part
    Route::post('add-user-profile-image/', 'ApiUserController@userImageUpload');
    Route::post('update-user-design-settings/', 'ApiUserController@userDesignSettingsUpdate');
    Route::post('deactivate_user/', 'ApiUserController@dactivateUser');
    Route::post('update_user_settings/', 'ApiUserController@updtaeUserAccountSettings');
    Route::post('unfollow_user/', 'ApiUserController@unFollowUser');
    Route::post('follow_user/', 'ApiUserController@followUser');
    Route::post('get-followed-user-list/', 'ApiUserController@getFollwedUsersList');
    Route::post('get-following-user-list/', 'ApiUserController@getFollwingUsersList');
    Route::post('get-following-user-details/', 'ApiUserController@getFollwingUsersdetails');
    Route::post('get-user-image/', 'ApiUserController@getuserImage');
    Route::post('get-all-user-image/', 'ApiUserController@getalluserImage');
    Route::post('set-user-image/', 'ApiUserController@setuserImage');


    //Test  
    Route::post('send-test-mail/', 'ApiUserController@sendTestMail');

    //Forget Password
    Route::post('forget_password/', 'ApiUserController@forgetPassword'); // Get link By Email
    Route::post('authenticate_user_key/', 'ApiUserController@confirmForgetPasswordKey');  // Authenticate User
    Route::post('set_password/', 'ApiUserController@changePassword'); // Change Password
    //Post
    Route::post('add_post/', 'ApiPostController@add_post');
    Route::post('share_post/', 'ApiPostController@share_post');
    Route::post('like_post/', 'ApiPostController@like_post');
    Route::post('get_posts/', 'ApiPostController@getPosts');
    Route::post('get_posts_test/', 'ApiPostController@testPost'); /* For testing perpouse */
    Route::post('post_audio_video/', 'ApiPostController@postAudioVideo');
    Route::post('upload_temp_video_audio/','ApiPostController@upload_temp_video_audio');

    //Support Ticket
    Route::post('add_support_request/', 'ApiSupportRequestController@addSupportRequest');
    Route::post('get_support_requests/', 'ApiSupportRequestController@getRequests');
    Route::post('get_support_post', 'ApiSupportRequestController@getResponse');
    Route::post('add_comments', 'ApiSupportRequestController@addcomments');
    Route::post('get_comments', 'ApiSupportRequestController@getcomments');


    //Audio & Video List
    Route::post('total_media_count/', 'ApiUserController@getTotalMediaCount');
    Route::post('get_audio_list/', 'ApiUserController@getAudioList');
    Route::post('get_video_list/', 'ApiUserController@getVideoList');
    Route::get('usersearch/', 'ApiUserController@usersearch');
    Route::get('usersearchtest/', 'ApiUserController@usersearchtest');

    //Leaderboard & Wall of fame
    Route::post('get_leaderboard/', 'ApiLeaderboardLogController@getLeaderboardList');
    Route::post('get_wall_of_fame/', 'ApiLeaderboardLogController@getWallOfFameList');
    Route::get('get_wall_of_fame_year/', 'ApiLeaderboardLogController@getLeaderboardYearList');
    Route::post('get_wall_of_fame_month/', 'ApiLeaderboardLogController@getLeaderboardMonthList');
    Route::post('get_numberone/', 'ApiLeaderboardLogController@get_numberone');
    Route::post('getTopRankByCountry/','ApiLeaderboardLogController@getTopRankByCountry');

    //Route::post('add_log_test/', 'ApiLeaderboardLogController@addLog');
    //Notification
    Route::post('get_notification/', 'ApiNotificationLogController@getNotification');
    Route::post('get_notification_details/', 'ApiNotificationLogController@getNotificationDetails');
    Route::post('set_all_notification_read/', 'ApiNotificationLogController@setNotificationRead');

    //User Profile Section
    Route::post('get_profile_details/', 'ApiProfileController@getUserProfileDetails');
    Route::post('get_profile_posts/', 'ApiProfileController@getProfilePosts');
    Route::post('getProfileAudioPosts/', 'ApiProfileController@getProfileAudioPosts');
    Route::post('getProfileVideoPosts/', 'ApiProfileController@getProfileVideoPosts');
    Route::post('changepermission/', 'ApiProfileController@changepermission');



    //request attachment 
    // Route::post('get_attachment/', 'ApiSupportRequestAttachmentController@getsupportrequestattachment');
    //Report POst
    Route::post('report_post/', 'ApiReportPostController@reportPost');

    Route::get('get_all_plan/', 'ApiPlanController@getplan');
    Route::post('plan_subscription/', 'ApiPlanController@subscription');
    Route::post('bidwinpayment/', 'ApiPlanController@bidwinpayment');
    Route::get('getregions/', 'ApiAdvertiseController@getregions');
    Route::post('useraddlist/', 'ApiAdvertiseController@useraddlist');
    Route::post('getpages/', 'ApiAdvertiseController@getpages');
    Route::post('postadvertisement/', 'ApiAdvertiseController@postadvertisement');

    Route::post('postcreative/', 'ApiAdvertiseController@postcreative');
    Route::post('pastcreative/', 'ApiAdvertiseController@pastcreative');

    Route::post('sendmsg/', 'ApiPlanController@sendmsg');
    //*********************start bidding api***************//
    Route::post('bidlisting/', 'ApiAdvertiseController@bidlisting');



    Route::post('get-ad-country-regions/', 'ApiAdvertiseController@getRegions');
    Route::post('get-ad-country-divisions/', 'ApiAdvertiseController@getDivisions');
    Route::post('get-ad-country-states/', 'ApiAdvertiseController@getStates');

    Route::post('bidpost/', 'ApiAdvertiseController@bidpost');
    Route::post('bidpostfinal/', 'ApiAdvertiseController@bidpostfinal');
    Route::post('bidfinalpost/', 'ApiAdvertiseController@bidfinalpost');
    Route::post('bidpostlist/', 'ApiAdvertiseController@bidpostlist');
    Route::post('bidcalculation/', 'ApiAdvertiseController@bidcalculation');
    Route::post('mybid/', 'ApiAdvertiseController@mybid');
    Route::post('mybidmodal/', 'ApiAdvertiseController@mybidmodal');
    Route::post('mybidupdate/', 'ApiAdvertiseController@mybidupdate');
    Route::post('buynowbid/', 'ApiAdvertiseController@buynowbid');

    Route::post('showvideoadd/', 'ApiAdvertiseController@showvideoadd');
    Route::post('showimageadd/', 'ApiAdvertiseController@showimageadd');
    Route::post('getmylastbid/', 'ApiAdvertiseController@getmylastbid');
    Route::get('winbidcron/', 'ApiAdvertiseController@winbidcron');
    Route::post('saveboostsetting/', 'ApiAdvertiseController@saveboostsetting');
    //Route::post('get_all_plan/', 'ApiPlanController@subscription');    
    Route::post('contactusmail/', 'ApiController@contactusmail');
    Route::post('reportchecking/', 'ApiController@reportchecking');
    Route::get('getallboostplan/', 'ApiController@getallboostplan');
    Route::post('getallboostcount/', 'ApiController@getallboostcount');
    Route::post('plan_subscribeboost/', 'ApiPlanController@subscribeboost');
    Route::post('postyourboost/', 'ApiPlanController@postyourboost');
    Route::post('postyourboostcheck/', 'ApiPlanController@postyourboostcheck');
    Route::post('getcurrentboostedlocation/', 'ApiPlanController@getcurrentboostedlocation');
    Route::post('deleteyourpost/', 'ApiPostController@deleteyourpost');
    Route::post('changepostpermission/', 'ApiPostController@changepostpermission');

    //api route for get the jackpot price
    Route::post('get_jackpot_amount/', 'ApiJackpotController@get_jackpot_amount')->name('get_jackpot_amount');

    //stripe API integration for Subscription, Boost plan, advertisement plans
    Route::post('create_customer/', 'ApiStripePayment@create_customer')->name('create_customer');
    Route::post('insert_card/', 'ApiStripePayment@insert_card')->name('insert_card');
    Route::post('get_card_list/', 'ApiStripePayment@get_card_list')->name('get_card_list');
    Route::post('card_details/', 'ApiStripePayment@card_details')->name('card_details');
    Route::post('delete_card/', 'ApiStripePayment@delete_card')->name('delete_card');
    Route::post('create_subscription/', 'ApiStripePayment@create_subscription')->name('create_subscription');
    Route::post('create_default_card/', 'ApiStripePayment@create_default_card')->name('create_default_card');
    Route::post('boost_plan/', 'ApiStripePayment@boost_plan')->name('boost_plan');

    //api route for user crypto wallet
    Route::post('update_address/', 'ApiUsercrypto@update_address')->name('update_address');
    Route::post('add_address/', 'ApiUsercrypto@add_address')->name('add_address');
    Route::post('get_address_list/', 'ApiUsercrypto@get_address_list')->name('get_address_list');
    Route::post('delete_address/', 'ApiUsercrypto@delete_address')->name('delete_address');
    Route::post('get_remain_cryptlist/', 'ApiUsercrypto@get_remain_cryptlist')->name('get_remain_cryptlist');

    Route::get('get_state_list/', 'ApiLocationController@get_state_list')->name('get_state_list');

    //shufti pro api
    Route::post('get_address/', 'ApiUserController@get_address')->name('get_address');
    Route::post('update_address/', 'ApiUserController@update_address')->name('update_address');
    Route::get('get_shufti_pro_response/', 'ApiUserController@get_shufti_pro_response')->name('get_shufti_pro_response');
    Route::post('verify_shuftipro/', 'ApiUserController@verify_shuftipro')->name('verify_shuftipro');

    //get introduction video
    Route::get('get_jivin_video/', 'ApiController@get_jivin_video')->name('get_jivin_video');

    //linked in api
    Route::post('get_access_token_linkedin/', 'SocialloginController@get_access_token_linkedin')->name('get_access_token_linkedin');
    Route::post('get_user_profile_linkedin/', 'SocialloginController@get_user_profile_linkedin')->name('get_user_profile_linkedin');
    Route::post('get_user_email_linkedin/', 'SocialloginController@get_user_email_linkedin')->name('get_user_email_linkedin');

    //instagram
    Route::post('get_access_token_instagram/', 'SocialloginController@get_access_token_instagram')->name('get_access_token_instagram');
    Route::post('get_user_profile_instagram/', 'SocialloginController@get_user_profile_instagram')->name('get_user_profile_instagram');
    
    //twitter
    Route::get('get_access_token_twitter/{flag?}','SocialloginController@get_access_token_twitter')->name('get_access_token_twitter');
    Route::post('get_user_profile_twitter/','SocialloginController@get_user_profile_twitter')->name('get_user_profile_twitter');
    
    //snapchat
    Route::get('signup_snapchat','SocialloginController@signup_snapchat')->name('signup_snapchat');
    Route::post('snapchat_auth/','SocialloginController@snapchat_auth')->name('snapchat_auth');
    
    Route::post('get_total_video_votes/','ApiUserController@get_total_video_votes')->name('get_total_video_votes');
    Route::post('get_total_audio_votes/','ApiUserController@get_total_audio_votes')->name('get_total_audio_votes');
    
    Route::post('get_stats/','ApiController@get_stats')->name('get_stats');
    
    Route::post('get_video_post_by_user_id/','ApiPostController@get_video_post_by_user_id');
    Route::post('get_audio_post_by_user_id/','ApiPostController@get_audio_post_by_user_id');
    
});
