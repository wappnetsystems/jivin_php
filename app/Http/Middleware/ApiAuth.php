<?php

namespace App\Http\Middleware;

use Closure;
use App\Login_log;
use App\Api;
class ApiAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        
        if(!$request->header('Authorization')){
            $response=Api::make_response(0, [], "Access Denied. Invalid Authorization Header.");
            
            return response()->json($response);
        }
       
        $token_check = Login_log::where(['auth_token' => $request->header('Authorization'), 'user_id' => $request->input('user_id')])->get(['id']);
        if ($token_check->count() == 0) {
            
            /*if (in_array($request->route()->getName(), $allowed_route)) {
                return $next($request);
            }*/
           $response= Api::make_response(0, [], "Access Denied. Invalid Authorization Header.");
            return response()->json($response);
        }
        
        return $next($request);
    }

}
