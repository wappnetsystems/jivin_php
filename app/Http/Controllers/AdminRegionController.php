<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use App\Country;
use App\Region;
use App\Devision;
use App\State;

class AdminRegionController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function listRegion() {
        $countries = Country::where('is_active', '=', 1)->get()->toArray();
        return view('admin.location.region')->with('countries', $countries);
    }

    public function getList() {
        $output['aaData'] = [];
        $aColumns = array(
            'id'
            , 'country_id'
            , 'country_name'
            , 'name'
            , 'is_active'
            , 'id'
        );
        $regions = Region::get()->toArray();
        // /dd($regions);
        foreach ($regions as $key => $region) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($region[$aColumns[$i]] === NULL) {
                    $region[$aColumns[$i]] = "";
                }
                $row[] = $region[$aColumns[$i]];
            }
            $output['aaData'][] = $row;
        }
        $output = array(
            "draw" => intval(1),
            "data" => $output['aaData']
        );
        return $output;
    }

    public function postAddNewRegion(Request $request) {
        // echo $request->name;exit();
        if ($request->id) {
            $region = Region::where('id', '=', $request->id)->first();
            if (Region::where('name', '=', $request->name)->where('id', '!=', $request->id)->first()) {
                return redirect('admin/region/list')->with('alert-danger', 'Region Already Exists');
            }
            $region->country_id = $request->country_id;
            $region->name = str_replace(' ', '_', $request->name);
            $region->slug = str_replace(' ', '_', $request->name);
            $region->is_active = $request->is_active;
            if ($region->save()) {
                return redirect('admin/region/list')->with('alert-success', 'Region Updated!');
            } else {
                return redirect('admin/region/list')->with('alert-danger', 'Region Updation faild');
            }
        } else {
            $region = new Region;
            if ($region::where('name', '=', $request->name)->first()) {
                return redirect('admin/region/list')->with('alert-danger', 'Region Already Exists');
            } else {
                // echo $request->name;exit();
                $region->country_id = $request->country_id;
                $region->name = str_replace(' ', '_', $request->name);
                ;
                $region->slug = str_replace(' ', '_', $request->name);
                ;
                $region->is_active = $request->is_active;
                if ($region->save()) {
                    return redirect('admin/region/list')->with('alert-success', 'New Region Created!');
                } else {
                    return redirect('admin/region/list')->with('alert-danger', 'Region Creation faild');
                }
            }
        }
    }

    public function updateStatus(Request $request) {
        $region = Region::where('id', '=', $request->id)->first();
        $region->is_active = $request->status;
        if ($region->save()) {
            return 'true';
        } else {
            return 'false';
        }
    }

}
