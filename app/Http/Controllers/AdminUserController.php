<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\UserBoost;
use App\Posts;
use App\PostLogs;
use App\AdBid;
use App\LeaderboardLog;
use App\Events;
use App\Followers;
use App\Admin;
use App\UserCryptoAddress;
use Illuminate\Support\Facades\Validator;

class AdminUserController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function listUsers() {
        return view('admin.User.index');
    }

    public function getList() {
        $output['aaData'] = [];
        $aColumns = array(
            'id'
            , 'user_image_url'
            , 'name'
            , 'email'
            , 'username'
            , 'is_active'
                //,'id'
        );
        $users = User::get()->toArray();
        foreach ($users as $key => $user) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($user[$aColumns[$i]] === NULL) {
                    $user[$aColumns[$i]] = "";
                }
                $row[] = $user[$aColumns[$i]];
            }
            $output['aaData'][] = $row;
        }
        $output = array(
            "draw" => intval(1),
            "data" => $output['aaData']
        );
        return $output;
    }

    public function updateStatus(Request $request) {
        $user = User::where('id', '=', $request->id)->first();
        $user->is_active = $request->status;
        if ($user->save()) {

            if ($request->status == 0) {
                $postsmstr = Posts::where("user_id", $request->id)->get();
                foreach ($postsmstr as $key => $value) {
                    $updateposts = Posts::find($value['id']);
                    $updateposts->is_active = 0;
                    $updateposts->save();

                    $postLogsMstr = PostLogs::where("post_id", $value['id'])->get();
                    if (count($postLogsMstr) > 0) {
                        foreach ($postLogsMstr as $key1 => $value1) {
                            $updatepostLog = PostLogs::find($value1['id']);
                            $updatepostLog->updated_at = $value1['updated_at'];
                            $updatepostLog->is_active = 0;
                            $updatepostLog->save();
                        }
                    }
                }

                $allAdBid = AdBid::where("user_id", $request->id)->get();
                if (count($allAdBid) > 0) {
                    foreach ($allAdBid as $key2 => $value2) {
                        $updateAdBid = AdBid::find($value2['id']);
                        $updateAdBid->is_active = 0;
                        $updateAdBid->save();
                    }
                }

                $allleaderboardLog = LeaderboardLog::where('user_id', $request->id)->get();
                if (count($allleaderboardLog) > 0) {
                    foreach ($allleaderboardLog as $key3 => $value3) {
                        $updateLeaderboardLog = LeaderboardLog::find($value3['id']);
                        $updateLeaderboardLog->is_active = 0;
                        $updateLeaderboardLog->save();
                    }
                }
                $allFollowers = Followers::where('user_id', $request->id)->orWhere('is_following', $request->id)->get();
                if (count($allFollowers) > 0) {
                    foreach ($allFollowers as $key4 => $value4) {
                        $updateFollowers = Followers::find($value4['id']);
                        $updateFollowers->is_active = 0;

                        if ($updateFollowers->user_id == $request->id) {

                            if ($userupdate = User::find($updateFollowers->is_following)) {

                                $userupdate->decrement('total_follows');
                                $userupdate->save();
                            }
                        }

                        $updateFollowers->save();
                    }
                }

                $allEvents = Events::where('user_id', $request->id)->get();
                if (count($allEvents) > 0) {
                    foreach ($allEvents as $key5 => $value5) {
                        $updateEvents = Events::find($value5['id']);
                        $updateEvents->is_active = 0;
                        $updateEvents->save();
                    }
                }
            } else {
                $postsmstr = Posts::where("user_id", $request->id)->get();
                foreach ($postsmstr as $key => $value) {
                    $updateposts = Posts::find($value['id']);
                    $updateposts->is_active = 1;
                    $updateposts->save();

                    $postLogsMstr = PostLogs::where("post_id", $value['id'])->get();
                    if (count($postLogsMstr) > 0) {
                        foreach ($postLogsMstr as $key1 => $value1) {
                            $updatepostLog = PostLogs::find($value1['id']);
                            $updatepostLog->updated_at = $value1['updated_at'];
                            $updatepostLog->is_active = 1;
                            $updatepostLog->save();
                        }
                    }
                }

                $allAdBid = AdBid::where("user_id", $request->id)->get();
                if (count($allAdBid) > 0) {
                    foreach ($allAdBid as $key2 => $value2) {
                        $updateAdBid = AdBid::find($value2['id']);
                        $updateAdBid->is_active = 1;
                        $updateAdBid->save();
                    }
                }

                $allleaderboardLog = LeaderboardLog::where('user_id', $request->id)->get();
                if (count($allleaderboardLog) > 0) {
                    foreach ($allleaderboardLog as $key3 => $value3) {
                        $updateLeaderboardLog = LeaderboardLog::find($value3['id']);
                        $updateLeaderboardLog->is_active = 1;
                        $updateLeaderboardLog->save();
                    }
                }
                $allFollowers = Followers::where('user_id', $request->id)->orWhere('is_following', $request->id)->get();
                if (count($allFollowers) > 0) {
                    foreach ($allFollowers as $key4 => $value4) {
                        $updateFollowers = Followers::find($value4['id']);
                        $updateFollowers->is_active = 1;

                        if ($updateFollowers->user_id == $request->id) {

                            if ($userupdate = User::find($updateFollowers->is_following)) {
                                $userupdate->increment('total_follows');
                                $userupdate->save();
                            }
                        }

                        $updateFollowers->save();
                    }
                }
                $allEvents = Events::where('user_id', $request->id)->get();
                if (count($allEvents) > 0) {
                    foreach ($allEvents as $key5 => $value5) {
                        $updateEvents = Events::find($value5['id']);
                        $updateEvents->is_active = 1;
                        $updateEvents->save();
                    }
                }
            }


            return 'true';
        } else {
            return 'false';
        }
    }

    public function admin_credential_rules(array $data) {
        $messages = [
            'current_password.required' => 'Please enter current password',
            'password.required' => 'Please enter password',
            'password.confirmed' => 'The confirm password doesn’t match',
        ];

        $validator = \Validator::make($data, [
                    'current_password' => 'required',
                    'password' => 'required|confirmed',
                    'password_confirmation' => 'required',
                        ], $messages);

        return $validator;
    }

    public function save_settings(Request $request) {

        $request_data = $request->all();
        $get_current_user = Admin::find(1);
        $validator = $this->admin_credential_rules($request_data);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {

            $current_password = $get_current_user->password;

            if (\Hash::check($request_data['current_password'], $current_password)) {

                $user_id = $get_current_user->id;
                $obj_user = Admin::find($user_id);
                $obj_user->password = \Hash::make($request_data['password']);
                ;
                $obj_user->save();
                //return "ok";
                return redirect()->back()->with('success', 'Current password not matched!');
            } else {
                return redirect()->back()->withErrors(['current_password' => 'Current password not matched!']);
            }
        }
    }

    public function settings() {
        return view('admin.User.settings');
    }

    public function get_wallet_list(Request $request) {
        $validator = Validator::make($request->input(), [
                    'user_id' => 'required'
        ]);
        $user_id = $request->input('user_id');

        $user_wallet = UserCryptoAddress::where('user_id', $user_id)->get();

        $table_html = '<table class="table table-striped"><thead>'
                . '<th>Coin</th><th>Address</th></thead><tbody>';
        if ($user_wallet->count() == 0) {
            $table_html .= '<tr><td colspan="2">No record found.</td></tr>';
        } else {
            
            foreach ($user_wallet as $wallet) {
                $table_html .= '<tr><td>' . $wallet->coin_code . '</td><td>' . $wallet->address . '</td></tr>';
            }
        }
        $table_html .= '</tbody></table>';
        echo $table_html;
        die();
    }

}
