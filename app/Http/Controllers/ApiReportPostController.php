<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
//load Models
use App\Api;
use App\User;
use App\Posts;
use App\ReportPost;

class ApiReportPostController extends ApiGuardController
{
    // protected $apiMethods = [
    //     'getUserProfileDetails' => [
    //         'keyAuthentication' => false
    //     ],         
    //     'getProfilePosts' => [
    //         'keyAuthentication' => false
    //     ],               
    // ];

    public function reportPost(Request $request){
	 	$data = $request->json()->all();
	 	$response =  [];
	 	if (isset($data['report_by']) && isset($data['post_id']) && isset($data['title']) && isset($data['details'])) {
	 		//Check report by & post id is valide
	 		if (!User::where('id','=',$data['report_by'])->count()) {
	 			return $response =  Api::make_response(0,[],'Invalide user');
	 		}
	 		if (!Posts::where('id','=',$data['post_id'])->count()) {
	 			return $response =  Api::make_response(0,[],'Invalide post');
	 		}	 			 		
	 		//Check for dublicate report
	 		if (ReportPost::where('report_by','=',$data['report_by'])->where('post_id','=',$data['post_id'])->count()) {
	 			return $response =  Api::make_response(0,[],'Already reported');
	 		}
            $report = new ReportPost;
            if ($report->create($data)) {
            	$response =  Api::make_response(1,[],'Successfully reported');
            }else{
                $response =  Api::make_response(0,[],'Report unsuccessful');
            }
	 	}
	 	else{
	 		$response =  Api::make_response(0,[],'Invalide Param . Data not saved');
	 	}
	 	return $response;    	
    }
}
