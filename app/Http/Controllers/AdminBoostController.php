<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Plans;
use App\BoostPlans;
use \Validator,\Redirect;
class AdminBoostController extends Controller
{
    /* 
      Author : Dhiman Bhattacharya

      This Contoller is for Manage Boost plans 
	*/
	
    public function __construct(){
    	$this->middleware('admin');
    }
      # To List Interest Category
      public function listPlans(){


      	$boostplans = BoostPlans::get()->toArray();
        return view('admin.Boost.index')->with('boostplans', $boostplans);
      }
      public function postAddNewPlan(Request $request){
      	if ($request->id) {

      			$messsages = array(
      			'planname.required'=>'Plan name field is required!',
      			'country_boost_count.required_without_all' => 'Any one of (Country/Region/Division/State) count is required!'
      			);

				$validator = Validator::make($request->all(), [
					'planname' => 'required',
					//'numofboost' => 'required|numeric',
					'country_boost_count' => 'required_without_all:region_boost_count,division_boost_count,state_boost_count|numeric',
					'region_boost_count' => 'required_without_all:country_boost_count,division_boost_count,state_boost_count|numeric',
					'division_boost_count' => 'required_without_all:region_boost_count,country_boost_count,state_boost_count|numeric',
					'state_boost_count' => 'required_without_all:region_boost_count,division_boost_count,country_boost_count|numeric',
					'amount'=> 'required',
					'is_active' => 'required',
					],$messsages);

				if($validator->fails()){
					
					
					return Redirect::back()->withErrors($validator)->withInput();

				}else{


					$boostplane = BoostPlans::find($request->id);
					$boostplane->plan_name = $request->planname;
					$boostplane->plan_name = $request->planname;
					$boostplane->country_boost_count = $request->country_boost_count;
					$boostplane->region_boost_count = $request->region_boost_count;
					$boostplane->division_boost_count = $request->division_boost_count;
					$boostplane->state_boost_count = $request->state_boost_count;
					//$boostplane->num_of_boost = $request->numofboost;
					$boostplane->amount = $request->amount;
					$boostplane->is_active = $request->is_active;

					if ($boostplane->save()) {
						return redirect('admin/boost_plans/list')->with('alert-success', 'Plan Updated!');
					}else{
						return redirect('admin/boost_plans/list')->with('alert-danger', 'Plan updation faild');	      		
					}
				}
			     		
      	}else{

      			$messsages = array(
      			'planname.required'=>'Plan name field is required!',
      			'country_boost_count.required_without_all' => 'Any one of (Country/Region/Division/State) count is required!'
      			);

				$validator = Validator::make($request->all(), [
					'planname' => 'required',
					//'numofboost' => 'required|numeric',

					'country_boost_count' => 'required_without_all:region_boost_count,division_boost_count,state_boost_count|numeric',
					'region_boost_count' => 'required_without_all:country_boost_count,division_boost_count,state_boost_count|numeric',
					'division_boost_count' => 'required_without_all:region_boost_count,country_boost_count,state_boost_count|numeric',
					'state_boost_count' => 'required_without_all:region_boost_count,division_boost_count,country_boost_count|numeric',

					'amount'=> 'required',
					'is_active' => 'required',
					],$messsages);



				if($validator->fails()){
					
					$messages = $validator->messages();

					return Redirect::back()->withErrors($validator)->withInput();

				}else{
					$boostplane = new BoostPlans;
					$boostplane->plan_name = $request->planname;

					$boostplane->country_boost_count = $request->country_boost_count;
					$boostplane->region_boost_count = $request->region_boost_count;
					$boostplane->division_boost_count = $request->division_boost_count;
					$boostplane->state_boost_count = $request->state_boost_count;
					//$boostplane->num_of_boost = $request->numofboost;
					$boostplane->amount = $request->amount;
					$boostplane->is_active = $request->is_active;

					if ($boostplane->save()) {
						return redirect('admin/boost_plans/list')->with('alert-success', 'Plan Updated!');
					}else{
						return redirect('admin/boost_plans/list')->with('alert-danger', 'Plan updation faild');	      		
					}
				}





        }
      }
		public function updateStatus(Request $request){

		$BoostPlansCount = BoostPlans::where('id',$request->id)->count();
		//print_r($BoostPlansCount);
			if($BoostPlansCount == 0){
				return 'false';	
			}else{
				$plan = BoostPlans::where('id','=',$request->id)->first();
				//print_r($plan);
				$plan->is_active = $request->status;
				if ($plan->save()) {
					return 'true';
				}else{
				return 'false';	      		
				}
			}

		
		}
}
