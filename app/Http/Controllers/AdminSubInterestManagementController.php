<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\InterestCategory;
use App\Interests;
use App\SubInterest;
use Illuminate\Support\Facades\Redirect;
use App\Api;

class AdminSubInterestManagementController extends Controller
{
    /* 
      Author : Debasis Chakraborty

      This Contoller is for Manage Interests 
    */
	public function __construct(){
    	$this->middleware('admin');
	}

      # To List Interest Category 
      public function listInterestCategory(){
		  $interestCategory = InterestCategory::orderby('id','desc')->get()->toArray();
        return view('admin.InterestManagement.subinterests')->with('interestCategory', $interestCategory);
      }

      public function postAddNewCategoryOld(Request $request){
		if(trim($request->order)!='' && $request->order!='0'){

		if($getSubCategory = SubInterest::where('order','=',$request->order)->where('id','!=',$request->id)->count()>0){
			return redirect('admin/interest-sub-category/list')->with('alert-danger', 'Order already used');
		}else{
      		if ($request->id) {
				$interestSubCategory = SubInterest::where('id','=',$request->id)->first();
				if (SubInterest::where('name','=',$request->name)->where('id','!=',$request->id)->first()) {
					return redirect('admin/interest-sub-category/list')->with('alert-danger', 'Interest Already Exists');
				}  	
				$interestSubCategory->name = $request->name;
				$interestSubCategory->interest_category_id = $request->cat_main_id;
				$interestSubCategory->is_active = $request->is_active;
				$interestSubCategory->order = $request->order;
				if ($interestSubCategory->save()) {
					return redirect('admin/interest-sub-category/list')->with('alert-success', 'Interest Updated!');
				}else{
				return redirect('admin/interest-sub-category/list')->with('alert-danger', 'Interest Updation faild');	      		
				}      		
			}else{
				$interestSubCategory = new SubInterest;
				if ($interestSubCategory::where('name','=',$request->name)->first()) {
					return redirect('admin/interest-sub-category/list')->with('alert-danger', 'Interest Already Exists');
				}else{
					$interestSubCategory->name = $request->name;
					$interestSubCategory->interest_category_id = $request->cat_main_id;
					$interestSubCategory->is_active = $request->is_active;
					$interestSubCategory->order = $request->order;
					if ($interestSubCategory->save()) {
						return redirect('admin/interest-sub-category/list')->with('alert-success', 'New Interest Created!');
					}else{
					return redirect('admin/interest-sub-category/list')->with('alert-danger', 'Interest Creation faild');	      		
					}
				}
			}	
		}

		}else{
			return redirect('admin/interest-sub-category/list')->with('alert-danger', 'Order field can not be empty');
		}


      }

      public function postAddNewCategory(Request $request){

			$response =  [];

      		if ($request->id) {
				$oldorder = SubInterest::where('order','=',$request->order)->first()->id;
				$interestSubCategory = SubInterest::where('id','=',$request->id)->first();
				$interestSubCategoryorder = $interestSubCategory->order;

				$interestSubCategory->name = $request->name;
				$interestSubCategory->interest_category_id = $request->cat_main_id;
				$interestSubCategory->is_active = $request->is_active;
				$interestSubCategory->order = $request->order;

				if ($interestSubCategory->save()) {

      				$oldorderupdate = SubInterest::find($oldorder);
      				$oldorderupdate->order = $interestSubCategoryorder;
      				$oldorderupdate->is_active = 1;
      				$oldorderupdate->save();

      				$response =  Api::make_response(1,['updated successfully'],'All okay');
					
				}else{
					$response =  Api::make_response(0,[''],'Something wrong');
				//return redirect('admin/interest-sub-category/list')->with('alert-danger', 'Interest Updation faild');	      		
				}      		
			}else{
					$interestSubCategory = new SubInterest;
					$interestSubCategory->name = $request->name;
					$interestSubCategory->interest_category_id = $request->cat_main_id;
					$interestSubCategory->is_active = $request->is_active;
					$interestSubCategory->order = $request->order;
					if ($interestSubCategory->save()) {
						$interestSubCategoryorder = SubInterest::max('order');
						$oldorder = SubInterest::where('order','=',$request->order)->first()->id;

						$oldorderupdate = SubInterest::find($oldorder);
	      				$oldorderupdate->order = $interestSubCategoryorder + 1;
	      				$oldorderupdate->is_active = 1;
	      				$oldorderupdate->save();

						$response =  Api::make_response(1,['inserted successfully'],'All okay');
					}else{
						$response =  Api::make_response(0,[''],'Something wrong');  		
					}
				}
				return $response;  

			}	
		
      public function updateStatus(Request $request){
      	$interestSubCategory = SubInterest::where('id','=',$request->id)->first();
      	$interestSubCategory->is_active = $request->status;
      	if ($interestSubCategory->save()) {
      		return 'true';
      	}else{
  		   return 'false';	      		
      	}      	
      }
	  public function order(Request $request){
		$response =  [];
		if(trim($request->order)!='' && $request->order!='0'){
			$getSubCategory = SubInterest::where('order','=',$request->order)->where('id','!=',$request->id);
			if($getSubCategory->count() >0){
				$response =  Api::make_response(0,["Order '".$request->order."'  already occupied by ".$getSubCategory->first()->name],'Order already used');     
			}else{
				$response =  Api::make_response(1,[],'All okay');
			}
		}else{
			$response =  Api::make_response(0,[],'Order field can not be empty');
		}
		return $response;    	
	  }

      public function delete(Request $request){
      	$interestSubCategory = SubInterest::where('id','=',$request->id)->first();
      	if ($interestSubCategory->delete()) {
      		return 'true';
      	}else{
  		   return 'false';	      		
      	}      	
      }      
      public function getList(){
      	    $output['aaData'] = [];
			$aColumns = array(
				                 'id'
			                    ,'interest_category_id'
			                    ,'interest_category_name'
			                    ,'name'
			                    ,'is_active'
								,'id'
								,'order'
							);
			$allInterestCategory = SubInterest::orderBy('order','desc')->get()->toArray();
			foreach ($allInterestCategory as $key => $interestSubCategory) {
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if($interestSubCategory[ $aColumns[$i]]=== NULL){$interestSubCategory[ $aColumns[$i]]= "";}
						$row[] = $interestSubCategory[ $aColumns[$i] ];

				}
				$output['aaData'][] = $row;
			}
			$output = array(
			    "draw"            => intval(1),
			    "data"            => $output['aaData']
			); 	
      	return $output;
      }
}
