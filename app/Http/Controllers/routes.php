<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::group(['middleware' => ['web']], function(){
// 	Route::auth();
// 	Route::get('/home', 'HomeController@index');
// });

Route::group(['prefix' => 'social/','middleware' => ['web']], function(){
    //Twitter login
    Route::get('twitter_login', 'ApiUserController@redirectToProvider');
    Route::get('twitter_login/callback', 'ApiUserController@handleProviderCallback');
}); 

//Mobile Api

Route::group(['prefix' => 'mobileapi','namespace'=>'Mobileapi'], function () {

    Route::post('mobile_authenticate','UserController@mobile_authenticate')->name('mobile_authenticate');
    Route::post('mobile_logout', 'UserController@mobile_logout');
    Route::post('mobile_registerUser','UserController@mobile_registerUser')->name('mobile_registerUser');
    Route::get('mobile_interest_category','UserController@mobile_interest_category')->name('mobile_interest_category');
    Route::post('mobile_interest_category_by_id','UserController@mobile_interest_category_by_id')->name('mobile_interest_category_by_id');
    Route::get('mobile_country_list/', 'UserController@mobile_country_list');
    Route::post('mobile_country_regions/', 'UserController@mobile_country_regions');
    Route::post('mobile_country_divisions/', 'UserController@mobile_country_divisions');
    Route::post('mobile_country_states/', 'UserController@mobile_country_states');
    Route::post('mobile_forget_password/', 'UserController@mobile_forget_password'); // Get link By Email
    Route::post('mobile_authenticate_user_key/', 'UserController@mobile_authenticate_user_key');  // Authenticate User
    Route::post('mobile_change_password/', 'UserController@mobile_change_password'); // Change Password
    Route::post('mobile_followed_user_list/', 'UserController@mobile_followed_user_list'); //mobile_followed_user_list
    Route::post('mobile_following_user_list/', 'UserController@mobile_following_user_list');//mobile_following_user_list
    Route::post('mobile_following_user_details/', 'UserController@mobile_following_user_details');
    Route::post('mobile_add_user_profile_image/', 'UserController@mobile_add_user_profile_image');
    Route::get('mobile_map_location/', 'UserController@mobile_map_location');
});

Route::group(['middleware' => ['web']], function () {
    //Login Routes...
    Route::get('/','AdminAuth\AuthController@showLoginForm');
    Route::get('admin','AdminAuth\AuthController@showLoginForm');
    Route::get('admin/login','AdminAuth\AuthController@showLoginForm');
    Route::post('admin/login','AdminAuth\AuthController@login');
    Route::get('admin/logout','AdminAuth\AuthController@logout');
    
    //Dash Board
    Route::get('admin/dashboard', 'AdminController@index');
     //Ajax Call
    Route::get('interest-category-ajax-list', 'AdminInterestManagementController@getList');
    Route::post('interest-category-ajax-status-update', 'AdminInterestManagementController@updateStatus');
    Route::post('interest-category-ajax-delete', 'AdminInterestManagementController@delete');  

    Route::get('interest-category-sub-ajax-list', 'AdminSubInterestManagementController@getList');
    Route::post('interest-category-sub-ajax-status-update', 'AdminSubInterestManagementController@updateStatus');
    Route::post('interest-category-sub-ajax-delete', 'AdminSubInterestManagementController@delete');
    Route::post('interest-category-sub-ajax-order', 'AdminSubInterestManagementController@order');

    Route::get('user_ajax_list', 'AdminUserController@getList');
    Route::post('user-ajax-status-update', 'AdminUserController@updateStatus');

    Route::get('membership_plan_ajax_list', 'AdminPlanController@getList');
    Route::post('plan-ajax-status-update', 'AdminPlanController@updateStatus'); 

    Route::post('boost-plan-ajax-status-update', 'AdminBoostController@updateStatus');   

    Route::get('country_list', 'AdminCountryController@getList');
    Route::post('country-ajax-status-update', 'AdminCountryController@updateStatus');   

    Route::get('region_list', 'AdminRegionController@getList');
    Route::post('region-ajax-status-update', 'AdminRegionController@updateStatus');   
    
    Route::get('devision_list', 'AdminDevisionController@getList');
    Route::post('devision-ajax-status-update', 'AdminDevisionController@updateStatus'); 

    Route::get('state_list', 'AdminStateController@getList');
    Route::post('state-ajax-status-update', 'AdminStateController@updateStatus'); 

    Route::get('cms_list', 'AdminCMSController@getList');
    Route::post('cms-ajax-status-update', 'AdminCMSController@updateStatus');    

   Route::get('advertise_list', 'AdminAdvertiseController@getList');
   Route::post('advertise-ajax-status-update', 'AdminAdvertiseController@updateStatus'); 
   Route::post('advertiseagain-ajax-status-update', 'AdminAdvertiseController@updateStatusagain'); 


   
}); 

Route::group(['prefix' => 'admin/'], function() {

    //Interest Category Management
    Route::get('interest-category/list', 'AdminInterestManagementController@listInterestCategory');
    Route::post('interest-category/add','AdminInterestManagementController@postAddNewCategory');

    //Interest Sub Category Management
    Route::get('interest-sub-category/list', 'AdminSubInterestManagementController@listInterestCategory');
    Route::post('interest-sub-category/add','AdminSubInterestManagementController@postAddNewCategory');

    //Advertise Management
    Route::get('advertise/list', 'AdminAdvertiseController@listAdvertise');
    Route::post('advertise/add','AdminAdvertiseController@postAddNewAdvertise');
    Route::get('advertise-settings/list', 'AdminAdvertiseController@settingsAdvertise');

    Route::get('post/details','AdminPostController@details');
    Route::post('post/delete','AdminPostController@delete');
    //Country Management
    Route::get('country/list', 'AdminCountryController@listCountry');
    Route::post('country/add','AdminCountryController@postAddNewCountry');

    //Region Management
    Route::get('region/list', 'AdminRegionController@listRegion');
    Route::post('region/add','AdminRegionController@postAddNewRegion');

    //Devision Management
    Route::get('devision/list', 'AdminDevisionController@listDevision');
    Route::post('devision/add','AdminDevisionController@postAddNewDevision');        

    //State Management
    Route::get('state/list', 'AdminStateController@listState');
    Route::post('state/add','AdminStateController@postAddNewState');


    //Manage Plans   
    Route::get('memership_plans/list', 'AdminPlanController@listPlans');
    Route::post('memership_plan/add','AdminPlanController@postAddNewPlan'); 

    //Manage Plans   
    Route::get('boost_plans/list', 'AdminBoostController@listPlans');
    Route::post('boost_plan/add','AdminBoostController@postAddNewPlan'); 

    //Manage User
    Route::get('user/list', 'AdminUserController@listUsers');

    //CMS Management
    Route::get('cms/list', 'AdminCMSController@listCms');
    Route::post('cms/add','AdminCMSController@postAddNewCms');

    //Support Request   
    Route::get('support_request/list/inbox', 'AdminSupportRequestController@listSupportyRequest'); 


    Route::get('support_request/list/searchinbox', 'AdminSupportRequestController@searchinbox'); 
    Route::get('support_request/list/searchtrashbox', 'AdminSupportRequestController@searchtrashbox'); 

    Route::get('support_request/list/deleted', 'AdminSupportRequestController@listSupportRequest'); 
 
       Route::get('support_request/details/{ticket_no}/{user_id}', 'AdminSupportRequestController@DetailsSupportyRequest'); 
    Route::get('support_request/add', 'AdminSupportRequestController@AddSupportyRequest');

    Route::post('support_request/addsupport/inbox', 'AdminSupportRequestController@addsupport');
    Route::get('support_request/delete/{id}','AdminSupportRequestController@delete');
// Route::get('support_request/list/search','AdminSupportRequestController@search');
 Route::post('support_request/updatesupport/reply/{ticket_no}', 'AdminSupportRequestController@addreply');


    Route::post('support_request/details/addsupport/inbox', 'AdminSupportRequestController@addsupport');

      //Route::get('post/details','AdminPostController@details');
    Route::get('adregion/list', 'AdminAdRegionController@listadregion');
    Route::post('adregion/add', 'AdminAdRegionController@postAdd');
    Route::get('adregion/delete/{id}', 'AdminAdRegionController@delete');

   
    Route::get('adpage/list', 'AdminAdPagesController@listadpage');
    Route::post('adpage/add', 'AdminAdPagesController@postAdd');
    
    Route::get('adpage/details/{id}', 'AdminAdPagesController@details');
    Route::post('ad_price_details/add', 'AdminAdPagesController@ad_price_details');    
        //Route::get('adpage/add', 'AdminAdPagesController@ad_price_details');

    //Site Setting Management
    Route::get('sitesetting', 'AdminSiteSettingController@index');
    Route::post('sitesetting/update','AdminSiteSettingController@update');

    Route::match(['get', 'post'],'sitesetting/change-bid-amount','AdminSiteSettingController@updateBidAmount');
    Route::get('sitesetting/get-page-by-region/{id}','AdminSiteSettingController@getPageByRegion');

    Route::get('revenue/index', 'AdminRevenueController@index');
    //Route::post('sitesetting/update','AdminSiteSettingController@update');

    Route::get('settings', 'AdminUserController@settings');
    Route::post('save_settings', 'AdminUserController@save_settings');
    
    
    
});
//All Api Part
Route::group(['prefix' => 'api/v1/','middleware' => ['web']], function(){
    //User Auth Part
    Route::get('sitesetiing/', 'ApiController@sitesetiing');
    Route::post('logout/', 'ApiController@logoutUser');
    Route::post('login/', 'ApiController@loginUser');
    Route::post('register/', 'ApiController@registerUser');
    Route::post('check_auth_key/', 'ApiController@keyIsValide');
    Route::post('check_user/', 'ApiController@checkUserState');
    Route::get('getallinterestcategory/', 'ApiController@getInterestCategory');
    Route::get('getinterestcategorybyid/{id}', 'ApiController@getInterestCategoryById');

    Route::post('deactiveaccount/','ApiController@deactiveaccount');
    Route::post('userdetails/','ApiController@userdetails');  
      
    //Event Part
    Route::post('add-event/', 'ApiEventController@addEvent');
    Route::post('get-event-list/', 'ApiEventController@getEventList');
    Route::post('get_event_date_list/', 'ApiEventController@getEventDateList');
    Route::get('get_date/', 'ApiEventController@getDate');

    //Location Part
    Route::get('get-country-list/', 'ApiLocationController@getCountryList');
    Route::post('get-country-regions/', 'ApiLocationController@getRegions');
    Route::post('get-country-divisions/', 'ApiLocationController@getDivisions');
    Route::post('get-country-states/', 'ApiLocationController@getStates');
    Route::get('save-all-location-us/', 'ApiLocationController@saveUsCountry');
    Route::get('get-map-location/', 'ApiLocationController@getMapLocations');

    //User Wall Part
    Route::post('add-user-profile-image/', 'ApiUserController@userImageUpload');
    Route::post('update-user-design-settings/', 'ApiUserController@userDesignSettingsUpdate');
    Route::post('deactivate_user/', 'ApiUserController@dactivateUser');
    Route::post('update_user_settings/', 'ApiUserController@updtaeUserAccountSettings'); 
    Route::post('unfollow_user/', 'ApiUserController@unFollowUser');    
    Route::post('follow_user/', 'ApiUserController@followUser');    
    Route::post('get-followed-user-list/', 'ApiUserController@getFollwedUsersList');    
    Route::post('get-following-user-list/', 'ApiUserController@getFollwingUsersList');  
    Route::post('get-following-user-details/', 'ApiUserController@getFollwingUsersdetails');    
    Route::post('get-user-image/', 'ApiUserController@getuserImage');
    Route::post('get-all-user-image/', 'ApiUserController@getalluserImage');
    Route::post('set-user-image/', 'ApiUserController@setuserImage');


    //Test  
    Route::post('send-test-mail/', 'ApiUserController@sendTestMail'); 

    //Forget Password
    Route::post('forget_password/', 'ApiUserController@forgetPassword'); // Get link By Email
    Route::post('authenticate_user_key/', 'ApiUserController@confirmForgetPasswordKey');  // Authenticate User
    Route::post('set_password/', 'ApiUserController@changePassword'); // Change Password

    //Post
    Route::post('add_post/', 'ApiPostController@add_post');
    Route::post('share_post/', 'ApiPostController@share_post');
    Route::post('like_post/', 'ApiPostController@like_post');
    Route::post('get_posts/', 'ApiPostController@getPosts');
    Route::post('get_posts_test/', 'ApiPostController@testPost');/*For testing perpouse*/
    Route::post('post_audio_video/', 'ApiPostController@postAudioVideo');

    //Support Ticket
    Route::post('add_support_request/', 'ApiSupportRequestController@addSupportRequest');
    Route::post('get_support_requests/', 'ApiSupportRequestController@getRequests');
    Route::post('get_support_post','ApiSupportRequestController@getResponse');
    Route::post('add_comments','ApiSupportRequestController@addcomments');
    Route::post('get_comments','ApiSupportRequestController@getcomments');


    //Audio & Video List
    Route::post('total_media_count/', 'ApiUserController@getTotalMediaCount');
    Route::post('get_audio_list/', 'ApiUserController@getAudioList');
    Route::post('get_video_list/', 'ApiUserController@getVideoList');
    Route::get('usersearch/', 'ApiUserController@usersearch');
    Route::get('usersearchtest/', 'ApiUserController@usersearchtest');

    //Leaderboard & Wall of fame
    Route::post('get_leaderboard/', 'ApiLeaderboardLogController@getLeaderboardList');
    Route::post('get_wall_of_fame/', 'ApiLeaderboardLogController@getWallOfFameList');
    Route::get('get_wall_of_fame_year/', 'ApiLeaderboardLogController@getLeaderboardYearList');
    Route::post('get_wall_of_fame_month/', 'ApiLeaderboardLogController@getLeaderboardMonthList');
    Route::post('get_numberone/', 'ApiLeaderboardLogController@get_numberone');

    //Route::post('add_log_test/', 'ApiLeaderboardLogController@addLog');

    //Notification
    Route::post('get_notification/', 'ApiNotificationLogController@getNotification');
    Route::post('get_notification_details/', 'ApiNotificationLogController@getNotificationDetails');
    Route::post('set_all_notification_read/', 'ApiNotificationLogController@setNotificationRead');

    //User Profile Section
    Route::post('get_profile_details/', 'ApiProfileController@getUserProfileDetails');
    Route::post('get_profile_posts/', 'ApiProfileController@getProfilePosts');
    Route::post('getProfileAudioPosts/', 'ApiProfileController@getProfileAudioPosts');
    Route::post('getProfileVideoPosts/', 'ApiProfileController@getProfileVideoPosts');
    Route::post('changepermission/', 'ApiProfileController@changepermission');

    
    
    //request attachment 

    // Route::post('get_attachment/', 'ApiSupportRequestAttachmentController@getsupportrequestattachment');

    //Report POst
    Route::post('report_post/', 'ApiReportPostController@reportPost');
    
    Route::get('get_all_plan/', 'ApiPlanController@getplan');
    Route::post('plan_subscription/', 'ApiPlanController@subscription');
    Route::post('bidwinpayment/', 'ApiPlanController@bidwinpayment');   
    Route::get('getregions/', 'ApiAdvertiseController@getregions');
    Route::post('useraddlist/', 'ApiAdvertiseController@useraddlist');
    Route::post('getpages/', 'ApiAdvertiseController@getpages');
    Route::post('postadvertisement/', 'ApiAdvertiseController@postadvertisement');   
     
    Route::post('postcreative/', 'ApiAdvertiseController@postcreative');
    Route::post('pastcreative/', 'ApiAdvertiseController@pastcreative');
    
    Route::post('sendmsg/', 'ApiPlanController@sendmsg');
    //*********************start bidding api***************//
    Route::post('bidlisting/', 'ApiAdvertiseController@bidlisting');      



    Route::post('get-ad-country-regions/', 'ApiAdvertiseController@getRegions');
    Route::post('get-ad-country-divisions/', 'ApiAdvertiseController@getDivisions');
    Route::post('get-ad-country-states/', 'ApiAdvertiseController@getStates');

    Route::post('bidpost/', 'ApiAdvertiseController@bidpost');
    Route::post('bidpostfinal/', 'ApiAdvertiseController@bidpostfinal');    
    Route::post('bidfinalpost/', 'ApiAdvertiseController@bidfinalpost');    
    Route::post('bidpostlist/', 'ApiAdvertiseController@bidpostlist');
    Route::post('bidcalculation/', 'ApiAdvertiseController@bidcalculation');
    Route::post('mybid/', 'ApiAdvertiseController@mybid');
    Route::post('mybidmodal/', 'ApiAdvertiseController@mybidmodal');
    Route::post('mybidupdate/', 'ApiAdvertiseController@mybidupdate');
    Route::post('buynowbid/', 'ApiAdvertiseController@buynowbid');
    
    Route::post('showvideoadd/', 'ApiAdvertiseController@showvideoadd');
    Route::post('showimageadd/', 'ApiAdvertiseController@showimageadd');
    Route::post('getmylastbid/', 'ApiAdvertiseController@getmylastbid');
    Route::get('winbidcron/', 'ApiAdvertiseController@winbidcron');
    Route::post('saveboostsetting/', 'ApiAdvertiseController@saveboostsetting');    
    //Route::post('get_all_plan/', 'ApiPlanController@subscription');    
    Route::post('contactusmail/', 'ApiController@contactusmail');
    Route::post('reportchecking/', 'ApiController@reportchecking');
    Route::get('getallboostplan/', 'ApiController@getallboostplan');
    Route::post('getallboostcount/', 'ApiController@getallboostcount');
    Route::post('plan_subscribeboost/', 'ApiPlanController@subscribeboost');
    Route::post('postyourboost/', 'ApiPlanController@postyourboost');
    Route::post('postyourboostcheck/', 'ApiPlanController@postyourboostcheck');
    Route::post('getcurrentboostedlocation/', 'ApiPlanController@getcurrentboostedlocation');
    Route::post('deleteyourpost/', 'ApiPostController@deleteyourpost');
    Route::post('changepostpermission/', 'ApiPostController@changepostpermission');


});
