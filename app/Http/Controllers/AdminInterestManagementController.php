<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\InterestCategory;
use App\Interests;
use App\SubInterest;
use Illuminate\Support\Facades\Redirect;

class AdminInterestManagementController extends Controller
{
    /* 
      Author : Debasis Chakraborty

      This Contoller is for Manage Interests 
	*/   
	 public function __construct(){
    	$this->middleware('admin');
    }
      # To List Interest Category
      public function listInterestCategory(){
        return view('admin.InterestManagement.index');
      }
      public function postAddNewCategory(Request $request){
      	if ($request->id) {
      		$interestCategory = InterestCategory::where('id','=',$request->id)->first();
	      	if (InterestCategory::where('name','=',$request->name)->where('id','!=',$request->id)->first()) {
	      		return redirect('admin/interest-category/list')->with('alert-danger', 'Interest Category Already Exists');
	      	}  		
	      	$interestCategory->name = $request->name;
	      	$interestCategory->is_active = $request->is_active;
	      	if ($interestCategory->save()) {
	      		return redirect('admin/interest-category/list')->with('alert-success', 'Interest Category Updated!');
	      	}else{
      		   return redirect('admin/interest-category/list')->with('alert-danger', 'Interest Category Updation faild');	      		
	      	}      		
      	}else{
	      	$interestCategory = new InterestCategory;
	      	if ($interestCategory::where('name','=',$request->name)->first()) {
	      		return redirect('admin/interest-category/list')->with('alert-danger', 'Interest Category Already Exists');
	      	}else{
		      	$interestCategory->name = $request->name;
		      	$interestCategory->is_active = $request->is_active;
		      	if ($interestCategory->save()) {
		      		return redirect('admin/interest-category/list')->with('alert-success', 'New Interest Category Created!');
		      	}else{
	      		   return redirect('admin/interest-category/list')->with('alert-danger', 'Interest Category Creation faild');	      		
		      	}
	      	}
        }
      }
      public function updateStatus(Request $request){
      	$interestCategory = InterestCategory::where('id','=',$request->id)->first();
      	$interestCategory->is_active = $request->status;
      	if ($interestCategory->save()) {
      		return 'true';
      	}else{
  		   return 'false';	      		
      	}      	
      }
      public function delete(Request $request){
      	$interestCategory = InterestCategory::where('id','=',$request->id)->first();
      	if ($interestCategory->delete()) {
      		return 'true';
      	}else{
  		   return 'false';	      		
      	}      	
      }      
      public function getList(){
      	    $output['aaData'] = [];
			$aColumns = array(
				                 'id'
			                    ,'name'
			                    ,'is_active'
								,'id'
							);
			$allInterestCategory = InterestCategory::get()->toArray();
			foreach ($allInterestCategory as $key => $interestCategory) {
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if($interestCategory[ $aColumns[$i]]=== NULL){$interestCategory[ $aColumns[$i]]= "";}
						$row[] = $interestCategory[ $aColumns[$i] ];

				}
				$output['aaData'][] = $row;
			}
			$output = array(
			    "draw"            => intval(1),
			    "data"            => $output['aaData']
			); 	
      	return $output;
      }
}
