<?php

namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use App\User;
use App\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\UserCryptoAddress;
use App\Crypto_prices;

class ApiUsercrypto extends ApiGuardController {

    protected $apiMethods = [
        'update_address' => [
            'keyAuthentication' => true
        ],
        'add_address' => [
            'keyAuthentication' => true
        ],
        'get_address_list' => [
            'keyAuthentication' => true
        ],
        'delete_address'=>[
            'keyAuthentication' => true
        ],
        'get_remain_cryptlist'=>[
            'keyAuthentication'=>true
        ]
    ];

    public function get_address_list(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Error Occurred. Please logout and login again.');
        }

        $user_id = $request->input('user_id');

        $user_crypto_data = UserCryptoAddress::where('user_id', $user_id)->get();

        if ($user_crypto_data->count() == 0) {
            return Api::make_response(0, [], 'No user wallet address available');
        }

        return Api::make_response(1, $user_crypto_data, 'User crypto wallet addresses found.');
    }
    
    public function get_remain_cryptlist(Request $request) {
        $validator= Validator::make($request->all(),[
            'user_id'=>'required',
        ]);
        if($validator->fails()){
            return Api::make_response(0, [], "Please follow validation rules.");
        }
        
        $user_id=$request->input('user_id');
        $user_crypto_data = UserCryptoAddress::where('user_id', $user_id)->get(['coin_code'])->toArray();
        
        $user_crypto_arr= array_column($user_crypto_data, 'coin_code');
        
        $coin_list=$remain_coin_list= Crypto_prices::where('status','Enabled')->get(['coin_code'])->toArray();
       
        foreach($coin_list as $key=>$coin){
            if(in_array($coin['coin_code'], $user_crypto_arr)){
                unset($remain_coin_list[$key]);
            }
        }
        return Api::make_response(1, array_values($remain_coin_list), "");
    }

    public function update_address(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'address' => 'required',
                    'crypto_id' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }

        $user_id = $request->input('user_id');
        $address = $request->input('address');
        $crypto_id = $request->input('crypto_id');

        if (UserCryptoAddress::where(['id' => $crypto_id, 'user_id' => $user_id])->update(['address' => $address])) {
            $wallet_data = UserCryptoAddress::where('id', $crypto_id)->get();
            $user_crypto_data = UserCryptoAddress::where('user_id', $user_id)->get();
            return Api::make_response(1, $user_crypto_data, 'user ' . $wallet_data[0]->coin_code . ' address updated successfully.');
        } else {
            return Api::make_response(0, $user_crypto_data, 'Error Occurred. Try Again!');
        }
    }

    public function add_address(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'coin_code' => 'required',
                    'address' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], "Please follow validation rules.");
        }

        $address_arr = [
            'user_id' => $request->input('user_id'),
            'coin_code' => $request->input('coin_code'),
            'address' => $request->input('address'),
            'created_at' => date('Y-m-d H:i:s'),
            'created_ip' => $request->ip(),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_ip' => $request->ip()
        ];

        if (UserCryptoAddress::insert($address_arr)) {
            $user_crypto_data = UserCryptoAddress::where('user_id', $request->input('user_id'))->get();
            return Api::make_response(1, $user_crypto_data, "Record inserted successfully.");
        } else {
            return Api::make_response(0, [], "Error Occurred. Try Again!");
        }
    }

    public function delete_address(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'crypto_id' => 'required'
        ]);
        if($validator->fails()){
            return Api::make_response(0, [], "Please follow all validation rules.");
        }
        $user_id=$request->input('user_id');
        $crypto_id=$request->input('crypto_id');
        
        if(UserCryptoAddress::where(['user_id'=>$user_id,'id'=>$crypto_id])->delete()){
            $user_crypto_data = UserCryptoAddress::where('user_id', $user_id)->get();
            return Api::make_response(1, $user_crypto_data, 'Address deleted successfully.');
        }
        else{
            return Api::make_response(0, [], 'Error Occurred. Try Again!');
        }
        
    }

}
