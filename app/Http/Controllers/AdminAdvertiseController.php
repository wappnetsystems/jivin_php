<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Advertise;
class AdminAdvertiseController extends Controller
{
    public function __construct(){
    	$this->middleware('admin');
    }
public function listAdvertise(){
        return view('admin.advertise.index');
		//echo "AdminAdvertiseController";
      }

 public function postAddNewAdvertise(Request $request){
       // echo $request->name;exit();
      	if ($request->id) {
      		$advertise = Advertise::where('id','=',$request->id)->first();
	      	if (Advertise::where('title','=',$request->name)->where('id','!=',$request->id)->first()) {
	      		return redirect('admin/advertise/list')->with('alert-danger', 'Advertise Already Exists');
	      	}  		
	      	$advertise->title = str_replace(' ', '_', $request->name);;
	      	$advertise->slug = str_replace(' ', '_', $request->name);;
	      	$advertise->is_active = $request->is_active;
	      	$advertise->is_bidding_on=$request->is_bidding_on;
	      	if ($advertise->save()) {
	      		return redirect('admin/advertise/list')->with('alert-success', 'Advertise Updated!');
	      	}else{
      		   return redirect('admin/advertise/list')->with('alert-danger', 'Advertise Updation faild');	      		
	      	}      		
      	}else{
	      	$advertise = new Advertise;
	      	if ($advertise::where('title','=',$request->name)->first()) {
	      		return redirect('admin/advertise/list')->with('alert-danger', 'Advertise Already Exists');
	      	}else{
	      		// echo $request->name;exit();
	      	$advertise->title = str_replace(' ', '_', $request->name);;
	      	$advertise->slug = str_replace(' ', '_', $request->name);;
	      $advertise->is_active = $request->is_active;
	      	$advertise->is_bidding_on=$request->is_bidding_on;
		      	if ($advertise->save()) {
		      		return redirect('admin/advertise/list')->with('alert-success', 'New  Advertise Created!');
		      	}else{
	      		   return redirect('admin/advertise/list')->with('alert-danger', 'Advertise Creation faild');	      		
		      	}
	      	}
        }
      }      
      public function getList(){
      	    $output['aaData'] = [];
			$aColumns = array(
				                 'id'
			                    ,'title'
			                    ,'is_active'
								,'is_bidding_on'
							);
			$advertises = Advertise::get()->toArray();
			//dd($advertises);
			foreach ($advertises as $key => $advertise) {
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if($advertise[ $aColumns[$i]]=== NULL){$advertise[ $aColumns[$i]]= "";}
						$row[] = $advertise[ $aColumns[$i] ];

				}
				$output['aaData'][] = $row;
			}
			$output = array(
			    "draw"            => intval(1),
			    "data"            => $output['aaData']
			); 	
      	return $output;
      	dd($output);
      }    
      public function updateStatus(Request $request){
      	$advertise = Advertise::where('id','=',$request->id)->first();
      	$advertise->is_active = $request->status;
      	 $advertise->is_bidding_on=$request->is_bidding_on;
      	if ($advertise->save()) {
      		return 'true';
      	}else{
  		   return 'false';	      		
      	}      	
      }

     public function updateStatusagain(Request $request){
      	$advertise = Advertise::where('id','=',$request->id)->first();
      	//$advertise->is_active = $request->status;
      	 $advertise->is_bidding_on=$request->is_bidding_on;
      	if ($advertise->save()) {
      		return 'true';
      	}else{
  		   return 'false';	      		
      	}      	
      }
}
