<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\SiteSetting;
use App\AdRegions;
use App\AdPages;

class AdminSiteSettingController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        $rerurn = array();
        $data = SiteSetting::find(1);
        if (!empty($data)) {
            $rerurn = $data;
        }
        return view('admin.sitesetting.index', $rerurn);
    }

    public function update(Request $request) {

        if ($request->id) {

            $data = SiteSetting::find($request->id);
            $data->boost_text = $request->boost_text;
            $data->adspace_text = $request->adspace_text;
            $data->creative_text = $request->creative_text;
            $data->bidslot_text = $request->bidslot_text;
            $data->bidreview_text = $request->bidreview_text;
            $data->yourbid_text = $request->yourbid_text;
            $data->adlocation_text = $request->adlocation_text;
            $data->jackpot_text=$request->jackpot_text;
            if ($data->save()) {
                return redirect('admin/sitesetting')->with('alert-success', 'Site Setting Updated!');
            } else {
                return redirect('admin/sitesetting')->with('alert-danger', 'Site Setting Updation faild');
            }
        }
    }

    public function updateBidAmount(Request $request) {

        $regionList = AdRegions::with('getalladpages')
                ->where('status', 1)
                ->get();

        if ($request->isMethod('post')) {
            $req_data = $request->all();
            $ad_pages = AdPages::find($req_data['page_id']);
            if ($ad_pages) {
                $ad_pages->buy_now_price = $request['bid_amount'];
                $ad_pages->min_bid = $request['min_bid'];
                $ad_pages->save();
            } else {

                return redirect()->back()->with('alert-danger', 'Oh snap! Error in saving!');
            }

            return redirect()->back()->with('alert-success', 'Amount successfully updated!');
        }

        return view('admin.sitesetting.bid-amount', compact('regionList'));
    }

    public function getPageByRegion($id) {

        $regionList = AdRegions::with('getalladpages')
                ->where('id', $id)
                ->first();

        $html = '<option value="">Select Page</option>';
        if ($regionList) {
            foreach ($regionList['getalladpages'] as $key => $value) {
                $html .= '<option data-min-bid="' . $value->min_bid . '" data-bid-amount="' . $value->buy_now_price . '" value="' . $value->id . '">' . $value->page_name . '</option>';
            }
        }

        return \Response::json(['html' => $html]);
    }

    /*
      public function postAddNewCms(Request $request){

      if ($request->id) {
      $cms = Cms::where('id','=',$request->id)->first();
      if (Cms::where('page_title','=',$request->page_title)->where('id','!=',$request->id)->first()) {
      return redirect('admin/cms/list')->with('alert-danger', 'Cms Already Exists');
      }
      $cms->page_title = str_replace(' ', '_', $request->page_title);
      $cms->page_slug = str_replace(' ', '_', $request->page_title);
      $cms->page_html = $request->page_html;
      $cms->is_active = $request->is_active;
      if ($cms->save()) {
      return redirect('admin/cms/list')->with('alert-success', 'Cms Updated!');
      }else{
      return redirect('admin/cms/list')->with('alert-danger', 'Cms Updation faild');
      }
      }else{
      $cms = new Cms;
      if ($cms::where('page_title','=',$request->page_title)->first()) {
      return redirect('admin/cms/list')->with('alert-danger', 'Cms Already Exists');
      }else{
      // echo $request->page_title;exit();
      $cms->page_title = str_replace(' ', '_', $request->page_title);
      $cms->page_slug = str_replace(' ', '_', $request->page_title);
      $cms->page_html = $request->page_html;
      $cms->is_active = $request->is_active;
      if ($cms->save()) {
      return redirect('admin/cms/list')->with('alert-success', 'New Cms Created!');
      }else{
      return redirect('admin/cms/list')->with('alert-danger', 'Cms Creation faild');
      }
      }
      }
      }
      public function getList(){
      $output['aaData'] = [];
      $aColumns = array(
      'id'
      ,'page_title'
      ,'page_html'
      ,'is_active'
      ,'id'
      );
      $countries = Cms::get()->toArray();
      // /dd($countries);
      foreach ($countries as $key => $cms) {
      $row = array();
      for ( $i=0 ; $i<count($aColumns) ; $i++ )
      {
      if($cms[ $aColumns[$i]]=== NULL){$cms[ $aColumns[$i]]= "";}
      $row[] = $cms[ $aColumns[$i] ];

      }
      $output['aaData'][] = $row;
      }
      $output = array(
      "draw"            => intval(1),
      "data"            => $output['aaData']
      );
      return $output;
      }
      public function updateStatus(Request $request){
      $cms = Cms::where('id','=',$request->id)->first();
      $cms->is_active = $request->status;
      if ($cms->save()) {
      return 'true';
      }else{
      return 'false';
      }
      }
     */
}
