<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;

use App\Http\Requests;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
//load Models
use App\Api;
use App\User;
use App\Followers;
use App\Posts;
use App\PostLogs;
use App\PostLikeLog;
use App\PostShareLog;

use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;
class ApiProfileController extends ApiGuardController
{
    protected $apiMethods = [
        'getUserProfileDetails' => [
            'keyAuthentication' => false
        ],         
        'getProfilePosts' => [
            'keyAuthentication' => false
        ],               
    ];

    public function getUserProfileDetails(Request $request){
	 	$data = $request->json()->all();
	 	$response =  [];
	 	if (isset($data['username']) && isset($data['current_user_id'])) {
	 		$current_user_id = $data['current_user_id'];
           if ($user = User::where('username', '=', urldecode($data['username']))->where('is_active',1)->first()) {
				$user = User::with('accountSettings','designSettings','events','isFollowing','follows')->find($user->id);
				
				$following = Followers::where('is_following' ,$user->id)->where('user_id' ,$current_user_id)->first();
				
                $response =  Api::make_response(1,['is_following'=>$following?true:false,'user_info'=>$user,],'Profile Found');
           }
           else{
             $response =  Api::make_response(0,[],'Invalide Username');           
           }
	 	}
	 	else{
	 		$response =  Api::make_response(0,[],'Invalide Param . Data not saved');
	 	}
	 	return $response;    	
    }

		public function arrayPaginator($array, $request)
		{
				$page = Input::get('page', 1);
				$perPage = 10;
				$offset = ($page * $perPage) - $perPage;

				return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
						['path' => $request->url(), 'query' => $request->query()]);
		}

    public function getProfilePostsOld(Request $request){
		$data = $request->json()->all();
		$response =  [];
		$page = $data['page'];
		$itemsPerPage = $data['itemsPerPage'];
		if(isset($data['username']) && $user = User::where('username','=',$data['username'])->first()){
     
		 if($user->id != $data['current_user_id']){

				$isuserFollower = Followers::where('is_following',$user->id)->where('user_id',$data['current_user_id'])->where('is_active',1)->first();
		  		
					if($isuserFollower){
					
					$getPosts = PostLogs::where('visible_by','=',$user->id)->where('post_type','self')->where('is_active','=',1)->whereIn('is_public', [1, 2])->orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();
					
					}else{
					
					$getPosts = PostLogs::where('visible_by','=',$user->id)->where('post_type','self')->where('is_active','=',1)->where('is_public','=',2)->orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();
					
					}

				
			}else{
					
					$getPosts = PostLogs::where('visible_by','=',$user->id)->where('post_type','self')->where('is_active','=',1)->orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();
					
			}
			
			      $finalArray = [];
			      if (isset($getPosts['data']) && !empty($getPosts['data'])) {
			        $i = 0;
			        foreach ($getPosts['data'] as $key => $value) {
						  
			              $finalArray[$i] = $value;
			              if(PostLikeLog::where('post_id','=',$value['post_id'])->where('liked_by','=',$data['current_user_id'])->first()){
			                 $finalArray[$i]['is_liked'] = true;
			              }
			              if(PostShareLog::where('post_id','=',$value['post_id'])->where('shared_by','=',$data['current_user_id'])->first()){
			                 $finalArray[$i]['is_shared'] = true;
			              }            
			          $i++;
			        }
			        $getPosts["data"] = $finalArray;
			        $response =  Api::make_response(1,[$getPosts],'Posts Found');
			      }else{
			        $response =  Api::make_response(0,[],'Posts not Found');
			      }            
		}else{
          $response =  Api::make_response(0,[],'Invalide user name');
		}
		 return $response;
    }


    public function getProfilePosts(Request $request){
		$data = $request->json()->all();
		$response =  [];
		$page = $data['page'];
		$itemsPerPage = $data['itemsPerPage'];
		if(isset($data['username']) && $user = User::where('username','=',$data['username'])->first()){
     
		 if($user->id != $data['current_user_id']){

				$isuserFollower = Followers::where('is_following',$user->id)->where('user_id',$data['current_user_id'])->where('is_active',1)->first();
		  		
					if($isuserFollower){
					
					//$getPosts = Posts::where('user_id','=',$user->id)->where('is_active','=',1)->whereIn('is_public', [1, 2])->orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();
				  $getPosts =  DB::select("SELECT posts.*,posts.created_at AS abc FROM  posts WHERE is_active = 1 AND is_public IN(1, 2) AND user_id = '".$user->id."' ORDER BY  abc DESC");
					}else{
					
					//$getPosts = Posts::where('user_id','=',$user->id)->where('is_active','=',1)->where('is_public','=',2)->orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();
				  $getPosts =  DB::select("SELECT posts.*,posts.created_at AS abc FROM  posts WHERE is_active = 1 AND is_public = 2 AND user_id = '".$user->id."' ORDER BY  abc DESC");
					}

				
			}else{

				$getPosts =  DB::select("(SELECT posts. * , post_boost.created_at AS abc FROM  posts LEFT JOIN  post_boost ON  posts.id =  post_boost.post_id WHERE  post_boost.user_id = '".$user->id."' GROUP BY  post_boost.post_id ,  post_boost.created_at )UNION(SELECT posts.*,posts.created_at AS abc FROM  posts WHERE is_active = 1 AND user_id = '".$user->id."')ORDER BY  abc DESC");
				//$getPosts = Posts::where('user_id','=',$user->id)->where('is_active','=',1)->orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();

			}

$getPosts = $this->arrayPaginator($getPosts, $request)->toArray();
$getPosts = json_decode(json_encode($getPosts), true);
			
			      $finalArray = [];
			      if (isset($getPosts['data']) && !empty($getPosts['data'])) {
			        $i = 0;
			        foreach ($getPosts['data'] as $key => $value) {


						  
			              $finalArray[$i] = $value;
										$userdetails = User::find($value['user_id']);
										if($userdetails){
											$finalArray[$i]['created_by_img'] = $userdetails['user_image_url'];
											$finalArray[$i]['created_by_name'] = $userdetails['name'];
											$finalArray[$i]['created_by_user_id'] = $userdetails['id'];
											$finalArray[$i]['created_by_user_name'] = $userdetails['username'];
											
										}
										if($value['is_shared']==0){
											if($value['user_id'] == $data['current_user_id'] ){
												if($value['is_boost']==1){
													$finalArray[$i]['display_text'] = "Boosted a new post";
												}else{
													$finalArray[$i]['display_text'] = "Added a new post";
												}
												
											}else{
												//$finalArray[$i]['display_text'] = $userdetails['name']." added a new post";
												$finalArray[$i]['display_text'] = " Added a new post";
											}
										}else{
											$sharedname = User::find($value['shared_id'])->name;
											$finalArray[$i]['display_text'] = "shared ".$sharedname."'s post";
										}

										$finalArray[$i]['post_id'] = $key.$value['id'];

										if($value['video_name']!=''){

											$finalArray[$i]['video_url'] = env('IMG_URL').'post/'.$value['video_name'];
											$finalArray[$i]['media_type'] = "video";
											$finalArray[$i]['audio_url'] = "";

										}else if($value['audio_name']!=''){
											$finalArray[$i]['audio_url'] = env('IMG_URL').'post/'.$value['audio_name'];
											$finalArray[$i]['video_url'] = "";
											$finalArray[$i]['media_type'] = "audio";
										}else{
											$finalArray[$i]['media_type'] = "";
											$finalArray[$i]['video_url'] = "";
											$finalArray[$i]['audio_url'] = "";
										}		
										
			              if(PostLikeLog::where('post_id','=',$value['id'])->where('liked_by','=',$data['current_user_id'])->first()){
			                 $finalArray[$i]['is_liked'] = true;
			              }else{
											 $finalArray[$i]['is_liked'] = false;
										}
			              if(PostShareLog::where('post_id','=',$value['id'])->where('shared_by','=',$data['current_user_id'])->first()){
			                 $finalArray[$i]['is_shared'] = true;
			              }else{
											 $finalArray[$i]['is_shared'] = false;
										}       

										$finalArray[$i]['time_ago'] = $this->timeAgo($value['abc']);
										$finalArray[$i]['total_vote'] = $value['total_like'];

			          $i++;
			        }
			        $getPosts["data"] = $finalArray;
			        $response =  Api::make_response(1,[$getPosts],'Posts Found');
			      }else{
			        $response =  Api::make_response(0,[],'Posts not Found');
			      }            
		}else{
          $response =  Api::make_response(0,[],'Invalid user name');
		}
		 return $response;
    }

		public function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }

	  public function getProfileAudioPostsOld(Request $request){
		$data = $request->json()->all();
		$response =  [];
		$page = $data['page'];
		$itemsPerPage = $data['itemsPerPage'];
		if(isset($data['username']) && $user = User::where('username','=',$data['username'])->first()){
          //Get post Details
			//$getPosts = PostLogs::where('visible_by','=',$user->id)->where('is_active','=',1)->orderBy('boost_order','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();
			if($user->id != $data['current_user_id']){
				$getPosts = PostLogs::wherehas('getpost',function($q) use ($data){
					$q->where('total_like','>',0)->where('audio_name','!=','');
				})
				->where('visible_by','=',$user->id)->where('post_type','self')->where('is_active','=',1)->where('is_public','=',1)->orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();
			}else{
				$getPosts = PostLogs::wherehas('getpost',function($q) use ($data){
					$q->where('total_like','>',0)->where('audio_name','!=','');
				})
				->where('visible_by','=',$user->id)->where('post_type','self')->where('is_active','=',1)->orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();
			}
			//return $getPosts;
			      $finalArray = [];
			      if (isset($getPosts['data']) && !empty($getPosts['data'])) {
			        $i = 0;
			        foreach ($getPosts['data'] as $key => $value) {
			              //dd($value['post_id']);
						  
			              $finalArray[$i] = $value;
			              if(PostLikeLog::where('post_id','=',$value['post_id'])->where('liked_by','=',$data['current_user_id'])->first()){
			                 $finalArray[$i]['is_liked'] = true;
			              }
			              if(PostShareLog::where('post_id','=',$value['post_id'])->where('shared_by','=',$data['current_user_id'])->first()){
			                 $finalArray[$i]['is_shared'] = true;
			              }            
			          $i++;
			        }
			        $getPosts["data"] = $finalArray;
			        $response =  Api::make_response(1,[$getPosts],'Posts Found');
			      }else{
			        $response =  Api::make_response(0,[],'Posts not Found');
			      }            
		}else{
          $response =  Api::make_response(0,[],'Invalide user name');
		}
		 return $response;
    }
 	public function getProfileAudioPosts(Request $request){
      $data = $request->json()->all();
      $response =  [];
      $condition = '';
      //dd($data);

      if ($data['user_id']) {
              $user = User::where('id', '=', $data['user_id'])->first();
          if ($user) {

                  if(isset($data['loginuser']) && $data['loginuser'] == $data['user_id']){
                    $getPosts = Posts::where('user_id','=',$data['user_id'])->where('audio_name','!=',NULL)->where('total_like','>',0)->orderBy('created_at','DESC')->get();
                  }else{
                    $getPosts = Posts::where('user_id','=',$data['user_id'])->where('is_public',1)->where('total_like','>',0)->where('audio_name','!=',NULL)->orderBy('created_at','DESC')->get();
                  }

              if ($getPosts->count()) {
                $response =  Api::make_response(1,[$getPosts],'Video Found');
              }else{
                $response =  Api::make_response(0,[],'No Video Found');
              }
              
            }else{
              $response =  Api::make_response(0,[],'Error Occurred');
            }     
        
      }else{
        $response =  Api::make_response(0,[],'Json Request Error');
      }
      
      return $response;      
    }
	
	public function getProfileVideoPostsOld(Request $request){
		$data = $request->json()->all();
		$response =  [];
		$page = $data['page'];
		$itemsPerPage = $data['itemsPerPage'];
		if(isset($data['username']) && $user = User::where('username','=',$data['username'])->first()){
          //Get post Details
			if($user->id != $data['current_user_id']){
				$getPosts = PostLogs::wherehas('getpost',function($q) use ($data){
					$q->where('total_like','>',0)->where('video_name','!=','');
				})
				->where('visible_by','=',$user->id)->where('post_type','self')->where('is_active','=',1)->where('is_public','=',1)->orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();
			}else{
				$getPosts = PostLogs::wherehas('getpost',function($q) use ($data){
					$q->where('total_like','>',0)->where('video_name','!=','');
				})
				->where('visible_by','=',$user->id)->where('post_type','self')->where('is_active','=',1)->orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();
			}
			//return $getPosts;
			      $finalArray = [];
			      if (isset($getPosts['data']) && !empty($getPosts['data'])) {
			        $i = 0;
			        foreach ($getPosts['data'] as $key => $value) {
			              //dd($value['post_id']);
						  
			              $finalArray[$i] = $value;
			              if(PostLikeLog::where('post_id','=',$value['post_id'])->where('liked_by','=',$data['current_user_id'])->first()){
			                 $finalArray[$i]['is_liked'] = true;
			              }
			              if(PostShareLog::where('post_id','=',$value['post_id'])->where('shared_by','=',$data['current_user_id'])->first()){
			                 $finalArray[$i]['is_shared'] = true;
			              }            
			          $i++;
			        }
			        $getPosts["data"] = $finalArray;
			        $response =  Api::make_response(1,[$getPosts],'Posts Found');
			      }else{
			        $response =  Api::make_response(0,[],'Posts not Found');
			      }            
		}else{
          $response =  Api::make_response(0,[],'Invalide user name');
		}
		 return $response;
    }

	public function getProfileVideoPosts(Request $request){
      $data = $request->json()->all();
      $response =  [];
      $condition = '';
      //dd($data);

      if ($data['user_id']) {
              $user = User::where('id', '=', $data['user_id'])->first();
          if ($user) {

                  if(isset($data['loginuser']) && $data['loginuser'] == $data['user_id']){
                    $getPosts = Posts::where('user_id','=',$data['user_id'])->where('video_name','!=',NULL)->where('total_like','>',0)->orderBy('created_at','DESC')->paginate();
                  }else{
                    $getPosts = Posts::where('user_id','=',$data['user_id'])->where('is_public',1)->where('total_like','>',0)->where('video_name','!=',NULL)->orderBy('created_at','DESC')->paginate();
                  }

              if ($getPosts->count()) {
                $response =  Api::make_response(1,[$getPosts],'Video Found');
              }else{
                $response =  Api::make_response(0,[],'No Video Found');
              }
              
            }else{
              $response =  Api::make_response(0,[],'Error Occurred');
            }     
        
      }else{
        $response =  Api::make_response(0,[],'Json Request Error');
      }
      
      return $response;      
    }

		public function changepermission(Request $request){
      $data = $request->json()->all();
      $response =  [];
      if ($data['user_id']) {
              $user = User::where('id', '=', $data['user_id'])->first();

          if ($user) {

									if($data['permission']!='' && $data['postid']!=''){
										$poststab = Posts::find($data['postid']);
										$poststab->is_public = $data['permission'];
									}

									if($poststab->save()){
										if(isset($data['loginuser']) && $data['loginuser'] == $data['user_id']){
											if($data['type'] == "video"){
												$getPosts = Posts::where('user_id','=',$data['user_id'])->where('video_name','!=',NULL)->where('total_like','>',0)->orderBy('created_at','DESC')->paginate();
											}elseif ($data['type'] == "audio") {
												$getPosts = Posts::where('user_id','=',$data['user_id'])->where('audio_name','!=',NULL)->where('total_like','>',0)->orderBy('created_at','DESC')->paginate();
											}
										}else{
											if($data['type'] == "video"){
												$getPosts = Posts::where('user_id','=',$data['user_id'])->where('is_public',1)->where('total_like','>',0)->where('video_name','!=',NULL)->orderBy('created_at','DESC')->paginate();
											}elseif ($data['type'] == "audio") {
												$getPosts = Posts::where('user_id','=',$data['user_id'])->where('is_public',1)->where('total_like','>',0)->where('audio_name','!=',NULL)->orderBy('created_at','DESC')->paginate();
											}
										}
									}

              if ($getPosts->count()) {
                $response =  Api::make_response(1,[$getPosts],'Video Found');
              }else{
                $response =  Api::make_response(0,[],'No Video Found');
              }
              
            }else{
              $response =  Api::make_response(0,[],'Error Occurred');
            }     
        
      }else{
        $response =  Api::make_response(0,[],'Json Request Error');
      }
      
      return $response;    
		}
    
}
