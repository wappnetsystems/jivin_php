<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use App\Country;
use App\Region;
use App\Devision;
use App\State;

class AdminCountryController extends Controller
{    
	public function __construct(){
	$this->middleware('admin');
	}
      public function listCountry(){
        return view('admin.location.country');
      }
      public function postAddNewCountry(Request $request){
       // echo $request->name;exit();
      	if ($request->id) {
      		$country = Country::where('id','=',$request->id)->first();
	      	if (Country::where('name','=',$request->name)->where('id','!=',$request->id)->first()) {
	      		return redirect('admin/country/list')->with('alert-danger', 'Country Already Exists');
	      	}  		
	      	$country->name = str_replace(' ', '_', $request->name);;
	      	//$country->slug = str_replace(' ', '_', $request->name);;
	      	$country->is_active = $request->is_active;
	      	if ($country->save()) {
	      		return redirect('admin/country/list')->with('alert-success', 'Country Updated!');
	      	}else{
      		   return redirect('admin/country/list')->with('alert-danger', 'Country Updation faild');	      		
	      	}      		
      	}else{
	      	$country = new Country;
	      	if ($country::where('name','=',$request->name)->first()) {
	      		return redirect('admin/country/list')->with('alert-danger', 'Country Already Exists');
	      	}else{
	      		// echo $request->name;exit();
	      	$country->name = str_replace(' ', '_', $request->name);;
	      	$country->slug = str_replace(' ', '_', $request->name);;
	      	$country->is_active = $request->is_active;
		      	if ($country->save()) {
		      		return redirect('admin/country/list')->with('alert-success', 'New Country Created!');
		      	}else{
	      		   return redirect('admin/country/list')->with('alert-danger', 'Country Creation faild');	      		
		      	}
	      	}
        }
      }      
      public function getList(){
      	    $output['aaData'] = [];
			$aColumns = array(
				                 'id'
			                    ,'name'
			                    ,'is_active'
								,'id'
							);
			$countries = Country::get()->toArray();
			// /dd($countries);
			foreach ($countries as $key => $country) {
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if($country[ $aColumns[$i]]=== NULL){$country[ $aColumns[$i]]= "";}
						$row[] = $country[ $aColumns[$i] ];

				}
				$output['aaData'][] = $row;
			}
			$output = array(
			    "draw"            => intval(1),
			    "data"            => $output['aaData']
			); 	
      	return $output;
      }    
      public function updateStatus(Request $request){
      	$country = Country::where('id','=',$request->id)->first();
      	$country->is_active = $request->status;
      	if ($country->save()) {
      		return 'true';
      	}else{
  		   return 'false';	      		
      	}      	
      }        
}
