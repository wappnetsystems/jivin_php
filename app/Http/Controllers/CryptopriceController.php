<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crypto_prices;
use App\Api_credentials;

class CryptopriceController extends Controller {

    //cron job function will run every minute and update crypto currency price
    public function update_crypto_price(Request $request) {

        //fetch coin list
        $crypto_list = Crypto_prices::where('status', 'Enabled')->get();

        if ($crypto_list->count() == 0) {
            echo 'no currency available.';
            die();
        }
        
        //fetch api credentials
        $api_credentials= Api_credentials::where('id',1)->get();
        
        if($api_credentials->count()==0){
            echo 'no api credentials';
            die();
        }
        
        foreach ($crypto_list as $crypto) {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://min-api.cryptocompare.com/data/price?fsym={$crypto->coin_code}&tsyms=USD&api_key={$api_credentials[0]->cred_value}",
                CURLOPT_RETURNTRANSFER => true,
                
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
                die();
            } else {
                $result= json_decode($response,true);
                if(!isset($result['USD'])){
                    echo "Error"; 
                    die();
                }
                //update query to update price
                Crypto_prices::where('coin_code',$crypto->coin_code)->update(['coin_price_usd'=>$result['USD'],'updated_at'=>date('Y-m-d H:i:s'),'updated_ip'=>$request->ip()]);
                
            }
        }
        die();
    }

}
