<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\Http\Requests;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
//load Models
use App\Api;
use App\User;
use App\TempUser;
use App\InterestCategory;
use App\Followers;
use App\Interests;
use App\AccountSettings;
use App\SubInterest;
use App\Email;
use App\Subscription;
use App\Plans;
use App\UserCard;
use App\ReportPost;
use App\BoostPlans;
use App\UserSubcriptionHistory;
use App\UserBoost;
use App\Posts;
use App\PostLogs;
use App\AdBid;
use App\LeaderboardLog;
use App\Events;
use App\SiteSetting;
use App\UserImage;
use App\User_category_interest;
use App\Jivin_video;

class ApiController extends ApiGuardController {

    protected $apiMethods = [
        'loginUser' => [
            'keyAuthentication' => false
        ],
        'registerUser' => [
            'keyAuthentication' => false
        ],
        'getInterestCategory' => [
            'keyAuthentication' => false
        ],
        'logoutUser' => [
            'keyAuthentication' => false
        ],
        'keyIsValide' => [
            'keyAuthentication' => false
        ],
        'contactusmail' => [
            'keyAuthentication' => false
        ],
        'reportchecking' => [
            'keyAuthentication' => false
        ],
        'getallboostplan' => [
            'keyAuthentication' => false
        ],
        'getallboostcount' => [
            'keyAuthentication' => false
        ],
        'deactiveaccount' => [
            'keyAuthentication' => false
        ],
        'userdetails' => [
            'keyAuthentication' => false
        ],
        'sitesetiing' => [
            'keyAuthentication' => false
        ],
        'checkUserState' => [
            'keyAuthentication' => false
        ],
        'insert_old_interest_data' => [
            'keyAuthentication' => false
        ],
        'get_jivin_video'=>[
            'keyAuthentication' => false
        ]
    ];

    public function sitesetiing(Request $request) {
        $response = [];
        $data = SiteSetting::where('id', '=', 1)->first();
        if ($data) {
            $response = Api::make_response(1, $data, 'SiteSetting');
        } else {
            $response = Api::make_response(0, [], 'Invalid Param . Data not saved');
        }
        return $response;
    }

    public function keyIsValide(Request $request) {
        $data = $request->json()->all();
        $key = $data['key'];
        $apiKeyCount = ApiKey::where('key', '=', $key)->limit(1)->count();

        if ($apiKeyCount > 0)
            return Api::make_response(1, [], 'Key Is Valide');

        return Api::make_response(0, [], 'Key Is Not Valide');
    }

    public function checkUserState(Request $request) {
        $data = $request->json()->all();
        $key = $data['username'];
        $apiKeyCount = User::select('users.id')->join('subscription', 'users.id', '=', 'subscription.user_id')->where('users.username', '=', $key)->where('users.is_active', 1)->count();

        if ($apiKeyCount > 0)
            return Api::make_response(1, [], 'Valid User.');

        return Api::make_response(0, [], 'Invalid User');
    }

    /* Login User
      Created By : Debasis Chakraborty
     */

    public function logoutUser(Request $request) {
        $data = $request->json()->all();
        $response = [];
        if ($data) {
            if ($user = User::where('id', '=', $data['userid'])->first()) {
                $user->is_login = 0;
                $user->save();
                $userApiKey = ApiKey::where('user_id', '=', $user->id)->first()->delete();
                $user = User::where('id', '=', $data['userid'])->first();
                $response = Api::make_response(1, [], 'Logout Successfully');
            } else {
                $response = Api::make_response(0, [], 'User Not Exists');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalid Param . Data not saved');
        }
        return $response;
    }

    /* Login User
      Created By : Debasis Chakraborty
     */

    public function loginUser(Request $request) {
        $data = $request->json()->all();
        $response = [];
        //$data['social'] = "";
        if ($data) {
            if (isset($data['logintype']) && $data['logintype'] != "") {
                /* if ($user = User::where('username', '=', $data['username'])->first()) {
                  if (Hash::check($data['password'],$user->password))
                  {
                  $user->is_login = 1;
                  $user->save();
                  ApiKey::make($user->id);
                  $subscription = array();
                  $subdetils = Subscription::where('user_id',$user->id)->first();
                  if(!empty($subdetils)){
                  //$subscription['subdetails'] = $subdetils;
                  $subscription['Plansdetails']['plan'] = Plans::find($subdetils->package_id);
                  $subscription['remaining'] = (strtotime($subdetils->package_valide_to) - strtotime(date('Y-m-d')))/ (60 * 60 * 24);
                  }
                  $userApiKey = ApiKey::where('user_id', '=', $user->id)->first();
                  $user_account_settings = AccountSettings::where('user_id', '=', $user->id)->first();
                  if(!$user_account_settings){
                  $user_account_settings = new AccountSettings;
                  $user_account_settings->user_id = $user->id;
                  $user_account_settings->save();
                  }
                  $user = User::with('accountSettings','designSettings','events','isFollowing','follows')->find($user->id);
                  $suggestedFollowers = User::getfollowerSuggetions($user->id,5);
                  $suggestedFollowersIds = User::getfollowerSuggetionsIds($user->id,5);
                  $UserCardDetails = UserCard::where('user_id',$user->id)->get();


                  $response =  Api::make_response(1,['card_list'=>$UserCardDetails,'api_key'=>$userApiKey->key,'user_info'=>$user,'suggested_followers_ids'=>$suggestedFollowersIds,'suggested_followers'=>$suggestedFollowers,'subscription'=>$subscription],'Login Successfull');
                  }else{
                  $response =  Api::make_response(0,[],'Invalide Password. Please Check');
                  }
                  }
                  else{
                  $response =  Api::make_response(0,[],'Invalide Username. Please Register');
                  }
                 */
                if($data['logintype']=='instagram'){
                    $user = User::where('username', '=', $data['username'])->first();
                }
                else{
                $user = User::where('email', '=', $data['username'])->first();
                }
                if ($user) {

                    if ($user->is_active == 0) {
                        return Api::make_response(0, [], 'Inactive account! Please contact your administrator.');
                    }

                    if ($data['logintype'] == $user->registrationtype) {
                        $user->is_login = 1;
                        $user->save();
                        ApiKey::make($user->id);
                        $subscription = array();
                        $subdetils = Subscription::where('user_id', $user->id)->first();
                        if (!empty($subdetils)) {
                            //$subscription['subdetails'] = $subdetils;
                            $subscription['Plansdetails']['plan'] = Plans::find($subdetils->package_id);
                            $subscription['remaining'] = (strtotime($subdetils->package_valide_to) - strtotime(date('Y-m-d'))) / (60 * 60 * 24);
                        }
                        $userApiKey = ApiKey::where('user_id', '=', $user->id)->first();
                        $user_account_settings = AccountSettings::where('user_id', '=', $user->id)->first();
                        if (!$user_account_settings) {
                            $user_account_settings = new AccountSettings;
                            $user_account_settings->user_id = $user->id;
                            $user_account_settings->save();
                        }
                        $user = User::with('accountSettings', 'designSettings', 'events', 'isFollowing', 'follows')->find($user->id);
                        $suggestedFollowers = User::getfollowerSuggetions($user->id, 5);
                        $suggestedFollowersIds = User::getfollowerSuggetionsIds($user->id, 5);
                        $UserCardDetails = UserCard::where('user_id', $user->id)->get();


                        $response = Api::make_response(1, ['card_list' => $UserCardDetails, 'api_key' => $userApiKey->key, 'user_info' => $user, 'suggested_followers_ids' => $suggestedFollowersIds, 'suggested_followers' => $suggestedFollowers, 'subscription' => $subscription ? $subscription : null], 'Login Successfull');
                    } else {
                        $response = Api::make_response(0, [], 'Invalid social type. Please Check');
                    }
                } else {
                    $response = Api::make_response(0, [], 'Invalid user. Please Register');
                }
            } else {

                /*
                  if ($user = User::where('email', '=', $data['username'])->first()) {
                  //if (Hash::check($data['password'],$user->password))
                  //{
                  $user->is_login = 1;
                  $user->save();
                  ApiKey::make($user->id);
                  $subscription = array();
                  $subdetils = Subscription::where('user_id',$user->id)->first();
                  if(!empty($subdetils)){
                  //$subscription['subdetails'] = $subdetils;
                  $subscription['Plansdetails']['plan'] = Plans::find($subdetils->package_id);
                  $subscription['remaining'] = (strtotime($subdetils->package_valide_to) - strtotime(date('Y-m-d')))/ (60 * 60 * 24);
                  }
                  $userApiKey = ApiKey::where('user_id', '=', $user->id)->first();
                  $user_account_settings = AccountSettings::where('user_id', '=', $user->id)->first();
                  if(!$user_account_settings){
                  $user_account_settings = new AccountSettings;
                  $user_account_settings->user_id = $user->id;
                  $user_account_settings->save();
                  }
                  $user = User::with('accountSettings','designSettings','events','isFollowing','follows')->find($user->id);
                  $suggestedFollowers = User::getfollowerSuggetions($user->id,5);
                  $suggestedFollowersIds = User::getfollowerSuggetionsIds($user->id,5);
                  $UserCardDetails = UserCard::where('user_id',$user->id)->get();


                  $response =  Api::make_response(1,['card_list'=>$UserCardDetails,'api_key'=>$userApiKey->key,'user_info'=>$user,'suggested_followers_ids'=>$suggestedFollowersIds,'suggested_followers'=>$suggestedFollowers,'subscription'=>$subscription],'Login Successfull');

                  /*}else{
                  $response =  Api::make_response(0,[],'Invalide Password. Please Check');
                  } */
                /* }
                  else{
                  $response =  Api::make_response(0,[],'Invalide Username. Please Register');   if ($user = User::where('username', '=', $data['username'])->orWhere('email', '=', $data['username'])->where('is_active',1)->first()) {
                  }
                 */
                $user = User::where(function($query) use ($data) {
                            $query->orwhereRaw("TRIM(username) = ?", [trim($data['username'])])
                                    ->orWhere('email', '=', $data['username']);
                        })->first();

                if ($user) {

                    if ($user->is_active == 0) {
                        return Api::make_response(0, [], 'Inactive account! Please contact your administrator.');
                    }
                    if (Hash::check($data['password'], $user->password)) {
                        $user->is_login = 1;
                        $user->save();
                        ApiKey::make($user->id);
                        $subscription = array();
                        $subdetils = Subscription::where('user_id', $user->id)->first();
                        if (!empty($subdetils)) {
                            //$subscription['subdetails'] = $subdetils;
                            $subscription['Plansdetails']['plan'] = Plans::find($subdetils->package_id);
                            $subscription['remaining'] = (strtotime($subdetils->package_valide_to) - strtotime(date('Y-m-d'))) / (60 * 60 * 24);
                        }
                        $userApiKey = ApiKey::where('user_id', '=', $user->id)->first();
                        $user_account_settings = AccountSettings::where('user_id', '=', $user->id)->first();
                        if (!$user_account_settings) {
                            $user_account_settings = new AccountSettings;
                            $user_account_settings->user_id = $user->id;
                            $user_account_settings->save();
                        }
                        $user = User::with('accountSettings', 'designSettings', 'events', 'isFollowing', 'follows')->find($user->id);
                        $suggestedFollowers = User::getfollowerSuggetions($user->id, 5);
                        $suggestedFollowersIds = User::getfollowerSuggetionsIds($user->id, 5);
                        $UserCardDetails = UserCard::where('user_id', $user->id)->get();


                        $response = Api::make_response(1, ['card_list' => $UserCardDetails, 'api_key' => $userApiKey->key, 'user_info' => $user, 'suggested_followers_ids' => $suggestedFollowersIds, 'suggested_followers' => $suggestedFollowers, 'subscription' => $subscription ? $subscription : null], 'Login Successfull');
                    } else {
                        $response = Api::make_response(0, [], 'Opps! Your credentials does not match.');
                    }
                } else {
                    $response = Api::make_response(0, [], 'Opps! Your credentials does not match.');
                }
            }
        } else {
            $response = Api::make_response(0, [], 'Opps! looks like a non categorized request!');
        }
        return $response;
    }

    /* Register User
      Created By : Debasis Chakraborty
     */

    public function registerUser(Request $request) {

        $data = $request->json()->all();
        $response = [];
        if ($data) {
            if (!isset($data['social_uid'])) {
                $data['social_uid'] = null;
            }
            if (!isset($data['secondary_email'])) {
                $data['secondary_email'] = "";
            }
            if (!isset($data['country_id']) || empty($data['country_id'])) {
                $data['country_id'] = null;
            }
            if (!isset($data['region_id']) || empty($data['region_id'])) {
                $data['region_id'] = null;
            }
            if (!isset($data['division_id']) || empty($data['division_id'])) {
                $data['division_id'] = null;
            }
            if (!isset($data['state_id']) || empty($data['state_id'])) {
                $data['state_id'] = null;
            }


            if ($data['user_id'] == 0) {

                if (User::where('email', '=', TRIM($data['email']))->exists()) {
                    // user found
                    return $response = Api::make_response(0, [], 'User Email Already Exists');
                }
                if (User::where('username', '=', TRIM($data['username']))->exists()) {
                    // user found
                    return $response = Api::make_response(0, [], 'Username Already Exists');
                }
                if ($tempUser = TempUser::where('email', '=', TRIM($data['email']))->first()) {
                    //Temp user found
                    $tempUser->delete();
                    $tempUser = new TempUser;
                } elseif ($tempUser = TempUser::where('username', '=', TRIM($data['username']))->where(\DB::raw('username is not null'))->first()) {
                    //Temp user found
                    $tempUser->delete();
                    $tempUser = new TempUser;
                } else {
                    # new temp user
                    $tempUser = new TempUser;
                }
                $name = TRIM($data['name']);
                $email = TRIM($data['email']);
                $username = TRIM($data['username']);
                $birth_month = $data['birth_month'];
                $birth_year = $data['birth_year'];
                $password = $data['password'];
                $is_following = $data['is_following'];
                $interests = $data['user_interests'];
                $secondary_email = $data['secondary_email'];
                $country_id = $data['country_id'];
                $region_id = $data['region_id'];
                $division_id = $data['division_id'];
                $state_id = $data['state_id'];
                $social_uid = $data['social_uid'];
                if (isset($data['user_type'])) {
                    $user_type = $data['user_type'];
                }
            } else {

                if (isset($data['birth_month'])) {
                    $birth_month = $data['birth_month'];
                } else {
                    $birth_month = '';
                }
                if (isset($data['birth_year'])) {
                    $birth_year = $data['birth_year'];
                } else {
                    $birth_year = '';
                }
                # update temp user
                $data['user_id'] = TRIM($data['user_id']);
                $tempUser = TempUser::find($data['user_id']);

                if (!$tempUser) {
                    if ($tempuser_record = TempUser::where('username', $username)->orwhere('email', $email)->orderby('id', 'desc')->first()) {
                        $tempUser = $tempuser_record;
                    } else {
                        return Api::make_response(0, [], 'Oh snap! Data not matched to our records. please try from the begining.');
                    }
                }

                $name = (empty($tempUser->name) ? $data['name'] : $tempUser->name);
                //$email=$data['email'];
                $email = (isset($data['email']) ? $data['email'] : $tempUser->email);
                $secondary_email = (empty($tempUser->secondary_email) ? $data['secondary_email'] : $tempUser->secondary_email);
                $username = (empty($tempUser->username) ? $data['username'] : $tempUser->username);
                $birth_month = (empty($tempUser->birth_month) ? $birth_month : $tempUser->birth_month);
                $birth_year = (empty($tempUser->birth_year) ? $birth_year : $tempUser->birth_year);
                $is_following = (empty($tempUser->is_following) ? $data['is_following'] : $tempUser->is_following);
                $interests = (empty($tempUser->interests) ? $data['user_interests'] : $tempUser->interests);

                $country_id = (empty($tempUser->country_id) ? $data['country_id'] : $tempUser->country_id);
                $region_id = (empty($tempUser->region_id) ? $data['region_id'] : $tempUser->region_id);
                $division_id = (empty($tempUser->division_id) ? $data['division_id'] : $tempUser->division_id);
                $state_id = (empty($tempUser->state_id) ? $data['state_id'] : $tempUser->state_id);
                $social_uid = (empty($tempUser->social_uid) ? $data['social_uid'] : $tempUser->social_uid);
                $user_type = (empty($tempUser->user_type) ? $data['user_type'] : $tempUser->user_type);
            }

            $tempUser->name = $name;
            $tempUser->email = $email;
            $tempUser->secondary_email = $secondary_email;
            $tempUser->username = $data['username'] ? $data['username'] : $username;
            $tempUser->birth_month = $birth_month;
            $tempUser->birth_year = $birth_year;
            $tempUser->country_id = $country_id;
            $tempUser->region_id = $region_id;
            $tempUser->division_id = $division_id;
            $tempUser->state_id = $state_id;
            if (isset($user_type)) {
                $tempUser->user_type = $user_type;
            }

            if (isset($data['social_uid'])) {
                $tempUser->social_uid = $social_uid;
            }
            if (isset($data['registrationtype'])) {
                //$register = $data['registrationtype'];
                $tempUser->registrationtype = $data['registrationtype'];
            }

            if (isset($data['password']) && !empty($data['password'])) {
                $tempUser->password = bcrypt($data['password']);
            }
            $tempUser->is_following = $is_following;
            if ($interests != 'all') {
                $tempUser->interests = $interests;
            } else {
                //$allInterests = SubInterest::where('is_active', '=', 1)->select('id')->get()->toArray();
                $allInterests = InterestCategory::where('is_active', '=', 1)->select('id')->get()->toArray();
                $allInterestsArray = [];
                foreach ($allInterests as $key => $interestId) {
                    $allInterestsArray[] = $interestId['id'];
                }
                //dd($allInterestsArray);
                $interests = implode(',', $allInterestsArray);
                $tempUser->interests = $interests;
            }

            if ($tempUser->save()) {
                if ($data['finish_reg'] == 1) {
                    $tempUser = TempUser::find($tempUser->id);
                    //dd($tempUser);
                    if (User::where('email', '=', $tempUser->email)->exists()) {
                        // user found

                        $response = Api::make_response(0, [], 'User Email Already Exists');
                    } elseif (User::where('username', '=', $tempUser->username)->exists()) {
                        // user found
                        $response = Api::make_response(0, [], 'Username Already Exists');
                    } else {
                        //User create
                        $user = new User;
                        $user->name = $tempUser->name;
                        $user->email = $tempUser->email;
                        $user->secondary_email = $tempUser->secondary_email;
                        $user->username = $tempUser->username;
                        $user->birth_month = $tempUser->birth_month;
                        $user->birth_year = $tempUser->birth_year;
                        $user->password = $tempUser->password;
                        $user->user_image = $tempUser->user_image;
                        $user->registrationtype = $tempUser->registrationtype;
                        $user->user_type = $tempUser->user_type;
                        $user->is_login = 1;
                        if ($user->save()) {

                            /* Save User Image */
                            $userimage = new UserImage;
                            $userimage->user_id = $user->id;
                            $userimage->image_type = "Profile";
                            $userimage->user_image = $tempUser->user_image;
                            $userimage->save();
                            /* Save User Image */

                            $interests = explode(',', $tempUser->interests);
                            $followers = explode(',', $tempUser->is_following);
                            //dd($interests);
                            foreach ($interests as $key => $interest_id) {
                                $newInterest = new Interests;
                                $newInterest->user_id = $user->id;
                                $newInterest->interest_id = $interest_id;

                                if ($interest_id) {
                                    $newInterest->save();
                                }

                                //insert in our new user_category_interest table, we will not use interest table
                                $user_category_interest_arr = [
                                    'interest_cat_id' => $interest_id,
                                    'user_id' => $user->id,
                                    'created_ip' => $request->ip()
                                ];
                                $user_category_interest_check = User_category_interest::where($user_category_interest_arr)->get();
                                if ($user_category_interest_check->count() > 0) {
                                    continue;
                                }
                                User_category_interest::insert($user_category_interest_arr);
                            }
                            foreach ($followers as $key => $follow_id) {
                                $newFollow = new Followers;
                                $newFollow->user_id = $user->id;
                                $newFollow->is_following = $follow_id;
                                if ($follow_id) {
                                    $newFollow->save();
                                    $user->increment('total_follows');
                                }


                                $follow_user = User::find($follow_id);
                                if ($follow_user) {
                                    $follow_user->increment('total_followed_by');
                                }
                            }
                            //$user = User::find($user->id);
                            $user_account_settings = new AccountSettings;
                            $user_account_settings->user_id = $user->id;
                            $user_account_settings->country_id = $tempUser->country_id;
                            $user_account_settings->region_id = $tempUser->region_id;
                            $user_account_settings->division_id = $tempUser->division_id;
                            $user_account_settings->state_id = $tempUser->state_id;
                            $user_account_settings->background_image = $tempUser->background_image;

                            if (isset($tempUser->registrationtype)) {
                                switch ($tempUser->registrationtype) {
                                    case 'facebook':
                                        $user_account_settings->social_link_facebook = "http://facebook.com/" . $tempUser->social_uid;
                                        break;
                                    case 'twitter':
                                        $user_account_settings->social_link_twitter = "http://twitter.com/" . $tempUser->social_uid;
                                        break;
                                    case 'linkedin':
                                        $user_account_settings->social_link_linkedin = $tempUser->social_uid;
                                        break;
                                    default:
                                        break;
                                }
                            }
                            $user_account_settings->save();

                            $apiKey = ApiKey::make($user->id);
                            $user = User::find($user->id);
                            $suggestedFollowers = User::getfollowerSuggetions($user->id, 5);
                            $suggestedFollowersIds = User::getfollowerSuggetionsIds($user->id, 5);
                            Email::sendMail($user->id);

                            if (isset($data['plan_id'])) {
                                $plan_dtails = Plans::find($data['plan_id']);
                                $user_id = $data['user_id'];
                                $expired_date = date('Y-m-d', strtotime("+" . $plan_dtails['duration'] . " days"));

                                $addsubscription = new Subscription;
                                $addsubscription->user_id = $user->id;
                                $addsubscription->package_id = $data['plan_id'];
                                $addsubscription->package_valide_from = date('Y-m-d');
                                $addsubscription->package_valide_to = $expired_date;
                                $addsubscription->is_active = 1;
                                /*
                                  $plan_dtails = Plans::where('type','free')->first();
                                  $user_id = $data['user_id'];
                                  $expired_date = date('Y-m-d', strtotime("+".$plan_dtails['duration']." days"));

                                  $addsubscription = new Subscription;
                                  $addsubscription->user_id = $user->id;
                                  $addsubscription->package_id = $data['plan_id'];
                                  $addsubscription->package_valide_from = date('Y-m-d');
                                  $addsubscription->package_valide_to = $expired_date;
                                  $addsubscription->is_active = 1;
                                 */
                            } else {
                                $plan_dtails = Plans::where('type', 'free')->first();
                                $user_id = $data['user_id'];
                                $expired_date = date('Y-m-d', strtotime("+" . $plan_dtails['duration'] . " days"));

                                $addsubscription = new Subscription;
                                $addsubscription->user_id = $user->id;
                                $addsubscription->package_id = $plan_dtails['id']; //$data['plan_id'];
                                $addsubscription->package_valide_from = date('Y-m-d');
                                $addsubscription->package_valide_to = $expired_date;
                                $addsubscription->is_active = 1;
                                /*
                                  $plan_dtails = Plans::find($data['plan_id']);
                                  $user_id = $data['user_id'];
                                  $expired_date = date('Y-m-d', strtotime("+".$plan_dtails['duration']." days"));

                                  $addsubscription = new Subscription;
                                  $addsubscription->user_id = $user->id;
                                  $addsubscription->package_id = $data['plan_id'];
                                  $addsubscription->package_valide_from = date('Y-m-d');
                                  $addsubscription->package_valide_to = $expired_date;
                                  $addsubscription->is_active = 1;
                                  'subscription'=>$subscription],
                                 */
                            }
                            $addsubscription->save();
                            $subscription['Plansdetails']['plan'] = $plan_dtails;
                            $response = Api::make_response(1, ['is_temporary' => 0, 'api_key' => $apiKey->key, 'user_info' => $user, 'suggested_followers_ids' => $suggestedFollowersIds, 'suggested_followers' => $suggestedFollowers, 'subscription' => $subscription], 'User Data saved');
                        } else {
                            $response = Api::make_response(0, [], 'Data not saved');
                        }
                    }
                } else {
                    $suggestedFollowers = User::getfollowerSuggetionsFromTempUser($tempUser->interests, 5);

                    $suggestedFollowersIds = User::getfollowerSuggetionsFromTempUserIds($tempUser->interests, 5);
                    $response = Api::make_response(1, ['is_temporary' => 1, 'api_key' => '', 'user_info' => $tempUser, 'suggested_followers_ids' => $suggestedFollowersIds, 'suggested_followers' => $suggestedFollowers], 'User Data saved');
                }
            } else {
                $response = Api::make_response(0, [], 'Data not saved');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalid Param . Data not saved');
        }
        //$data['user_id']
        return $response;
    }

    /* To get all interest category by id
      Created By : Debasis Chakraborty
     */

    public function getInterestCategory() {
        $interestCategories = InterestCategory::where('is_active', 1)->get();
        $response = [];
        if ($interestCategories) {
            $response = Api::make_response(1, $interestCategories->toArray(), 'All Active Interest Category Found');
        } else {
            $response = Api::make_response(0, [], 'No Interest Category Found');
        }
        return Api::replace_null_with_empty_string($response);
    }

    /* To get single interest category by id
      Created By : Debasis Chakraborty
     */

    public function getInterestCategoryById($id) {
        $interestCategory = InterestCategory::find($id);
        $response = [];
        if ($interestCategory) {
            $response = Api::make_response(1, $interestCategory->toArray(), 'Interest Category Found');
        } else {
            $response = Api::make_response(0, [], 'Interest Category Not Found');
        }
        return Api::replace_null_with_empty_string($response);
    }

    /* Contact us mail send
      Created By : Dhiman Bhattacharya
     */

    public function contactusmail(Request $request) {
        $data = $request->json()->all();
        $response = [];

        if (!Email::sendcontactusMail($data)) {
            $response = Api::make_response(1, [], 'Cuntact us mail send successfully');
        } else {
            $response = Api::make_response(0, [], 'Something went wrong');
        }
        return $response;
    }

    public function reportchecking(Request $request) {
        $data = $request->json()->all();
        $postid = $data['postid'];
        $loginuserid = $data['loginuserid'];
        $response = [];
        $reportpost = ReportPost::where('post_id', $postid)->where('report_by', $loginuserid)->get();

        //echo json_encode($reportpost);die;
        if (count($reportpost) > 0) {
            $response = Api::make_response(0, [], 'Sorry, You have already report on this post');
        } else {
            $response = Api::make_response(1, [], 'You can report');
        }
        return $response;
    }

    public function getallboostplan() {
        $boostplans = BoostPlans::where('is_active', 1)->get();
        $response = [];
        if (count($boostplans) > 0) {
            $response = Api::make_response(1, $boostplans, 'Boost plan get successfully');
        } else {
            $response = Api::make_response(0, [], 'You can report');
        }
        return $response;
    }

    public function getallboostcount(Request $request) {

        // $data = $request->json()->all();
        // $userid = $data['user_id'];
        // $response =  [];
        // $allow_boost_count = 0 ;
        // $num_of_boost = 0 ;
        // $totalboost = 0;		
        // $subsplan = UserSubcriptionHistory::where('user_id',$userid)->with('getplan')->get();
        // foreach ($subsplan as $key => $value) {
        // 	if (count($value->getplan) > 0) {
        // 		$allow_boost_count += $value->getplan[0]->allow_boost_count;
        // 		$totalboost += $value->getplan[0]->allow_boost_count;
        // 	}			
        // }
        // $userboost = UserBoost::with('userboostplans')->where('user_id',$userid)->get();
        // foreach ($userboost as $key => $value) {
        // 		$num_of_boost += $value->userboostplans->num_of_boost;
        // 		$totalboost += $value->userboostplans->num_of_boost;
        // }
        // $ret = array('allow_boost_count' => $allow_boost_count,'num_of_boost' => $num_of_boost , 'totalboost' => $totalboost);
        // $response =  Api::make_response(1,$ret,'Boost counter');
        // return $response;

        $data = $request->json()->all();
        $userid = $data['user_id'];
        $response = [];
        $usertble = User::find($data['user_id']);
        if (count($usertble) > 0) {
            $response = Api::make_response(1, $usertble, 'Your details');
        } else {
            $response = Api::make_response(0, [0], 'Somthing wrong');
        }

        return $response;
    }

    public function deactiveaccount(Request $request) {
        $data = $request->json()->all();
        $userid = $data['user_id'];
        $response = [];
        $usertble = User::find($data['user_id']);
        $usertble->is_active = 0;
        if ($usertble->save()) {
            $postsmstr = Posts::where("user_id", $data['user_id'])->get();
            foreach ($postsmstr as $key => $value) {
                $updateposts = Posts::find($value['id']);
                $updateposts->is_active = 0;
                $updateposts->save();

                $postLogsMstr = PostLogs::where("post_id", $value['id'])->get();
                if (count($postLogsMstr) > 0) {
                    foreach ($postLogsMstr as $key1 => $value1) {
                        $updatepostLog = PostLogs::find($value1['id']);
                        $updatepostLog->updated_at = $value1['updated_at'];
                        $updatepostLog->is_active = 0;
                        $updatepostLog->save();
                    }
                }
            }

            $allAdBid = AdBid::where("user_id", $data['user_id'])->get();
            if (count($allAdBid) > 0) {
                foreach ($allAdBid as $key2 => $value2) {
                    $updateAdBid = AdBid::find($value2['id']);
                    $updateAdBid->is_active = 0;
                    $updateAdBid->save();
                }
            }

            $allleaderboardLog = LeaderboardLog::where('user_id', $data['user_id'])->get();
            if (count($allleaderboardLog) > 0) {
                foreach ($allleaderboardLog as $key3 => $value3) {
                    $updateLeaderboardLog = LeaderboardLog::find($value3['id']);
                    $updateLeaderboardLog->is_active = 0;
                    $updateLeaderboardLog->save();
                }
            }

            $allFollowers = Followers::where('user_id', $data['user_id'])->orWhere('is_following', $data['user_id'])->get();
            if (count($allFollowers) > 0) {
                foreach ($allFollowers as $key4 => $value4) {
                    $updateFollowers = Followers::find($value4['id']);
                    $updateFollowers->is_active = 0;

                    if ($updateFollowers->user_id == $data['user_id']) {

                        $userupdate = User::find($updateFollowers->is_following);
                        if ($userupdate) {
                            $userupdate->decrement('total_follows');
                            $userupdate->save();
                        }
                    }

                    $updateFollowers->save();
                }
            }
            $allEvents = Events::where('user_id', $data['user_id'])->get();
            if (count($allEvents) > 0) {
                foreach ($allEvents as $key5 => $value5) {
                    $updateEvents = Followers::find($value5['id']);
                    if ($updateEvents) {
                        $updateEvents->is_active = 0;
                        $updateEvents->save();
                    }
                }
            }

            $response = Api::make_response(1, $usertble, 'You account has been deactived! We are waiting to hear from yu soon.');
        } else {
            $response = Api::make_response(0, [0], 'Oops!!!You are not able to deactive your account.');
        }

        return $response;
    }

    public function userdetails(Request $request) {
        $data = $request->json()->all();
        $userid = $data['user_id'];
        $response = [];
        $usertble = User::find($data['user_id'])->total_boost;
        //$usertble = User::find($data['user_id']);
        if (count($usertble) > 0) {
            $response = Api::make_response(1, $usertble, 'Your details');
        } else {
            $response = Api::make_response(0, [0], 'Somthing wrong');
        }

        return $response;
    }

    public function insert_old_interest_data() {
        $interest_data = Interests::get();
        foreach ($interest_data as $interest) {
            $interest_category = SubInterest::where('id', $interest->interest_id)->get();
            foreach ($interest_category as $icat) {
                $insert_arr = [
                    'interest_cat_id' => $icat->interest_category_id,
                    'user_id' => $interest->user_id
                ];
                $check_insert = User_category_interest::where($insert_arr)->get();
                if ($check_insert->count() > 0) {
                    continue;
                }
                User_category_interest::insert($insert_arr);
            }
        }
    }
    
    public function get_jivin_video(){
        $video_detail= Jivin_video::where('id',1)->get();
        $video_path= url('uploads/'.$video_detail[0]['video_file']);
        
        return Api::make_response(1, ['video_link'=>$video_path], "");
        
    }

}
