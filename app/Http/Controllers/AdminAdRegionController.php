<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\AdRegions, App\AdPages;
use \Validator,\Redirect;

class AdminAdRegionController extends Controller
{

		public function __construct(){
			$this->middleware('admin');
		}

		public function listadregion(Request $request){

		$data = AdRegions::orderBy('id','desc')->paginate(5);
		//echo json_encode($data);die;
        return view('admin.adregions.index')->with('data',$data);
		//echo "AdminAdvertiseController";
      }

	  public function delete($id){

			$checkPage = AdPages::where("ad_region_id",$id)->count();
			if($checkPage>0){
				return redirect::back()->with('error', 'Sorry! This region already used in ad Page section');
			}else{
				$adregions = AdRegions::find($id);
				if($adregions->delete()){
					return Redirect::back()->with('success','Ad regions deleted successfully');
				}else{
					return redirect::back()->with('error', 'Something wrong in the system');
				}
			}
			/*
			*/

	  }

		function slugit($str, $replace=array(), $delimiter='-') {
				
			if ( !empty($replace) ) {
			$str = str_replace((array)$replace, ' ', $str);
			}
			$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
			$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
			$clean = strtolower(trim($clean, '-'));
			$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
			return $clean;
		}

      public function postAdd(Request $request){

      		$hiddenid = $request->id;

        // $validator                    = Validator::make($request->all(), [
		// 		'name' => 'required',
		// 		'image' => 'required',
		// 		'height' => 'required|numeric',
		// 		'width' => 'required|numeric',
		// 		'css_class'=> 'required',
		// 		'is_active' => 'required',
        //                                                                ]);
        // if($validator->fails()){
        //     $messages = $validator->messages();
        //     return Redirect::back()->withErrors($validator)->withInput();
        // }else{


      		if($hiddenid!=''){

				$validator                    = Validator::make($request->all(), [
						'name' => 'required',
						//'image' => 'required',
						'height' => 'required|numeric',
						'width' => 'required|numeric',
						'css_class'=> 'required',
						'is_active' => 'required',
																			]);
				if($validator->fails()){
					$messages = $validator->messages();
					return Redirect::back()->withErrors($validator)->withInput();
				}else{
						
      			$dataEdit = AdRegions::find($hiddenid);
      			$dataEdit->name = $request->name;
      			$dataEdit->slug = $this->slugit($request->name);
      			$dataEdit->height = $request->height;

				if($request->image){

					$filename = substr( md5( time() ), 0, 15) . '.jpg'; 
					$path = public_path('ad_region/' . $filename);

					if (move_uploaded_file($_FILES["image"]["tmp_name"],$path)){
						$dataEdit->image = $filename;
					}
				}
      			$dataEdit->width = $request->width;
      			$dataEdit->status = $request->is_active;
				$dataEdit->css_class = $request->css_class;
      			if($dataEdit->save()){
      				return Redirect::back()->with('success','Ad regions Edited successfully');
      			}else{
      				return redirect::back()->with('error', 'Something wrong in the system');
      			}
				}
      		}else{

$validator                    = Validator::make($request->all(), [
						'name' => 'required',
						'image' => 'required',
						'height' => 'required|numeric',
						'width' => 'required|numeric',
						'css_class'=> 'required',
						'is_active' => 'required',
																			]);
				if($validator->fails()){
					$messages = $validator->messages();
					return Redirect::back()->withErrors($validator)->withInput();
				}else{

      			$dataInsert = new AdRegions;
      			$dataInsert->name = $request->name;
      			$dataInsert->slug = $this->slugit($request->name);
      			$dataInsert->height = $request->height;
      			$dataInsert->width = $request->width;
      			$dataInsert->status = $request->is_active;
				$dataInsert->css_class = $request->css_class;


				if($request->image){

					$filename = substr( md5( time() ), 0, 15) . '.jpg'; 
					$path = public_path('ad_region/' . $filename);

					if (move_uploaded_file($_FILES["image"]["tmp_name"],$path)){
						$dataInsert->image = $filename;
					}
				}

      			if($dataInsert->save())
      			{
      				return Redirect::back()->with('success','Ad regions Added successfully');
      			}else{
      				return redirect::back()->with('error', 'Something wrong in the system');
      			}
      		}
      	}
  	}
}

