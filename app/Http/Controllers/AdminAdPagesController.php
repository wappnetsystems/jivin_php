<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\AdRegions, App\AdPages, App\AdPriceMaster, App\Country, App\Region, App\State;
use \Validator,\Redirect;

class AdminAdPagesController extends Controller
{
  public function __construct(){
    $this->middleware('admin');
  }

	public function listadpage(Request $request){
		$data = array();
		$data['allregion'] = AdRegions::get();
		$data['list'] = AdPages::orderBy('id','desc')->paginate(5);
		//echo json_encode($data);die;
        return view('admin.adpage.index')->with('data',$data);
    }

    public function details($id,Request $request){
		$data = array();
		$data['allpage'] = AdPages::get();
		//$data['country'] = Country::get();
    //$allcountry = Country::select('id')->get()->makeHidden(['img_url'])->toArray();
//print_r($allcountry[0]['id']);die;
    //$allregion = Region::select('id')->whereIn('id', $allcountry[0]['id'])->get()->toArray();
    //$allregion = Region::get()->toArray();
    //print_r($allregion);die;

    //print_r($allregion);
    $data['state'] = State::get();

    /*
    $i= 0;
    $stateArray = array();
    $allcountry = Country::get();
    foreach ($allcountry as $key => $value) {
        //echo json_encode($value->regions);
        $country_id = $value['id'];
        foreach ($value->regions as $key => $value1) {
          //echo json_encode($value1->divisions);
          $region_id = $value1['id'];
          foreach ($value1->divisions as $key => $value2) {
            //echo json_encode($value2->states);
            $stateArray[$i]['state'] = $value2->states;
            $stateArray[$i]['state']['regions'] = $region_id;
            $stateArray[$i]['state']['country'] = $country_id;
            //array_push();
            //echo $value2['id']."<br>";
            /*
            $stateArray[$i]['state']['id'] = $value2->states->id;
            $stateArray[$i]['state']['divisions'] = $value2->states['division_id'];
            $stateArray[$i]['state']['name'] = $value2->states['name'];
            $stateArray[$i]['state']['slug'] = $value2->states['slug'];

             $i =  $i + 1;
          }
        }
    }
    $data['stateArray']= $stateArray;
    //echo json_encode($data);die;
    //echo "<pr>";print_r($data);die;
            */
		$data['list'] = AdPriceMaster::where('ad_page_id',$id)->orderBy('id','desc')->paginate(5);
		//echo json_encode($data);die;
        return view('admin.adpage.details')->with('data',$data);
    }
    
    public function ad_price_details(Request $request){

      $ad_page_id = $request->ad_page_id;
      $hiddenid = $request->id;


        $validator                    = Validator::make($request->all(), [
        'to_time' => 'required',
        'from_time' => 'required',
        'state' => 'required',
        'min_bid' => 'required|numeric',
        'buy_now_price' => 'required|numeric',
        'is_active' => 'required',
                                                                       ]);
        if($validator->fails()){
            $messages = $validator->messages();
            return Redirect::back()->withErrors($validator)->withInput();
        }else{

            if($hiddenid!=''){

              $dataEdit = AdPriceMaster::find($hiddenid);
              $dataEdit->ad_page_id = $ad_page_id;
              $dataEdit->state_id = $request->state;
              $dataEdit->to_time = $request->to_time;
              $dataEdit->from_time = $request->from_time;
              $dataEdit->min_bid = $request->min_bid;
              $dataEdit->buy_now_price = $request->buy_now_price;
              $dataEdit->status = $request->is_active;   
              //$dataInsert->save();

              if($dataEdit->save()){
                return Redirect::back()->with('success','Ad Price Master Updated successfully');
              }else{
                return redirect::back()->with('error', 'Something wrong in the system');
              }
              //echo "Edit";
            }else{
              $dataInsert = new AdPriceMaster;
              $dataInsert->ad_page_id = $ad_page_id;
              $dataInsert->state_id = $request->state;
              $dataInsert->to_time = $request->to_time;
              $dataInsert->from_time = $request->from_time;
              $dataInsert->min_bid = $request->min_bid;
              $dataInsert->buy_now_price = $request->buy_now_price;
              $dataInsert->status = $request->is_active;   
              $dataInsert->save();
              if($dataInsert->save())
              {
                return Redirect::back()->with('success','Ad Price Master Added successfully');
              }else{
                return redirect::back()->with('error', 'Something wrong in the system');
              }
              //echo "Add ".$ad_page_id;
            }
        }


    }


    public function postAdd(Request $request){

      	$hiddenid = $request->id;

        $validator                    = Validator::make($request->all(), [
				'pageurl' => 'required',
				'region' => 'required',
        'page_name' => 'required',
				'min_bid' => 'required|numeric',
				'buy_now_price' => 'required|numeric',
				'is_active' => 'required',
                                                                       ]);
        if($validator->fails()){
            $messages = $validator->messages();
            return Redirect::back()->withErrors($validator)->withInput();
        }else{

      		if($hiddenid!=''){

      			$dataEdit = AdPages::find($hiddenid);
      			$dataEdit->ad_region_id = $request->region;
      			$dataEdit->page_url = $request->pageurl;
            $dataEdit->page_name = $request->page_name;
      			$dataEdit->min_bid = $request->min_bid;
      			$dataEdit->buy_now_price = $request->buy_now_price;
      			$dataEdit->status = $request->is_active;
      			if($dataEdit->save()){
      				return Redirect::back()->with('success','Ad pages Edited successfully');
      			}else{
      				return redirect::back()->with('error', 'Something wrong in the system');
      			}
      		}else{

      			$dataInsert = new AdPages;
      			$dataInsert->ad_region_id = $request->region;
      			$dataInsert->page_url = $request->pageurl;
            $dataInsert->page_name = $request->page_name;
      			$dataInsert->min_bid = $request->min_bid;
      			$dataInsert->buy_now_price = $request->buy_now_price;
      			$dataInsert->status = $request->is_active;
      			if($dataInsert->save())
      			{
      				return Redirect::back()->with('success','Ad pages Added successfully');
      			}else{
      				return redirect::back()->with('error', 'Something wrong in the system');
      			}
      		}
      	}
  	}
}
