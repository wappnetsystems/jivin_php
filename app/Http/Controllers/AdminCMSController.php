<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Cms;

class AdminCMSController extends Controller
{

    public function __construct(){
    	$this->middleware('admin');
    }
      public function listCms(){
        return view('admin.cms.index');
      }
      public function postAddNewCms(Request $request){
       // echo $request->page_title;exit();
      	//dd($_POST);
      	if ($request->id) {
      		$cms = Cms::where('id','=',$request->id)->first();
	      	if (Cms::where('page_title','=',$request->page_title)->where('id','!=',$request->id)->first()) {
	      		return redirect('admin/cms/list')->with('alert-danger', 'Cms Already Exists');
	      	}  		
	      	$cms->page_title = str_replace(' ', '_', $request->page_title);
	      	$cms->page_slug = str_replace(' ', '_', $request->page_title);
	      	$cms->page_html = $request->page_html;
	      	$cms->is_active = $request->is_active;
	      	if ($cms->save()) {
	      		return redirect('admin/cms/list')->with('alert-success', 'Cms Updated!');
	      	}else{
      		   return redirect('admin/cms/list')->with('alert-danger', 'Cms Updation faild');	      		
	      	}      		
      	}else{
	      	$cms = new Cms;
	      	if ($cms::where('page_title','=',$request->page_title)->first()) {
	      		return redirect('admin/cms/list')->with('alert-danger', 'Cms Already Exists');
	      	}else{
	      		// echo $request->page_title;exit();
	      	$cms->page_title = str_replace(' ', '_', $request->page_title);
	      	$cms->page_slug = str_replace(' ', '_', $request->page_title);
	      	$cms->page_html = $request->page_html;
	      	$cms->is_active = $request->is_active;
		      	if ($cms->save()) {
		      		return redirect('admin/cms/list')->with('alert-success', 'New Cms Created!');
		      	}else{
	      		   return redirect('admin/cms/list')->with('alert-danger', 'Cms Creation faild');	      		
		      	}
	      	}
        }
      }      
      public function getList(){
      	    $output['aaData'] = [];
			$aColumns = array(
				                 'id'
			                    ,'page_title'
			                    ,'page_html'
			                    ,'is_active'
								,'id'
							);
			$countries = Cms::get()->toArray();
			// /dd($countries);
			foreach ($countries as $key => $cms) {
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if($cms[ $aColumns[$i]]=== NULL){$cms[ $aColumns[$i]]= "";}
						$row[] = $cms[ $aColumns[$i] ];

				}
				$output['aaData'][] = $row;
			}
			$output = array(
			    "draw"            => intval(1),
			    "data"            => $output['aaData']
			); 	
      	return $output;
      }    
      public function updateStatus(Request $request){
      	$cms = Cms::where('id','=',$request->id)->first();
      	$cms->is_active = $request->status;
      	if ($cms->save()) {
      		return 'true';
      	}else{
  		   return 'false';	      		
      	}      	
      }        
}
