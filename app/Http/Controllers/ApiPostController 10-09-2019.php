<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
//load Models
use App\Api;
use App\User;
use App\Posts;
use App\PostLogs;
use App\PostLikeLog;
use App\PostShareLog;
use App\LeaderboardLog;
use App\NotificationLog;
use App\Followers;
use App\PostBoost;
use Illuminate\Support\Facades\Input;

class ApiPostController extends ApiGuardController {

    protected $apiMethods = [
        // 'getCountryList' => [
        //     'keyAuthentication' => false
        // ]               
        'deleteyourpost' => [
            'keyAuthentication' => false
        ],
        'changepostpermission' => [
            'keyAuthentication' => false
        ]
    ];

    public function add_post(Request $request) {
        $data = $request->json()->all();
        $response = [];
        if (isset($data['user_id']) && User::where('id', '=', $data['user_id'])->first()) {
            $post = new Posts;
            $post->user_id = $data['user_id'];
            $post->text_details = $data['post_description'];
            if ($post->save()) {
                PostLogs::logPosts($post->id, $data['user_id'], 'self');
                $response = Api::make_response(1, ['post_id' => $post->id], 'New Post Added');
            } else {
                $response = Api::make_response(0, [], 'Something wrong! Please try again');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide user id');
        }

        return $response;
    }

    public function share_post(Request $request) {
        $data = $request->json()->all();
        $response = [];
        if (isset($data['user_id']) && User::where('id', '=', $data['user_id'])->first()) {
            if (isset($data['post_id']) && $PostsDetails = Posts::where('id', '=', $data['post_id'])->first()) {
                // if (PostShareLog::where('post_id','=',$data['post_id'])->where('shared_by','=',$data['user_id'])->first()) {
                //   return $response =  Api::make_response(0,[],'Post Already Shared');
                // }
                $PostsDetails->increment('total_share');
                $postShareLog = new PostShareLog;
                $postShareLog->post_id = ($PostsDetails->org_post_id) ? $PostsDetails->org_post_id : $data['post_id'];
                $postShareLog->shared_by = $data['user_id'];
                $postShareLog->save();
                //PostLogs::logPosts($data['post_id'],$data['user_id'],'shared'); old code
                $postShared = new Posts;
                $postShared->user_id = $data['user_id'];
                $postShared->text_details = $PostsDetails->text_details;
                $postShared->audio_name = $PostsDetails->audio_name;
                $postShared->video_name = $PostsDetails->video_name;
                $postShared->is_adult = $PostsDetails->is_adult;
                $postShared->total_like = 0;
                $postShared->total_share = 0;
                $postShared->is_active = $PostsDetails->is_active;
                $postShared->is_public = 2;
                $postShared->shared_id = $PostsDetails->user_id;
                $postShared->org_post_id = ($PostsDetails->org_post_id) ? $PostsDetails->org_post_id : $data['post_id'];
                $postShared->is_shared = 1;
                $postShared->save();


                $response = Api::make_response(1, ['total_share' => $PostsDetails->total_share], 'Post Shared');
            } else {
                $response = Api::make_response(0, [], 'Invalide post id');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide user id');
        }

        return $response;
    }

    public function like_post(Request $request) {
        $data = $request->json()->all();
        $response = [];
        if (isset($data['user_id']) && User::where('id', '=', $data['user_id'])->first()) {
            if (isset($data['post_id']) && $post = Posts::where('id', '=', $data['post_id'])->first()) {
                if ($post->is_shared) {
                    $post = Posts::find($post->org_post_id);
                }
                if (PostLikeLog::where('post_id', '=', $post->id)->where('liked_by', '=', $data['user_id'])->first()) {
                    $userDetails = User::where('id', '=', $data['user_id'])->first();
                    return $response = Api::make_response(0, ['total_votes' => $userDetails->total_votes, 'total_audio_votes' => $userDetails->total_audio_votes, 'total_video_votes' => $userDetails->total_video_votes], 'Post Already Liked');
                }
                if (LeaderboardLog::addLog($post->user_id) && $post->increment('total_like')) {
                    $postLikeLog = new PostLikeLog;
                    $postLikeLog->post_id = $post->id;
                    $postLikeLog->liked_by = $data['user_id'];
                    $postLikeLog->save();
                    $userDetails = User::where('id', '=', $data['user_id'])->first();

                    $response = Api::make_response(1, ['like_count' => $post->total_like, 'total_votes' => $userDetails->total_votes, 'total_audio_votes' => $userDetails->total_audio_votes, 'total_video_votes' => $userDetails->total_video_votes], 'Post Linked');
                    NotificationLog::sendNotificationOnLike($post->id, $data['user_id']);
                } else {
                    $response = Api::make_response(0, [], 'Please try again .  Error occoured. Please update user location');
                }
            } else {
                $response = Api::make_response(0, [], 'Invalide post id');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide user id');
        }

        return $response;
    }

    public function arrayPaginator($array, $request) {
        $page = Input::get('page', 1);
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
                ['path' => $request->url(), 'query' => $request->query()]);
    }

    public function getPosts(Request $request) {
        $data = $request->json()->all();
        $response = [];
        $page = $data['page'];
        $itemsPerPage = $data['itemsPerPage'];     //user_state

        $getPosts = [];

        if (isset($data['user_id']) && User::where('id', '=', $data['user_id'])->first()) {
            $user_state_id_sql = DB::select("select state_id from user_account_settings where user_id = '" . $data['user_id'] . "'");
            if (count($user_state_id_sql) > 0) {

                $user_state_id = $user_state_id_sql[0]->state_id;
            }
            // echo $user_state_id;
            // die;

            /* $queryOne = Posts::leftJoin('post_boost', 'posts.id', '=', 'post_boost.post_id')
              //->select(DB::raw('posts.*,post_boost.created_at as abc'))
              ->select('posts.*')
              ->where('post_boost.boost_location_id',30);

              $queryTwo = Posts::leftJoin('followers', 'posts.user_id', '=', 'followers.user_id')
              //->select(DB::raw('posts.*,posts.created_at AS abc'))
              ->select('posts.*')
              ->where(function ($query) use($data) {
              $query->where('followers.is_following',$data['user_id'])
              ->where('posts.is_public','!=',0);
              })
              ->orwhere(function ($query) use($data) {
              $query->where('posts.user_id',$data['user_id']);
              })
              ->groupBy('posts.id')

              ->orderBy('posts.id','desc')
              ->paginate($itemsPerPage)->toArray(); */

            //$getPosts = $queryOne->union($queryTwo)->orderBy('posts.created_at','desc')->paginate($itemsPerPage)->toArray();


            $getPosts = DB::select("(select posts.*,posts.id as boost_id,posts.user_id as boost_by,posts.created_at as abc from `posts` left join `followers` on `posts`.`user_id` = `followers`.`is_following` where `posts`.`is_active`= 1 and (`followers`.`user_id` = " . $data['user_id'] . " and `posts`.`is_public` != 0) or (`posts`.`user_id` = " . $data['user_id'] . ") group by `posts`.`id`) 
            union 
            (select posts.*,post_boost.id as boost_id,post_boost.user_id as boost_by,post_boost.created_at as abc from `posts` left join `post_boost` on `posts`.`id` = `post_boost`.`post_id` where `post_boost`.`boost_location_id` = '" . $user_state_id . "' and `posts`.`is_active`= 1)
            order by `abc` desc");

            $getPosts = $this->arrayPaginator($getPosts, $request)->toArray();




            $getPosts = json_decode(json_encode($getPosts), true);
            //echo "<pre>";print_r($getPosts['data']);die;

            $finalArray = [];
            if (isset($getPosts['data']) && !empty($getPosts['data'])) {
                $i = 0;
                foreach ($getPosts['data'] as $key => $value) {
                    $finalArray[$i] = $value;
                    $finalArray[$i]['is_own_boost']=0;
                    if ($value['is_boost'] == 1) {
                        $userdetails = User::find($value['boost_by']);
                        $postofdetails = User::find($value['user_id']);
                        $finalArray[$i]['post_of_img'] = $postofdetails['user_image_url'];
                        $finalArray[$i]['post_of_name'] = $postofdetails['name'];
                        $finalArray[$i]['post_of_user_id'] = $postofdetails['id'];
                        $finalArray[$i]['post_of_user_name'] = $postofdetails['username'];
                        if($value['boost_by']==$value['user_id']){
                            $finalArray[$i]['is_own_boost']=1;
                        }
                        else{
                            $finalArray[$i]['is_own_boost']=0;
                        }
                    } else {
                        $userdetails = User::find($value['user_id']);
                    }

                    if ($userdetails) {
                        $finalArray[$i]['created_by_img'] = $userdetails['user_image_url'];
                        $finalArray[$i]['created_by_name'] = $userdetails['name'];
                        $finalArray[$i]['created_by_user_id'] = $userdetails['id'];
                        $finalArray[$i]['created_by_user_name'] = $userdetails['username'];
                    }

                    if ($value['is_shared'] == 0) {
                        // if($value['user_id'] == $data['user_id'] ){
                        //     if($value['is_boost']==1){
                        //       $finalArray[$i]['display_text'] = " has boosted this post";
                        //     }else{
                        //       $finalArray[$i]['display_text'] = " added a new post";
                        //     }
                        // }else{
                        //   $finalArray[$i]['display_text'] = " added a new post";
                        // }
                        if ($value['is_boost'] == 1) {

                            $finalArray[$i]['display_text'] = " has boosted this post";
                        } else {
                            $finalArray[$i]['display_text'] = " added a new post";
                        }
                    } else {
                        $org_post_user = (Posts::find($value['org_post_id'])) ? Posts::find($value['org_post_id'])->user_id : $value['user_id'];
                        $sharedname = User::find($org_post_user)->name;

                        if ($value['is_boost'] == 1 && $value['boost_id']!=$value['id']) {
                            $finalArray[$i]['display_text'] = " has boosted this post ";
                        } else {
                            $finalArray[$i]['display_text'] = " shared " . $sharedname . "'s post";
                        }
                    }

                    if ($value['video_name'] != '') {

                        $finalArray[$i]['video_url'] = env('IMG_URL') . 'post/' . $value['video_name'];
                        $finalArray[$i]['media_type'] = "video";
                        $finalArray[$i]['audio_url'] = "";
                    } else if ($value['audio_name'] != '') {
                        $finalArray[$i]['audio_url'] = env('IMG_URL') . 'post/' . $value['audio_name'];
                        $finalArray[$i]['video_url'] = "";
                        $finalArray[$i]['media_type'] = "audio";
                    } else {
                        $finalArray[$i]['media_type'] = "";
                        $finalArray[$i]['video_url'] = "";
                        $finalArray[$i]['audio_url'] = "";
                    }
                    //env('IMG_URL').'post/'.

                    $post_id = ($value['org_post_id']) ? $value['org_post_id'] : $value['id'];

                    $finalArray[$i]['time_ago'] = $this->timeAgo($value['abc']);
                    $finalArray[$i]['total_vote'] = PostLikeLog::where('post_id', $post_id)->get()->count();
                    $finalArray[$i]['post_id'] = $key + 1;

                    if (PostLikeLog::where('post_id', '=', $post_id)->where('liked_by', '=', $data['user_id'])->first()) {
                        $finalArray[$i]['is_liked'] = true;
                    } else {
                        $finalArray[$i]['is_liked'] = false;
                    }
                    if (PostShareLog::where('post_id', '=', $value['id'])->where('shared_by', '=', $data['user_id'])->first()) {
                        $finalArray[$i]['is_shared'] = true;
                    } else {
                        $finalArray[$i]['is_shared'] = false;
                    }
                    $i++;
                }
                $getPosts["data"] = $finalArray;
                $response = Api::make_response(1, [$getPosts], 'Posts Found');
            } else {
                $response = Api::make_response(0, [], 'Posts not Found');
            }
            //$response =  Api::make_response(1,$getPosts,'Invalide user id');
        } else {
            $response = Api::make_response(0, [], 'Invalide user id');
        }
        return $response;
    }

    public function timeAgo($time_ago) {
        $time_ago = strtotime($time_ago);
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        }
        //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        }
        //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        }
        //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        }
        //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "$months months ago";
            }
        }
        //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }

    public function getPostsOld(Request $request) {
        $data = $request->json()->all();
        $response = [];
        $page = $data['page'];
        $itemsPerPage = $data['itemsPerPage'];
        if (isset($data['user_id']) && User::where('id', '=', $data['user_id'])->first()) {
            //dd($posts);
            //$getPosts = PostLogs::where('visible_by','=',$data['user_id'])->where('is_active','=',1)->orderBy('boost_order','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();//Old
            $getPosts = PostLogs::where(function ($query) use($data) {
                                $query->where('visible_by', '=', $data['user_id'])
                                ->where('is_active', '=', 1)
                                ->Where('is_public', '!=', 0);
                            })
                            ->orwhere(function ($query) use($data) {
                                $query->where('visible_by', '=', $data['user_id'])
                                ->where('created_by_user_id', '=', $data['user_id'])
                                ->where('is_active', '=', 1)
                                ->whereIn('is_public', [1, 2, 0]);
                            })

                            // ->where('visible_by','=',$data['user_id'])
                            // ->Where('is_public','!=',0)
                            ->orderBy('updated_at', 'DESC')->paginate($itemsPerPage)->toArray(); //new for boost 25-07
            //echo "<pre>";print_r($getPosts);die;
            $finalArray = [];
            if (isset($getPosts['data']) && !empty($getPosts['data'])) {
                $i = 0;
                foreach ($getPosts['data'] as $key => $value) {
                    $finalArray[$i] = $value;
                    if (PostLikeLog::where('post_id', '=', $value['post_id'])->where('liked_by', '=', $data['current_user_id'])->first()) {
                        $finalArray[$i]['is_liked'] = true;
                    }
                    if (PostShareLog::where('post_id', '=', $value['post_id'])->where('shared_by', '=', $data['current_user_id'])->first()) {
                        $finalArray[$i]['is_shared'] = true;
                    }
                    $i++;
                }
                $getPosts["data"] = $finalArray;
                $response = Api::make_response(1, [$getPosts], 'Posts Found');
            } else {
                $response = Api::make_response(0, [], 'Posts not Found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide user id');
        }

        return $response;
    }

    public function postAudioVideo(Request $request) {

        $response = [];

        if ($request->audio_video) {

            $temp_file = $_FILES['audio_video']['tmp_name'];
            $time = exec("ffmpeg -i $temp_file 2>&1 | grep Duration | cut -d ' ' -f 4 | sed s/,//");

            $parsed = date_parse($time);
            $seconds = (isset($parsed['hour']) ? ($parsed['hour'] * 3600) : 0) + (isset($parsed['minute']) ? ($parsed['minute'] * 60) : 0) + $parsed['second'];
            if ($seconds > 181) {
                return Api::make_response(0, [], 'Your file duration is greter than 3 minutes. Please upload a file within 3 minutes.');
            }

            if ($request->type == 'audio') {
                // $ext = ($_FILES["audio_video"]["name"]=='null' || empty($_FILES["audio_video"]["name"])) ? '.mp3' : ('.'.pathinfo($_FILES["audio_video"]["name"], PATHINFO_EXTENSION));
                $ext = '.mp3';
            } else {
                $ext = '.mp4';
            }

            $temp = $request->user_id . '-' . substr(md5($request->user_id . '-' . time()), 0, 15);
            $filename = $temp . $ext;
            $post_path = public_path('post/' . $filename);
            // $path = public_path('post/' . $filename);
            // if (move_uploaded_file($_FILES["audio_video"]["tmp_name"],$path)){
            exec("ffmpeg -i $temp_file $post_path");
            //exec("ffmpeg -i $temp_file -vf scale=400:320,setsar=1 $post_path");
            //var_dump($output); die();
            if (file_exists($post_path)) {
                /*
                  if( $request->type != 'audio') {
                  $filename = $temp.'.h264.mp4';
                  // $command = 'ffmpeg -i '.$path.' -an -vcodec libx264 -crf 23 '.public_path('post/' . $filename);
                  $command = 'ffmpeg -i '.$path.' -f mp4 -vcodec libx264 -preset slow -profile:v main -acodec aac -crf 25 '.public_path('post/' . $filename).' -hide_banner';
                  exec( $command);
                  } */

                $post = new Posts;
                $post->user_id = $request->user_id;
                $post->text_details = $request->post_description;
                if ($request->permission == "public") {
                    $post->is_public = "1";
                } elseif ($request->permission == "private") {
                    $post->is_public = "0";
                } elseif ($request->permission == "all") {
                    $post->is_public = "2";
                }

                if ($request->fileuploadtype == 1) {

                    if ($request->type == 'audio') {
                        $post->audio_name = ($request->filename) ? $request->filename : $filename;
                    } else {
                        $post->video_name = ($request->filename) ? $request->filename : $filename;
                    }

                    $post->save();
                }
                if (isset($request->user_id) && User::where('id', '=', $request->user_id)->first()) {
                    //PostLogs::logPosts($post->id,$request->user_id,'self'); Old Code
                    $response = Api::make_response(1, ['filename' => $filename], 'File Uploaded');
                } else {
                    $response = Api::make_response(0, [], 'Invalid user');
                }
            }
        } else {

            $post = new Posts;
            $post->user_id = $request->user_id;
            $post->text_details = $request->post_description;

            if ($request->permission == "public") {
                $post->is_public = "1";
            } elseif ($request->permission == "private") {
                $post->is_public = "0";
            } elseif ($request->permission == "all") {
                $post->is_public = "2";
            }

            if ($request->type == 'audio') {
                $post->audio_name = $request->filename;
            } else {
                $post->video_name = $request->filename;
            }
            if ($post->save()) {
                //PostLogs::logPosts($post->id,$request->user_id,'self'); Old code
                $response = Api::make_response(1, ['post_id' => $post->id], 'New Post Added');
            } else {
                $response = Api::make_response(0, [], 'Something wrong! Please try again');
            }
        }

        return $response;
    }

    public function changepostpermission(Request $request) {
        $data = $request->json()->all();
        $response = [];

        if ($data) {

            if ($data['permission'] != '' && $data['post_id'] != '') {

                //if($data['permission'] == 3){
                $data['permission'] = ($data['permission'] == 3) ? 0 : $data['permission'];
                //}
                $getPostLogs = PostLogs::where('created_by_user_id', $data['user_id'])->where('post_id', $data['post_id'])->get();
                if (count($getPostLogs) > 0) {
                    //$getPostLogs->timestamps = false;
                    foreach ($getPostLogs as $key => $value) {
                        //PostLogs->timestamps = false;
                        $updatePostLogs = PostLogs::find($value->id);
                        $updatePostLogs->timestamps = false;
                        //$updatePostLogs->updated_at = false;
                        //$updatePostLogs->updated_at = $value->updated_at;
                        $updatePostLogs->is_public = $data['permission'];
                        $updatePostLogs->save();
                    }
                }

                $updatePosts = Posts::find($data['post_id']);

                if ($updatePosts) {

                    $updatePosts->timestamps = false;
                    $updatePosts->is_public = $data['permission'];
                    $updatePosts->save();
                }
            }

            $response = Api::make_response(1, ['post_id' => $getPostLogs], 'Post permission chaged successfully');
        } else {
            $response = Api::make_response(0, [], 'Something wrong! Please try again');
        }

        return $response;
    }

    public function deleteyourpost(Request $request) {
        $data = $request->json()->all();

        $response = [];

        $postsmstr = Posts::where("id", $data['postid'])->where("user_id", $data['loginuserid'])->first();

        if ($postsmstr) {

            $countpostBoost = PostBoost::where('post_id', $data['postid'])->count();
            $countShareLog = PostShareLog::select('id')->where('post_id', $data['postid'])->count();
            if (($countpostBoost > 0) || ($countShareLog > 0)) {
                $response = Api::make_response(0, [], 'Sorry!!! You are not able to delete this post');
            } else {

                $deletePosts = Posts::find($data['postid']);

                $deletePosts->delete();
                $response = Api::make_response(1, [], 'Your post deleted successfully');
            }
        } else {
            $response = Api::make_response(0, [], 'Sorry!!! You are not able to delete this post');
        }
        return $response;
    }

    public function testPost(Request $request) {


        $data = $request->json()->all();
        $response = [];
        $page = $data['page'];
        $itemsPerPage = $data['itemsPerPage'];     //user_state

        $getPosts = [];

        if (isset($data['user_id']) && User::where('id', '=', $data['user_id'])->first()) {
            $user_state_id_sql = DB::select("select state_id from user_account_settings where user_id = '" . $data['user_id'] . "'");
            if (count($user_state_id_sql) > 0) {

                $user_state_id = $user_state_id_sql[0]->state_id;
            }
            // echo $user_state_id;
            // die;

            /* $queryOne = Posts::leftJoin('post_boost', 'posts.id', '=', 'post_boost.post_id')
              //->select(DB::raw('posts.*,post_boost.created_at as abc'))
              ->select('posts.*')
              ->where('post_boost.boost_location_id',30);

              $queryTwo = Posts::leftJoin('followers', 'posts.user_id', '=', 'followers.user_id')
              //->select(DB::raw('posts.*,posts.created_at AS abc'))
              ->select('posts.*')
              ->where(function ($query) use($data) {
              $query->where('followers.is_following',$data['user_id'])
              ->where('posts.is_public','!=',0);
              })
              ->orwhere(function ($query) use($data) {
              $query->where('posts.user_id',$data['user_id']);
              })
              ->groupBy('posts.id')

              ->orderBy('posts.id','desc')
              ->paginate($itemsPerPage)->toArray(); */

            //$getPosts = $queryOne->union($queryTwo)->orderBy('posts.created_at','desc')->paginate($itemsPerPage)->toArray();


            $getPosts = DB::select("(select posts.*,posts.created_at as abc from `posts` left join `followers` on `posts`.`user_id` = `followers`.`is_following` where `posts`.`is_active`= 1 and (`followers`.`user_id` = " . $data['user_id'] . " and `posts`.`is_public` != 0) or (`posts`.`user_id` = " . $data['user_id'] . ") group by `posts`.`id`) 
        union 
        (select posts.*,post_boost.created_at as abc from `posts` left join `post_boost` on `posts`.`id` = `post_boost`.`post_id` where `post_boost`.`boost_location_id` = '" . $user_state_id . "' and `posts`.`is_active`= 1)
        order by `abc` desc");

            $getPosts = $this->arrayPaginator($getPosts, $request)->toArray();




            $getPosts = json_decode(json_encode($getPosts), true);
            //echo "<pre>";print_r($getPosts['data']);die;

            $finalArray = [];
            if (isset($getPosts['data']) && !empty($getPosts['data'])) {
                $i = 0;
                foreach ($getPosts['data'] as $key => $value) {
                    $finalArray[$i] = $value;
                    $userdetails = User::find($value['user_id']);
                    if ($userdetails) {
                        $finalArray[$i]['created_by_img'] = $userdetails['user_image_url'];
                        $finalArray[$i]['created_by_name'] = $userdetails['name'];
                        $finalArray[$i]['created_by_user_id'] = $userdetails['id'];
                        $finalArray[$i]['created_by_user_name'] = $userdetails['username'];
                    }

                    if ($value['is_shared'] == 0) {
                        if ($value['user_id'] == $data['user_id']) {
                            if ($value['is_boost'] == 1) {
                                $finalArray[$i]['display_text'] = " has boosted this post";
                            } else {
                                $finalArray[$i]['display_text'] = " added a new post";
                            }
                        } else {
                            $finalArray[$i]['display_text'] = " added a new post";
                        }
                    } else {
                        $sharedname = User::find($value['shared_id'])->name;
                        $finalArray[$i]['display_text'] = " shared " . $sharedname . "'s post";
                    }

                    if ($value['video_name'] != '') {

                        $finalArray[$i]['video_url'] = env('IMG_URL') . 'post/' . $value['video_name'];
                        $finalArray[$i]['media_type'] = "video";
                        $finalArray[$i]['audio_url'] = "";
                    } else if ($value['audio_name'] != '') {
                        $finalArray[$i]['audio_url'] = env('IMG_URL') . 'post/' . $value['audio_name'];
                        $finalArray[$i]['video_url'] = "";
                        $finalArray[$i]['media_type'] = "audio";
                    } else {
                        $finalArray[$i]['media_type'] = "";
                        $finalArray[$i]['video_url'] = "";
                        $finalArray[$i]['audio_url'] = "";
                    }
                    //env('IMG_URL').'post/'.

                    $finalArray[$i]['time_ago'] = $this->timeAgo($value['abc']);
                    $finalArray[$i]['total_vote'] = $value['total_like'];
                    $finalArray[$i]['post_id'] = $key + 1;






                    if (PostLikeLog::where('post_id', '=', $value['id'])->where('liked_by', '=', $data['user_id'])->first()) {
                        $finalArray[$i]['is_liked'] = true;
                    } else {
                        $finalArray[$i]['is_liked'] = false;
                    }
                    if (PostShareLog::where('post_id', '=', $value['id'])->where('shared_by', '=', $data['user_id'])->first()) {
                        $finalArray[$i]['is_shared'] = true;
                    } else {
                        $finalArray[$i]['is_shared'] = false;
                    }
                    $i++;
                }
                $getPosts["data"] = $finalArray;
                $response = Api::make_response(1, [$getPosts], 'Posts Found');
            } else {
                $response = Api::make_response(0, [], 'Posts not Found');
            }
            //$response =  Api::make_response(1,$getPosts,'Invalide user id');
        } else {
            $response = Api::make_response(0, [], 'Invalide user id');
        }
        return $response;
    }
    
    public function get_video_post_by_user_id(Request $request){
        $user_id=$request->input('user_id');
        $post_list= Posts::where(['user_id'=>$user_id,'is_active'=>1,'audio_name'=>NULL])->where('video_name','!=',NULL)->orderBy('created_at','DESC')->get();
        
        return Api::make_response(1, $post_list, '');
    }
    
    public function get_audio_post_by_user_id(Request $request){
        $user_id=$request->input('user_id');
        $post_list= Posts::where(['user_id'=>$user_id,'is_active'=>1,'video_name'=>NULL])->where('audio_name','!=',NULL)->orderBy('created_at','DESC')->get();
        return Api::make_response(1, $post_list, '');
    }

}
