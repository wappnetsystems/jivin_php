<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Plans;
use App\BoostPlans;
use \Validator,
    \Redirect;
use App\UserTransaction;
use DB;
use App\Jackpot_amount;
use App\Region;
use App\Devision;
use App\State;

class AdminRevenueController extends Controller {
    /*
      Author : Dhiman Bhattacharya

      This Contoller is for UserTransaction;
     */

    public function __construct() {
        $this->middleware('admin');
    }

    # To List Interest Category

    public function index(Request $request) {

        if (isset($_GET['year'])) {
            $year = $_GET['year'];
        } else {
            $year = '2017';
        }

        if (isset($_GET['phase'])) {
            $phase = $_GET['phase'];
        } else {
            $phase = '2';
        }

        $phasearray = '';
        if ($phase == '1') {
            $phasearray = '01,02,03,04';
        } elseif ($phase == '2') {
            $phasearray = '05,06,07,08';
        } elseif ($phase == '3') {
            $phasearray = '09,10,11,12';
        }
        //print_r($phasearray);die;

        $quesryStr = "SELECT a.tfor,IF(b.totalRevenue IS NULL,0,b.totalRevenue) totalRevenue FROM(SELECT DISTINCT `transaction_for` as tfor FROM user_transaction ) a LEFT JOIN (SELECT `transaction_for` , SUM( `amount` ) AS 'totalRevenue', created_at FROM `user_transaction` WHERE DATE_FORMAT( `created_at` , '%Y' ) = '" . $year . "' GROUP BY transaction_for HAVING DATE_FORMAT( `created_at` , '%m' ) IN ( " . $phasearray . " )) b ON a.tfor=b.transaction_for";
        //$quesryStr = "SELECT `transaction_for` , SUM( `amount` ) as 'totalRevenue' FROM `user_transaction` WHERE DATE_FORMAT(  `created_at` ,  '%Y' ) = '".$year."'  AND (DATE_FORMAT(  `created_at` ,  '%m' ) IN (".$phasearray.")) GROUP BY `transaction_for` ";
        //echo $quesryStr;die;
        $boostplans = DB::select($quesryStr);
        // dd($boostplans);
        $jackpot_amt_data= Jackpot_amount::where(['area_type'=>'Country','location_id'=>1])->get();

        $region = Region::where('is_active', 1)->get();
        $division = Devision::where('is_active', 1)->get();
        $state = State::where('is_active', 1)->get();
        //print_r($jackpot_amt_data[0]->amount); die();
        
        if($jackpot_amt_data->count()>0){
            $jackpot_amt=$jackpot_amt_data[0]->amount;
        }
        else{
            $jackpot_amt=0.00;
        }
        
        return view('admin.Revenue.index', ['boostplans' => $boostplans,
            'jackpot_amt'=>$jackpot_amt,
            'region' => $region, 'division' => $division, 'state' => $state
        ]);
    }

    public function update_jackpot(Request $request) {

        //print_r($request->all()); die();

        $validator = Validator::make($request->all(), [
                    'region_amount' => 'required|numeric',
                    'division_amount' => 'required|numeric',
                    'state_amount' => 'required|numeric',
                    'region' => 'required|numeric',
                    'division' => 'required|numeric',
                    'state' => 'required|numeric',
                    'country' => 'required|numeric',
                    'country_amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('admin/revenue/index')->with('alert-danger', 'Validation error occurred. Try Again!');
        }

        // insert/update country
        $country_data = Jackpot_amount::where(['location_id' => $request->input('country'), 'area_type' => 'Country'])->get();

        if ($country_data->count() > 0) {
            $country_jackpot_arr = [
                'amount' => $request->input('country_amount'),
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_ip' => $request->ip()
            ];
            Jackpot_amount::where(['id' => $country_data[0]->id])->update($country_jackpot_arr);
        } else {
            $country_jackpot_arr = [
                'amount' => $request->input('country_amount'),
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_ip' => $request->ip(),
                'area_type' => 'Country',
                'location_id' => $request->input('country')
            ];
            Jackpot_amount::insert($country_jackpot_arr);
        }
        
        //insert/update region
        $region_data = Jackpot_amount::where(['location_id' => $request->input('region'), 'area_type' => 'Region'])->get();

        if ($region_data->count() > 0) {
            $region_jackpot_arr = [
                'amount' => $request->input('region_amount'),
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_ip' => $request->ip()
            ];
            Jackpot_amount::where(['id' => $region_data[0]->id])->update($region_jackpot_arr);
        } else {
            $region_jackpot_arr = [
                'amount' => $request->input('region_amount'),
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_ip' => $request->ip(),
                'area_type' => 'Region',
                'location_id' => $request->input('region')
            ];
            Jackpot_amount::insert($region_jackpot_arr);
        }

        //insert/update Division
        $division_data = Jackpot_amount::where(['location_id' => $request->input('division'), 'area_type' => 'Division'])->get();

        if ($division_data->count() > 0) {
            $division_jackpot_arr = [
                'amount' => $request->input('division_amount'),
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_ip' => $request->ip()
            ];
            Jackpot_amount::where(['id' => $division_data[0]->id])->update($division_jackpot_arr);
        } else {
            $division_jackpot_arr = [
                'amount' => $request->input('division_amount'),
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_ip' => $request->ip(),
                'area_type' => 'Division',
                'location_id' => $request->input('division')
            ];
            Jackpot_amount::insert($division_jackpot_arr);
        }

        //insert/update state
        $state_data = Jackpot_amount::where(['location_id' => $request->input('state'), 'area_type' => 'State'])->get();

        if ($state_data->count() > 0) {
            $state_jackpot_arr = [
                'amount' => $request->input('state_amount'),
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_ip' => $request->ip()
            ];
            Jackpot_amount::where(['id' => $state_data[0]->id])->update($state_jackpot_arr);
        } else {
            $state_jackpot_arr = [
                'amount' => $request->input('state_amount'),
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_ip' => $request->ip(),
                'area_type' => 'State',
                'location_id' => $request->input('state')
            ];
            Jackpot_amount::insert($state_jackpot_arr);
        }

        return redirect('admin/revenue/index')->with('alert-success', 'Manual jackpot amount updated successfully.');
    }

    public function get_jackpot_price(Request $request) {
        $validator = Validator::make($request->all(), [
                    'area_type' => 'required',
                    'location_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false]);
        }
        $area_type = $request->input('area_type');
        $location_id = $request->input('location_id');

        $jackpot_data = Jackpot_amount::where(['area_type' => $area_type, 'location_id' => $location_id])->get();
        if ($jackpot_data->count() == 0) {
            return response()->json(['status' => false]);
        } else {
            return response()->json(['status' => true, 'amount' => $jackpot_data[0]->amount]);
        }
    }

    public function postAddNewPlan(Request $request) {
        if ($request->id) {

            $validator = Validator::make($request->all(), [
                        'planname' => 'required',
                        //'numofboost' => 'required|numeric',
                        'country_boost_count' => 'required|numeric',
                        'region_boost_count' => 'required|numeric',
                        'division_boost_count' => 'required|numeric',
                        'state_boost_count' => 'required|numeric',
                        'amount' => 'required',
                        'is_active' => 'required',
            ]);

            if ($validator->fails()) {

                $messsages = array(
                    'planname.required' => 'You cant leave Email field empty',
                    //'numofboost.required'=>'You cant leave name field empty',
                    //'numofboost.numeric'=>'The field has to be :min chars long',
                    'country_boost_count' => 'required|numeric',
                    'region_boost_count' => 'required|numeric',
                    'division_boost_count' => 'required|numeric',
                    'state_boost_count' => 'required|numeric',
                    'amount' => 'You cant leave amount field empty',
                    'is_active' => 'You cant leave status field empty'
                );
                return Redirect::back()->withErrors($validator)->withInput();
            } else {

                $boostplane = BoostPlans::find($request->id);
                $boostplane->plan_name = $request->planname;
                $boostplane->plan_name = $request->planname;
                $boostplane->country_boost_count = $request->country_boost_count;
                $boostplane->region_boost_count = $request->region_boost_count;
                $boostplane->division_boost_count = $request->division_boost_count;
                $boostplane->state_boost_count = $request->state_boost_count;
                //$boostplane->num_of_boost = $request->numofboost;
                $boostplane->amount = $request->amount;
                $boostplane->is_active = $request->is_active;

                if ($boostplane->save()) {
                    return redirect('admin/boost_plans/list')->with('alert-success', 'Plan Updated!');
                } else {
                    return redirect('admin/boost_plans/list')->with('alert-danger', 'Plan updation faild');
                }
            }
        } else {



            $validator = Validator::make($request->all(), [
                        'planname' => 'required',
                        //'numofboost' => 'required|numeric',
                        'country_boost_count' => 'required|numeric',
                        'region_boost_count' => 'required|numeric',
                        'division_boost_count' => 'required|numeric',
                        'state_boost_count' => 'required|numeric',
                        'amount' => 'required',
                        'is_active' => 'required',
            ]);



            if ($validator->fails()) {

                $messages = $validator->messages();

                return Redirect::back()->withErrors($validator)->withInput();
            } else {
                $boostplane = new BoostPlans;
                $boostplane->plan_name = $request->planname;

                $boostplane->country_boost_count = $request->country_boost_count;
                $boostplane->region_boost_count = $request->region_boost_count;
                $boostplane->division_boost_count = $request->division_boost_count;
                $boostplane->state_boost_count = $request->state_boost_count;
                //$boostplane->num_of_boost = $request->numofboost;
                $boostplane->amount = $request->amount;
                $boostplane->is_active = $request->is_active;

                if ($boostplane->save()) {
                    return redirect('admin/boost_plans/list')->with('alert-success', 'Plan Updated!');
                } else {
                    return redirect('admin/boost_plans/list')->with('alert-danger', 'Plan updation faild');
                }
            }
        }
    }

    public function updateStatus(Request $request) {

        $BoostPlansCount = BoostPlans::where('id', $request->id)->count();
        if ($BoostPlansCount > 0) {
            return 'false';
        } else {
            $plan = BoostPlans::where('id', '=', $request->id)->first();
            $plan->is_active = $request->status;
            if ($plan->save()) {
                return 'true';
            } else {
                return 'false';
            }
        }
    }

}
