<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
//load Models
use App\Api;
use App\User;
use App\SupportRequest;
use App\SupportRequestReaplyLog;
use  App\SupportRequestAttachment;
use DB;
class ApiSupportRequestController extends ApiGuardController
{
    protected $apiMethods = [
        'addSupportRequest' => [
            'keyAuthentication' => false
        ]    
,
  'getResponse' => [
            'keyAuthentication' => false
        ] 
        ,
        'addcomments' => [
            'keyAuthentication' => false
        ] 
,
    'getcomments' => [
            'keyAuthentication' => false
        ] ,
        'getRequests' => [
            'keyAuthentication' => true
        ] 
    ];

    public function addSupportRequest(Request $request){

    $data = $request->all();
    // echo "<pre>";print_r(Input::all());
    // print_r($data);die;
    $files =$request->file('filename');
    $file_count=count($files);
    $uploadcount=0;
   

    if(isset($files))
    {    
      foreach ($files as $one) {
        $filename       = time()."_".$one->getClientOriginalName();
        $extension=$one->getClientOriginalExtension();
        $image=$one->move(public_path('uploads'),$filename);

        $uploadcount++;
        $extensionname[]=$extension;
        $listfilenames[] = $filename;
      }

      if(!empty($listfilenames))
      {
        $list_count=1;
       
      }
                            
      if ($uploadcount == $file_count){

        $support_request=SupportRequest::create(array(

        'ticket_no'=>rand(),
        'user_id'=>$request->user_id,
        'subject'=> $request->subject,
        'user_asked'=>$request->user_asked,
        'is_deleted'=>0,
        'is_main'=>1,
        'is_attachments'=>$list_count,
        'user_type'=>'User'
        ));    
      }

      for($i=0;$i<$file_count;$i++)
      {
        $support=SupportRequestAttachment::create(array(
         'request_id'=>$support_request->id,
         'file_name'=> $listfilenames[$i],
         'file_type'=> $extensionname[$i]
         ));
      }
      $user=User::find($request->user_id);
      $user_name=$user->name;
      $user_image=$name=$user->user_image;

      return  response(array('success'=>1,'id'=>$support_request->id,'ticket_no'=> $support_request->ticket_no,'user_id'=> $support_request->user_id,'user-name'=>$user_name,'user-image'=>$user_image,'subject'=> $support_request->subject,'user_asked'=> $support_request->user_asked,'file_name'=>$listfilenames));

      }
      else
      { 
            $support_request=SupportRequest::create(array(     
              'ticket_no'=>rand(),
               'user_id'=>$request->user_id,
               'subject'=> $request->subject,
               'user_asked'=>$request->user_asked,
               'is_deleted'=>0,
               'is_main'=>1,
              'user_type'=>'User'
            ));

          $user=User::find($request->user_id);
          $user_name=$user->name;
          $user_image=$name=$user->user_image;

         return  response(array('success'=>1,'id'=>$support_request->id,'ticket_no'=> $support_request->ticket_no,'user_id'=> $support_request->user_id,'user-name'=>$user_name,'user-image'=>$user_image,'subject'=> $support_request->subject,'user_asked'=> $support_request->user_asked,'file_name'=>null));

      }

    }

    public function getRequests(Request $request){
      
	   $data = $request->json()->all();
	   $response =  [];
      $page = $data['page'];
      $itemsPerPage = $data['itemsPerPage'];     
	   if(isset($data['user_id']) && User::where('id','=',$data['user_id'])->first()){

          $getRequests = SupportRequest::with('attachments')->where('user_id','=',$data['user_id'])->where('is_main','=',1)->orderBy('id','DESC')->where('is_deleted',0)->paginate($itemsPerPage)->toArray();
          //dd($getRequests);
          foreach ($getRequests['data'] as $key => $mainRequest) {
            $getRequests['data'][$key]['comments'] = SupportRequest::getCommets($mainRequest['ticket_no']);
          }
        if ($getRequests) {
        	$response =  Api::make_response(1, $getRequests,'Support requests Found');
        }else{
        	$response =  Api::make_response(0,[],'Support request Found');
        }
           
          
	   }else{
         $response =  Api::make_response(0,[],'Invalide user id');
	   }
        
	   return $response;      	
    }    

//     public function getResponse(Request $request)
//     {   
//       $res=Array();
//       $response=Array();
//       $data = $request->json()->all();
//  //dd($data);
//       $response=SupportRequest::with('attachments')->where('user_id','=',$data['user_id'])->orderBy('created_at')->get()->toArray();
//      // dd($response);
//         if($response)
//         {
//     //       foreach($response[0] as $key=>$val)
//     //       {
//     //          $res[$key]=$val;
//     // }

// //     foreach($val as $values)
// //     {
// //       $vals=$values;
// //     }
// // return $vals->n;
//     return $response =  Api::make_response(1,[$response],'List');
  
//   }
//     else
//     {
//       return response(array('not found'));
//     }
// }

public function addcomments(Request $request)
{
$ticket_no=$request->ticket_no;
$user_id=$request->user_id;
$user_asked=$request->user_asked;
//$is_main=$request->is_main;
$files =$request->file('filename');
   $file_count=count($files);
   $uploadcount=0;
   

    if(isset($files))
    {
    
foreach ($files as $one) {

   $filename       = time()."_".$one->getClientOriginalName();
    $extension=$one->getClientOriginalExtension();
         $image=$one->move(public_path('uploads'),$filename);

  $uploadcount++;
 $extensionname[]=$extension;
   $listfilenames[] = $filename;



                }
              
             

             //$extension=implode(',',$extensionname);
                //$list=implode(',',$listfilenames);
            
                if(!empty($listfilenames))
                {
                  $list_count=1;
                 
                }
              
              
        if ($uploadcount == $file_count){
  
 $support_request=SupportRequest::create(array(
       
    'ticket_no'=>$ticket_no,
     'user_id'=>$user_id,
     'subject'=>'',
     'user_asked'=>$user_asked,
     'is_deleted'=>0,
     'is_main'=>0,
      'is_attachments'=>$list_count
     ));    
        }


for($i=0;$i<$file_count;$i++)
{
  $support=SupportRequestAttachment::create(array(
     'request_id'=>$support_request->id,
     'file_name'=> $listfilenames[$i],
     'file_type'=> $extensionname[$i]
     ));
}
//$support->save();
$user=User::find($request->user_id);
$user_name=$user->name;
$user_image=$name=$user->user_image;

      return  response(array('success'=>1,'id'=>$support_request->id,'ticket_no'=> $support_request->ticket_no,'is_main'=>$support_request->is_main,'user_id'=> $support_request->user_id,'user-name'=>$user_name,'user-image'=>$user_image,'subject'=> $support_request->subject,'user_asked'=> $support_request->user_asked,'file_name'=>$listfilenames));

      }
      else
      { 
//$d=(str_random(5,9999));

        $support_request=SupportRequest::create(array(
     
     'ticket_no'=>$ticket_no,
     'user_id'=>$user_id,
     'subject'=>'',
     'user_asked'=>$user_asked,
     'is_deleted'=>0,
     'is_main'=>0,
      'is_attachments'=>0
     )); 



// dd($support_request->ticket_no);
        //$support->save();
$user=User::find($request->user_id);
$user_name=$user->name;
$user_image=$name=$user->user_image;


 return  response(array('success'=>1,'id'=>$support_request->id,'ticket_no'=> $support_request->ticket_no,'is_main'=>$support_request->is_main,'user_id'=> $support_request->user_id,'user-name'=>$user_name,'user-image'=>$user_image,'subject'=> $support_request->subject,'user_asked'=> $support_request->user_asked,'file_name'=>null));


}


}


public function getcomments(Request $request)
{

  $data=$request->json()->all();

  $getSupportRequestInfo=SupportRequest::with('attachments')
                                        ->where('is_main','=',1)
                                        ->where('user_id','=',$data['user_id']);

  if ($data['ticket_no']) {

    $getSupportRequestInfo = $getSupportRequestInfo
                              ->where('ticket_no','=',$data['ticket_no']);
  }

  $getSupportRequestInfo = $getSupportRequestInfo->orderBy('created_at')
                                                ->get()->toArray();

    if(count($getSupportRequestInfo))
    {
     return Api::make_response(1,[$getSupportRequestInfo],'Support request list found');
    }
    else
    {
     return Api::make_response(0,[],'Support request list not found');
    }
}
}