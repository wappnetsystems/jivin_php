<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use App\Country;
use App\Region;
use App\Devision;
use App\State;

class AdminStateController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function listState() {
        $devision = Devision::where('is_active', '=', 1)->get()->toArray();
        return view('admin.location.state')->with('devision', $devision);
    }

    public function getList() {
        $output['aaData'] = [];
        $aColumns = array(
            'id'
            , 'division_id'
            , 'devision_name'
            , 'name'
            , 'is_active'
            ,'state_compete'
            , 'id'
        );
        $states = State::get()->toArray();
        // /dd($states);
        foreach ($states as $key => $state) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($state[$aColumns[$i]] === NULL) {
                    $state[$aColumns[$i]] = "";
                }
                $row[] = $state[$aColumns[$i]];
            }
            $output['aaData'][] = $row;
        }
        $output = array(
            "draw" => intval(1),
            "data" => $output['aaData']
        );
        return $output;
    }

    public function postAddNewState(Request $request) {
        // echo $request->name;exit();
        if ($request->id) {
            $state = State::where('id', '=', $request->id)->first();
            if (State::where('name', '=', $request->name)->where('id', '!=', $request->id)->first()) {
                return redirect('admin/state/list')->with('alert-danger', 'State Already Exists');
            }
            $state->division_id = $request->devision_id;
            $state->name = str_replace(' ', '_', $request->name);
            ;
            $state->slug = str_replace(' ', '_', $request->name);
            ;
            $state->is_active = $request->is_active;
            if ($state->save()) {
                return redirect('admin/state/list')->with('alert-success', 'State Updated!');
            } else {
                return redirect('admin/state/list')->with('alert-danger', 'State Updation faild');
            }
        } else {
            $state = new State;
            if ($state::where('name', '=', $request->name)->first()) {
                return redirect('admin/state/list')->with('alert-danger', 'State Already Exists');
            } else {
                // echo $request->name;exit();
                $state->division_id = $request->devision_id;
                $state->name = str_replace(' ', '_', $request->name);
                ;
                $state->slug = str_replace(' ', '_', $request->name);
                ;
                $state->is_active = $request->is_active;
                if ($state->save()) {
                    return redirect('admin/state/list')->with('alert-success', 'New State Created!');
                } else {
                    return redirect('admin/state/list')->with('alert-danger', 'State Creation faild');
                }
            }
        }
    }

    public function updateStatus(Request $request) {
        $state = State::where('id', '=', $request->id)->first();
        $state->is_active = $request->status;
        if ($state->save()) {
            return 'true';
        } else {
            return 'false';
        }
    }
    
    public function updateStateComplete(Request $request) {
        $state = State::where('id',$request->id)->first();
        $state->state_compete = $request->state_compete;
        if($state->save()){
            return 'true';
        }
        else{
            return 'json';
        }
    }

}
