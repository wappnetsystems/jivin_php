<?php

namespace App\Http\Controllers;

use App\Api;
use App\Country;
//use DB;
use App\Devision;
use App\Followers;
use App\LeaderboardLog;
use App\User;
//load Models
use App\Region;
use App\State;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Illuminate\Http\Request;

class ApiLeaderboardLogController extends ApiGuardController {

    protected $apiMethods = [
        'getLeaderboardYearList' => [
            'keyAuthentication' => false,
        ],
        'getLeaderboardMonthList' => [
            'keyAuthentication' => false,
        ],
    ];

    public function getTopRankByCountry(Request $request) {
        $user_id = $request->input('user_id');

        $month = date('m');

        if ($month >= 1 && $month <= 4) {
            $start_month = 1;
            $end_month = 4;
        } else if ($month >= 5 && $month <= 8) {
            $start_month = 5;
            $end_month = 8;
        } else {
            $start_month = 9;
            $end_month = 12;
        }

        $getLeaderboardList = LeaderboardLog::with('userInfo')->where('year', date('Y'))
                ->where('month', '>=', $start_month)
                ->where('month', '<=', $end_month)
                ->where('is_active', 1)
                ->where('country_id', 1)
                ->groupBy('user_id')
                ->selectRaw('*, sum(total_vote) as total_vote')
                // ->selectRaw('*,`leaderboard_log`.`user_id` as l_u_id, (select count(*) from `posts` inner join `post_like_log` on `posts`.`id`=`post_like_log`.`post_id` where `posts`.`user_id` = `l_u_id` AND (`posts`.`video_name` is not NULL OR `posts`.`audio_name` is not NULL)) as `total_vote`')
                ->orderBy('total_vote', 'DESC')
                ->limit(5)
                ->get();
        return Api::make_response(1, $getLeaderboardList, "Success");
    }

    public function getLeaderboardList(Request $request) {
        $data = $request->json()->all();
        $response = [];
        if ($data) {
            $now = new \DateTime('now');
            // $now = new \DateTime('01-05-2018');

            $compititionspan = '';

            if ($data['is_wall_of_fame']) {
                if (isset($data['month']) && !empty($data['month'])) {
                    $current_month = $data['month'];
                } else {
                    $current_month = $now->format('m');
                }
                if (isset($data['year']) && !empty($data['year'])) {
                    $current_year = $data['year'];
                } else {
                    $current_year = $now->format('Y');
                }
            } else {
                $current_month = $now->format('m');
                $current_year = $now->format('Y');
            }

            $days = 0;

            $current_leaderboard = LeaderboardLog::getCurrentLeaderBoard($current_month);

            $current_leaderboard_end_date = LeaderboardLog::getCurrentLeaderBoardEndingDate($current_leaderboard);
            $current_leaderboard_month = LeaderboardLog::getCurrentLeaderBoardMonth($current_leaderboard);

            if ($current_leaderboard == 1) {
                $compititionspan = "1st Competition - Jan - Apr " . $current_year;
            } elseif ($current_leaderboard == 2) {
                $compititionspan = "2nd Competition - May - Aug " . $current_year;
            } elseif ($current_leaderboard == 3) {
                $compititionspan = "3rd Competition - Sep - Dec " . $current_year;
            }

            $condition = [];
            $img_url = "";
            $filtername = "";
            if (!empty($data['country_slug'])) {
                $country_id = Api::getLocationId('Country', $data['country_slug']);
                $condition['country_id'] = $country_id;
                $img_url = Country::find($country_id)->img_url;
                $filtername = Country::find($country_id)->name;
            }
            if (!empty($data['region_slug'])) {
                $condition['region_id'] = Api::getLocationId('Region', $data['region_slug']);
                $img_url = Region::find(Api::getLocationId('Region', $data['region_slug']))->img_url;
                $filtername = Region::find(Api::getLocationId('Region', $data['region_slug']))->name;
                $filtername = $filtername . " region";
            }
            if (!empty($data['division_slug'])) {
                $condition['devision_id'] = Api::getLocationId('Devision', $data['division_slug']);
                $img_url = Devision::find(Api::getLocationId('Devision', $data['division_slug']))->img_url;
                $filtername = Devision::find(Api::getLocationId('Devision', $data['division_slug']))->name;
                $filtername = $filtername . " division";
            }
            if (!empty($data['state_slug'])) {
                $condition['state_id'] = Api::getLocationId('State', $data['state_slug']);
                $img_url = State::find(Api::getLocationId('State', $data['state_slug']))->img_url;
                $filtername = State::find(Api::getLocationId('State', $data['state_slug']))->name;
            }

            if (empty($data['region_slug']) && !$data['is_wall_of_fame']) {

                $condition['leader_board_no'] = $current_leaderboard;
                $gerRegionOfCountry = Region::where('country_id', '=', Api::getLocationId('Country', $data['country_slug']))->where('is_active', 1)->get();
                $top = 250;
                if (!empty($data['region_slug'])) {
                    $top = 200;
                } elseif (!empty($data['division_slug'])) {
                    $top = 150;
                } elseif (!empty($data['state_slug'])) {
                    $top = 100;
                }

                foreach ($gerRegionOfCountry as $key => $region) {
                    $condition['region_id'] = $region->id;
                    $getLeaderboardList = LeaderboardLog::with('userInfo')
                            ->where($condition)
                            ->where('is_active', 1)
                            ->where('leader_board_no', $current_leaderboard)
                            ->limit($top)
                            ->groupBy('user_id')
                            ->selectRaw('*, sum(total_vote) as total_vote')
                            // ->selectRaw('*,`leaderboard_log`.`user_id` as l_u_id, (select count(*) from `posts` inner join `post_like_log` on `posts`.`id`=`post_like_log`.`post_id` where `posts`.`user_id` = `l_u_id` AND (`posts`.`video_name` is not NULL OR `posts`.`audio_name` is not NULL)) as `total_vote`')
                            ->orderBy('total_vote', 'DESC')
                            ->get();
                    // dd($getLeaderboardList);

                    $top = $data['top'];
                    $finalList[$region->name] = $this->getFinalList($getLeaderboardList, $top);
                }

                //echo json_encode($finalList);die;
            } else {

                // if ($data['is_wall_of_fame']) {
                //     if ($current_leaderboard == 1) {
                //         $condition['leader_board_no'] = 3;
                //         $current_leaderboard_end_date = LeaderboardLog::getCurrentLeaderBoardEndingDate(3);
                //         $current_leaderboard_month = LeaderboardLog::getCurrentLeaderBoardMonth(3);
                //         $current_year = date("Y", strtotime("-1 year"));
                //         $condition['year'] = $current_year;
                //         $compititionspan = "3rd Competition - Sep - Dec " . date("Y", strtotime("-1 year"));
                //     } else {
                //         $condition['leader_board_no'] = $current_leaderboard - 1;
                //         if ($condition['leader_board_no'] == 1) {
                //             $compititionspan = "1st Competition - Jan - Apr " . $current_year;
                //         } elseif ($condition['leader_board_no'] == 2) {
                //             $compititionspan = "2nd Competition - May - Aug " . $current_year;
                //         }
                //         $condition['year'] = $current_year;
                //         $current_leaderboard_end_date = LeaderboardLog::getCurrentLeaderBoardEndingDate($current_leaderboard - 1);
                //         $current_leaderboard_month = LeaderboardLog::getCurrentLeaderBoardMonth($current_leaderboard - 1);
                //     }
                // } else {
                $condition['leader_board_no'] = $current_leaderboard;
                //     $condition['year'] = $current_year;
                // }
                // $date1 = new \DateTime("now"); //current date or any date
                $date1 = new \DateTime(date('Y-m-d')); //current date or any date
                $date2 = new \DateTime($current_leaderboard_end_date); //Future date
                $diff = $date2->diff($date1)->format("%a"); //find difference
                $days = intval($diff) + 1;
                $ongoing_month = $now->format('m');
                $ongoing_month_leaderboard = LeaderboardLog::getCurrentLeaderBoard($ongoing_month);
                $ongoing_year = $now->format('Y');
                // echo $ongoing_year .'<'. $current_year .' || ('.$current_leaderboard.' == '.$ongoing_month_leaderboard.' && '.$ongoing_year.' == '.$current_year .') || ('.$ongoing_month_leaderboard .' > '. $current_leaderboard .' && '. $ongoing_year.' == '.$current_year.')';die;

                /* if(isset($data['only_wall_of_fame']) && (($ongoing_year < $current_year) || ($current_leaderboard==$ongoing_month_leaderboard && ($ongoing_year==$current_year)) || ($ongoing_month_leaderboard > $current_leaderboard && $ongoing_year==$current_year))) { */
                // dd($current_leaderboard);
                if (isset($data['only_wall_of_fame']) && $this->checkMonth($current_month, $current_year)) {

                    $response = Api::make_response(0, [], 'Coming Soon!', ['compitition' => $compititionspan, 'leaderboarDaysleft' => $days, 'currentYear' => $current_year, 'current_leaderboard_month' => $current_leaderboard_month, 'img_url' => $img_url, 'filtername' => $filtername, 'top' => $data['top']]);
                    return $response;
                }

                if (isset($data['only_wall_of_fame'])) {
                    $getLeaderboardList = LeaderboardLog::with('userInfo')
                            ->where($condition)
                            //->limit($data['top'])
                            ->where('is_active', 1)
                            // ->where('month',$current_leaderboard)
                            ->where('year', $current_year)
                            ->groupBy('user_id')
                            ->selectRaw('*, sum(total_vote) as total_vote')
                            // ->selectRaw('*,`leaderboard_log`.`user_id` as l_u_id, (select count(*) from `posts` inner join `post_like_log` on `posts`.`id`=`post_like_log`.`post_id` where `posts`.`user_id` = `l_u_id` AND (`posts`.`video_name` is not NULL OR `posts`.`audio_name` is not NULL)) as `total_vote`')
                            ->orderBy('total_vote', 'DESC')
                            ->get();
                    // dd($current_leaderboard);
                } else {

                    $getLeaderboardList = LeaderboardLog::with('userInfo')
                            ->where($condition)
                            //->limit($data['top'])
                            ->where('is_active', 1)
                            ->groupBy('user_id')
                            ->selectRaw('*, sum(total_vote) as total_vote')
                            // ->selectRaw('*,`leaderboard_log`.`user_id` as l_u_id, (select count(*) from `posts` inner join `post_like_log` on `posts`.`id`=`post_like_log`.`post_id` where `posts`.`user_id` = `l_u_id` AND (`posts`.`video_name` is not NULL OR `posts`.`audio_name` is not NULL)) as `total_vote`')
                            ->orderBy('total_vote', 'DESC')
                            ->get();
                }


                $top = $data['top'];
                $finalList = $this->getFinalList($getLeaderboardList, $top);
                // dd($finalList);
            }


            //echo json_encode($finalList);die;
            //Followers
            $followinglist = array();
            if (isset($data['user_id'])) {
                $followinglist = Followers::where('user_id', $data['user_id'])->pluck('is_following')->toArray();
            }
            //echo json_encode($is_following);die;

            $finalListArray = [];
            $i = 0;
            if ($data['is_wall_of_fame']) {
                foreach ($finalList as $flList) {

                    if ($flList['rank'] < 4 && $data['is_wall_of_fame']) {
                        $finalListArray[$i]['user_id'] = $flList['user_id'];
                        $finalListArray[$i]['country_id'] = $flList['country_id'];
                        $finalListArray[$i]['region_id'] = $flList['region_id'];
                        $finalListArray[$i]['devision_id'] = $flList['devision_id'];
                        $finalListArray[$i]['state_id'] = $flList['state_id'];
                        $finalListArray[$i]['total_vote'] = $flList['total_vote'];
                        $finalListArray[$i]['rank'] = $flList['rank'];
                        $finalListArray[$i]['user_full_name'] = $flList['user_full_name'];
                        $finalListArray[$i]['user_name'] = $flList['user_name'];
                        $finalListArray[$i]['user_info'] = $flList['userInfo'];

                        if (in_array($flList['user_id'], $followinglist)) {
                            $finalListArray[$i]['is_following'] = true;
                        } else {
                            $finalListArray[$i]['is_following'] = false;
                        }
                        $i = $i + 1;
                    }
                }
            } else {
                $finalListArray = $finalList;
            }
            //echo json_encode($finalListArray);die;
            //echo $daysleft;
            //$finalList['daysleft'] = $days;
            if (count($finalListArray)) {
                $response = Api::make_response(1, [$finalListArray], 'Leaderboar Found', ['compitition' => $compititionspan, 'leaderboarDaysleft' => $days, 'currentYear' => $current_year, 'current_leaderboard_month' => $current_leaderboard_month, 'img_url' => $img_url, 'filtername' => $filtername, 'top' => $data['top']]);
            } else {
                $response = Api::make_response(0, [], 'No Data Found', ['img_url' => $img_url, 'filtername' => $filtername, 'compitition' => $compititionspan, 'currentYear' => $current_year, 'current_leaderboard_month' => $current_leaderboard_month]);
            }
        } else {
            $response = Api::make_response(0, [], 'Invalid Param . Data not saved');
        }

        return $response;
    }

    private function checkMonth($given_month, $given_year) {

        $date1 = new \DateTime("now");
        // $date1 = new \DateTime('01-05-2018');
        $ongoing_month = intval($date1->format('m'));
        $ongoing_year = intval($date1->format('Y'));
        $given_month = intval($given_month);
        $given_year = intval($given_year);

        if ($given_year == $ongoing_year) {

            $given_month = $given_month + 3;
            if ($ongoing_month > $given_month) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function getWallOfFameList(Request $request) {

        $data = $request->json()->all();
        $response = [];
        if ($data) {
            $now = new \DateTime('now');
            $current_month = $now->format('m');
            $current_year = $now->format('Y');
            $current_leaderboard = LeaderboardLog::getCurrentLeaderBoard($current_month);
            $current_leaderboard_end_date = LeaderboardLog::getCurrentLeaderBoardEndingDate($current_leaderboard);
            $current_leaderboard_month = LeaderboardLog::getCurrentLeaderBoardMonth($current_leaderboard);
            $condition = [];
            $img_url = "";
            if (!empty($data['country_slug'])) {
                $country_id = Api::getLocationId('Country', $data['country_slug']);
                $condition['country_id'] = $country_id;
                $img_url = Country::find($country_id)->img_url;
            }
            if (!empty($data['region_slug'])) {
                $condition['region_id'] = Api::getLocationId('Region', $data['region_slug']);
                $img_url = Region::find(Api::getLocationId('Region', $data['region_slug']))->img_url;
            }
            if (!empty($data['division_slug'])) {
                $condition['devision_id'] = Api::getLocationId('Devision', $data['division_slug']);
                $img_url = Devision::find(Api::getLocationId('Devision', $data['division_slug']))->img_url;
            }
            if (!empty($data['state_slug'])) {
                $condition['state_id'] = Api::getLocationId('State', $data['state_slug']);
                $img_url = State::find(Api::getLocationId('State', $data['state_slug']))->img_url;
            }

            if (empty($data['region_slug']) && !$data['is_wall_of_fame']) {

                $condition['leader_board_no'] = $current_leaderboard;
                $gerRegionOfCountry = Region::where('country_id', '=', Api::getLocationId('Country', $data['country_slug']))->get();
                foreach ($gerRegionOfCountry as $key => $region) {
                    $condition['region_id'] = $region->id;
                    $getLeaderboardList = LeaderboardLog::with('userInfo')
                            ->where($condition)
                            //->limit($data['top'])
                            ->groupBy('user_id')
                            ->selectRaw('*, sum(total_vote) as total_vote')
                            ->orderBy('total_vote', 'DESC')
                            ->get();

                    $top = $data['top'];
                    $finalList[$region->name] = $this->getFinalList($getLeaderboardList, $top);
                }
            } else {
                // if ($data['is_wall_of_fame']) {
                //   if ($current_leaderboard == 1) {
                //     $condition['leader_board_no'] =3;
                //   }else{
                //     $condition['leader_board_no'] =$current_leaderboard-1;
                //     // $current_leaderboard_end_date =LeaderboardLog::getCurrentLeaderBoardEndingDate($current_leaderboard-1);
                //     //  $current_leaderboard_month =LeaderboardLog::getCurrentLeaderBoardMonth($current_leaderboard-1);
                //   }
                // }else{
                $condition['leader_board_no'] = $current_leaderboard;
                // }

                $getLeaderboardList = LeaderboardLog::with('userInfo')
                        ->where($condition)
                        //->limit($data['top'])
                        ->groupBy('user_id')
                        ->selectRaw('*, sum(total_vote) as total_vote')
                        ->orderBy('total_vote', 'DESC')
                        ->get();

                $top = $data['top'];
                $finalList = $this->getFinalList($getLeaderboardList, $top);
            }
            $date1 = new \DateTime("now"); //current date or any date
            $date2 = new \DateTime($current_leaderboard_end_date); //Future date
            $diff = $date2->diff($date1)->format("%a"); //find difference
            $days = intval($diff) + 1;
            //echo $daysleft;
            //$finalList['daysleft'] = $days;
            if (count($finalList)) {
                $response = Api::make_response(1, [$finalList], 'Leaderboar Found', ['leaderboarDaysleft' => $days, 'currentYear' => $current_year, 'current_leaderboard_month' => $current_leaderboard_month, 'img_url' => $img_url]);
            } else {
                $response = Api::make_response(0, [], 'No Data Found', ['img_url' => $img_url]);
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param . Data not saved');
        }
        return $response;
    }

    public function getFinalList($getLeaderboardList, $top) {

        $i = 1;
        $total_vote = 0;
        $rank = 1;
        $finalList = [];
        foreach ($getLeaderboardList as $key => $list) {
            if ($rank == $top && ($total_vote != $list->total_vote)) {
                break;
            }
            if ($total_vote == $list->total_vote) {
                // $top++;
                $i--;
                $list->rank = $i;
                $rank = $i;
                $i++;
            } else {
                $list->rank = $i;
                $rank = $i;
                $i++;
            }

            $total_vote = $list->total_vote;
            $finalList[] = $list;
        }
        return $finalList;
    }

    public function getLeaderboardYearList() {
        //DB::enableQueryLog();
        $now = new \DateTime('now');
        $current_month = $now->format('m');
        $current_year = $now->format('Y');
        $current_leaderboard = LeaderboardLog::getCurrentLeaderBoard($current_month);
        $response = [];
        $getYears = LeaderboardLog::select('year', 'id')
                        ->whereNotIn('id', function ($q) use ($current_leaderboard, $current_year) {
                            $q->select('id')->from('leaderboard_log')
                            ->where('leader_board_no', '=', $current_leaderboard)
                            ->where('year', '=', $current_year);
                        })->groupBy('year')->get();
        //print_r(DB::getQueryLog());exit;
        $yearsArray = [];
        $i = 0;
        foreach ($getYears as $key => $log) {
            $yearsArray[$i]["key"] = $log->year;
            $yearsArray[$i]["value"] = $log->year;
            $i++;
        }
        if ($yearsArray) {
            $response = Api::make_response(1, [$yearsArray], 'Wall of fame years found');
        } else {
            $response = Api::make_response(0, [], 'No Wall of fame year found');
        }

        return $response;
    }

    public function getLeaderboardMonthList(Request $request) {
        $data = $request->json()->all();
        $now = new \DateTime('now');
        $current_month = $now->format('m');
        $current_year = $now->format('Y');
        $current_leaderboard = LeaderboardLog::getCurrentLeaderBoard($current_month);
        $response = [];
        $getMonths = LeaderboardLog::select('leader_board_no', 'id')
                        ->where('year', '=', $data["year"])
                        ->whereNotIn('id', function ($q) use ($current_leaderboard, $current_year) {
                            $q->select('id')->from('leaderboard_log')
                            ->where('leader_board_no', '=', $current_leaderboard)
                            ->where('year', '=', $current_year);
                        })->groupBy('leader_board_no')->get();
        $monthsArray = [];
        $i = 0;
        foreach ($getMonths as $key => $log) {
            $monthsArray[$i]["key"] = $log->leader_board_no;
            $monthsArray[$i]["value"] = LeaderboardLog::getCurrentLeaderBoardMonth($log->leader_board_no);
            $i++;
        }
        if ($monthsArray) {
            $response = Api::make_response(1, [$monthsArray], 'Wall of fame months found');
        } else {
            $response = Api::make_response(0, [], 'No Wall of fame months found');
        }

        return $response;
    }

    public function get_numberone(Request $request) {

        $data = $request->json()->all();

        if (isset($data["user_id"])) {
            $user_id = $data["user_id"];
        } elseif (isset($data["user"])) {
            $user_id = $data["user"];
        }

        $now = new \DateTime('now');
        $current_month = $now->format('m');
        $current_year = $now->format('Y');
        $current_leaderboard = LeaderboardLog::getCurrentLeaderBoard($current_month);
        $prev_leaderboard = '';
        $prev_year = '';

        $condition = array();
        if ($current_leaderboard - 1 == 0) {
            $year = $current_year - 1;
            $leaderboard = 3;
        } else {
            $year = $current_year;
            $leaderboard = $current_leaderboard - 1;
        }

        $condition['year'] = $year;
        $condition['leader_board_no'] = $leaderboard;
        /* $condition['year'] = 2018;
          $condition['leader_board_no'] = 1; */

        $location_to_chose = array_rand(['country', 'region', 'state', 'devision']);
        // $location_to_chose = array_rand(['country']);
        $displayText = "Number 1 in ";

        if ($location_to_chose == 0) {
            $getCountry = LeaderboardLog::where($condition)->inRandomOrder()->first();

            // print "<pre>";
            // echo '$getCountry';
            // print_r($condition);
            // dd($getCountry);
            // die();
            if ($getCountry) {
                $condition['country_id'] = $getCountry->country_id;
                $displayText = $displayText . $getCountry->getCountry['name']; //" country";
            }
        } else if ($location_to_chose == 1) {
            $getregion = LeaderboardLog::where($condition)->inRandomOrder()->first();

            // print "<pre>";
            // echo '$getregion';
            // print_r($getregion);
            // die();
            if ($getregion) {
                $condition['region_id'] = $getregion->region_id;
                $displayText = $displayText . $getregion->getRegion['name'] . " region";
            }
        } else if ($location_to_chose == 2) {
            $getdevision = LeaderboardLog::where($condition)->inRandomOrder()->first();

            //   print "<pre>";
            //   echo '$getdevision';
            //   print_r($getdevision);
            //   die();
            if ($getdevision) {
                $condition['devision_id'] = $getdevision->devision_id;
                $displayText = $displayText . $getdevision->getState['name']; //." state";
            }
        } else {
            $getstate = LeaderboardLog::where($condition)->inRandomOrder()->first();

            // print "<pre>";
            // echo '$getstate';
            // print_r($getstate);
            // die();
            if ($getstate) {
                $condition['state_id'] = $getstate->state_id;
                $displayText = $displayText . $getstate->getDevision['name']; //." devision";
            }
        }
        $getLeaderboardList = LeaderboardLog::with('userInfo')
                ->where($condition)
                ->where('is_active', 1)
                // ->where('user_id', '!=', $user_id)
                ->groupBy('user_id')
                ->selectRaw('*, sum(total_vote) as total_vote_count')
                ->orderBy('total_vote_count', 'DESC')
                ->first();
        // dd($getLeaderboardList);

        $get_followers_list = array();
        $count_followed_by = NULL;
        if ($user = User::find($user_id)) {

            if ($request->followed_by_count != $user->total_followed_by) {
                $get_followers_list = $user->follows;
                $count_followed_by = count($get_followers_list);
            } else {
                $count_followed_by = $request->followed_by_count;
            }
        }


        if (isset($getLeaderboardList->user_id)) {
            $followersCheck = Followers::where('user_id', $user_id)->where('is_following', $getLeaderboardList->user_id)->first();

            $getLeaderboardList['is_following'] = false;
            $is_following['array'] = $followersCheck;
            if (!empty($followersCheck)) {
                $getLeaderboardList['is_following'] = true;
                $is_following['array'] = $followersCheck;
            }

            //echo json_encode($getLeaderboardList);die;

            /*
              $data = array();
              $data['getLeaderboardList'] =  $getLeaderboardList;
              $data['displayText'] =  $displayText; */

            if (!empty($getLeaderboardList)) {
                $response = Api::make_response(1, ['ranks' => $getLeaderboardList, 'followed_by' => $get_followers_list, 'followed_by_count' => $count_followed_by], $displayText);
            }
        } else {
            $response = Api::make_response(0, ['ranks' => new \stdClass(), 'followed_by' => $get_followers_list, 'followed_by_count' => $count_followed_by], "Record not found");
        }

        return $response;
    }

    public function addLog() {
        return LeaderboardLog::addLog(11);
    }

}
