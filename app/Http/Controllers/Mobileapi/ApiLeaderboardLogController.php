<?php

namespace App\Http\Controllers\Mobileapi;

use App\Api;
use App\Country;
//use DB;
use App\Devision;
use App\Followers;
use App\LeaderboardLog;
use App\Http\Controllers\Controller;
use App\User;
//load Models
use App\Region;
use Illuminate\Support\Facades\Validator;
use App\State;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Illuminate\Http\Request;

class ApiLeaderboardLogController extends Controller {


    public static function set_month($month){  //for month
        switch ($month) {
            case "1":
                return 01;
                break;            
        	case "2":
                return 05;
                break;            
        	case "3":
                return 10;
                break;            
        }
    }

    public function mobile_numberone(Request $request) {

        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'user' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        if (isset($data["user_id"])) {
            $user_id = $data["user_id"];
        } elseif (isset($data["user"])) {
            $user_id = $data["user"];
        }

        $now = new \DateTime('now');
        $current_month = $now->format('m');
        $current_year = $now->format('Y');
        $current_leaderboard = LeaderboardLog::getCurrentLeaderBoard($current_month);
        $prev_leaderboard = '';
        $prev_year = '';

        $condition = array();
        if ($current_leaderboard - 1 == 0) {
            $year = $current_year - 1;
            $leaderboard = 3;
        } else {
            $year = $current_year;
            $leaderboard = $current_leaderboard - 1;
        }

        $condition['year'] = $year;
        $condition['leader_board_no'] = $leaderboard;
        /* $condition['year'] = 2018;
          $condition['leader_board_no'] = 1; */

        $location_to_chose = array_rand(['country', 'region', 'state', 'devision']);
        // $location_to_chose = array_rand(['country']);
        $displayText = "Number 1 in ";

        if ($location_to_chose == 0) {
            $getCountry = LeaderboardLog::where($condition)->inRandomOrder()->first();

            // print "<pre>";
            // echo '$getCountry';
            // print_r($condition);
            // dd($getCountry);
            // die();
            if ($getCountry) {
                $condition['country_id'] = $getCountry->country_id;
                $displayText = $displayText . $getCountry->getCountry['name']; //" country";
            }
        } else if ($location_to_chose == 1) {
            $getregion = LeaderboardLog::where($condition)->inRandomOrder()->first();

            // print "<pre>";
            // echo '$getregion';
            // print_r($getregion);
            // die();
            if ($getregion) {
                $condition['region_id'] = $getregion->region_id;
                $displayText = $displayText . $getregion->getRegion['name'] . " region";
            }
        } else if ($location_to_chose == 2) {
            $getdevision = LeaderboardLog::where($condition)->inRandomOrder()->first();

            //   print "<pre>";
            //   echo '$getdevision';
            //   print_r($getdevision);
            //   die();
            if ($getdevision) {
                $condition['devision_id'] = $getdevision->devision_id;
                $displayText = $displayText . $getdevision->getState['name']; //." state";
            }
        } else {
            $getstate = LeaderboardLog::where($condition)->inRandomOrder()->first();

            // print "<pre>";
            // echo '$getstate';
            // print_r($getstate);
            // die();
            if ($getstate) {
                $condition['state_id'] = $getstate->state_id;
                $displayText = $displayText . $getstate->getDevision['name']; //." devision";
            }
        }
        $getLeaderboardList = LeaderboardLog::with('userInfo')
                ->where($condition)
                ->where('is_active', 1)
                // ->where('user_id', '!=', $user_id)
                ->groupBy('user_id')
                ->selectRaw('*, sum(total_vote) as total_vote_count')
                ->orderBy('total_vote_count', 'DESC')
                ->first();
        // dd($getLeaderboardList);

        $get_followers_list = array();
        $count_followed_by = NULL;
        if ($user = User::find($user_id)) {

            if ($request->followed_by_count != $user->total_followed_by) {
                $get_followers_list = $user->follows;
                $count_followed_by = count($get_followers_list);
            } else {
                $count_followed_by = $request->followed_by_count;
            }
        }


        if (isset($getLeaderboardList->user_id)) {
            $followersCheck = Followers::where('user_id', $user_id)->where('is_following', $getLeaderboardList->user_id)->first();

            $getLeaderboardList['is_following'] = false;
            $is_following['array'] = $followersCheck;
            if (!empty($followersCheck)) {
                $getLeaderboardList['is_following'] = true;
                $is_following['array'] = $followersCheck;
            }

            //echo json_encode($getLeaderboardList);die;

            /*
              $data = array();
              $data['getLeaderboardList'] =  $getLeaderboardList;
              $data['displayText'] =  $displayText; */

            if (!empty($getLeaderboardList)) {
                $response = Api::make_response(1, ['ranks' => $getLeaderboardList, 'followed_by' => $get_followers_list, 'followed_by_count' => $count_followed_by], $displayText);
            }
        } else {
            $response = Api::make_response(0, ['ranks' => new \stdClass(), 'followed_by' => $get_followers_list, 'followed_by_count' => $count_followed_by], "Record not found");
        }

        return $response;
    }

    public function month_interval_list(Request $request) {   //05/06/2020

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',       
                    'year' => 'required',  
                    
        ]);
        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        $leaderboard_list = [
                            ['leaderboard_no'=> 1,
                             'compitition_span' => 'Competition #1',
                             'compititionspan_month_year' => 'January - April'],
                             ['leaderboard_no'=> 2,
                             'compitition_span' => 'Competition #2',
                             'compititionspan_month_year' => 'May - August'],
                             ['leaderboard_no'=> 3,
                             'compitition_span' => 'Competition #3',
                             'compititionspan_month_year' => 'September - December']
                        ];

        return Api::make_response(1, $leaderboard_list, 'All Month Interval List.');
    }

    public function get_leaderboard_list(Request $request) {  //work 
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        
        $location_data = [];
        $page_type = $request_data['page'];
        $previous_month = $request_data['previous_month'];
        $previous_year = $request_data['previous_year'];
        $location_data = [];
        $finalListArray = [];
        
        
        if (empty($request_data['year']) && empty($request_data['previous_year']) ) {
            $previous_year = date('Y');
        }
        if (empty($request_data['leaderboard_no']) && empty($request_data['previous_month']) ) {
            $previous_month = date('m');
        }
        
        if (isset($request_data['year']) && $request_data['year']) {
            $previous_year = $request_data['year'];
        }
        if (isset($request_data['leaderboard_no']) && $request_data['leaderboard_no']) {
            $previous_month = $this->set_month($request_data['leaderboard_no']);
        }
        
        for ($lbc = 0; $lbc < 9; $lbc++) {
            if ($page_type == "Prev") {
                if ($lbc == 0) {
                    $current_month = $orginal_previous_month = $previous_month;
                    $current_year = $orginal_previous_year = $previous_year;
                } else {
                    $orginal_previous_month = $previous_month;
                    $orginal_previous_year = $previous_year;
                    $current_month = date('m', strtotime($previous_year . '-' . $previous_month . ' -4 months'));
                    $current_year = date('Y', strtotime($previous_year . '-' . $previous_month . ' -4 months'));
                    $previous_month = date('m', strtotime($orginal_previous_year . '-' . $orginal_previous_month . ' -4 months'));
                    $previous_year = date('Y', strtotime($orginal_previous_year . '-' . $orginal_previous_month . ' -4 months'));
                }
            } elseif ($page_type == "Next") {
                if ($lbc == 0) {
                    $current_month = $orginal_previous_month = $previous_month;
                    $current_year = $orginal_previous_year = $previous_year;
                } else {
                    $orginal_previous_month = $previous_month;
                    $orginal_previous_year = $previous_year;
                    $current_month = date('m', strtotime($previous_year . '-' . $previous_month . ' +4 months'));
                    $current_year = date('Y', strtotime($previous_year . '-' . $previous_month . ' +4 months'));
                    $previous_month = date('m', strtotime($orginal_previous_year . '-' . $orginal_previous_month . ' +4 months'));
                    $previous_year = date('Y', strtotime($orginal_previous_year . '-' . $orginal_previous_month . ' +4 months'));
                }
            } else {
                $current_month = $previous_month;
                $current_year = $previous_year;
            }

            $now = new \DateTime('now');
            $days = 0;
            //$current_month = $now->format('m');
            //$current_year = $now->format('Y');

            $current_leaderboard = LeaderboardLog::getCurrentLeaderBoard($current_month);

            $current_leaderboard_end_date = LeaderboardLog::getCurrentLeaderBoardEndingDate($current_leaderboard);
            //$current_leaderboard_month = LeaderboardLog::getCurrentLeaderBoardMonth($current_leaderboard);
            $compititionspan = $compititionspan_month_year = "";
            if ($current_leaderboard == 1) {
                $compititionspan = "Competition #1 ";
                $compititionspan_month_year = "January - April " . $current_year;
            } elseif ($current_leaderboard == 2) {
                $compititionspan = "Competition #2 ";
                $compititionspan_month_year = "May - August " . $current_year;
            } elseif ($current_leaderboard == 3) {
                $compititionspan = "Competition #3 ";
                $compititionspan_month_year = "September - December " . $current_year;
            }

            $condition = [];
            $top = 250;

            if (isset($request_data['country_id']) && $request_data['country_id']) {
                $condition['country_id'] = $request_data['country_id'];
                $country_data = Country::find($request_data['country_id']);
                $location_data['country'] = $country_data;
            }
            if (isset($request_data['region_id']) && $request_data['region_id']) {
                $condition['region_id'] = $request_data['region_id'];
                $region_data = Region::find($request_data['region_id']);
                $location_data['region'] = $region_data;
                $top = 200;
            }
            if (isset($request_data['devision_id']) && $request_data['devision_id']) {
                $condition['devision_id'] = $request_data['devision_id'];
                $division_data = Devision::find($request_data['devision_id']);
                $location_data['division'] = $division_data;
                $top = 150;
            }
            if (isset($request_data['state_id']) && $request_data['state_id']) {
                $condition['state_id'] = $request_data['state_id'];
                $state_data = State::find($request_data['state_id']);
                $location_data['state'] = $state_data;
                $top = 100;
            }
            $condition['leader_board_no'] = $current_leaderboard;

            $getLeaderboardList = LeaderboardLog::with('userInfo')
                    ->where($condition)
                    ->where('year', $current_year)
                    ->where('is_active', 1)
                    ->groupBy('user_id')
                    ->selectRaw('*, sum(total_vote) as total_vote')
                    // ->selectRaw('*,`leaderboard_log`.`user_id` as l_u_id, (select count(*) from `posts` inner join `post_like_log` on `posts`.`id`=`post_like_log`.`post_id` where `posts`.`user_id` = `l_u_id` AND (`posts`.`video_name` is not NULL OR `posts`.`audio_name` is not NULL)) as `total_vote`')
                    ->orderBy('total_vote', 'DESC')
                    ->get();
            $finalList = $this->getFinalList($getLeaderboardList, $top);
            $followinglist = array();
            if (isset($request_data['user_id'])) {
                $followinglist = Followers::where('user_id', $request_data['user_id'])->pluck('is_following')->toArray();
            }
            //---------------------------
            foreach ($finalList as $key => $flList) {
                $finalList[$key]->amount = rand(1000,9999);

                if (in_array($flList['user_id'], $followinglist)) {
                    $finalList[$key]->is_following = true;
                } else {
                    $finalList[$key]->is_following = false;
                }
            }
           
                $finalListArray[$lbc]['meta_data'] = ['location_data'=> $location_data,'compitition_text' => $compititionspan, 'compitition_month_year_text' => $compititionspan_month_year];
                $finalListArray[$lbc]['leaderboard_data'] = $finalList;
            
        }
        //---------------------------
        //$finalListArray = $finalList;

        if (count($finalListArray)) {
               if ($page_type == "Prev") {
                        $current_month = date('m', strtotime($previous_year . '-' . $previous_month . ' -4 months'));
                        $current_year = date('Y', strtotime($previous_year . '-' . $previous_month . ' -4 months'));
                }
                else{
                        $current_month = date('m', strtotime($previous_year . '-' . $previous_month . ' +4 months'));
                        $current_year = date('Y', strtotime($previous_year . '-' . $previous_month . ' +4 months'));
                }
                //dd($condition, $current_month ,$current_year );
            $response = Api::make_response(1, $finalListArray, 'Leaderboar Found',['previous_month'=>$current_month,'previous_year'=>$current_year]);
        } else {
            $response = Api::make_response(0, [], 'No Data Found', ['compitition_text' => $compititionspan, 'compitition_month_year_text' => $compititionspan_month_year, 'currentYear' => $current_year, 'current_leaderboard_month' => $current_leaderboard_month]);
        }
        return $response;
    }
    
    //17/06/2020
    public function mobile_leaderboard_list(Request $request) {  //work 
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        
        $all_response_data = [];
        $page_type = $request_data['page'];
        $previous_month = date('m');
        $previous_year =  date('Y');
        $location_data = [];
        $condition = [];
        
        
        if (empty($request_data['year'])) {
            $previous_year = date('Y');
        }

        if (empty($request_data['leaderboard_no'])) {
            $previous_month = date('m');
        }

        if (isset($request_data['year']) && $request_data['year']) {
            $previous_year = $request_data['year'];
        }

        if (isset($request_data['leaderboard_no']) && $request_data['leaderboard_no']) {
            $previous_month = $this->set_month($request_data['leaderboard_no']);
        }

        $current_month = $previous_month;
        $current_year = $previous_year;
        
        $current_leaderboard = LeaderboardLog::getCurrentLeaderBoard($current_month);

        $current_leaderboard_end_date = LeaderboardLog::getCurrentLeaderBoardEndingDate($current_leaderboard);
            if ($current_leaderboard == 1) {
                $compititionspan = "Competition #1 ";
                $compititionspan_month_year = "January - April " . $current_year;
            } elseif ($current_leaderboard == 2) {
                $compititionspan = "Competition #2 ";
                $compititionspan_month_year = "May - August " . $current_year;
            } elseif ($current_leaderboard == 3) {
                $compititionspan = "Competition #3 ";
                $compititionspan_month_year = "September - December " . $current_year;
            }

        $condition['leader_board_no'] = $current_leaderboard;
        $top = 250;

            if (isset($request_data['country_id']) && $request_data['country_id']) {
                $condition['country_id'] = $request_data['country_id'];
                $country_data = Country::find($request_data['country_id']);
                $location_data['country'] = $country_data;
            }
            if (isset($request_data['region_id']) && $request_data['region_id']) {
                $condition['region_id'] = $request_data['region_id'];
                $region_data = Region::find($request_data['region_id']);
                $location_data['region'] = $region_data;
                $top = 200;
            }
            if (isset($request_data['devision_id']) && $request_data['devision_id']) {
                $condition['devision_id'] = $request_data['devision_id'];
                $division_data = Devision::find($request_data['devision_id']);
                $location_data['division'] = $division_data;
                $top = 150;
            }
            if (isset($request_data['state_id']) && $request_data['state_id']) {
                $condition['state_id'] = $request_data['state_id'];
                $state_data = State::find($request_data['state_id']);
                $location_data['state'] = $state_data;
                $top = 100;
            }

        $getLeaderboardList = LeaderboardLog::with('userInfo')
                ->where($condition)
                ->where('year', $current_year)
                ->where('is_active', 1)
                ->groupBy('user_id')
                ->selectRaw('*, sum(total_vote) as total_vote')
                ->orderBy('total_vote', 'DESC')
                ->get();

        $finalList = $this->getFinalList($getLeaderboardList, $top);
        $followinglist = array();
            if (isset($request_data['user_id'])) {
                $followinglist = Followers::where('user_id', $request_data['user_id'])->pluck('is_following')->toArray();
            }
       
            foreach ($finalList as $key => $flList) {
                $finalList[$key]->amount = rand(1000,9999);

                if (in_array($flList['user_id'], $followinglist)) {
                    $finalList[$key]->is_following = true;
                } else {
                    $finalList[$key]->is_following = false;
                }

            }

            $all_response_data['meta_data'] = ['location_data'=> $location_data,'compitition_text' => $compititionspan, 'compitition_month_year_text' => $compititionspan_month_year];
            $all_response_data['leaderboard_data'] = $finalList;
        
        if (count($all_response_data)) {
            $response = Api::make_response(1, [$all_response_data], 'Leaderboar Found');
        } else {
            $response = Api::make_response(0, [], 'No Data Found', []);
        }
        return $response;
    }
    public function getFinalList($getLeaderboardList, $top) {

        $i = 1;
        $total_vote = 0;
        $rank = 1;
        $finalList = [];
        foreach ($getLeaderboardList as $key => $list) {
            if ($rank == $top && ($total_vote != $list->total_vote)) {
                break;
            }
            if ($total_vote == $list->total_vote) {
                // $top++;
                $i--;
                $list->rank = $i;
                $rank = $i;
                $i++;
            } else {
                $list->rank = $i;
                $rank = $i;
                $i++;
            }

            $total_vote = $list->total_vote;
            $finalList[] = $list;
        }
        return $finalList;
    }

}
