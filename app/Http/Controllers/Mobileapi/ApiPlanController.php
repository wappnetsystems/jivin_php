<?php

namespace App\Http\Controllers\Mobileapi;

use Illuminate\Http\Request;
use Hash;
use App\Http\Requests;
use App\Api;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Posts;
use App\Plans;
use App\PostLogs;
use App\PostLikeLog;
use App\PostShareLog;
use App\LeaderboardLog;
use App\NotificationLog;
use App\Subscription;
use App\Email;
use App\UserCard;
use App\Payeezy;
use App\UserTransaction;
use App\UserSubcriptionHistory;
use App\BoostPlans;
use App\UserBoost;
use App\PostBoost;
use App\Country;
use App\Region;
use App\Devision;
use App\State;
use App\AdBid;
use App\AccountSettings;
use App\SupportRequest;
use App\Lib\Upload_file;
use  App\SupportRequestAttachment;
use App\Followers;
use App\Crypto_prices;
use App\Jackpot_amount;
use App\SiteSetting;
use Illuminate\Support\Facades\DB;

class ApiPlanController extends Controller {

    public function sendmsg(Request $request) {
        $data = $request->json()->all();
        $response = [];
        $notificationLogAdd = new NotificationLog;
        $notificationLogAdd->activity_type = "contact_msg";
        $notificationLogAdd->activity_by = $data['activity_by'];
        $notificationLogAdd->visible_by = $data['visible_by'];
        $notificationLogAdd->related_id = 0;
        $notificationLogAdd->notification_text = "You have new message";
        $notificationLogAdd->msg_text = $data['msg'];
        $notificationLogAdd->is_read = 0;

        if ($notificationLogAdd->save()) {
            //if($data){
            $response = Api::make_response(1, $data, 'Plans get successfully');
        } else {
            $response = Api::make_response(0, [], 'Plans not found');
        }
        return $response;
    }

    public function mobile_get_all_plan(Request $request) {

        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = array();
        $plans = Plans::where('is_active', 1)->get()->toArray();


        foreach ($plans as $key => $eachPlan) {

            $all_boost_arr = array();
            $all_boost_location = array();

            if ($eachPlan['allow_boost_count']) {
                array_push($all_boost_arr, $eachPlan['allow_boost_count']);
                array_push($all_boost_location, 'Country');
            }if ($eachPlan['allow_boost_count_region']) {
                array_push($all_boost_arr, $eachPlan['allow_boost_count_region']);
                array_push($all_boost_location, 'Region');
            }if ($eachPlan['allow_boost_count_division']) {
                array_push($all_boost_arr, $eachPlan['allow_boost_count_division']);
                array_push($all_boost_location, 'Division');
            }if ($eachPlan['allow_boost_count_state']) {
                array_push($all_boost_arr, $eachPlan['allow_boost_count_state']);
                array_push($all_boost_location, 'State');
            }
            $plans[$key]['all_boost_arr'] = $all_boost_arr;
            $plans[$key]['all_boost_location'] = $all_boost_location;
        }
        // echo '<pre>';print_r($plans);die;


        if (!empty($plans)) {
            $response = Api::make_response(1, $plans, 'Plans list');
        } else {
            $response = Api::make_response(0, [], 'Plans not found');
        }

        return $response;
    }

    public function subscribeboost(Request $request) {
        $data = $request->json()->all();
        $response = [];
        $userCardlist = [];
        $num_of_boost = 0;
        $boosttype = '';

        // $response =  Api::make_response(1,$num_of_boost,$boosttype);	
        // return $response;  

        if ($data['plan_id'] != '' && $data['user_id'] != '') {

            if ($data['plan_id'] != '' && $data['user_id'] != '' && $data['save_card'] = '1') {

                $checkUserCard = UserCard::where('user_id', $data['user_id'])->where('card_no', $data['credit_card']['card_number'])->first();
                if (!empty($checkUserCard)) {
                    $checkUserCard->user_id = $data['user_id'];
                    $checkUserCard->card_type = $data['credit_card']['type'];
                    $checkUserCard->card_exp_date = $data['credit_card']['exp_date'];
                    $checkUserCard->cardholder_name = $data['credit_card']['cardholder_name'];
                    $checkUserCard->card_no = $data['credit_card']['card_number'];
                    $checkUserCard->status = 1;
                    $checkUserCard->save();
                } else {

                    $checkUserCard = new UserCard;
                    $checkUserCard->user_id = $data['user_id'];
                    $checkUserCard->card_type = $data['credit_card']['type'];
                    $checkUserCard->card_exp_date = $data['credit_card']['exp_date'];
                    $checkUserCard->cardholder_name = $data['credit_card']['cardholder_name'];
                    $checkUserCard->card_no = $data['credit_card']['card_number'];
                    $checkUserCard->status = 1;
                    $checkUserCard->save();
                }
                $plan_dtails = BoostPlans::find($data['plan_id']);
                $price = $plan_dtails->amount;
                $country_boost_count = $plan_dtails->country_boost_count;
                $region_boost_count = $plan_dtails->region_boost_count;
                $division_boost_count = $plan_dtails->division_boost_count;
                $state_boost_count = $plan_dtails->state_boost_count;
                /*
                  $boost_details = explode("***",$data['boost_type']);
                  $boost_details = explode("***",$data['boost_type']);

                  if($boost_details[0]!='' && $boost_details[0]>0){
                  $num_of_boost = $boost_details[0];
                  $boosttype = $boost_details[1];
                  }
                 */


                $card_details = array("amount" => $price * 100,
                    "card_number" => $data['credit_card']['card_number'],
                    "card_type" => $data['credit_card']['type'],
                    "card_holder_name" => $data['credit_card']['cardholder_name'],
                    "card_cvv" => $data['credit_card']['cvv'],
                    "card_expiry" => $data['credit_card']['exp_date'],
                    "merchant_ref" => "Astonishing-Sale",
                    "currency_code" => "USD",
                    "method" => "credit_card");

                $payeezy = new Payeezy;
                $transaction = json_decode($payeezy->purchase($card_details));

                if (isset($transaction->code) && $transaction->code) {
                    return Api::make_response(0, $transaction->code, 'Unauthorized Access! Please check your credentials.');
                }

                /* if($transaction->validation_status!='success' && $transaction->transaction_status!='approved') {
                  return Api::make_response(0,$transaction->code, 'Unauthorized Access! Please check your credentials.');
                  } */
                /* if($transaction->code != ''){
                  $response =  Api::make_response(0,$transaction->code,$transaction->message);
                  }else{


                  if($transaction->transaction_status == "approved"){ */


                //$transaction=array("transaction_status"=>"approved");
                //echo "santosh<pre>";print_r($transaction);die;
                if ($transaction->transaction_status == 'approved') {
                    $status = 1;
                } else {
                    $status = 0;
                }
                $instUserTransaction = new UserTransaction;
                $instUserTransaction->user_id = $data['user_id'];
                $instUserTransaction->transaction_details = json_encode($transaction);
                $instUserTransaction->transaction_for = "Boost Plan Subscription";
                $instUserTransaction->transaction_id = isset($transaction->transaction_id) ? $transaction->transaction_id : NULL;
                $instUserTransaction->user_card_id = !empty($checkUserCard->id) ? $checkUserCard->id : NULL;
                $instUserTransaction->method = $transaction->method;
                $instUserTransaction->transaction_status = $transaction->transaction_status;
                $instUserTransaction->bank_resp_code = isset($transaction->bank_resp_code) ? $transaction->bank_resp_code : NULL;
                $instUserTransaction->amount = $price;
                $instUserTransaction->status = $status;
                $instUserTransaction->save();

                //Email::sendsubScriptionMail($data['user_id']);	

                $userCardlist = UserCard::where('user_id', $data['user_id'])->get();


                $userboost = new UserBoost;
                $userboost->user_id = $data['user_id'];
                $userboost->boost_plan = $data['plan_id'];
                $userboost->is_active = 1;
                if ($userboost->save()) {
                    //****** boost update start ************//
                    //$user_boost = User::where("id",$data['user_id'])->first()->total_boost;

                    $user_boost = User::where("id", $data['user_id'])->first();
                    $user_boost_update = User::find($data['user_id']);
                    /*
                      $user_boost_update = User::find($data['user_id']);
                      if($boosttype == 'country'){
                      $user_boost_update->country_boost_avaliable = $user_boost['country_boost_avaliable']+$num_of_boost;
                      $user_boost_update->country_boost = $user_boost['country_boost']+$num_of_boost;

                      }else if($boosttype == 'region'){
                      $user_boost_update->region_boost_avaliable = $user_boost['region_boost_avaliable']+$num_of_boost;
                      $user_boost_update->region_boost = $user_boost['region_boost']+$num_of_boost;

                      }else if($boosttype == 'division'){
                      $user_boost_update->division_boost_avaliable = $user_boost['division_boost_avaliable']+$num_of_boost;
                      $user_boost_update->division_boost = $user_boost['division_boost']+$num_of_boost;

                      }else if($boosttype == 'state'){
                      $user_boost_update->state_boost_avaliable = $user_boost['state_boost_avaliable']+$num_of_boost;
                      $user_boost_update->state_boost = $user_boost['state_boost']+$num_of_boost;

                      }
                     */
                    //$user_boost_update->total_boost = $user_boost+$num_of_boost;

                    $user_boost_update->country_boost_avaliable = $user_boost['country_boost_avaliable'] + $country_boost_count;
                    $user_boost_update->country_boost = $user_boost['country_boost'] + $country_boost_count;

                    $user_boost_update->region_boost_avaliable = $user_boost['region_boost_avaliable'] + $region_boost_count;
                    $user_boost_update->region_boost = $user_boost['region_boost'] + $region_boost_count;

                    $user_boost_update->division_boost_avaliable = $user_boost['division_boost_avaliable'] + $division_boost_count;
                    $user_boost_update->division_boost = $user_boost['division_boost'] + $division_boost_count;

                    $user_boost_update->state_boost_avaliable = $user_boost['state_boost_avaliable'] + $state_boost_count;
                    $user_boost_update->state_boost = $user_boost['state_boost'] + $state_boost_count;

                    $user_boost_update->save();
                    //****** boost update end ************//

                    $response = Api::make_response(1, ['plan' => $plan_dtails, 'userboost' => $userboost, 'transaction' => $transaction, 'user_card_list' => $userCardlist], 'Plans get successfully');
                } else {
                    $response = Api::make_response(0, [], 'Plans not found');
                }
            }
        }
        return $response;
    }

    public function subscription(Request $request) {
        $data = $request->json()->all();
        $response = [];
        $userCardlist = [];

        if ($data['plan_id'] != '' && $data['user_id'] != '') {
            $plan_dtails = Plans::find($data['plan_id']);
            $boost_for_location = $plan_dtails->boost_for_location;
            $country_boost = ($plan_dtails->allow_boost_count) ? $plan_dtails->allow_boost_count : 0;
            $region_boost = ($plan_dtails->allow_boost_count_region) ? $plan_dtails->allow_boost_count_region : 0;
            $division_boost = ($plan_dtails->allow_boost_count_division) ? $plan_dtails->allow_boost_count_division : 0;
            $state_boost = ($plan_dtails->allow_boost_count_state) ? $plan_dtails->allow_boost_count_state : 0;

            $price = $plan_dtails->price;
            $plantype = $plan_dtails->type;
            $user_id = $data['user_id'];
            $expired_date = date('Y-m-d', strtotime("+" . $plan_dtails['duration'] . " days"));

            $checksubscription = Subscription::where('user_id', $data['user_id'])->first();


            /*             * ***** 20-07-2017 added for Subcription history start *** */
            $usersubcription = new UserSubcriptionHistory;
            $usersubcription->user_id = $data['user_id'];
            $usersubcription->package_id = $data['plan_id'];
            $usersubcription->save();
            /*             * ***** 20-07-2017 added for Subcription history end *** */

            //echo json_encode($card_details);die;
            if ($plantype != "Free") {

                if ($data['plan_id'] != '' && $data['user_id'] != '' && $data['save_card'] == '1') {

                    $checkUserCard = UserCard::where('user_id', $data['user_id'])->where('card_no', $data['credit_card']['card_number'])->first();
                    //echo json_encode($checkUserCard);die;
                    if (!empty($checkUserCard)) {
                        $instertUserCard = UserCard::find($checkUserCard->id);
                        $instertUserCard->user_id = $data['user_id'];
                        $instertUserCard->card_type = $data['credit_card']['type'];
                        $instertUserCard->card_exp_date = $data['credit_card']['exp_date'];
                        $instertUserCard->cardholder_name = $data['credit_card']['cardholder_name'];
                        $instertUserCard->card_no = $data['credit_card']['card_number'];
                        $instertUserCard->status = 1;
                        $instertUserCard->save();
                    } else {

                        $instertUserCard = new UserCard;
                        $instertUserCard->user_id = $data['user_id'];
                        $instertUserCard->card_type = $data['credit_card']['type'];
                        $instertUserCard->card_exp_date = $data['credit_card']['exp_date'];
                        $instertUserCard->cardholder_name = $data['credit_card']['cardholder_name'];
                        $instertUserCard->card_no = $data['credit_card']['card_number'];
                        $instertUserCard->status = 1;
                        $instertUserCard->save();
                    }
                }


                $card_details = array("amount" => $price * 100,
                    "card_number" => $data['credit_card']['card_number'],
                    "card_type" => $data['credit_card']['type'],
                    "card_holder_name" => $data['credit_card']['cardholder_name'],
                    "card_cvv" => $data['credit_card']['cvv'],
                    "card_expiry" => $data['credit_card']['exp_date'],
                    "merchant_ref" => "Astonishing-Sale",
                    "currency_code" => "USD",
                    "method" => "credit_card");
                /*
                  $transaction = json_decode(Payeezy::purchase($card_details));

                  //echo json_encode($transaction);die;

                  if($transaction->code != ''){
                  $response =  Api::make_response(0,$transaction->code,$transaction->message);
                  }else{


                  if($transaction->transaction_status == "approved"){
                 */

                $transaction = array("transaction_status" => "approved");

                $userCardlist = UserCard::where('user_id', $data['user_id'])->get();


                $instUserTransaction = new UserTransaction;
                $instUserTransaction->user_id = $data['user_id'];
                $instUserTransaction->transaction_details = json_encode($transaction);
                $instUserTransaction->transaction_for = "Plan Subscription";
                $instUserTransaction->amount = $price;
                $instUserTransaction->save();

                Email::sendsubScriptionMail($data['user_id']);
                if (!empty($checksubscription)) {

                    //echo $checksubscription['id'];die;
                    $addsubscription = Subscription::find($checksubscription['id']);
                    $addsubscription->user_id = $user_id;
                    $addsubscription->package_id = $data['plan_id'];
                    $addsubscription->package_valide_from = date('Y-m-d');
                    $addsubscription->package_valide_to = $expired_date;
                    $addsubscription->is_active = 1;
                    if ($addsubscription->save()) {
                        //****** boost update start ************//
                        /*
                          $user_boost = User::where("id",$data['user_id'])->first()->total_boost;
                          $user_boost_update = User::find($data['user_id']);
                          $user_boost_update->total_boost = $user_boost+$allow_boost_count;
                          $user_boost_update->save(); // boost_for_location
                         */
                        $user_boost = User::where("id", $data['user_id'])->first();

                        $user_boost_update = User::find($data['user_id']);
                        if ($plan_dtails->allow_boost_count) {
                            $user_boost_update->country_boost_avaliable = $user_boost['country_boost_avaliable'] + $country_boost;
                            $user_boost_update->country_boost = $user_boost['country_boost'] + $country_boost;
                        }
                        if ($plan_dtails->allow_boost_count_region) {
                            $user_boost_update->region_boost_avaliable = $user_boost['region_boost_avaliable'] + $region_boost;
                            $user_boost_update->region_boost = $user_boost['region_boost'] + $region_boost;
                        }
                        if ($plan_dtails->allow_boost_count_division) {
                            $user_boost_update->division_boost_avaliable = $user_boost['division_boost_avaliable'] + $division_boost;
                            $user_boost_update->division_boost = $user_boost['division_boost'] + $division_boost;
                        }
                        if ($plan_dtails->allow_boost_count_state) {
                            $user_boost_update->state_boost_avaliable = $user_boost['state_boost_avaliable'] + $state_boost;
                            $user_boost_update->state_boost = $user_boost['state_boost'] + $state_boost;
                        }
                        $user_boost_update->save();


                        //****** boost update end ************//
                        $response = Api::make_response(1, array('plan' => $plan_dtails, 'transaction' => $transaction, 'user_card_list' => $userCardlist), 'Plans details');
                    } else {
                        $response = Api::make_response(0, [], 'Somethings wrong');
                    }
                } else {
                    $addsubscription = new Subscription;
                    $addsubscription->user_id = $user_id;
                    $addsubscription->package_id = $data['plan_id'];
                    $addsubscription->package_valide_from = date('Y-m-d');
                    $addsubscription->package_valide_to = $expired_date;
                    $addsubscription->is_active = 1;
                    if ($addsubscription->save()) {
                        //****** boost update start ************//

                        $user_boost = User::where("id", $data['user_id'])->first()->total_boost;
                        $user_boost_update = User::find($data['user_id']);
                        $user_boost_update->total_boost = $user_boost + $country_boost + $region_boost + $division_boost + $state_boost;
                        $user_boost_update->save();
                        //****** boost update end ************//
                        $response = Api::make_response(1, array('plan' => $plan_dtails, 'transaction' => $transaction, 'user_card_list' => $userCardlist), 'Plans details');
                    } else {
                        $response = Api::make_response(0, [], 'Somethings wrong');
                    }
                }

                /*
                  }else{
                  $response =  Api::make_response(0,$transaction,'Transaction error');
                  }
                  } */

                //$response =  Api::make_response(0,$instertUserCard,'Transaction error');
            } else {

                if ($plantype == "Free") {

                    $userCardlist = UserCard :: where('user_id', $data['user_id'])->get();

                    Email::sendsubScriptionMail($data['user_id']);
                    if (!empty($checksubscription)) {
                        //echo $checksubscription['id'];die;
                        $addsubscription = Subscription::find($checksubscription['id']);
                        $addsubscription->user_id = $user_id;
                        $addsubscription->package_id = $data['plan_id'];
                        $addsubscription->package_valide_from = date('Y-m-d');
                        $addsubscription->package_valide_to = $expired_date;
                        $addsubscription->is_active = 1;
                        if ($addsubscription->save()) {
                            $response = Api::make_response(1, array('plan' => $plan_dtails, 'user_card_list' => $userCardlist), 'Plans details');
                        } else {
                            $response = Api::make_response(0, [], 'Somethings wrong');
                        }
                    } else {
                        $addsubscription = new Subscription;
                        $addsubscription->user_id = $user_id;
                        $addsubscription->package_id = $data['plan_id'];
                        $addsubscription->package_valide_from = date('Y-m-d');
                        $addsubscription->package_valide_to = $expired_date;
                        $addsubscription->is_active = 1;
                        if ($addsubscription->save()) {
                            $response = Api::make_response(1, array('plan' => $plan_dtails, 'user_card_list' => $userCardlist), 'Plans details');
                        } else {
                            $response = Api::make_response(0, [], 'Somethings wrong');
                        }
                    }
                } else {
                    $response = Api::make_response(0, $transaction, 'Transaction error');
                }
            }


            //echo json_encode($transaction->transaction_status);die;
        }
        return $response;
    }

    public function postyourboostold(Request $request) {
        $data = $request->json()->all();
        $response = [];
        $boostCount = User::where("id", $data['loginuserid'])->first()->total_boost;
        if ($boostCount > 0) {
            $userUpdate = User::find($data['loginuserid']);
            $userUpdate->total_boost = $boostCount - 1;
            if ($userUpdate->save()) {
                $postLogstbl = PostLogs::where("post_id", $data['postid'])->get();
                foreach ($postLogstbl as $key => $value) {
                    //$data = ['boost_order' => 1, "updated_at" =>date("Y-m-d H:i:s")];
                    //$postLogsupdate = PostLogs::find($value['id'])->update($data);
                    $postLogsupdate = PostLogs::find($value['id']);
                    $postLogsupdate->boost_order = 1;
                    $postLogsupdate->updated_at = date("Y-m-d H:i:s");
                    $postLogsupdate->save();
                }
            }

            $response = Api::make_response(1, [], 'You have successfully boosted');
        } else {
            $response = Api::make_response(0, [], 'Sorry!!! You do not have any boost in your account');
        }
        return $response;
    }

// use App\PostBoost;
// use App\Country;
// use App\Region;
// use App\Devision;
// use App\State;
    public function postyourboost(Request $request) {
        $data = $request->json()->all();
        $response = [];
        if ($data > 0) {

            if ($data['boost_location'] != '' && $data['boost_location_type'] != '' && $data['postid'] != '' && $data['loginuserid'] != '') {

                $userfind = User::find($data['loginuserid']);
                if ($data['boost_location_type'] == "Country") {

                    //$stateslist = Country::with("regions.divisions.states")->where("slug",$data['boost_location'])->get();
                    $sqlsatelist = "SELECT id FROM state WHERE division_id IN (SELECT id FROM division WHERE region_id IN(SELECT id FROM region WHERE country_id IN(SELECT id FROM country where slug = '" . $data['boost_location'] . "' and is_active = 1) and is_active = 1) and is_active = 1)and is_active = 1";
                    $userfind->decrement('country_boost_avaliable');
                } elseif ($data['boost_location_type'] == "Region") {
                    $sqlsatelist = "SELECT id FROM state WHERE division_id IN (SELECT id FROM division WHERE region_id IN(SELECT id FROM region WHERE slug =  '" . $data['boost_location'] . "' and is_active = 1)and is_active = 1) and is_active = 1";

                    $userfind->decrement('region_boost_avaliable');
                } elseif ($data['boost_location_type'] == "Division") {
                    $sqlsatelist = "SELECT id FROM state WHERE division_id IN (SELECT id FROM division WHERE slug ='" . $data['boost_location'] . "' and is_active = 1) and is_active = 1";
                    $userfind->decrement('division_boost_avaliable');
                } elseif ($data['boost_location_type'] == "State") {
                    $sqlsatelist = "SELECT id FROM state WHERE slug ='" . $data['boost_location'] . "' and is_active = 1";
                    $userfind->decrement('state_boost_avaliable');
                }
                if ($userfind->save()) {
                    $postUpdate = Posts::find($data['postid']);
                    $postUpdate->is_boost = 1;
                    $postUpdate->save();
                }
            }

            $stateslist = DB::select($sqlsatelist);
            if (count($stateslist)) {
                foreach ($stateslist as $key => $value) {

                    $addpostBoost = new PostBoost;
                    $addpostBoost->post_id = $data['postid'];
                    $addpostBoost->user_id = $data['loginuserid'];
                    $addpostBoost->boost_location_id = $value->id;
                    $addpostBoost->save();
                    //echo $value->id;
                }
            }

            $response = Api::make_response(1, $userfind, 'You have successfully boosted');
        } else {
            $response = Api::make_response(0, [], 'Sorry!!! You do not have any boost in your account');
        }
        return $response;
    }

    public function postyourboostcheck(Request $request) {
        $data = $request->json()->all();
        $response = [];
        if ($data > 0) {
            //$loginuser = User::find($data['loginuserid']);
            if ($data['location_type'] == "Country") {
                $loginuser = User::find($data['loginuserid'])->country_boost_avaliable;
                if ($loginuser < 0) {
                    $loginuser = 0;
                }
                $loginusertest = "You have " . $loginuser . " Country boost avaliable";
                $type = "Country";
            } elseif ($data['location_type'] == "Region") {
                $loginuser = User::find($data['loginuserid'])->region_boost_avaliable;
                if ($loginuser < 0) {
                    $loginuser = 0;
                }
                $loginusertest = "You have " . $loginuser . " Region boost avaliable";
                $type = "Region";
            } elseif ($data['location_type'] == "Division") {
                $loginuser = User::find($data['loginuserid'])->division_boost_avaliable;
                if ($loginuser < 0) {
                    $loginuser = 0;
                }
                $loginusertest = "You have " . $loginuser . " Division boost avaliable";
                $type = "Division";
            } elseif ($data['location_type'] == "State") {
                $loginuser = User::find($data['loginuserid'])->state_boost_avaliable;
                if ($loginuser < 0) {
                    $loginuser = 0;
                }
                $loginusertest = "You have " . $loginuser . " State boost avaliable";
                $type = "State";
            }
            //$loginuser = User::where('id',$data['loginuserid'])->where('')->first();

            $response = Api::make_response(1, ["userdtls" => $loginuser, "type" => $type, "boosttext" => $loginusertest], 'You have successfully boosted');
        } else {
            $response = Api::make_response(0, [], 'Sorry!!! You do not have any boost in your account');
        }
        return $response;
    }

    public function bidwinpayment(Request $request) {
        $data = $request->json()->all();
        $response = [];
        $userCardlist = [];

        if ($data['plan_id'] != '' && $data['user_id'] != '') {
            $plan_dtails = AdBid::find($data['plan_id']);

            $price = $plan_dtails->bid_amount;
            $user_id = $data['user_id'];


            if ($data['plan_id'] != '' && $data['user_id'] != '' && $data['save_card'] == '1') {

                $checkUserCard = UserCard::where('user_id', $data['user_id'])->where('card_no', $data['credit_card']['card_number'])->first();
                if (!empty($checkUserCard)) {
                    $instertUserCard = UserCard::find($checkUserCard->id);
                    $instertUserCard->user_id = $data['user_id'];
                    $instertUserCard->card_type = $data['credit_card']['type'];
                    $instertUserCard->card_exp_date = $data['credit_card']['exp_date'];
                    $instertUserCard->cardholder_name = $data['credit_card']['cardholder_name'];
                    $instertUserCard->card_no = $data['credit_card']['card_number'];
                    $instertUserCard->status = 1;
                    $instertUserCard->save();
                } else {

                    $instertUserCard = new UserCard;
                    $instertUserCard->user_id = $data['user_id'];
                    $instertUserCard->card_type = $data['credit_card']['type'];
                    $instertUserCard->card_exp_date = $data['credit_card']['exp_date'];
                    $instertUserCard->cardholder_name = $data['credit_card']['cardholder_name'];
                    $instertUserCard->card_no = $data['credit_card']['card_number'];
                    $instertUserCard->status = 1;
                    $instertUserCard->save();
                }
            }


            $card_details = array("amount" => $price * 100,
                "card_number" => $data['credit_card']['card_number'],
                "card_type" => $data['credit_card']['type'],
                "card_holder_name" => $data['credit_card']['cardholder_name'],
                "card_cvv" => $data['credit_card']['cvv'],
                "card_expiry" => $data['credit_card']['exp_date'],
                "merchant_ref" => "Astonishing-Sale",
                "currency_code" => "USD",
                "method" => "credit_card");

            $transaction = array("transaction_status" => "approved");

            $userCardlist = UserCard::where('user_id', $data['user_id'])->get();

            // echo json_encode($userCardlist);
            // die;
            $instUserTransaction = new UserTransaction;
            $instUserTransaction->user_id = $data['user_id'];
            $instUserTransaction->transaction_details = json_encode($transaction);
            $instUserTransaction->transaction_for = "Bid Payment";
            $instUserTransaction->amount = $price;
            $instUserTransaction->save();

            Email::sendsubScriptionMail($data['user_id']);

            $AdBid_update = AdBid::with('state')->find($data['plan_id']);

            if ($transaction['transaction_status'] == "approved") {
                if (count($AdBid_update) > 0) {
                    $AdBid_update->status = 2;
                    $AdBid_update->is_msg_sent = 1;
                    if ($AdBid_update->save()) {
                        $checknotificationLog = NotificationLog::where('adbid_id', $data['plan_id'])->first();
                        if (count($checknotificationLog) > 0) {
                            $dltNotificationLog = NotificationLog::find($checknotificationLog['id']);
                            $dltNotificationLog->delete();
                            /* find state name */
                            $sate_name = isset($AdBid_update['state']->name) ? $AdBid_update['state']->name : '';

                            $notificationLog = new NotificationLog;
                            $notificationLog->activity_by = $data['user_id'];
                            $notificationLog->visible_by = $data['user_id'];
                            $notificationLog->activity_type = "bid_win";
                            $notificationLog->notification_text = "You have won a Bid Location : $sate_name " . date('jS M, Y', strtotime($AdBid_update->publish_date)) . " (" . date('ga', strtotime($AdBid_update->from_time)) . " - " . date('ga', strtotime($AdBid_update->to_time)) . ")";
                            ;
                            $notificationLog->msg_text = "Your payment has been successfully made. You have won the Bid for the time slot of " . date('ga', strtotime($AdBid_update->from_time)) . " - " . date('ga', strtotime($AdBid_update->to_time)) . " for the date of " . date('jS M, Y', strtotime($AdBid_update->publish_date)) . " for the location $sate_name";
                            $notificationLog->adbid_id = "";
                            $notificationLog->save();
                        }

                        $response = Api::make_response(1, array('transaction' => $transaction, 'user_card_list' => $userCardlist), 'Payment successfully');
                    } else {
                        $response = Api::make_response(0, [], 'Somethings wrong');
                    }
                }
            } else {
                $response = Api::make_response(0, [], 'Somethings wrong');
            }
        }
        return $response;
    }

    public function mobile_current_boosted_location(Request $request) {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        if ($data['user_id'] != '') {
            $getUser = AccountSettings::where('user_id', $data['user_id'])->first()->toArray();
            if (count($getUser) > 0) {
                $response = Api::make_response(1, $getUser, 'User Boost');
            } else {
                $response = Api::make_response(0, [], 'Somethings wrong');
            }
        } else {
            $response = Api::make_response(0, [], 'Somethings wrong');
        }

        return $response;
    }

    public function mobile_all_boost_plan() {
        $boostplans = BoostPlans::where('is_active', 1)->get();
        $response = [];
        if (count($boostplans) > 0) {
            $response = Api::make_response(1, $boostplans, 'Boost plan get successfully');
        } else {
            $response = Api::make_response(0, [], 'No boost plan available.');
        }
        return $response;
    }

    public function mobile_all_boost_count(Request $request) {
        $data = $request->all();
        $userid = $data['user_id'];
        $response = [];
        $usertble = User::find($data['user_id'])->toArray();
        if (count($usertble) > 0 && !empty($usertble)) {
            $response = Api::make_response(1, $usertble, 'Your details');
        } else {
            $response = Api::make_response(0, [0], 'Somthing wrong');
        }

        return $response;
    }

    public function mobile_support_requests(Request $request) {   //nishit

        $data = $request->all();
        $response = [];
        $page = $data['page'];
        $itemsPerPage = $data['itemsPerPage'];

        if (isset($data['user_id']) && User::where('id', '=', $data['user_id'])->first()) {

            $getRequests = SupportRequest::with('attachments')->where('user_id', '=', $data['user_id'])->where('is_main', '=', 1)->orderBy('id', 'DESC')->where('is_deleted', 0)->paginate($itemsPerPage)->toArray();
            //dd($getRequests);
            foreach ($getRequests['data'] as $key => $mainRequest) {
                $getRequests['data'][$key]['comments'] = SupportRequest::getCommets($mainRequest['ticket_no']);
            }
            if ($getRequests) {
                $response = Api::make_response(1, $getRequests, 'Support requests Found');
            } else {
                $response = Api::make_response(0, [], 'Support request Found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide user id');
        }

        return $response;
    }

    //================================ 29/05/2020 ====================================================
    public function mobile_add_support(Request $request) {

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'input_subject' => 'required',
                    'input_message' => 'required'               
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        $files= $request->file("support_attch_file");

        $file_count= count($files);
       
        if($file_count > 0) {
            $list_count=1;
        } else {
            $list_count=0;
        }
        $insert_arr = [
            'user_id' => $request_data['user_id'],
            'subject' => $request_data['input_subject'],
            'user_asked' => $request_data['input_message'],
            'is_attachments'=> $list_count,
            'is_main'=>1,
        	'created_at' => date('Y-m-d H:i:s')
        	
        ];
        if (isset($request_data['input_link'])  && $request_data['input_link']) {
            $insert_arr['user_link'] = $request_data['input_link'];
        }
        $request_id = DB::table('support_request')->insertGetId($insert_arr);
        $support_attch_files =   $request->support_attch_file;
       
        if ( isset($request_data['support_attch_file'])  && $file_count > 0){
         
            foreach ($request->support_attch_file as $key => $file) {
                
                $result = Upload_file::upload_support_attachment_file($file);
                    if ($result['status']) {
                        
                        $SupportRequestModel = new SupportRequestAttachment();
                        $SupportRequestModel->request_id = $request_id;
                        $SupportRequestModel->file_name    = $result['storage_path'];
                        $SupportRequestModel->file_type    = $result['file_type'];
                        $SupportRequestModel->save();
                    }
            
            }
        	
        }

        return Api::make_response(1, [], 'Request support add successfully.');
    }
    //====================================================================================

    public function mobile_check_user_state(Request $request) {
        $data = $request->all();
        $key = $data['username'];
        $apiKeyCount = User::select('users.id')->join('subscription', 'users.id', '=', 'subscription.user_id')->where('users.username', '=', $key)->where('users.is_active', 1)->count();

        if ($apiKeyCount > 0)
            return Api::make_response(1, [], 'Valid User.');

        return Api::make_response(0, [], 'Invalid User');
    }

    public function mobileMapLocationLeaderboard() {
        $response = [];
        $allCountry = Country::where('is_active', '=', 1)->get();
        foreach ($allCountry as $key => $country) {
            $getRegion = Region::where('is_active', '=', 1)->where('country_id', '=', $country->id)->get();
            $response[$country->slug] = ['id' => $country->id];
            foreach ($getRegion as $key => $region) {
                //$response[$country->slug]['regions'][$region->slug] = [];
                $response[$country->slug]['regions'][$region->slug] = ['poly' => $region->poly, 'id' => $region->id];
                $getDivision = Devision::where('is_active', '=', 1)->where('region_id', '=', $region->id)->get();
                foreach ($getDivision as $key => $division) {
                    //$response[$country->slug]['regions'][$region->slug]['divisions'][$division->slug] = [];
                    $response[$country->slug]['regions'][$region->slug]['divisions'][$division->slug] = ['poly' => $division->poly, 'id' => $division->id];
                    $getStates = State::where('is_active', '=', 1)->where('division_id', '=', $division->id)->get();
                    foreach ($getStates as $key => $state) {
                        $response[$country->slug]['regions'][$region->slug]['divisions'][$division->slug]['states'][$state->name] = ['id' => $state->id, 'code' => $state->slug, 'poly' => $state->poly];
                    }
                }
            }
        }
        return response()->json($response);
    }

    public function mobileLeaderboardList(Request $request) {
        $data = $request->all();
        $response = [];
        if ($data) {
            $now = new \DateTime('now');
            // $now = new \DateTime('01-05-2018');

            $compititionspan = '';

            if ($data['is_wall_of_fame']) {
                if (isset($data['month']) && !empty($data['month'])) {
                    $current_month = $data['month'];
                } else {
                    $current_month = $now->format('m');
                }
                if (isset($data['year']) && !empty($data['year'])) {
                    $current_year = $data['year'];
                } else {
                    $current_year = $now->format('Y');
                }
            } else {
                $current_month = $now->format('m');
                $current_year = $now->format('Y');
            }

            $days = 0;

            $current_leaderboard = LeaderboardLog::getCurrentLeaderBoard($current_month);

            $current_leaderboard_end_date = LeaderboardLog::getCurrentLeaderBoardEndingDate($current_leaderboard);
            $current_leaderboard_month = LeaderboardLog::getCurrentLeaderBoardMonth($current_leaderboard);

            if ($current_leaderboard == 1) {
                $compititionspan = "1st Competition - Jan - Apr " . $current_year;
            } elseif ($current_leaderboard == 2) {
                $compititionspan = "2nd Competition - May - Aug " . $current_year;
            } elseif ($current_leaderboard == 3) {
                $compititionspan = "3rd Competition - Sep - Dec " . $current_year;
            }

            $condition = [];
            $img_url = "";
            $filtername = "";
            if (!empty($data['country_slug'])) {
                $country_id = Api::getLocationId('Country', $data['country_slug']);
                $condition['country_id'] = $country_id;
                $img_url = Country::find($country_id)->img_url;
                $filtername = Country::find($country_id)->name;
            }
            if (!empty($data['region_slug'])) {
                $condition['region_id'] = Api::getLocationId('Region', $data['region_slug']);
                $img_url = Region::find(Api::getLocationId('Region', $data['region_slug']))->img_url;
                $filtername = Region::find(Api::getLocationId('Region', $data['region_slug']))->name;
                $filtername = $filtername . " region";
            }
            if (!empty($data['division_slug'])) {
                $condition['devision_id'] = Api::getLocationId('Devision', $data['division_slug']);
                $img_url = Devision::find(Api::getLocationId('Devision', $data['division_slug']))->img_url;
                $filtername = Devision::find(Api::getLocationId('Devision', $data['division_slug']))->name;
                $filtername = $filtername . " division";
            }
            if (!empty($data['state_slug'])) {
                $condition['state_id'] = Api::getLocationId('State', $data['state_slug']);
                $img_url = State::find(Api::getLocationId('State', $data['state_slug']))->img_url;
                $filtername = State::find(Api::getLocationId('State', $data['state_slug']))->name;
            }

            if (empty($data['region_slug']) && !$data['is_wall_of_fame']) {

                $condition['leader_board_no'] = $current_leaderboard;
                $gerRegionOfCountry = Region::where('country_id', '=', Api::getLocationId('Country', $data['country_slug']))->where('is_active', 1)->get();
                $top = 250;
                if (!empty($data['region_slug'])) {
                    $top = 200;
                } elseif (!empty($data['division_slug'])) {
                    $top = 150;
                } elseif (!empty($data['state_slug'])) {
                    $top = 100;
                }

                foreach ($gerRegionOfCountry as $key => $region) {
                    $condition['region_id'] = $region->id;
                    $getLeaderboardList = LeaderboardLog::with('userInfo')
                            ->where($condition)
                            ->where('is_active', 1)
                            ->where('leader_board_no', $current_leaderboard)
                            ->limit($top)
                            ->groupBy('user_id')
                            ->selectRaw('*, sum(total_vote) as total_vote')
                            // ->selectRaw('*,`leaderboard_log`.`user_id` as l_u_id, (select count(*) from `posts` inner join `post_like_log` on `posts`.`id`=`post_like_log`.`post_id` where `posts`.`user_id` = `l_u_id` AND (`posts`.`video_name` is not NULL OR `posts`.`audio_name` is not NULL)) as `total_vote`')
                            ->orderBy('total_vote', 'DESC')
                            ->get();
                    // dd($getLeaderboardList);

                    $top = $data['top'];
                    $finalList[$region->name] = $this->getFinalList($getLeaderboardList, $top);
                }

                //echo json_encode($finalList);die;
            } else {
                $condition['leader_board_no'] = $current_leaderboard;
                $date1 = new \DateTime(date('Y-m-d')); //current date or any date
                $date2 = new \DateTime($current_leaderboard_end_date); //Future date
                $diff = $date2->diff($date1)->format("%a"); //find difference
                $days = intval($diff) + 1;
                $ongoing_month = $now->format('m');
                $ongoing_month_leaderboard = LeaderboardLog::getCurrentLeaderBoard($ongoing_month);
                $ongoing_year = $now->format('Y');
                // dd($current_leaderboard);
                if (isset($data['only_wall_of_fame']) && $this->checkMonth($current_month, $current_year)) {

                    $response = Api::make_response(0, [], 'Coming Soon!', ['compitition' => $compititionspan, 'leaderboarDaysleft' => $days, 'currentYear' => $current_year, 'current_leaderboard_month' => $current_leaderboard_month, 'img_url' => $img_url, 'filtername' => $filtername, 'top' => $data['top']]);
                    return $response;
                }

                if (isset($data['only_wall_of_fame'])) {
                    $getLeaderboardList = LeaderboardLog::with('userInfo')
                            ->where($condition)
                            //->limit($data['top'])
                            ->where('is_active', 1)
                            // ->where('month',$current_leaderboard)
                            ->where('year', $current_year)
                            ->groupBy('user_id')
                            ->selectRaw('*, sum(total_vote) as total_vote')
                            // ->selectRaw('*,`leaderboard_log`.`user_id` as l_u_id, (select count(*) from `posts` inner join `post_like_log` on `posts`.`id`=`post_like_log`.`post_id` where `posts`.`user_id` = `l_u_id` AND (`posts`.`video_name` is not NULL OR `posts`.`audio_name` is not NULL)) as `total_vote`')
                            ->orderBy('total_vote', 'DESC')
                            ->get();
                    // dd($current_leaderboard);
                } else {

                    $getLeaderboardList = LeaderboardLog::with('userInfo')
                            ->where($condition)
                            //->limit($data['top'])
                            ->where('is_active', 1)
                            ->groupBy('user_id')
                            ->selectRaw('*, sum(total_vote) as total_vote')
                            // ->selectRaw('*,`leaderboard_log`.`user_id` as l_u_id, (select count(*) from `posts` inner join `post_like_log` on `posts`.`id`=`post_like_log`.`post_id` where `posts`.`user_id` = `l_u_id` AND (`posts`.`video_name` is not NULL OR `posts`.`audio_name` is not NULL)) as `total_vote`')
                            ->orderBy('total_vote', 'DESC')
                            ->get();
                }


                $top = $data['top'];
                $finalList = $this->getFinalList($getLeaderboardList, $top);
                // dd($finalList);
            }


            //echo json_encode($finalList);die;
            //Followers
            $followinglist = array();
            if (isset($data['user_id'])) {
                $followinglist = Followers::where('user_id', $data['user_id'])->pluck('is_following')->toArray();
            }
            //echo json_encode($is_following);die;

            $finalListArray = [];
            $i = 0;
            if ($data['is_wall_of_fame']) {
                foreach ($finalList as $flList) {

                    if ($flList['rank'] < 4 && $data['is_wall_of_fame']) {
                        $finalListArray[$i]['user_id'] = $flList['user_id'];
                        $finalListArray[$i]['country_id'] = $flList['country_id'];
                        $finalListArray[$i]['region_id'] = $flList['region_id'];
                        $finalListArray[$i]['devision_id'] = $flList['devision_id'];
                        $finalListArray[$i]['state_id'] = $flList['state_id'];
                        $finalListArray[$i]['total_vote'] = $flList['total_vote'];
                        $finalListArray[$i]['rank'] = $flList['rank'];
                        $finalListArray[$i]['user_full_name'] = $flList['user_full_name'];
                        $finalListArray[$i]['user_name'] = $flList['user_name'];
                        $finalListArray[$i]['user_info'] = $flList['userInfo'];

                        if (in_array($flList['user_id'], $followinglist)) {
                            $finalListArray[$i]['is_following'] = true;
                        } else {
                            $finalListArray[$i]['is_following'] = false;
                        }
                        $i = $i + 1;
                    }
                }
            } else {
                $finalListArray = $finalList;
            }
            //echo json_encode($finalListArray);die;
            //echo $daysleft;
            //$finalList['daysleft'] = $days;
            if (count($finalListArray)) {
                $response = Api::make_response(1, [$finalListArray], 'Leaderboar Found', ['compitition' => $compititionspan, 'leaderboarDaysleft' => $days, 'currentYear' => $current_year, 'current_leaderboard_month' => $current_leaderboard_month, 'img_url' => $img_url, 'filtername' => $filtername, 'top' => $data['top']]);
            } else {
                $response = Api::make_response(0, [], 'No Data Found', ['img_url' => $img_url, 'filtername' => $filtername, 'compitition' => $compititionspan, 'currentYear' => $current_year, 'current_leaderboard_month' => $current_leaderboard_month]);
            }
        } else {
            $response = Api::make_response(0, [], 'Invalid Param . Data not saved');
        }

        return $response;
    }

    public function getFinalList($getLeaderboardList, $top) {

        $i = 1;
        $total_vote = 0;
        $rank = 1;
        $finalList = [];
        foreach ($getLeaderboardList as $key => $list) {
            if ($rank == $top && ($total_vote != $list->total_vote)) {
                break;
            }
            if ($total_vote == $list->total_vote) {
                // $top++;
                $i--;
                $list->rank = $i;
                $rank = $i;
                $i++;
            } else {
                $list->rank = $i;
                $rank = $i;
                $i++;
            }

            $total_vote = $list->total_vote;
            $finalList[] = $list;
        }
        return $finalList;
    }

    //this api function will return jackpot amount in different currency
    public function mobile_jackpot_amount(Request $request) {

        $validator = Validator::make($request->all(), [
                    'area_type' => 'required',
                    'location_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Validation errors. Try Again!');
        }

        $area_type = $request->input('area_type');
        $location_id = $request->input('location_id');

        $crypto_prices = Crypto_prices::where('status', 'Enabled')->get();

        if ($crypto_prices->count() == 0) {
            return Api::make_response(0, [], 'No currency found.');
        }
        $amount_arr = [];
        $jackpot_amt_obj = Jackpot_amount::where(['area_type' => $area_type, 'location_id' => $location_id])->get();
        if ($jackpot_amt_obj->count() == 0) {
            return Api::make_response(0, [], 'No jackpot found.');
        }
        $jackpot_amt = $jackpot_amt_obj[0]->amount;
        foreach ($crypto_prices as $key => $crypto) {

            $amount_arr[0][$crypto->coin_code] = number_format($jackpot_amt / $crypto->coin_price_usd, 2);
        }
        $amount_arr[0]['USD'] = $jackpot_amt;
        if (empty($amount_arr)) {
            return Api::make_response(0, [], 'No record found.');
        }

        //send jackpot text together in msg
        $site_setting = SiteSetting::where('id', 1)->get();
        if ($site_setting->count() == 0) {
            $msg = 'NA';
        } else {
            $msg = $site_setting[0]->jackpot_text;
        }
        return Api::make_response(1, $amount_arr, $msg);
    }
    
    public function boost_purchase_confirm(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'plan_id' => 'required',
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        $loggedin_user_detail = User::where('id',$request_data['user_id'])->get();
        //once payment gateway will be finaled we will add payment gateway related code here
        
        //get plan details
        $plan_details = BoostPlans::where('id',$request_data['plan_id'])->get();
        
        if($plan_details->count()==0){
            return Api::make_response(0, [], "No plan available with given details. Please try again.");
        }
        
        $user_boost_arr=[
            'country_boost'=>$loggedin_user_detail[0]->country_boost+$plan_details[0]->country_boost_count,
            'region_boost'=>$loggedin_user_detail[0]->region_boost+$plan_details[0]->region_boost_count,
            'division_boost'=>$loggedin_user_detail[0]->division_boost+$plan_details[0]->division_boost_count,
            'state_boost'=>$loggedin_user_detail[0]->state_boost+$plan_details[0]->state_boost_count,
            'country_boost_avaliable'=>$loggedin_user_detail[0]->country_boost_avaliable+$plan_details[0]->region_boost_count,
            'region_boost_avaliable'=>$loggedin_user_detail[0]->region_boost_avaliable+$plan_details[0]->region_boost_count,
            'division_boost_avaliable'=>$loggedin_user_detail[0]->division_boost_avaliable+$plan_details[0]->division_boost_count,
            'state_boost_avaliable'=>$loggedin_user_detail[0]->state_boost_avaliable+$plan_details[0]->state_boost_count,
            'total_boost'=>$loggedin_user_detail[0]->total_boost+$plan_details[0]->country_boost_count+$plan_details[0]->region_boost_count+
                $plan_details[0]->division_boost_count+$plan_details[0]->state_boost_count,
            'updated_at'=>date('Y-m-d H:i:s')
        ];
       
        User::where('id',$request_data['user_id'])->update($user_boost_arr);
        return Api::make_response(1, [], "Boost successfully added.");
        
    }
    
    public function get_user_boost_counts(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        
        $boost_details= User::where('id',$request_data['user_id'])->get(['country_boost','country_boost_avaliable',
            'region_boost','region_boost_avaliable','division_boost','division_boost_avaliable',
            'state_boost','state_boost_avaliable']);
        
        if($boost_details->count()==0){
            return Api::make_response(0, [], 'No record found. please try again.');
        }
        
        $boost_details[0]->country_used_boost = $boost_details[0]->country_boost - $boost_details[0]->country_boost_avaliable;
        $boost_details[0]->region_used_boost = $boost_details[0]->region_boost - $boost_details[0]->region_boost_avaliable;
        $boost_details[0]->division_used_boost = $boost_details[0]->division_boost - $boost_details[0]->division_boost_avaliable;
        $boost_details[0]->state_used_boost = $boost_details[0]->state_boost - $boost_details[0]->state_boost_avaliable;
        
        return Api::make_response(1, $boost_details, "Boost records available");
        
    }

}
