<?php

namespace App\Http\Controllers\Mobileapi;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Api;
use App\Posts;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\UserImage;
use App\Lib\Upload_file;
class GallaryController extends Controller {
    
    private $limit=20;


    public function get_video_files_by_user(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'page_number'=>'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        $offset = ($request_data['page_number']-1) * $this->limit;
        $video_post = Posts::where('user_id',$request_data['user_id'])
                                ->where('post_type','Video')
                                ->where('is_shared',0)
                                ->orderBy('id','DESC')
                                ->offset($offset)
                                ->limit($this->limit)
                                ->get();
        
        if($video_post->count() == 0){
            return Api::make_response(0, [], "No record found.");
        }
        
        return Api::make_response(1, $video_post, "Video record available.");
        
    }
    
    public function get_audio_files_by_user(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'page_number'=>'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        $offset = ($request_data['page_number']-1) * $this->limit;
        $audio_post = Posts::where('user_id',$request_data['user_id'])
                                ->where('post_type','Audio')
                                ->where('is_shared',0)
                                ->orderBy('id','DESC')
                                ->offset($offset)
                                ->limit($this->limit)
                                ->get();
        
        if($audio_post->count() == 0){
            return Api::make_response(0, [], "No record found.");
        }
        
        return Api::make_response(1, $audio_post, "Audio record available.");
        
    }
    
    public function get_images_list(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'page_number'=>'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        $offset = ($request_data['page_number']-1) * $this->limit;
        
        $user_images = UserImage::where('user_id',$request_data['user_id'])
                                ->offset($offset)
                                ->limit($this->limit)
                                ->orderBy('id','DESC')
                                ->get();
        if($user_images->count() == 0){
            return Api::make_response(0, [], "No record found.");
        }
        return Api::make_response(1, $user_images, "Images available.");
    }
    
    public function gallery_upload_image(Request $request){
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'image_file'=>'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        
        if ($request->hasFile('image_file')) {
            $result = Upload_file::upload_user_image_file($request);
            if (!$result['status']) {
                return Api::make_response(0, [], "Error during file upload. Please try again.");
            }
            $file_path = $result['storage_path'];

            $image_arr = [
                'user_id' => $request_data['user_id'],
                'user_image' => $file_path,
                'image_type' => 'Profile',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'is_active' => 1
            ];
            $id = UserImage::insertGetId($image_arr);
            
            
            return Api::make_response(1, [], "Image uploaded successfully.");
        } else {
            return Api::make_response(0, [], 'Please upload video file.');
        }
        
    }
    
}
