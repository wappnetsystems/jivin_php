<?php

namespace App\Http\Controllers\Mobileapi;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Posts;
use Illuminate\Support\Facades\Validator;
use App\Lib\Upload_file;
use App\Api;
use App\UserAccountSetting;
use App\Lib\General_functions;
use App\PostBoost;
use Illuminate\Support\Facades\DB;
use App\LeaderboardLog;
use App\Followers;
use App\PostLogs;
use App\Post_comment;

class PostController extends Controller {

    private $limit;
    private $s3_link = "https://jivinmedia.nyc3.cdn.digitaloceanspaces.com/";
    private $post_url  = "https://jivin.com/post/id";
    public function __construct() {
        $this->limit = 20;
    }

    //========================29/05/2020 nishit =============================
    public function get_wall_post_list(Request $request) {    //this chnage
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'page_number' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();


        $offset = ($request_data['page_number'] - 1) * $this->limit;
        $user_account_setting = UserAccountSetting::where('user_id', $request_data['user_id'])->get();
        $select_fields = ['posts.*', 'posts.created_at as date_sort', 'users.name as post_user_name','users.username','users.user_image'];
        $follower_query = Posts::select($select_fields)
                ->with(['post_comment' => function($query) {
                        $query->limit(5);
                    }])
                ->join('users', 'users.id', '=', 'posts.user_id')
                ->leftJoin('followers', 'followers.is_following', '=', 'posts.user_id')
                ->where(function ($query) use ($request_data) {
                    if (isset($request_data['search_text'])) {

                        $query->where('posts.text_details', 'like','%' . $request_data['search_text'] . '%');
                        $query->orWhere('users.name','like','%' . $request_data['search_text'] . '%');
                    }

                })
                ->where(function($query) use($request_data) {
                    $query->where(function($sub_query) use($request_data) {
                        $sub_query->where('followers.user_id', $request_data['user_id']);
                        $sub_query->where('is_public', '!=', 0);
                    })
                    ->orWhere('posts.user_id', $request_data['user_id']);
                })
                ->where('posts.is_active', 1)
                ->groupBy('posts.id');
        $boost_query = Posts::select($select_fields)
                ->with(['post_comment' => function($query) {
                        $query->limit(5);
                    }])
                ->leftJoin('post_boost', 'post_boost.post_id', '=', 'posts.id')
                ->join('users', 'users.id', '=', 'posts.user_id')
                ->where(function ($query) use ($request_data) {
                    if (isset($request_data['search_text'])) {

                        $query->where('posts.text_details', 'like','%' . $request_data['search_text'] . '%');
                        $query->orWhere('users.name','like','%' . $request_data['search_text'] . '%');
                    }

                })
                ->where('post_boost.boost_location_id', $user_account_setting[0]->state_id)
                ->where('posts.is_active', 1)
                //->union($follower_query)
                //->orderBy('posts.created_at','DESC')
                //->limit(20)->offset(0)
                ->groupBy('posts.id');
        $post_list = Posts::select($select_fields)
                        ->with(['post_comment' => function($query) {
                                $query->limit(5);
                            }])
                        ->union($boost_query)->union($follower_query)
                        ->join('users', 'users.id', '=', 'posts.user_id')
                        ->where(function ($query) use ($request_data) {
                            if (isset($request_data['search_text'])) {
        
                                $query->where('posts.text_details', 'like','%' . $request_data['search_text'] . '%');
                                $query->orWhere('users.name','like','%' . $request_data['search_text'] . '%');
                            }
        
                        })
                        ->groupBy('posts.id')
                        ->orderBy('date_sort', 'DESC')
                        ->limit($this->limit)->offset($offset)->get();
        if ($post_list->count() > 0) {

            foreach ($post_list as $key => $post) {

                //check if post is shared then get content of orginal post
                if ($post->is_shared) {
                    $original_post_detail = Posts::where('id', $post->org_post_id)->with(['userposts'])->get();
                    /* if ($original_post_detail[0]->post_type == 'Video' && $original_post_detail[0]->video_name) {
                      $original_post_detail[0]->video_name = Upload_file::get_post_file_path($original_post_detail[0]->post_type, $original_post_detail[0]->video_name, $original_post_detail[0]->id);
                      } else if ($original_post_detail[0]->post_type == 'Audio' && $original_post_detail[0]->audio_name) {
                      $original_post_detail[0]->audio_name = Upload_file::get_post_file_path($original_post_detail[0]->post_type, $original_post_detail[0]->audio_name, $original_post_detail[0]->id);
                      } */
                    $post_list[$key]->original_post = $original_post_detail;
                } else {
                    $post_list[$key]->original_post = [];
                }

                /* if ($post->post_type == 'Video' && $post->video_name) {
                  $post_list[$key]->video_name = Upload_file::get_post_file_path($post->post_type, $post->video_name, $post->id);
                  } else if ($post->post_type == 'Audio' && $post->audio_name) {
                  $post_list[$key]->audio_name = Upload_file::get_post_file_path($post->post_type, $post->audio_name, $post->id);
                  } */

                $post_list[$key]->time_ago = General_functions::time_elapsed_string($post->created_at);
                if (empty($post_list[$key]->original_post)) {
                    if ($post->user_id == $request_data['user_id']) {
                        $post_list[$key]->post_owner = 1;
                        $post_list[$key]->already_shared = 1;
                    } else {
                        $post_list[$key]->post_owner = 0;

                        //check if user has already shared it
                        $share_check = \App\PostShareLog::where('post_id', $post->id)
                                        ->where('shared_by', $request_data['user_id'])
                                        ->get(['id'])->count();
                        if ($share_check > 0) {
                            $post_list[$key]->already_shared = 1;
                        } else {
                            $post_list[$key]->already_shared = 0;
                        }
                    }

                    $already_vote_check = \App\PostLikeLog::where('post_id', $post->id)
                                    ->where('liked_by', $request_data['user_id'])
                                    ->get(['id'])->count();
                    if ($already_vote_check > 0) {
                        $post_list[$key]->already_voted = 1;
                    } else {
                        $post_list[$key]->already_voted = 0;
                    }
                } else {
                    //check all thing for original post
                    $post = $post_list[$key]->original_post[0];
                    if ($post->user_id == $request_data['user_id']) {
                        $post_list[$key]->original_post[0]->post_owner = 1;
                        $post_list[$key]->original_post[0]->already_shared = 1;
                    } else {
                        $post_list[$key]->original_post[0]->post_owner = 0;

                        //check if user has already shared it
                        $share_check = \App\PostShareLog::where('post_id', $post->id)
                                        ->where('shared_by', $request_data['user_id'])
                                        ->get(['id'])->count();
                        if ($share_check > 0) {
                            $post_list[$key]->original_post[0]->already_shared = 1;
                        } else {
                            $post_list[$key]->original_post[0]->already_shared = 0;
                        }
                    }

                    $already_vote_check = \App\PostLikeLog::where('post_id', $post->id)
                                    ->where('liked_by', $request_data['user_id'])
                                    ->get(['id'])->count();
                    if ($already_vote_check > 0) {
                        $post_list[$key]->original_post[0]->already_voted = 1;
                    } else {
                        $post_list[$key]->original_post[0]->already_voted = 0;
                    }
                    #--- post_share_url
                    $post_list[$key]->original_post[0]->post_url  = $this->post_url;
                }
                if ($post_list[$key]->user_image && $post_list[$key]->user_image != "default.jpg") {
                    $post_list[$key]->user_image = $this->s3_link.'profile_image/'.$post_list[$key]->user_image;
                    //$post_list[$key]->user_image = asset('image_file/' . $post_list[$key]->user_image);
                } else {
                    $post_list[$key]->user_image = "";
                }
                //--------
                // if ($post_list[$key]->video_thumb ) {
                //     $post_list[$key]->video_thumb = $this->s3_link.'video_thumb_path/'.$post_list[$key]->video_thumb;
                //     //$post_list[$key]->user_image = asset('image_file/' . $post_list[$key]->user_image);
                // } else {
                //     $post_list[$key]->video_thumb = "";
                // }
            }

            return Api::make_response(1, $post_list, "Post available");
        } else {
            return Api::make_response(0, [], "No new post available.");
        }
    }

    //================================================================================
    public function get_post_details_by_id(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'post_id' => 'required'
        ]);
        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        $select_fields = ['posts.*', 'posts.created_at as date_sort', 'users.name as post_user_name'];

        $post_list = Posts::select($select_fields)
                ->with(['post_comment' => function($query) {
                        $query->limit(5);
                    }])
                ->join('users', 'users.id', '=', 'posts.user_id')
                ->where('posts.id', $request_data['post_id'])
                ->where('posts.is_active', 1)
                ->get();

        if ($post_list->count() == 0) {
            return Api::make_response(0, [], "No post available.");
        }

        $post_list[0]->time_ago = General_functions::time_elapsed_string($post_list[0]->created_at);


        //check if post is shared then get content of orginal post
        if ($post_list[0]->is_shared) {
            $original_post_detail = Posts::where('id', $post_list[0]->org_post_id)->with(['userposts'])->get();
            /* if ($original_post_detail[0]->post_type == 'Video' && $original_post_detail[0]->video_name) {
              $original_post_detail[0]->video_name = Upload_file::get_post_file_path($original_post_detail[0]->post_type, $original_post_detail[0]->video_name, $original_post_detail[0]->id);
              } else if ($original_post_detail[0]->post_type == 'Audio' && $original_post_detail[0]->audio_name) {
              $original_post_detail[0]->audio_name = Upload_file::get_post_file_path($original_post_detail[0]->post_type, $original_post_detail[0]->audio_name, $original_post_detail[0]->id);
              } */
            $post_list[0]->original_post = $original_post_detail;
        } else {
            $post_list[0]->original_post = [];
        }
        if (empty($post_list[0]->original_post)) {
            if ($post_list[0]->user_id == $request_data['user_id']) {
                $post_list[0]->post_owner = 1;
                $post_list[0]->already_shared = 1;
            } else {
                $post_list[0]->post_owner = 0;

                //check if user has already shared it
                $share_check = \App\PostShareLog::where('post_id', $post_list[0]->id)
                                ->where('shared_by', $request_data['user_id'])
                                ->get(['id'])->count();
                if ($share_check > 0) {
                    $post_list[0]->already_shared = 1;
                } else {
                    $post_list[0]->already_shared = 0;
                }
            }
            $already_vote_check = \App\PostLikeLog::where('post_id', $post_list[0]->id)
                            ->where('liked_by', $request_data['user_id'])
                            ->get(['id'])->count();
            if ($already_vote_check > 0) {
                $post_list[0]->already_voted = 1;
            } else {
                $post_list[0]->already_voted = 0;
            }
        }
        else{
            if ($post_list[0]->original_post[0]->user_id == $request_data['user_id']) {
                $post_list[0]->original_post[0]->post_owner = 1;
                $post_list[0]->original_post[0]->already_shared = 1;
            } else {
                $post_list[0]->original_post[0]->post_owner = 0;

                //check if user has already shared it
                $share_check = \App\PostShareLog::where('post_id', $post_list[0]->original_post[0]->id)
                                ->where('shared_by', $request_data['user_id'])
                                ->get(['id'])->count();
                if ($share_check > 0) {
                    $post_list[0]->original_post[0]->already_shared = 1;
                } else {
                    $post_list[0]->original_post[0]->already_shared = 0;
                }
            }
            $already_vote_check = \App\PostLikeLog::where('post_id', $post_list[0]->original_post[0]->id)
                            ->where('liked_by', $request_data['user_id'])
                            ->get(['id'])->count();
            if ($already_vote_check > 0) {
                $post_list[0]->original_post[0]->already_voted = 1;
            } else {
                $post_list[0]->original_post[0]->already_voted = 0;
            }
            #--- post_share_url
            $post_list[0]->original_post[0]->post_url  = $this->post_url;
        }




        /* if ($post_list[0]->post_type == 'Video' && $post_list[0]->video_name) {
          $post_list[0]->video_name = Upload_file::get_post_file_path($post_list[0]->post_type, $post_list[0]->video_name, $post_list[0]->id);
          } else if ($post_list[0]->post_type == 'Audio' && $post_list[0]->audio_name) {
          $post_list[0]->audio_name = Upload_file::get_post_file_path($post_list[0]->post_type, $post_list[0]->audio_name, $post_list[0]->id);
          } */



        return Api::make_response(1, $post_list, "Post details are showing.");
    }

    public function create_video_post(Request $request) {

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'text_details' => 'required',
                    'is_public' => 'required',
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $time = "";
        $thumb_size = '320x240';
        $interval = 2;
        $thumb_file_name = time() . rand(1000, 9999) . uniqid() . '.jpg';
        $thumb_image = public_path('video_file/video_thumb/' . $thumb_file_name);
        if (isset($_FILES['video_file']['tmp_name'])) {
            $temp_file = $_FILES['video_file']['tmp_name'];
            $time = exec("ffmpeg -i $temp_file 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,//");
            if ($time != "") {
                $only_time = explode('.', $time);
                if (isset($only_time[0])) {
                    $time = $only_time[0];
                }
            }
            exec("ffmpeg -i $temp_file -deinterlace -an -ss $interval -f mjpeg -t 1 -r 1 -y -s $thumb_size $thumb_image 2>&1");
            if (file_exists($thumb_image)) {
            
                Upload_file::upload_video_thumb($thumb_image, $thumb_file_name);
                @unlink($thumb_image);
            } else {

                $thumb_file_name = "";
            }
        } else {
            return Api::make_response(0, [], "Error during file upload. Please try again.");
        }
        //check file uploaded and upload it on server and store in table

        if ($request->hasFile('video_file')) {
            $result = Upload_file::upload_video_file($request);
            if (!$result['status']) {
                return Api::make_response(0, [], "Error during file upload. Please try again.");
            }
            $file_path = $result['storage_path'];


            $post_arr = [
                'user_id' => $request->input('user_id'),
                'text_details' => $request->input('text_details'),
                'video_name' => $file_path,
                'is_public' => $request->input('is_public'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'post_type' => 'Video',
                'video_audio_time' => $time,
                'video_thumb' => $thumb_file_name
            ];
            $id = Posts::insertGetId($post_arr);

            //make post log and notification log
            PostLogs::logPosts($id, $request->input('user_id'), 'self');

            return Api::make_response(1, ['post_id' => $id], "Video successfully posted.");
        } else {
            return Api::make_response(0, [], 'Please upload video file.');
        }
    }

    public function create_audio_post(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'text_details' => 'required',
                    'is_public' => 'required',
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $time = "";
        if (isset($_FILES['audio_file']['tmp_name'])) {
            $temp_file = $_FILES['audio_file']['tmp_name'];
            $time = exec("ffmpeg -i $temp_file 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,//");
            if ($time != "") {
                $only_time = explode('.', $time);
                if (isset($only_time[0])) {
                    $time = $only_time[0];
                }
            }
        }

        //check file uploaded and upload it on server and store in table
        if ($request->hasFile('audio_file')) {
            $result = Upload_file::upload_audio_file($request);
            if (!$result['status']) {
                return Api::make_response(0, [], "Error during file upload. Please try again.");
            }
            $file_path = $result['storage_path'];

            $post_arr = [
                'user_id' => $request->input('user_id'),
                'text_details' => $request->input('text_details'),
                'audio_name' => $file_path,
                'is_public' => $request->input('is_public'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'post_type' => 'Audio',
                'video_audio_time' => $time,
            ];
            $id = Posts::insertGetId($post_arr);

            //make post log and notification log
            PostLogs::logPosts($id, $request->input('user_id'), 'self');

            return Api::make_response(1, ['post_id' => $id], "Audio successfully posted.");
        } else {
            return Api::make_response(0, [], 'Please upload audio file.');
        }
    }

    public function create_normal_post(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'text_details' => 'required',
                    'is_public' => 'required',
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }

        $post_arr = [
            'user_id' => $request->input('user_id'),
            'text_details' => $request->input('text_details'),
            'is_public' => $request->input('is_public'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'post_type' => 'Text'
        ];
        $id = Posts::insertGetId($post_arr);

        //make post log and notification log
        PostLogs::logPosts($id, $request->input('user_id'), 'self');

        return Api::make_response(1, ['post_id' => $id], "New post successfully submitted");
    }

    public function report_post(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'post_id' => 'required',
                    'title' => 'required',
                    'details' => 'required'
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        $check_already_reported = \App\ReportPost::where('report_by', $request_data['user_id'])
                        ->where('post_id', $request_data['post_id'])
                        ->where('is_active', 1)
                        ->get(['id'])->count();
        if ($check_already_reported > 0) {
            return Api::make_response(0, [], 'You have already reported this post. Jivin team will take actions on your report soon.');
        }

        $report_arr = [
            'report_by' => $request_data['user_id'],
            'post_id' => $request_data['post_id'],
            'title' => $request_data['title'],
            'details' => $request_data['details'],
            'is_active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        \App\ReportPost::insert($report_arr);
        return Api::make_response(1, [], "Post is reported. Jivin team will check and take actions accordingly.");
    }

    public function vote_on_post(Request $request) {    //work on
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'post_id' => 'required',
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();

        //get post details
        $post_details = Posts::where('id', $request_data['post_id'])->get();

        //check if post is shared post
        if ($post_details[0]->is_shared) {
            $post_details = Posts::where('id', $post_details[0]->org_post_id)->get();
        }

        //check if already voted
        $already_vote_check = \App\PostLikeLog::where('post_id', $post_details[0]->id)
                ->where('liked_by', $request_data['user_id'])
                ->get(['id']);
        if ($already_vote_check->count() > 0) {
            return Api::make_response(0, [], "You have already voted.");

            //delete already voted
            //\App\PostLikeLog::where('id', $already_vote_check[0]->id)->delete();
            //Posts::where($post_details->id)->decrement('total_like');
            //return Api::make_response(1, [], 'Your vote is removed on this post.');
        }

        $vote_arr = [
            'post_id' => $post_details[0]->id,   //bug 15/06/2020
            'liked_by' => $request_data['user_id'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        \App\PostLikeLog::insert($vote_arr);
        Posts::where('id', $post_details[0]->id)->increment('total_like');
        LeaderboardLog::addLog($post_details[0]->user_id);

        return Api::make_response(1, [], 'You have successfully voted for this post.');
    }

    //shared means jivi it
    public function jivi_it(Request $request) {    //work on
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'post_id' => 'required',
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        //check already shared or not
        $check_shared = \App\PostShareLog::where(['post_id' => $request_data['post_id'], 'shared_by' => $request_data['user_id']])
                        ->get(['id'])->count();
        if ($check_shared > 0) {
            return Api::make_response(0, [], "This post is already shared by you.");
        }

        $post_detail = Posts::where('id', $request_data['post_id'])
                ->get();
        if ($post_detail->count() == 0) {
            return Api::make_response(0, [], "Error Occurred.Try Again.");
        }
        //check if post is already shared post
        if ($post_detail[0]->is_shared == 1) {
            $post_detail = Posts::where('id', $post_detail[0]['org_post_id'])
                    ->get();

            $check_shared = \App\PostShareLog::where(['post_id' => $post_detail[0]->id, 'shared_by' => $request_data['user_id']])
                            ->get(['id'])->count();
            if ($check_shared > 0) {
                return Api::make_response(0, [], "This post is already shared by you.");
            }
        }

        $share_log_arr = [
            'post_id' => $post_detail[0]->id,
            'shared_by' => $request_data['user_id'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        \App\PostShareLog::insert($share_log_arr);

        //increment share
        Posts::where('id', $post_detail[0]->id)->increment('total_share');

        $new_post_arr = [
            'user_id' => $request_data['user_id'],
            'text_details' => $post_detail[0]->text_details,
            'audio_name' => $post_detail[0]->audio_name,
            'video_name' => $post_detail[0]->video_name,
            'is_adult' => $post_detail[0]->is_adult,
            'total_like' => 0,
            'total_share' => 0,
            'is_active' => $post_detail[0]->is_active,
            'is_public' => 2,
            'shared_id' => $post_detail[0]->user_id,
            'org_post_id' => $post_detail[0]->id,
            'is_shared' => 1,
            'post_type' => $post_detail[0]->post_type,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        Posts::insert($new_post_arr);
        PostLogs::logPosts($request_data['post_id'], $request_data['user_id'], 'shared');
        return Api::make_response(1, ['total_share' => $post_detail[0]->total_share + 1], 'Post Shared');
    }

    public function boost_post(Request $request) {   //work on
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'post_id' => 'required',
                    'boost_location' => 'required',
                    'boost_location_type' => 'required'
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();

        $logged_in_user_detail = User::where('id', $request_data['user_id'])->get();

        if ($request_data['boost_location_type'] == "Country") {

            if ($logged_in_user_detail[0]->country_boost_avaliable <= 0) {
                return Api::make_response(0, [], "Sorry!!! You do not have any boost in your account. Please purchase some boost to boost the post.");
            }

            $sqlsatelist = "SELECT id FROM state WHERE division_id IN (SELECT id FROM division WHERE region_id IN(SELECT id FROM region WHERE country_id IN(SELECT id FROM country where id = '" . $request_data['boost_location'] . "' and is_active = 1) and is_active = 1) and is_active = 1)and is_active = 1";
            User::where('id', $request_data['user_id'])->decrement('country_boost_avaliable');
        } elseif ($request_data['boost_location_type'] == "Region") {
            if ($logged_in_user_detail[0]->region_boost_avaliable <= 0) {
                return Api::make_response(0, [], "Sorry!!! You do not have any boost in your account. Please purchase some boost to boost the post.");
            }

            $sqlsatelist = "SELECT id FROM state WHERE division_id IN (SELECT id FROM division WHERE region_id IN(SELECT id FROM region WHERE id =  '" . $request_data['boost_location'] . "' and is_active = 1)and is_active = 1) and is_active = 1";

            User::where('id', $request_data['user_id'])->decrement('region_boost_avaliable');
        } elseif ($request_data['boost_location_type'] == "Division") {
            if ($logged_in_user_detail[0]->division_boost_avaliable <= 0) {
                return Api::make_response(0, [], "Sorry!!! You do not have any boost in your account. Please purchase some boost to boost the post.");
            }

            $sqlsatelist = "SELECT id FROM state WHERE division_id IN (SELECT id FROM division WHERE id ='" . $request_data['boost_location'] . "' and is_active = 1) and is_active = 1";
            User::where('id', $request_data['user_id'])->decrement('division_boost_avaliable');
        } elseif ($request_data['boost_location_type'] == "State") {
            if ($logged_in_user_detail[0]->state_boost_avaliable <= 0) {
                return Api::make_response(0, [], "Sorry!!! You do not have any boost in your account. Please purchase some boost to boost the post.");
            }

            $sqlsatelist = "SELECT id FROM state WHERE id ='" . $request_data['boost_location'] . "' and is_active = 1";
            User::where('id', $request_data['user_id'])->decrement('state_boost_avaliable');
        }

        $postUpdate = Posts::find($request_data['post_id']);
        $postUpdate->is_boost = 1;
        $postUpdate->save();

        $stateslist = DB::select($sqlsatelist);

        if (count($stateslist)) {
            foreach ($stateslist as $key => $value) {
                $addpostBoost = new PostBoost;
                $addpostBoost->post_id = $request_data['post_id'];
                $addpostBoost->user_id = $request_data['user_id'];
                $addpostBoost->boost_location_id = $value->id;
                $addpostBoost->save();
            }
        }
        return Api::make_response(1, [], 'You have successfully boosted');
    }

    public function get_ranking_on_wall(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        $user_id = $request_data['user_id'];
        $now = new \DateTime('now');
        $current_month = $now->format('m');
        $current_year = $now->format('Y');
        $current_leaderboard = LeaderboardLog::getCurrentLeaderBoard($current_month);
        $prev_leaderboard = '';
        $prev_year = '';

        $condition = array();
        if ($current_leaderboard - 1 == 0) {
            $year = $current_year - 1;
            $leaderboard = 3;
        } else {
            $year = $current_year;
            $leaderboard = $current_leaderboard - 1;
        }

        $condition['year'] = $year;
        $condition['leader_board_no'] = $leaderboard;
        /* $condition['year'] = 2018;
          $condition['leader_board_no'] = 1; */

        $location_to_chose = array_rand(['country', 'region', 'state', 'devision']);
        // $location_to_chose = array_rand(['country']);
        $displayText = " Winner";

        if ($location_to_chose == 0) {
            $getCountry = LeaderboardLog::where($condition)->inRandomOrder()->first();

            // print "<pre>";
            // echo '$getCountry';
            // print_r($condition);
            // dd($getCountry);
            // die();
            if ($getCountry) {
                $condition['country_id'] = $getCountry->country_id;
                $displayText = $getCountry->getCountry['name'] . $displayText; //" country";
            }
        } else if ($location_to_chose == 1) {
            $getregion = LeaderboardLog::where($condition)->inRandomOrder()->first();

            // print "<pre>";
            // echo '$getregion';
            // print_r($getregion);
            // die();
            if ($getregion) {
                $condition['region_id'] = $getregion->region_id;
                $displayText = $getregion->getRegion['name'] . $displayText;
            }
        } else if ($location_to_chose == 2) {
            $getdevision = LeaderboardLog::where($condition)->inRandomOrder()->first();

            //   print "<pre>";
            //   echo '$getdevision';
            //   print_r($getdevision);
            //   die();
            if ($getdevision) {
                $condition['devision_id'] = $getdevision->devision_id;
                $displayText = $getdevision->getState['name'] . $displayText; //." state";
            }
        } else {
            $getstate = LeaderboardLog::where($condition)->inRandomOrder()->first();

            // print "<pre>";
            // echo '$getstate';
            // print_r($getstate);
            // die();
            if ($getstate) {
                $condition['state_id'] = $getstate->state_id;
                $displayText = $getstate->getDevision['name'] . $displayText; //." devision";
            }
        }
        $getLeaderboardList = LeaderboardLog::with('userInfo')
                ->where($condition)
                ->where('is_active', 1)
                // ->where('user_id', '!=', $user_id)
                ->groupBy('user_id')
                ->selectRaw('*, sum(total_vote) as total_vote_count')
                ->orderBy('total_vote_count', 'DESC')
                ->first();
        

        foreach ($getLeaderboardList as $key => $value) {
            $getLeaderboardList['rank_id'] = $key+1;
        }       
        // dd($getLeaderboardList);

        $get_followers_list = array();
        $count_followed_by = NULL;
        if ($user = User::find($user_id)) {

            if ($request->followed_by_count != $user->total_followed_by) {
                $get_followers_list = $user->follows;
                $count_followed_by = count($get_followers_list);
            } else {
                $count_followed_by = $request->followed_by_count;
            }
        }


        if (isset($getLeaderboardList->user_id)) {
            $followersCheck = Followers::where('user_id', $user_id)->where('is_following', $getLeaderboardList->user_id)->first();

            $getLeaderboardList['is_following'] = false;
            $is_following['array'] = $followersCheck;
            if (!empty($followersCheck)) {
                $getLeaderboardList['is_following'] = true;
                $is_following['array'] = $followersCheck;
            }

            //echo json_encode($getLeaderboardList);die;

            /*
              $data = array();
              $data['getLeaderboardList'] =  $getLeaderboardList;
              $data['displayText'] =  $displayText; */

            if (!empty($getLeaderboardList)) {
                $response = Api::make_response(1, ['ranks' => $getLeaderboardList, 'followed_by' => $get_followers_list, 'followed_by_count' => $count_followed_by], $displayText);
            }
        } else {
            $response = Api::make_response(0, ['ranks' => new \stdClass(), 'followed_by' => $get_followers_list, 'followed_by_count' => $count_followed_by], "Record not found");
        }

        return $response;
    }

    public function add_post_comment(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'post_id' => 'required',
                    'comment' => 'required'
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();

        $comment_obj = new Post_comment();

        $comment_obj->post_id = $request_data['post_id'];
        $comment_obj->user_id = $request_data['user_id'];
        $comment_obj->comment = $request_data['comment'];
        $comment_obj->created_at = date('Y-m-d H:i:s');
        $comment_obj->created_ip = $request->ip();
        $comment_obj->updated_at = date('Y-m-d H:i:s');
        $comment_obj->updated_ip = $request->ip();
        $comment_obj->save();

        return Api::make_response(1, [], "Comment added successfully.");
    }

    public function get_comments_by_post(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'post_id' => 'required',
                    'page_number' => 'required'
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        $offset = ($request_data['page_number'] - 1) * $this->limit;
        $comment_list = Post_comment::where('post_id', $request_data['post_id'])
                ->where('is_deleted', 0)
                ->orderBy('id', 'DESC')
                ->limit($this->limit)
                ->offset($offset)
                ->get();
        if ($comment_list->count() == 0) {
            return Api::make_response(0, [], "No records available.");
        }
        return Api::make_response(1, $comment_list, "Records found.");
    }
    
    public function edit_post_comment(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'comment_id' => 'required',
                    'comment'=>"required"
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        
        $comment_obj = Post_comment::find($request_data['comment_id']);
        $comment_obj->comment = $request_data['comment']; 
        $comment_obj->updated_at = date('Y-m-d H:i:s');
        $comment_obj->updated_ip = $request->ip();
        $comment_obj->save();

        return Api::make_response(1, [], "Comment updated successfully.");
    }
    
    public function comment_report(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'comment_id' => 'required',
                    'report_reason'=>'required',
                    'report_title'=>'required'
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        
        $post_comment_obj= Post_comment::find($request_data['comment_id']);
        $post_comment_obj->is_reported=1;
        $post_comment_obj->reported_by=$request_data['user_id'];
        $post_comment_obj->report_title=$request_data['report_title'];
        $post_comment_obj->report_reason=$request_data['report_reason'];
        $post_comment_obj->report_datetime=date('Y-m-d H:i:s');
        $post_comment_obj->updated_at=date('Y-m-d H:i:s');
        $post_comment_obj->updated_ip=$request->ip();
        $post_comment_obj->save();
        
        return Api::make_response(1, [], "Post is successfully reported.");
    }
    
    public function delete_comment(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'comment_id' => 'required',
                    
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        
        $comment_obj= Post_comment::where(['user_id'=>$request_data['user_id'],'id'=>$request_data['comment_id']])
                ->first();
        if(empty($comment_obj)){
            return Api::make_response(0, [], "Invalid access request.");
        }
        $comment_obj->is_deleted=1;
        $comment_obj->updated_at=date('Y-m-d H:i:s');
        $comment_obj->updated_ip=$request->ip();
        $comment_obj->save();
        return Api::make_response(1, [], "Your comment successfully deleted.");
    }


}
