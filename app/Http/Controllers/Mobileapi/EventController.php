<?php

namespace App\Http\Controllers\Mobileapi;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Lib\Upload_file;
use App\Stripe_customer;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\DB;
use App\Api;
use App\User;
use App\TempUser;
use App\InterestCategory;
use App\Interests;
use App\AccountSettings;
use App\SubInterest;
use App\Email;
use App\Subscription;
use App\Plans;
use App\UserCard;
use App\ReportPost;
use App\BoostPlans;
use App\UserSubcriptionHistory;
use App\UserBoost;
use App\Posts;
use App\PostLogs;
use App\AdBid;
use App\LeaderboardLog;
use App\Events;
use App\SiteSetting;
use App\UserImage;
use App\User_category_interest;
use App\Jivin_video;
use App\Country;
use App\Region;
use App\Devision;
use App\State;
use Illuminate\Pagination\LengthAwarePaginator;
use App\UserAccountSetting;
use App\ResetPassword;
use App\CustomeKey;
use App\Followers;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Chrisbjr\ApiGuard\Models\ApiKey;
use App\PostLikeLog;
use App\PostShareLog;
use App\UserCryptoAddress;
use App\Crypto_prices;
use Illuminate\Support\Facades\Hash;

class EventController extends Controller {

	private $limit;

    public function __construct() {
        $this->limit = 5;
    }
    
    //nishit raval
	public function mobile_add_event(Request $request) {

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'title' => 'required',
                    'name_of_place' => 'required',
                    'street_number' => 'required',
                    'street_name' => 'required',
                    'city' => 'required',
                    'state_id' => 'required',
                    'start_date' => 'required',
                    'start_time' => 'required',
                    'end_date' => 'required',
                    'end_time' => 'required',
                    'event_description' => 'required',
                    'is_active' => 'required',
                    'country_id' => 'required'
                    //'full_address' => 'required'
                    
                    
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }


        $insert_event = [
        	'user_id' => $request->get('user_id'),
            'title' => $request->get('title'),
            'name_of_place' => $request->get('name_of_place'),
            'street_number' => $request->get('street_number'),
            'street_name' => $request->get('street_name'),
            'city' => $request->get('city'),
            'country_id' => $request->get('country_id'),
        	// 'region_id' => $request->get('region_id'),
        	// 'division_id' => $request->get('division_id'),
            'state_id' => $request->get('state_id'),
            'start_date' => $request->get('start_date'),
            'start_time' => $request->get('start_time'),
            'end_date' => $request->get('end_date'),
            'end_time' => $request->get('end_time'),
            'event_description' => $request->get('event_description'),
            'is_apply' => 'No',
            'is_active' => $request->get('is_active'),
        	'created_at' => date('Y-m-d H:i:s')
        	
        ];
 
        DB::table('event')->insert($insert_event);
        return Api::make_response(1, [], 'Event Data Inserted.');
    }

     //02/06/2020
     public function mobile_edit_event(Request $request){

    	$validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'event_id' => 'required',
                'title' => 'required',
                'name_of_place' => 'required',
                'street_number' => 'required',
                'street_name' => 'required',
                'city' => 'required',
                 'country_id' => 'required',
                // 'region_id' => 'required',
                // 'division_id' => 'required',
                'state_id' => 'required',
                'start_date' => 'required',
                'start_time' => 'required',
                'end_date' => 'required',
                'end_time' => 'required',
                'event_description' => 'required',
                'is_active' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }

        $update_event = [
        	'user_id' => $request->get('user_id'),
            'title' => $request->get('title'),
            'name_of_place' => $request->get('name_of_place'),
            'street_number' => $request->get('street_number'),
            'street_name' => $request->get('street_name'),
            'city' => $request->get('city'),
            'country_id' => $request->get('country_id'),
        	// 'region_id' => $request->get('region_id'),
        	// 'division_id' => $request->get('division_id'),
            'state_id' => $request->get('state_id'),
            'start_date' => $request->get('start_date'),
            'start_time' => $request->get('start_time'),
            'end_date' => $request->get('end_date'),
            'end_time' => $request->get('end_time'),
            'event_description' => $request->get('event_description'),
            'is_active' => $request->get('is_active'),
        	'updated_at' => date('Y-m-d H:i:s')
        	
        ];
        Events::where('id',$request->get('event_id'))->update($update_event);
        return Api::make_response(1, [], 'Event Data Edited.');
    }

    //02/06/2020
    public function mobile_delete_event(Request $request){

    	$validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'event_id' => 'required',
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }

        Events::where('id',$request->get('event_id'))->delete();
        return Api::make_response(1, [], 'Event Deleted Successfully.');
    }

    //27/05/2020 Created by nishit raval
    public function get_all_events_list(Request $request){  //nishit
    	$validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'country_id' => 'required'
        ]);
    	if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();

        if(isset($request_data['offset']) && !empty($request_data['offset'])){
    		$request_data['offset'] = $request_data['offset'];
    	}else{
    		$request_data['offset'] = 1;
        }
        
        $offset = ($request_data['offset'] - 1) * $this->limit;
        $get_fields = ['users.name as user_name', 'country.name as country_name','region.name as region_name',
                       'division.name as division_name','state.name as state_name','name_of_place','city','full_address',
                       'event.start_date','event.start_time', 'event.event_description','event.is_active','event.created_at'];

        $partial_query = Events::with('userInfo')
            ->where('event.country_id','=',$request_data['country_id'])
            //->where('event.is_active','1') 
            ->where('event.is_apply','No')
            ->where(function ($query) use ($request_data) {
                if (!empty($request_data['start_date']) && !empty($request_data['end_date'])) {
                    $query->whereDate('start_date', '>=', $request_data['start_date'])
                            ->whereDate('end_date', '<=', $request_data['end_date']);
                }
            });
           
        if (isset($request_data['region_id']) && !empty($request_data['region_id'])) {  
            $partial_query->where('region_id', $request_data['region_id']);
        } 
        if (isset($request_data['division_id']) && !empty($request_data['division_id'])) {
            $partial_query->where('division_id', $request_data['division_id']);
        }
        if (isset($request_data['state_id']) && !empty($request_data['state_id'])) {
            $partial_query->where('state_id', $request_data['state_id']);
        }

        $all_events = $partial_query->offset($offset)->limit($this->limit)
                ->get();

                foreach ($all_events as $key => $event) {
                    $all_events[$key]->is_live = $event->is_active;
                    // $currentDateTime = date('Y-m-d H:i:s');
                    // $newDateTime = date('h:i A', strtotime($currentDateTime));

                    $dateTime = new \DateTime('now'); 
                    
                    $currentDateTime = $dateTime->format("Y-m-d h:i A"); 
        
                    $startDateTime = $event->start_date.' '.$event->start_time;
                    $endDateTime = $event->end_date.' '.$event->end_time;
    
                    if ($currentDateTime >= $startDateTime && $currentDateTime < $endDateTime) {
                        $all_events[$key]->is_active = 1;
                    } else {
                        $all_events[$key]->is_active = 0;
                    }
                   
                }
        
        if ($all_events->count() == 0) {
            return response()->json(['success' => 0, 'msg' => "No record found", 'data' => [], 'error' => []]);
        }
        return response()->json(['success' => 1, 'msg' => "All Event Data", 'data' => $all_events]);
  	}	

    //nishit raval
    public function get_my_events(Request $request){
    	$validator = Validator::make($request->all(), [
                    'user_id' => 'required'
        ]);

    	if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }

    	$request_data = $request->all();
        $response_data = [];
     
    	if(isset($request_data['offset']) && !empty($request_data['offset'])){
    		$request_data['offset'] = $request_data['offset'];
    	}else{
    		$request_data['offset'] = 1;
        }
        $offset = ($request_data['offset'] - 1) * $this->limit;
        $condition = [];
    
        if (isset($request_data['country_id']) && $request_data['country_id']) {
            $condition['country_id'] = $request_data['country_id'];
        }
        if (isset($request_data['region_id']) && $request_data['region_id']) {
            $condition['region_id'] = $request_data['region_id'];
        }
        if (isset($request_data['devision_id']) && $request_data['devision_id']) {
            $condition['devision_id'] = $request_data['devision_id'];
        }
        if (isset($request_data['state_id']) && $request_data['state_id']) {
            $condition['state_id'] = $request_data['state_id'];
        }


        $my_events = Events::where('user_id',$request->get('user_id'))
            ->where('event.start_date','>=', date('Y-m-d'))
            //->whereTime('event.start_time','>=', time() )
            //->where(DB::raw('strtotime(event.start_time)'), '>',time())
            ->where(function ($query) use ($request_data) {
                if (!empty($request_data['start_date']) && !empty($request_data['end_date'])) {
                    $query->whereDate('start_date', '>=', $request_data['start_date'])
                            ->whereDate('end_date', '<=', $request_data['end_date']);
                }
            })
            ->where($condition)
            ->orderBy('start_date', 'ASC')
            ->limit($this->limit)
            ->offset($offset)->get()->toArray();

            //$id_arr = [];
            foreach ($my_events as $key => $event) {
                // if ($event->start_update_time >= time()) {
                //     array_push($id_arr,$event->id);
                // }
                $my_events[$key]['is_live'] = $event['is_active'];
            }
            

            $response_data['user_info'] = User::where('id', $request->get('user_id'))->select('id', 'name', 'username','email', 'user_image')->get()->toArray();
            $response_data['my_events'] = $my_events;
            
            return Api::make_response(1, $response_data, 'My Event Data.');
  	}	

    public function mobile_event_apply(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'event_id' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $data = $request->all();

        $update_arr = [
            'event_apply_datetime' => date('Y-m-d H:i:s'),
            'is_apply' => 'Yes'
        ];
        
        DB::table('event')->where('user_id',$data['user_id'])
            ->where('id',$data['event_id'])
            ->update($update_arr);
        return Api::make_response(1, [], 'Event successfully applied!.');
    }

    //nishit raval
    public function get_event_by_id(Request $request){   //10/06/2020
    	$validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'event_id' => 'required'
        ]);

    	if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }

    	$request_data = $request->all();
        $response_data = [];
        $condition = [];

        $events = Events::with('userInfo')->where('id', $request->get('event_id'))->get()->toArray();
        $response_data['events_data'] = $events;
            
        return Api::make_response(1, $response_data, 'Event Data.');
  	}	

      
   
    //--------------------------------------- Pending ---------------------------------------------------------
  
}


