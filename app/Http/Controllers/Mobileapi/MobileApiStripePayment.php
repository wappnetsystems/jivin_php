<?php

namespace App\Http\Controllers\Mobileapi;

/**
 * Description of ApiStripePayment
 *
 * @author kishan
 */
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
use App\Api;
use App\Stripe_settings;
use App\User_stripe_data;
use App\Stripe_customer;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use App\UserSubcriptionHistory;
use App\BoostPlans;
use App\UserBoost;
use App\PostBoost;
use App\UserTransaction;
use App\Email;
use App\Subscription;

class MobileApiStripePayment extends Controller {

    protected $apiMethods = [
        'create_customer' => [
            'keyAuthentication' => true
        ],
        'insert_card' => [
            'keyAuthentication' => true
        ],
        'get_card_list' => [
            'keyAuthentication' => true
        ],
        'card_details' => [
            'keyAuthentication' => true
        ],
        'delete_card' => [
            'keyAuthentication' => true
        ],
        'create_subscription' => [
            'keyAuthentication' => true
        ],
        'cancel_subscription' => [
            'keyAuthentication' => true
        ],
        'create_default_card' => [
            'keyAuthentication' => true
        ]
    ];
    private $stripe_credentials;
    private $publish_key;
    private $secret_key;

    public function __construct() {

        require_once app_path() . '/stripe/init.php';

        $this->stripe_credentials = Stripe_settings::orderBy('id', 'ASC')->get();
        if ($this->stripe_credentials->count() == 0) {
            return Api::make_response(0, [], 'Error Occurred. Try Again!');
        }
        $this->publish_key = $this->stripe_credentials[0]->value;
        $this->secret_key = $this->stripe_credentials[1]->value;

        //set stripe api key
        \Stripe\Stripe::setApiKey($this->secret_key);
    }

    public function create_customer(Request $request) {

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json(['status'=>false,'msg'=>'Please follow validation rules.','data'=>[]]);
        }

        $user_id     = $request->input('user_id');
        $user_detail = User::where('id', $user_id)->get();

        if ($user_detail->count() == 0) {
            return Api::make_response(0, [], 'No record found.');
        }

        //call create customer api
        try {
            $stripe_customer = Stripe_customer::where(['user_id' => $user_id])->get();
            if ($stripe_customer->count() > 0) {
                return Api::make_response(0, [], 'Customer already created.');
            }
            $customer_response = \Stripe\Customer::create([
                        'description' => 'Customer of email ' . $user_detail[0]->email,
                        "email" => $user_detail[0]->email,
            ]);

            if (isset($customer_response->error)) {
                return Api::make_response(0, [], 'Error Occurred. Try Again!');
            }

            $customer_id = $customer_response->id;

            Stripe_customer::insert(['user_id' => $user_id, 'customer_id' => $customer_id,
                'created_ip' => $request->ip(), 'updated_ip' => $request->ip()
            ]);
            return Api::make_response(1, [], 'Customer created successfully.');
        } catch (Exception $exc) {
            return Api::make_response(0, [], 'Error Occurred. Try Again!');
        }
    }

    public function mobile_insert_card(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'number' => 'required',
                    'exp_month' => 'required',
                    'exp_year' => 'required',
                    'cvc' => 'required',
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Validation errors. Try Again!');
        }
        $user_id = $request->input('user_id');
        $customer_data = Stripe_customer::where('user_id', $user_id)->get();

        if ($customer_data->count() == 0) {
            return Api::make_response(0, [], 'No record found.');
        }

        //first create token of credit card details
        $card_arr = [
            'card' => [
                "number" => $request->input('number'),
                "exp_month" => $request->input('exp_month'),
                "exp_year" => $request->input('exp_year'),
                "cvc" => $request->input('cvc')
            ]
        ];
        $token = \Stripe\Token::create($card_arr);

        if (isset($token->id)) {
            $customer = \Stripe\Customer::retrieve($customer_data[0]->customer_id);
            $card_details = $customer->sources->create(array("source" => $token->id));

            User_stripe_data::insert([
                'user_id' => $user_id,
                'card_id' => $card_details->id,
                'created_ip' => $request->ip(),
                'updated_ip' => $request->ip()
            ]);

            $card_list = \Stripe\Customer::retrieve($customer_data[0]->customer_id)->sources->all([
                'limit' => 25, 'object' => 'card']);

            return Api::make_response(1, $card_list->data, 'New card successfully added.');
        } else {
            return Api::make_response(0, [], 'Error Occurred. Try Again!');
        }
    }

    public function mobile_get_card_list(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'msg'=>'Please follow validation rules.','data'=>[]]);
        }

        $user_id = $request->input('user_id');
        $customer_data = Stripe_customer::where('user_id', $user_id)->get();

        if ($customer_data->count() == 0) {
            return Api::make_response(0, [], 'No record found.');
        }

        $card_details = \Stripe\Customer::retrieve($customer_data[0]->customer_id)->sources->all([
            'limit' => 25, 'object' => 'card']);

        if (isset($card_details->data) && count($card_details->data) > 0) {

            return Api::make_response(1, $card_details->data, 'Card details found');
        } else {
            return Api::make_response(0, [], 'No record found.');
        }
    }

    public function card_details(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'card_id' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Validation errors. Try Again!');
        }
        $user_id = $request->input('user_id');
        $customer_data = Stripe_customer::where('user_id', $user_id)->get();

        if ($customer_data->count() == 0) {
            return Api::make_response(0, [], 'No record found.');
        }
        $customer = \Stripe\Customer::retrieve($customer_data[0]->customer_id);
        $card = $customer->sources->retrieve($request->input('card_id'));
        return Api::make_response(1, $card, 'No record found.');
    }

    public function delete_card(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'card_id' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Validation errors. Try Again!');
        }

        $user_id = $request->input('user_id');
        $customer_data = Stripe_customer::where('user_id', $user_id)->get();

        if ($customer_data->count() == 0) {
            return Api::make_response(0, [], 'No record found.');
        }
        $customer = \Stripe\Customer::retrieve($customer_data[0]->customer_id);
        $delete_response = $customer->sources->retrieve($request->input('card_id'))->delete();

        if (isset($delete_response->deleted) && $delete_response->deleted) {
            User_stripe_data::where(['user_id' => $user_id, 'card_id' => $request->input('card_id')])->delete();

            $card_list = \Stripe\Customer::retrieve($customer_data[0]->customer_id)->sources->all([
                'limit' => 25, 'object' => 'card']);

            return Api::make_response(1, $card_list->data, 'Card successfully removed.');
        } else {
            return Api::make_response(0, [], 'Error Occurred. Try Again!');
        }
    }

    public function create_subscription(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'package_id' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Validation errors. Try Again!');
        }
        $user_id = $request->input('user_id');
        $customer_data = Stripe_customer::where('user_id', $user_id)->get();

        if ($customer_data->count() == 0) {
            return Api::make_response(0, [], 'No record found.');
        }
        $package_id = $request->input('package_id');

        $plan_detail = \App\Plans::find($package_id);

        if ($plan_detail->count() == 0) {
            return Api::make_response(0, [], 'Error Occurred. Try Again!');
        }

        /* $plan_detail=\Stripe\Plan::retrieve($plan_id);

          if(!isset($plan_detail->id)){
          return Api::make_response(0, [], 'Error Occurred. Try Again!');
          } */
        $country_boost = ($plan_detail->allow_boost_count) ? $plan_detail->allow_boost_count : 0;
        $region_boost = ($plan_detail->allow_boost_count_region) ? $plan_detail->allow_boost_count_region : 0;
        $division_boost = ($plan_detail->allow_boost_count_division) ? $plan_detail->allow_boost_count_division : 0;
        $state_boost = ($plan_detail->allow_boost_count_state) ? $plan_detail->allow_boost_count_state : 0;

        $price = $plan_detail->price;
        $plantype = $plan_detail->type;
        $checksubscription = Subscription::where('user_id', $user_id)->first();

        if ($plantype != "Free") {

            if (empty($checksubscription) || $checksubscription['subscription_id'] == '') {

                $subscrition_detail = \Stripe\Subscription::create([
                            "customer" => $customer_data[0]->customer_id,
                            "items" => [
                                    [
                                    "plan" => $plan_detail->plan_id,
                                ]
                            ]
                ]);
            } else {

                $previous_subscription = \Stripe\Subscription::retrieve($checksubscription['subscription_id']);
                $subscrition_detail = \Stripe\Subscription::update(
                                $checksubscription['subscription_id'], [
                            "items" => [
                                    [
                                    "id" => $previous_subscription->items->data[0]->id,
                                    "plan" => $plan_detail->plan_id,
                                ]
                            ]
                                ]
                );
            }
        }

        if ($plantype == "Free" || isset($subscrition_detail->id)) {

            $usersubcription = new UserSubcriptionHistory;
            $usersubcription->user_id = $user_id;
            $usersubcription->package_id = $package_id;
            $usersubcription->save();
            $expired_date = date('Y-m-d', strtotime("+" . $plan_detail['duration'] . " days"));



            if ($plantype != "Free") {
                $transaction = array("transaction_status" => "approved", "subscription_data" => $subscrition_detail);
                $instUserTransaction = new UserTransaction;
                $instUserTransaction->user_id = $user_id;
                $instUserTransaction->transaction_details = json_encode($transaction);
                $instUserTransaction->transaction_for = "Plan Subscription";
                $instUserTransaction->amount = $price;
                $instUserTransaction->save();

                Email::sendsubScriptionMail($user_id);


                if (!empty($checksubscription)) {

                    //echo $checksubscription['id'];die;
                    $addsubscription = Subscription::find($checksubscription['id']);
                    $addsubscription->user_id = $user_id;
                    $addsubscription->package_id = $package_id;
                    $addsubscription->package_valide_from = date('Y-m-d');
                    $addsubscription->package_valide_to = $expired_date;
                    $addsubscription->is_active = 1;
                    $addsubscription->subscription_id = $subscrition_detail->id;
                    if ($addsubscription->save()) {
                        //****** boost update start ************//
                        /*
                          $user_boost = User::where("id",$data['user_id'])->first()->total_boost;
                          $user_boost_update = User::find($data['user_id']);
                          $user_boost_update->total_boost = $user_boost+$allow_boost_count;
                          $user_boost_update->save(); // boost_for_location
                         */
                        $user_boost = User::where("id", $user_id)->first();

                        $user_boost_update = User::find($user_id);
                        if ($plan_detail->allow_boost_count) {
                            $user_boost_update->country_boost_avaliable = $user_boost['country_boost_avaliable'] + $country_boost;
                            $user_boost_update->country_boost = $user_boost['country_boost'] + $country_boost;
                        }
                        if ($plan_detail->allow_boost_count_region) {
                            $user_boost_update->region_boost_avaliable = $user_boost['region_boost_avaliable'] + $region_boost;
                            $user_boost_update->region_boost = $user_boost['region_boost'] + $region_boost;
                        }
                        if ($plan_detail->allow_boost_count_division) {
                            $user_boost_update->division_boost_avaliable = $user_boost['division_boost_avaliable'] + $division_boost;
                            $user_boost_update->division_boost = $user_boost['division_boost'] + $division_boost;
                        }
                        if ($plan_detail->allow_boost_count_state) {
                            $user_boost_update->state_boost_avaliable = $user_boost['state_boost_avaliable'] + $state_boost;
                            $user_boost_update->state_boost = $user_boost['state_boost'] + $state_boost;
                        }
                        $user_boost_update->save();


                        //****** boost update end ************//
                        $response = Api::make_response(1, array('plan' => $plan_detail), 'Plans details');
                    } else {
                        $response = Api::make_response(0, [], 'Somethings wrong');
                    }
                } else {
                    $addsubscription = new Subscription;
                    $addsubscription->user_id = $user_id;
                    $addsubscription->package_id = $package_id;
                    $addsubscription->package_valide_from = date('Y-m-d');
                    $addsubscription->package_valide_to = $expired_date;
                    $addsubscription->is_active = 1;
                    $addsubscription->subscription_id = $subscrition_detail->id;
                    if ($addsubscription->save()) {
                        //****** boost update start ************//

                        $user_boost = User::where("id", $user_id)->first()->total_boost;
                        $user_boost_update = User::find($user_id);
                        $user_boost_update->total_boost = $user_boost + $country_boost + $region_boost + $division_boost + $state_boost;
                        $user_boost_update->save();
                        //****** boost update end ************//
                        $response = Api::make_response(1, array('plan' => $plan_detail), 'Plans details');
                    } else {
                        $response = Api::make_response(0, [], 'Somethings wrong');
                    }
                }
            } else {

                if (!empty($checksubscription)) {

                    $user_subscribe_detail = \App\Subscription::where('user_id', $user_id)->get();

                    if ($user_subscribe_detail->count() == 0) {
                        return Api::make_response(0, [], "Error Occurred. Try Again!");
                    }

                    $sub = \Stripe\Subscription::retrieve($user_subscribe_detail[0]->subscription_id);
                    $cancel_result = $sub->cancel();

                    //echo $checksubscription['id'];die;
                    $addsubscription = Subscription::find($checksubscription['id']);
                    $addsubscription->user_id = $user_id;
                    $addsubscription->package_id = $package_id;
                    $addsubscription->package_valide_from = date('Y-m-d');
                    $addsubscription->package_valide_to = $expired_date;
                    $addsubscription->is_active = 1;
                    $addsubscription->subscription_id = '';
                    if ($addsubscription->save()) {
                        $response = Api::make_response(1, array('plan' => $plan_detail), 'Plans details');
                    } else {
                        $response = Api::make_response(0, [], 'Somethings wrong');
                    }
                } else {

                    $addsubscription = new Subscription;
                    $addsubscription->user_id = $user_id;
                    $addsubscription->package_id = $package_id;
                    $addsubscription->package_valide_from = date('Y-m-d');
                    $addsubscription->package_valide_to = $expired_date;
                    $addsubscription->is_active = 1;
                    $addsubscription->subscription_id = '';
                    if ($addsubscription->save()) {
                        $response = Api::make_response(1, array('plan' => $plan_detail), 'Plans details');
                    } else {
                        $response = Api::make_response(0, [], 'Somethings wrong');
                    }
                }
                Email::sendsubScriptionMail($user_id);
            }
            return $response;
            $user_subscription_detail = \App\Subscription::where('user_id', $user_id)->get();
            if ($user_subscription_detail->count() == 0) {
                //insert record in database
                $subscrition_arr = [
                    'user_id' => $user_id,
                    'package_id' => $package_id,
                    'package_valide_from' => date('Y-m-d'),
                    'subscription_id' => $subscrition_detail->id,
                    'is_active' => 1,
                    'created_at' => date('Y-m-d H:m:i'),
                    'updated_at' => date('Y-m-d H:m:i'),
                ];
                \App\Subscription::insert($subscrition_arr);
            } else {
                //update record in database
                $subscrition_arr = [
                    'package_id' => $package_id,
                    'package_valide_from' => date('Y-m-d'),
                    'subscription_id' => $subscrition_detail->id,
                    'is_active' => 1,
                    'updated_at' => date('Y-m-d H:m:i'),
                ];
                \App\Subscription::where('user_id', $user_id)->update($subscrition_arr);
            }
            return Api::make_response(1, [], 'Subscription plan successfully activated.');
        } else {
            return Api::make_response(0, [], 'Error Occurred. Try Again!');
        }
    }

    public function create_default_card(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'card_id' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Validation errors. Try Again!');
        }
        $user_id = $request->input('user_id');
        $card_id = $request->input('card_id');

        $customer_data = Stripe_customer::where('user_id', $user_id)->get();

        if ($customer_data->count() == 0) {
            return Api::make_response(0, [], 'No customer found.');
        }

        $update_detail = \Stripe\Customer::update($customer_data[0]->customer_id, ['default_source' => $card_id]);

        if (!isset($update_detail['id'])) {
            return Api::make_response(0, [], 'Error Occurred. Try Again!');
        }
        return Api::make_response(1, [], 'Default card successfully updated for subscription.');
    }

    public function cancel_subscription(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Validation errors. Try Again!');
        }
        $user_id = $request->input('user_id');

        $user_subscribe_detail = \App\Subscription::where('user_id', $user_id)->get();

        if ($user_subscribe_detail->count() == 0) {
            return Api::make_response(0, [], "Error Occurred. Try Again!");
        }

        $sub = \Stripe\Subscription::retrieve($user_subscribe_detail[0]->subscription_id);
        $cancel_response = $sub->cancel();

        if (isset($cancel_response->id)) {

            $subscription_arr = [
                'package_id' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'subscription_id' => ''
            ];
            \App\Subscription::where('user_id', $user_id)->update($subscription_arr);
            return Api::make_response(1, [], 'Subscription cancelled successfully.');
        } else {
            return Api::make_response(0, [], 'Error Occurred. Try Again!');
        }
    }

    public function boost_plan(Request $request) {

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'card_id' => 'required',
                    'plan_id' =>'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Validation errors. Try Again!');
        }

        $data = $request->json()->all();
        $response = [];

        $plan_dtails = BoostPlans::find($data['plan_id']);
        $price = $plan_dtails->amount;
        $country_boost_count = $plan_dtails->country_boost_count;
        $region_boost_count = $plan_dtails->region_boost_count;
        $division_boost_count = $plan_dtails->division_boost_count;
        $state_boost_count = $plan_dtails->state_boost_count;

        //code for charge card in stripe and check transaction and if approved then update database accordingly
        $customer_data = Stripe_customer::where('user_id', $data['user_id'])->get();

        if ($customer_data->count() == 0) {
            return Api::make_response(0, [], 'No record found.');
        }
        
        $charge_response = \Stripe\Charge::create([
                    "amount" => $price*100,
                    "currency" => 'USD',
                    "source" => $request->input('card_id'),
                    "description" => "User purchased Boost plan",
                    "customer" => $customer_data[0]->customer_id
        ]);
        
        if (isset($charge_response->id)) {
            $instUserTransaction = new UserTransaction;
            $instUserTransaction->user_id = $data['user_id'];
            $instUserTransaction->transaction_details = json_encode($charge_response);
            $instUserTransaction->transaction_for = "Boost Plan Subscription";
            $instUserTransaction->transaction_id = $charge_response->balance_transaction;
            $instUserTransaction->user_card_id = $request->input('card_id');
            $instUserTransaction->method = $charge_response->object;
            $instUserTransaction->transaction_status = $charge_response->status;
            $instUserTransaction->bank_resp_code = NULL;
            $instUserTransaction->amount = $price;
            $instUserTransaction->status = 1;
            $instUserTransaction->save();

            $userboost = new UserBoost;
            $userboost->user_id = $data['user_id'];
            $userboost->boost_plan = $data['plan_id'];
            $userboost->is_active = 1;
            if ($userboost->save()) {
                //****** boost update start ************//

                $user_boost = User::where("id", $data['user_id'])->first();
                $user_boost_update = User::find($data['user_id']);

                $user_boost_update->country_boost_avaliable = $user_boost['country_boost_avaliable'] + $country_boost_count;
                $user_boost_update->country_boost = $user_boost['country_boost'] + $country_boost_count;

                $user_boost_update->region_boost_avaliable = $user_boost['region_boost_avaliable'] + $region_boost_count;
                $user_boost_update->region_boost = $user_boost['region_boost'] + $region_boost_count;

                $user_boost_update->division_boost_avaliable = $user_boost['division_boost_avaliable'] + $division_boost_count;
                $user_boost_update->division_boost = $user_boost['division_boost'] + $division_boost_count;

                $user_boost_update->state_boost_avaliable = $user_boost['state_boost_avaliable'] + $state_boost_count;
                $user_boost_update->state_boost = $user_boost['state_boost'] + $state_boost_count;

                $user_boost_update->save();
                //****** boost update end ************//

                $response = Api::make_response(1, ['plan' => $plan_dtails, 'userboost' => $userboost], 'Plans get successfully purchased');
            } else {
                $response = Api::make_response(0, [], 'Plans not found');
            }
        }
        else{
            
        }

        return $response;
    }

}

