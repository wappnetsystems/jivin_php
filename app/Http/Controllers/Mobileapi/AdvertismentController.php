<?php

namespace App\Http\Controllers\Mobileapi;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Lib\Upload_file;
use Illuminate\Support\Facades\DB;
use App\Api;
use App\User;
use App\Mobile_advertisment;
use App\Mobile_advertisement_types;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Hash;

class AdvertismentController extends Controller {

	private $limit;

    public function __construct() {
        $this->limit = 5;
    }
    
    //nishit raval 02/06/2020
	public function mobile_create_advertisment(Request $request) {

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'advertisement_types_id' => 'required',
                    'ad_date' => 'required',
                    'ad_start_time' => 'required',
                    'ad_end_time' => 'required',
                    'advertisement_file' => 'required'          
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        if ($request->hasFile('advertisement_file')){
        	
        	$result = Upload_file::upload_advertisment_file($request);
            if (!$result['status']) {
                return Api::make_response(0, [], "Error during file upload. Please try again.");
            }
            $file_path = $result['storage_path'];
        }else{
        	$file_path = "";
        }

        $insert_ad = [
            'user_id' => $request_data['user_id'],
            'mobile_advertisement_types_id' => $request_data['advertisement_types_id'],
            'ad_date' => $request_data['ad_date'],
            'ad_start_time' => $request_data['ad_start_time'],
            'ad_end_time' => $request_data['ad_end_time'],
            'ad_attach_file' => $file_path,
            'created_at' => date('Y-m-d H:i:s'),
            'created_ip' => $request->ip(),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_ip' => $request->ip()
        	
        ];
 
        Mobile_advertisment::insert($insert_ad);
        return Api::make_response(1, [], 'Advertisment successfully created.');
    }

    public function mobile_update_advertisment(Request $request) {

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'advertisment_id' => 'required',
                    'advertisement_types_id' => 'required',
                    'ad_date' => 'required',
                    'ad_start_time' => 'required',
                    'ad_end_time' => 'required'
                    //'ad_attach_file' => 'required'          
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        
        $update_ad = [
            'user_id' => $request_data['user_id'],
            'mobile_advertisement_types_id' => $request_data['advertisement_types_id'],
            'ad_date' => $request_data['ad_date'],
            'ad_start_time' => $request_data['ad_start_time'],
            'ad_end_time' => $request_data['ad_end_time'],
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_ip' => $request->ip()
        ];

        if ($request->hasFile('advertisement_file')){
        	
        	$result = Upload_file::upload_advertisment_file($request);
            if (!$result['status']) {
                return Api::make_response(0, [], "Error during file upload. Please try again.");
            }
            $file_path = $result['storage_path'];
            $update_ad['ad_attach_file'] = $file_path;
        }
 
        Mobile_advertisment::where('id',$request->get('advertisment_id'))->update($update_ad);
        return Api::make_response(1, [], 'Advertisment successfully updated.');
    }

    public function mobile_advertisment_types(Request $request){
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $response_data = [];

        $advartisments_types = Mobile_advertisement_types::where('is_active',1)->get()->toArray();
        
        $response_data['advartisments_types']  = $advartisments_types;
        
        return Api::make_response(1, $response_data, 'My Advertisment Data.');
    }	

    public function mobile_my_advertisment(Request $request){
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }

        $request_data = $request->all();
        $response_data = [];
    
        if(isset($request_data['offset']) && !empty($request_data['offset'])){
            $request_data['offset'] = $request_data['offset'];
        }else{
            $request_data['offset'] = 1;
        }
        $offset = ($request_data['offset'] - 1) * $this->limit;
        $condition = [];

        $my_advartisments = Mobile_advertisment::join('mobile_advertisement_types','mobile_advertisement_types.id','=','mobile_advertisment.mobile_advertisement_types_id')    
           ->where('mobile_advertisment.user_id',$request->get('user_id'))
            ->limit($this->limit)
            ->offset($offset)->get(['mobile_advertisment.*','mobile_advertisement_types.ad_title','mobile_advertisement_types.ad_details']);

            foreach ($my_advartisments as $key => $value) {
                $my_advartisments[$key]->advertisement_file_url = Upload_file::get_post_file_path('Advertisement', $value->ad_attach_file, 0);
            }
            $response_data['user_info'] = User::where('id', $request->get('user_id'))->select('id', 'name', 'username','email', 'user_image')->get()->toArray();
            $response_data['my_advartisments'] = $my_advartisments;
            
            return Api::make_response(1, $response_data, 'My Advertisment Data.');
    }	

    public function mobile_get_advertisment_by_type(Request $request){
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'advertisment_type' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }

        $request_data = $request->all();
        $response_data = [];
    
        if(isset($request_data['offset']) && !empty($request_data['offset'])){
            $request_data['offset'] = $request_data['offset'];
        }else{
            $request_data['offset'] = 1;
        }
        $offset = ($request_data['offset'] - 1) * $this->limit;
        $condition = [];

        $advartisments = Mobile_advertisment::with('userInfo')
            ->join('mobile_advertisement_types','mobile_advertisement_types.id','=','mobile_advertisment.mobile_advertisement_types_id')    
            ->where('mobile_advertisment.mobile_advertisement_types_id',$request_data['advertisment_type'])
            ->limit($this->limit)
            ->offset($offset)->get(['mobile_advertisment.*','mobile_advertisement_types.ad_title','mobile_advertisement_types.ad_details']);

            foreach ($advartisments as $key => $value) {
                $advartisments[$key]->advertisement_file_url = Upload_file::get_post_file_path('Advertisement', $value->ad_attach_file, 0);
            }
        $response_data['advartisments'] = $advartisments;
            
        return Api::make_response(1, $response_data, 'Advertisment Data.');
    }	

    }


