<?php

namespace App\Http\Controllers\Mobileapi;

use Illuminate\Http\Request;
use Hash;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use App\Http\Controllers\Controller;
//load Models
use App\Api;
use App\User;
use App\NotificationLog;

class NotificationController extends Controller {
    private $limit=20;
    public function mobile_notification_details(Request $request) {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'notification_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        if (isset($data['notification_id'])) {
            $notificationId = $data['notification_id'];
            $fillnotificationLog = NotificationLog::find($notificationId);
            $arrayCondition = ['contact_msg', 'bid_win']; //in_array("Glenn", $people)
            //if($fillnotificationLog->activity_type != 'contact_msg'){
            if (!in_array($fillnotificationLog->activity_type, $arrayCondition)) {
                $notificationDetails = NotificationLog::with(NotificationLog::getWithModel($notificationId))->where('id', '=', $notificationId)->first();
            } else {
                $notificationDetails = $fillnotificationLog;
            }


            if ($notificationDetails->count()) {
                $userFrmDb = User::where('id', '=', $notificationDetails['activity_by'])->first();
                if ($userFrmDb->count()) {
                    $notificationDetails->username = $userFrmDb['username'];
                }
                $response = Api::make_response(1, [$notificationDetails], 'Details found');
            } else {
                $response = Api::make_response(0, [], 'Details not found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param.');
        }
        return $response;
    }

    public function setNotificationRead(Request $request) {
        $data = $request->json()->all();
        $response = [];
        if (isset($data['user_id'])) {
            $affected = NotificationLog::where('visible_by', '=', $data['user_id'])->update(array('is_read' => 1));
            $response = Api::make_response(1, ['unread_notification_count' => 0], 'All notification read');
        } else {
            $response = Api::make_response(0, [], 'Invalide Param.');
        }
        return $response;
    }

    public function mobile_notification(Request $request) {
        $data = $request->all();
        $response = [];

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'pagination' => 'required',
                    'page' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $data['itemsPerPage'] = 20;

        if ($data) {
            if ($data['pagination']) {
                $getNotification = NotificationLog::where('visible_by', '=', $data['user_id'])->orderBy('id', 'DESC')->paginate($data['itemsPerPage']);
            } else {
                $getNotification = NotificationLog::where('visible_by', '=', $data['user_id'])->orderBy('id', 'DESC')->get();
            }
            $newNotificationCount = NotificationLog::where('visible_by', '=', $data['user_id'])
                    ->where('is_read', '=', 0)
                    ->count();
            if ($getNotification->count()) {
                $response = Api::make_response(1, [$getNotification], 'Notification Found', ['new_notification_count' => $newNotificationCount]);
            } else {
                $response = Api::make_response(0, [], 'Notification not Found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param.');
        }
        return $response;
    }

    public function get_notification_list(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'page_number' => 'required',
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        
        $offset=($request_data['page_number']-1)* $this->limit;
        $notification_list = NotificationLog::where('visible_by', $request_data['user_id'])
                ->with(['postDetails'])
                ->orderBy('id','DESC')
                ->limit($this->limit)
                ->offset($offset)
                ->get();
        
        if($notification_list->count()==0){
            return Api::make_response(0, [], "No notification available.");
        }
        return Api::make_response(1, $notification_list, "Notifications available.");
    }
    
    public function mark_notification_read(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'notification_id' => 'required',
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        
        //check if this notification is for this user
        $check_result = NotificationLog::where('id',$request_data['notification_id'])
                                        ->where('visible_by',$request_data['user_id'])
                                        ->get(['id'])->count();
        if($check_result==0){
            return Api::make_response(0, [], "Invalid access by user. Try Again.");
        }
        
        //mark notification as read
        NotificationLog::where('id',$request_data['notification_id'])
                ->update(['is_read'=>1,'updated_at'=>date('Y-m-d H:i:s')]);
        return Api::make_response(1, [], "Notification is marked as read.");
    }

}
