<?php

namespace App\Http\Controllers\Mobileapi;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Lib\Upload_file;
use Illuminate\Support\Facades\DB;
use App\Api;
use App\User;
use App\Chats;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Hash;

class ChatController extends Controller {

	private $limit;

    public function __construct() {
        $this->limit = 25;
    }
    
    public function mobile_send_message(Request $request) {

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'to_user_id' => 'required',
                    'message_body' => 'required'    
                    
        ]);
        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $requet_data = $data = $request->all();
            
            $chatModel = new Chats;
            $chatModel->user_id = $requet_data['user_id'];
            $chatModel->to_user_id = $requet_data['to_user_id'];
            $chatModel->message_body = $requet_data['message_body'];
            $chatModel->created_at = date('Y-m-d H:i:s');
          
            if ($chatModel->save()) { 

                return Api::make_response(1, [], 'Meesage sent successfully.');
            }
            $response = Api::make_response(0, [], 'Error Occurred');
    }

    public function mobile_get_all_message(Request $request) {

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'target_user_id' => 'required',
                    'page_number' => 'required'   
                    
        ]);
        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data  = $request->all();
        $offset = ($request_data['page_number'] - 1) * $this->limit;
        $response_data = [];
        $select_fileds = ['users.name as login_user_name','users.username as login_username','users.user_image as login_user_image','to_user.name as target_user_name','to_user.username as target_useranme','to_user.user_image as target_user_image',
                           'chats.user_id','chats.to_user_id', 'chats.message_body','chats.created_at'];    
        
        $meesages = Chats::join('users','users.id','=','chats.user_id')
            ->join('users as to_user ','to_user.id','=','chats.to_user_id')
            ->where(function ($query) use ($request_data) {
                    $query->where('chats.user_id', '=', $request_data['user_id'])
                        ->where('chats.to_user_id', '=', $request_data['target_user_id']);
                })->orWhere(function ($query) use ($request_data) {
                    $query->where('chats.user_id', '=', $request_data['target_user_id'])
                        ->where('chats.to_user_id', '=', $request_data['user_id']);
                })
                ->offset($offset)
                ->limit($this->limit)
                ->get($select_fileds)->toArray();
                
            if (count($meesages) > 0 ) {
                foreach ($meesages as $key => $value) {
                    $meesages[$key]['login_user_image'] = Upload_file::get_post_file_path('Profile', $value['login_user_image'], 0);
                    $meesages[$key]['target_user_image'] = Upload_file::get_post_file_path('Profile', $value['target_user_image'], 0);
                }
                $response_data['meesages']  = $meesages;
            } else {
                $response_data['meesages']  = [];
                $response = Api::make_response(0, $response_data, 'Messages Not Found');
            }
            return Api::make_response(1, $response_data, 'Get All messages');     
    }


}


