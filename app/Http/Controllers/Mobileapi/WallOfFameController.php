<?php

namespace App\Http\Controllers\Mobileapi;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Api;
use App\Posts;
use App\LeaderboardLog;
use Illuminate\Support\Facades\Validator;
use App\Followers;
use App\Country;
use App\Devision;
use App\Region;
use App\State;

class WallOfFameController extends Controller
{

    //referance this
    public static function set_month($month)
    {  //for month
        switch ($month) {
            case "1":
                return 01;
                break;
            case "2":
                return 05;
                break;
            case "3":
                return 10;
                break;
        }
    }

    public function filter_arr($array, $key)
    {
        $response = array_filter($array, function($ar) use($key) {
            return ($ar['id'] == $key);      
        });
        
        $index = key($response);
        //appending value in our array 0th Position
        array_unshift($array, $array[$index]);
        //now make it unique.
        $array2 = array_unique($array,SORT_REGULAR);
        //Re-indexing
        $final = array_values($array2);
        return $final;
    }

    public function get_competition_winners(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            //'page'=>'required'
            //'previous_month'=>'required',
            //'previous_year'=>'required',
            //'competition_year' => 'required',
            //'competition_month' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        $all_response_data = [];
        $page_type = $request_data['page'];
        $previous_month = $request_data['previous_month'];
        $previous_year =  $request_data['previous_year'];
        $location_data = [];

        if (empty($request_data['year']) && empty($request_data['previous_year'])) {
            $previous_year = date('Y');
        }

        if (empty($request_data['leaderboard_no']) && empty($request_data['previous_month'])) {
            $previous_month = date('m');
        }

        if (isset($request_data['year']) && $request_data['year']) {
            $previous_year = $request_data['year'];
        }

        if (isset($request_data['leaderboard_no']) && $request_data['leaderboard_no']) {
            $previous_month = $this->set_month($request_data['leaderboard_no']);
        }

        //dd($previous_month, $previous_year);

        for ($lbc = 0; $lbc < 9; $lbc++) {
            if ($page_type == "Prev") {
                if ($lbc == 0) {
                    $current_month = $orginal_previous_month = $previous_month;
                    $current_year = $orginal_previous_year = $previous_year;
                } else {
                    $orginal_previous_month = $previous_month;
                    $orginal_previous_year = $previous_year;
                    $current_month = date('m', strtotime($previous_year . '-' . $previous_month . ' -4 months'));
                    $current_year = date('Y', strtotime($previous_year . '-' . $previous_month . ' -4 months'));
                    $previous_month = date('m', strtotime($orginal_previous_year . '-' . $orginal_previous_month . ' -4 months'));
                    $previous_year = date('Y', strtotime($orginal_previous_year . '-' . $orginal_previous_month . ' -4 months'));
                }
            } elseif ($page_type == "Next") {
                if ($lbc == 0) {
                    $current_month = $orginal_previous_month = $previous_month;
                    $current_year = $orginal_previous_year = $previous_year;
                } else {
                    $orginal_previous_month = $previous_month;
                    $orginal_previous_year = $previous_year;
                    $current_month = date('m', strtotime($previous_year . '-' . $previous_month . ' +4 months'));
                    $current_year = date('Y', strtotime($previous_year . '-' . $previous_month . ' +4 months'));
                    $previous_month = date('m', strtotime($orginal_previous_year . '-' . $orginal_previous_month . ' +4 months'));
                    $previous_year = date('Y', strtotime($orginal_previous_year . '-' . $orginal_previous_month . ' +4 months'));
                }
            } else {
                $current_month = $previous_month;
                $current_year = $previous_year;
            }


            $now = new \DateTime('now');
            $days = 0;
            $current_leaderboard = LeaderboardLog::getCurrentLeaderBoard($current_month);

            $current_leaderboard_end_date = LeaderboardLog::getCurrentLeaderBoardEndingDate($current_leaderboard);
            if ($current_leaderboard == 1) {
                $compititionspan = "Competition #1 ";
                $compititionspan_month_year = "January - April " . $current_year;
            } elseif ($current_leaderboard == 2) {
                $compititionspan = "Competition #2 ";
                $compititionspan_month_year = "May - August " . $current_year;
            } elseif ($current_leaderboard == 3) {
                $compititionspan = "Competition #3 ";
                $compititionspan_month_year = "September - December " . $current_year;
            }

            $date1 = new \DateTime(date('Y-m-d')); //current date or any date
            $date2 = new \DateTime($current_leaderboard_end_date); //Future date
            $diff = $date2->diff($date1)->format("%a"); //find difference
            $days = intval($diff) + 1;
            $ongoing_month = $now->format('m');

            $condition = [];


            if (isset($request_data['country_id']) && $request_data['country_id']) {

                $condition['country_id'] = $request_data['country_id'];
                $country_data = Country::find($request_data['country_id']);
                $location_data['country'] = $country_data;
            }
            if (isset($request_data['region_id']) && $request_data['region_id']) {
                $condition['region_id'] = $request_data['region_id'];
                $region_data = Region::find($request_data['region_id']);
                $location_data['region'] = $region_data;
            }
            if (isset($request_data['devision_id']) && $request_data['devision_id']) {
                $condition['devision_id'] = $request_data['devision_id'];
                $division_data = Devision::find($request_data['devision_id']);
                $location_data['division'] = $division_data;
            }
            if (isset($request_data['state_id']) && $request_data['state_id']) {
                $condition['state_id'] = $request_data['state_id'];
                $state_data = State::find($request_data['state_id']);
                $location_data['state'] = $state_data;
            }
            $condition['leader_board_no'] = $current_leaderboard;

            if ($this->checkMonth($current_month, $current_year)) {
                //$response = Api::make_response(0, [], 'Coming Soon!', ['compitition_text' => $compititionspan, 'compitition_month_year_text' => $compititionspan_month_year, 'leaderboarDaysleft' => $days, 'currentYear' => $current_year]);
                //return $response;
                $finalList = [];
            } else {

                $getLeaderboardList = LeaderboardLog::with('userInfo')
                    ->where($condition)
                    //->limit($data['top'])
                    ->where('is_active', 1)
                    // ->where('month',$current_leaderboard)
                    ->where('year', $current_year)
                    ->groupBy('user_id')
                    ->selectRaw('*, sum(total_vote) as total_vote')
                    // ->selectRaw('*,`leaderboard_log`.`user_id` as l_u_id, (select count(*) from `posts` inner join `post_like_log` on `posts`.`id`=`post_like_log`.`post_id` where `posts`.`user_id` = `l_u_id` AND (`posts`.`video_name` is not NULL OR `posts`.`audio_name` is not NULL)) as `total_vote`')
                    ->orderBy('total_vote', 'DESC')
                    ->get();

                $finalList = $this->getFinalList($getLeaderboardList, 3);
                $followinglist = array();
                if (isset($request_data['user_id'])) {
                    $followinglist = Followers::where('user_id', $request_data['user_id'])->pluck('is_following')->toArray();
                }
            }
            $finalListArray = [];
            $i = 0;
            foreach ($finalList as $flList) {

                if ($flList['rank'] < 4) {
                    $finalListArray[$i]['user_id'] = $flList['user_id'];
                    $finalListArray[$i]['country_id'] = $flList['country_id'];
                    $finalListArray[$i]['region_id'] = $flList['region_id'];
                    $finalListArray[$i]['devision_id'] = $flList['devision_id'];
                    $finalListArray[$i]['state_id'] = $flList['state_id'];
                    $finalListArray[$i]['total_vote'] = $flList['total_vote'];
                    $finalListArray[$i]['rank'] = $flList['rank'];
                    $finalListArray[$i]['user_full_name'] = $flList['user_full_name'];
                    $finalListArray[$i]['user_name'] = $flList['user_name'];
                    $finalListArray[$i]['user_info'] = $flList['userInfo'];

                    if (in_array($flList['user_id'], $followinglist)) {
                        $finalListArray[$i]['is_following'] = true;
                    } else {
                        $finalListArray[$i]['is_following'] = false;
                    }
                    $i = $i + 1;
                }
            }
            $all_response_data[$lbc]['meta_data'] = ['location_data' => $location_data, 'compitition_text' => $compititionspan, 'compitition_month_year_text' => $compititionspan_month_year, 'leaderboarDaysleft' => $days];
            $all_response_data[$lbc]['winner_data'] = $finalListArray;
        }
        if (count($all_response_data)) {
            if ($page_type == "Prev") {
                $current_month = date('m', strtotime($previous_year . '-' . $previous_month . ' -4 months'));
                $current_year = date('Y', strtotime($previous_year . '-' . $previous_month . ' -4 months'));
            } else {
                $current_month = date('m', strtotime($previous_year . '-' . $previous_month . ' +4 months'));
                $current_year = date('Y', strtotime($previous_year . '-' . $previous_month . ' +4 months'));
            }
            $response = Api::make_response(1, $all_response_data, 'Top Wall Of Fame Found', ['previous_month' => $current_month, 'previous_year' => $current_year]);
        } else {
            $response = Api::make_response(0, [], 'No Data Found', []);
        }
        return $response;
    }

    //-------------- NEW 
    public function mobile_competition_winners(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }

        $request_data = $request->all();
        $all_response_data = [];
        $page_type = $request_data['page'];
        $previous_month = date('m');
        $previous_year =  date('Y');
        $location_data = [];
        $condition = [];

        if (empty($request_data['year'])) {
            $previous_year = date('Y', strtotime(date('Y') . '-' .  date('m') . ' -4 months'));
        }

        if (empty($request_data['leaderboard_no'])) {
            $previous_month = date('m', strtotime(date('Y') . '-' .  date('m') . ' -4 months'));
        }

        if (isset($request_data['year']) && $request_data['year']) {
            $previous_year = $request_data['year'];
        }

        if (isset($request_data['leaderboard_no']) && $request_data['leaderboard_no']) {
            $previous_month = $this->set_month($request_data['leaderboard_no']);
        }

        $current_month = $previous_month;
        $current_year = $previous_year;


        $current_leaderboard = LeaderboardLog::getCurrentLeaderBoard($current_month);

        $current_leaderboard_end_date = LeaderboardLog::getCurrentLeaderBoardEndingDate($current_leaderboard);
        if ($current_leaderboard == 1) {
            $compititionspan = "Competition #1 ";
            $compititionspan_month_year = "January - April " . $current_year;
        } elseif ($current_leaderboard == 2) {
            $compititionspan = "Competition #2 ";
            $compititionspan_month_year = "May - August " . $current_year;
        } elseif ($current_leaderboard == 3) {
            $compititionspan = "Competition #3 ";
            $compititionspan_month_year = "September - December " . $current_year;
        }

        $current_leaderboard = $current_leaderboard; //- 1;
        $min_leaderboard_year = LeaderboardLog::where('year', '<=', $current_year)->min('year');
        $year_arr = [];



        $condition['leader_board_no'] = $current_leaderboard;
        //$region_list = DB::table('region')->where('country_id', $request_data['country_id'])->where('is_active', 1)->pluck('id');
        $country_list = [$request_data['country_id']];
        foreach ($country_list as $country_key =>  $country) {

            $all_response_data['country_data'][$country_key]['winner_data'] = [];
            $all_response_data['country_data'][$country_key]['meta_data'] = [];

                $condition['country_id'] = $request_data['country_id'];
                $country_data = Country::find($request_data['country_id']);
                //$location_data['country'] = $country_data;

                $getLeaderboardList = LeaderboardLog::with('userInfo')
                    ->where($condition)
                    //->limit($data['top'])
                    ->where('is_active', 1)
                    // ->where('month',$current_leaderboard)
                    ->where('year', $current_year)
                    ->groupBy('user_id')
                    ->selectRaw('*, sum(total_vote) as total_vote')
                    // ->selectRaw('*,`leaderboard_log`.`user_id` as l_u_id, (select count(*) from `posts` inner join `post_like_log` on `posts`.`id`=`post_like_log`.`post_id` where `posts`.`user_id` = `l_u_id` AND (`posts`.`video_name` is not NULL OR `posts`.`audio_name` is not NULL)) as `total_vote`')
                    ->orderBy('total_vote', 'DESC')
                    ->get();

                $finalList = $this->getFinalList($getLeaderboardList, 3);
                $followinglist = array();
                if (isset($request_data['user_id'])) {
                    $followinglist = Followers::where('user_id', $request_data['user_id'])->pluck('is_following')->toArray();
                }

                $finalListArray = [];
                $i = 0;
                foreach ($finalList as $flList) {

                    if ($flList['rank'] < 4) {
                        $finalListArray[$i]['user_id'] = $flList['user_id'];
                        $finalListArray[$i]['country_id'] = $flList['country_id'];
                        $finalListArray[$i]['region_id'] = $flList['region_id'];
                        $finalListArray[$i]['devision_id'] = $flList['devision_id'];
                        $finalListArray[$i]['state_id'] = $flList['state_id'];
                        $finalListArray[$i]['total_vote'] = $flList['total_vote'];
                        $finalListArray[$i]['rank'] = $flList['rank'];
                        $finalListArray[$i]['user_full_name'] = $flList['user_full_name'];
                        $finalListArray[$i]['user_name'] = $flList['user_name'];
                        $finalListArray[$i]['user_info'] = $flList['userInfo'];

                        if (in_array($flList['user_id'], $followinglist)) {
                            $finalListArray[$i]['is_following'] = true;
                        } else {
                            $finalListArray[$i]['is_following'] = false;
                        }
                        $i = $i + 1;
                    }
                }

        $all_response_data['country_data'][$country_key]['winner_data'] = $finalListArray;
        $all_response_data['country_data'][$country_key]['meta_data'] = ['location_data' => $country_data, 'compitition_text' => $compititionspan, 'compitition_month_year_text' => $compititionspan_month_year];
    }
        //code for region data -- start
        //code for get region
        $region_list = Region::where('country_id',  $request_data['country_id'])
            ->where('is_active',1)
            ->get()->toArray();
        
        if(isset($request_data['region_id']) && $request_data['region_id']){
            $region_list = $this->filter_arr($region_list, $request_data['region_id']);
        }
            
        $condition = [];
        $condition['leader_board_no'] = $current_leaderboard;
        foreach ($region_list as $reg_key =>  $region) {

            $all_response_data['region_data'][$reg_key]['winner_data'] = [];
            $all_response_data['region_data'][$reg_key]['meta_data'] = [];
            $all_response_data['region_data'][$reg_key]['meta_data'] = ['location_data' => $region, 'compitition_text' => $compititionspan, 'compitition_month_year_text' => $compititionspan_month_year];
            // if(isset($request_data['region_id']) && $request_data['region_id']!=$region->id){
            //     continue;
            // }
            $condition['region_id'] = $region['id'];
            $getLeaderboardList = LeaderboardLog::with('userInfo')
                ->where($condition)
                ->where('is_active', 1)
                ->where('year', $current_year)
                ->groupBy('user_id')
                ->selectRaw('*, sum(total_vote) as total_vote')
                ->orderBy('total_vote', 'DESC')
                ->get();

            $finalList = $this->getFinalList($getLeaderboardList, 3);
            $followinglist = array();
            if (isset($request_data['user_id'])) {
                $followinglist = Followers::where('user_id', $request_data['user_id'])->pluck('is_following')->toArray();
            }

            $finalListArray = [];
            $i = 0;
            foreach ($finalList as $flList) {   

                if ($flList['rank'] < 4) {
                    $finalListArray[$i]['user_id'] = $flList['user_id'];
                    $finalListArray[$i]['country_id'] = $flList['country_id'];
                    $finalListArray[$i]['region_id'] = $flList['region_id'];
                    $finalListArray[$i]['devision_id'] = $flList['devision_id'];
                    $finalListArray[$i]['state_id'] = $flList['state_id'];
                    $finalListArray[$i]['total_vote'] = $flList['total_vote'];
                    $finalListArray[$i]['rank'] = $flList['rank'];
                    $finalListArray[$i]['user_full_name'] = $flList['user_full_name'];
                    $finalListArray[$i]['user_name'] = $flList['user_name'];
                    $finalListArray[$i]['user_info'] = $flList['userInfo'];

                    if (in_array($flList['user_id'], $followinglist)) {
                        $finalListArray[$i]['is_following'] = true;
                    } else {
                        $finalListArray[$i]['is_following'] = false;
                    }
                    $i = $i + 1;
                }
            }

            $all_response_data['region_data'][$reg_key]['winner_data'] = $finalListArray;
            //$all_response_data['region_data'][$reg_key]['meta_data'] = ['location_data' => $region, 'compitition_text' => $compititionspan, 'compitition_month_year_text' => $compititionspan_month_year];
        }
       
        //code for region data -- end

        //code for division data --start
        //get division data
        $condition = [];
        $condition['leader_board_no'] = $current_leaderboard;
        $division_list=Devision::where('is_active',1)->get()->toArray();
        if(isset($request_data['devision_id']) && $request_data['devision_id']){
            $division_list = $this->filter_arr($division_list, $request_data['devision_id']);
        }
        foreach($division_list as $div_key =>  $division){

            $all_response_data['division_data'][$div_key]['winner_data'] = [];
            $all_response_data['division_data'][$div_key]['meta_data'] = [];
            $all_response_data['division_data'][$div_key]['meta_data'] = ['location_data' => $division, 'compitition_text' => $compititionspan, 'compitition_month_year_text' => $compititionspan_month_year];

            // if(isset($request_data['region_id']) && $request_data['region_id']!=$region->id){
            //     $condition['region_id'] = $region->id;
            // }
            // if(isset($request_data['devision_id']) && $request_data['devision_id']!=$division->id){
            //     continue;
            // }
            $condition['devision_id'] = $division['id'];
            $getLeaderboardList = LeaderboardLog::with('userInfo')
                ->where($condition)
                ->where('is_active', 1)
                ->where('year', $current_year)
                ->groupBy('user_id')
                ->selectRaw('*, sum(total_vote) as total_vote')
                ->orderBy('total_vote', 'DESC')
                ->get();

            $finalList = $this->getFinalList($getLeaderboardList, 3);
            $followinglist = array();
            if (isset($request_data['user_id'])) {
                $followinglist = Followers::where('user_id', $request_data['user_id'])->pluck('is_following')->toArray();
            }

            $finalListArray = [];
            $i = 0;
            foreach ($finalList as $flList) {

                if ($flList['rank'] < 4) {
                    $finalListArray[$i]['user_id'] = $flList['user_id'];
                    $finalListArray[$i]['country_id'] = $flList['country_id'];
                    $finalListArray[$i]['region_id'] = $flList['region_id'];
                    $finalListArray[$i]['devision_id'] = $flList['devision_id'];
                    $finalListArray[$i]['state_id'] = $flList['state_id'];
                    $finalListArray[$i]['total_vote'] = $flList['total_vote'];
                    $finalListArray[$i]['rank'] = $flList['rank'];
                    $finalListArray[$i]['user_full_name'] = $flList['user_full_name'];
                    $finalListArray[$i]['user_name'] = $flList['user_name'];
                    $finalListArray[$i]['user_info'] = $flList['userInfo'];

                    if (in_array($flList['user_id'], $followinglist)) {
                        $finalListArray[$i]['is_following'] = true;
                    } else {
                        $finalListArray[$i]['is_following'] = false;
                    }
                    $i = $i + 1;
                }
            }

            $all_response_data['division_data'][$div_key]['winner_data'] = $finalListArray;
            //$all_response_data['division_data'][$div_key]['meta_data'] = ['location_data' => $division, 'compitition_text' => $compititionspan, 'compitition_month_year_text' => $compititionspan_month_year];
        }
        //code for division data --end

        //code for state data --start
        //get state data
        $condition = [];
        $condition['leader_board_no'] = $current_leaderboard;
        $state_list=State::where('is_active',1)->get()->toArray();
        if(isset($request_data['state_id']) && $request_data['state_id']){
            $state_list = $this->filter_arr($state_list, $request_data['state_id']);
        }
        foreach($state_list as $state_key => $state){
            
            $all_response_data['state_data'][$state_key]['winner_data'] =[];
            $all_response_data['state_data'][$state_key]['meta_data'] = [];
            $all_response_data['state_data'][$state_key]['meta_data'] = ['location_data' => $state, 'compitition_text' => $compititionspan, 'compitition_month_year_text' => $compititionspan_month_year];

            // if(isset($request_data['region_id']) && $request_data['region_id']!=$region->id){
            //     $condition['region_id'] = $region->id;
            // }
            // if(isset($request_data['devision_id']) && $request_data['devision_id']!=$division->id){
            //     $condition['devision_id'] = $division->id;
            // }
            // if(isset($request_data['state_id']) && $request_data['state_id']!=$state->id){
            //     continue;
            // }
            $condition['state_id'] = $state['id'];
            $getLeaderboardList = LeaderboardLog::with('userInfo')
                ->where($condition)
                ->where('is_active', 1)
                ->where('year', $current_year)
                ->groupBy('user_id')
                ->selectRaw('*, sum(total_vote) as total_vote')
                ->orderBy('total_vote', 'DESC')
                ->get();

            $finalList = $this->getFinalList($getLeaderboardList, 3);
            $followinglist = array();
            if (isset($request_data['user_id'])) {
                $followinglist = Followers::where('user_id', $request_data['user_id'])->pluck('is_following')->toArray();
            }

            $finalListArray = [];
            $i = 0;
            foreach ($finalList as $flList) {

                if ($flList['rank'] < 4) {
                    $finalListArray[$i]['user_id'] = $flList['user_id'];
                    $finalListArray[$i]['country_id'] = $flList['country_id'];
                    $finalListArray[$i]['region_id'] = $flList['region_id'];
                    $finalListArray[$i]['devision_id'] = $flList['devision_id'];
                    $finalListArray[$i]['state_id'] = $flList['state_id'];
                    $finalListArray[$i]['total_vote'] = $flList['total_vote'];
                    $finalListArray[$i]['rank'] = $flList['rank'];
                    $finalListArray[$i]['user_full_name'] = $flList['user_full_name'];
                    $finalListArray[$i]['user_name'] = $flList['user_name'];
                    $finalListArray[$i]['user_info'] = $flList['userInfo'];

                    if (in_array($flList['user_id'], $followinglist)) {
                        $finalListArray[$i]['is_following'] = true;
                    } else {
                        $finalListArray[$i]['is_following'] = false;
                    }
                    $i = $i + 1;
                }
            }

            $all_response_data['state_data'][$state_key]['winner_data'] = $finalListArray;
            //$all_response_data['state_data'][$state_key]['meta_data'] = ['location_data' => $state, 'compitition_text' => $compititionspan, 'compitition_month_year_text' => $compititionspan_month_year];
            
        }
        //code for state data --end

        if (count($all_response_data)) {
            $response = Api::make_response(1, $all_response_data, 'Top Wall Of Fame Found');
        } else {
            $response = Api::make_response(0, [], 'No Data Found', []);
        }
        return $response;
    }
    //--------------

    public function getFinalList($getLeaderboardList, $top)
    {

        $i = 1;
        $total_vote = 0;
        $rank = 1;
        $finalList = [];
        foreach ($getLeaderboardList as $key => $list) {
            if ($rank == $top && ($total_vote != $list->total_vote)) {
                break;
            }
            if ($total_vote == $list->total_vote) {
                // $top++;
                $i--;
                $list->rank = $i;
                $rank = $i;
                $i++;
            } else {
                $list->rank = $i;
                $rank = $i;
                $i++;
            }

            $total_vote = $list->total_vote;
            $finalList[] = $list;
        }
        return $finalList;
    }

    private function checkMonth($given_month, $given_year)
    {

        $date1 = new \DateTime("now");
        // $date1 = new \DateTime('01-05-2018');
        $ongoing_month = intval($date1->format('m'));
        $ongoing_year = intval($date1->format('Y'));
        $given_month = intval($given_month);
        $given_year = intval($given_year);

        if ($given_year == $ongoing_year) {

            $given_month = $given_month + 3;
            if ($ongoing_month > $given_month) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
