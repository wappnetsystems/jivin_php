<?php

namespace App\Http\Controllers\Mobileapi;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Stripe_customer;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\DB;
use App\Api;
use App\User;
use App\TempUser;
use App\Lib\Upload_file;
use App\InterestCategory;
use App\Interests;
use App\AccountSettings;
use App\SubInterest;
use App\Email;
use App\Subscription;
use App\Plans;
use App\UserCard;
use App\ReportPost;
use App\BoostPlans;
use App\UserSubcriptionHistory;
use App\UserBoost;
use App\Posts;
use App\PostLogs;
use App\AdBid;
use App\LeaderboardLog;
use App\Events;
use App\SiteSetting;
use App\UserImage;
use App\User_category_interest;
use App\Jivin_video;
use App\Country;
use App\Region;
use App\Devision;
use App\State;
use Illuminate\Pagination\LengthAwarePaginator;
use App\UserAccountSetting;
use App\ResetPassword;
use App\CustomeKey;
use App\Followers;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Chrisbjr\ApiGuard\Models\ApiKey;
use App\PostLikeLog;
use App\PostShareLog;
use App\UserCryptoAddress;
use App\Crypto_prices;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller {

    private $limit = 20;

    /* public function __construct(Request $request) 
      {
      $auth_header=$request->header('Authorization');
      $user_id=$request->input('user_id');

      $login_details= Login_log::where(['user_id'=>$user_id,'auth_token'=>$auth_header])->get();

      if($login_details->count()==0){

      echo json_encode(['status' => false, 'msg' => 'Access Denied.', 'data' => []]);
      die();
      }

      } */

    // login api
    public function mobile_authenticate(Request $request) {

        $data = $request->all();
        $validator = Validator::make($request->all(), [
                    //'logintype'=>'required',
                    'username' => 'required',
                    //'password' => 'required',
                    'device_type' => 'required',
                    'fcm_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        //$data['social'] = "";
        if ($data) {
            if (isset($data['logintype']) && $data['logintype'] != "") {
                if ($data['logintype'] == 'instagram') {
                    $user = User::where('username', '=', $data['username'])->first();
                } else {
                    $user = User::where('email', '=', $data['username'])->orWhere('username', '=', $data['username'])->first();
                }
                if ($user) {

                    if ($user->is_active == 0) {
                        return Api::make_response(0, [], 'Inactive account! Please contact your administrator.');
                    }

                    if ($data['logintype'] == $user->registrationtype) {
                        $user->is_login = 1;
                        $user->save();
                        ApiKey::make($user->id);
                        $subscription = array();
                        $subdetils = Subscription::where('user_id', $user->id)->first();
                        if (!empty($subdetils)) {
                            //$subscription['subdetails'] = $subdetils;
                            $subscription['Plansdetails']['plan'] = Plans::find($subdetils->package_id);
                            $subscription['remaining'] = (strtotime($subdetils->package_valide_to) - strtotime(date('Y-m-d'))) / (60 * 60 * 24);
                        }
                        $userApiKey = ApiKey::where('user_id', '=', $user->id)->first();
                        $user_account_settings = AccountSettings::where('user_id', '=', $user->id)->first();
                        if (!$user_account_settings) {
                            $user_account_settings = new AccountSettings;
                            $user_account_settings->user_id = $user->id;
                            $user_account_settings->save();
                        }
                        $user = User::with('accountSettings', 'userAddressSettings', 'designSettings', 'events', 'isFollowing', 'follows')->find($user->id);
                        $suggestedFollowers = User::getfollowerSuggetions($user->id, 5);
                        $suggestedFollowersIds = User::getfollowerSuggetionsIds($user->id, 5);
                        $UserCardDetails = UserCard::where('user_id', $user->id)->get();

                        //make login log of device
                        $token = md5(time() . '-' . rand(1000, 9999));
                        $login_log_arr = [
                            'device_id' => $request->input('fcm_id'),
                            'device_type' => $request->input('device_type'),
                            'user_id' => $user->id,
                            'auth_token' => $token,
                            'created_at' => date('Y-m-d H:i:s'),
                            'created_ip' => $request->ip()
                        ];
                        \App\Login_log::insert($login_log_arr);

                        $response = Api::make_response(1, ['auth_token' => $token, 'card_list' => $UserCardDetails, 'api_key' => $userApiKey->key, 'user_info' => $user, 'suggested_followers_ids' => $suggestedFollowersIds, 'suggested_followers' => $suggestedFollowers, 'subscription' => $subscription ? $subscription : null], 'Login Successfull');
                    } else {
                        $response = Api::make_response(0, [], 'Invalid social type. Please Check');
                    }
                } else {
                    $response = Api::make_response(0, [], 'Invalid user. Please Register');
                }
            } else {
                $user = User::where(function($query) use ($data) {
                            $query->orwhereRaw("TRIM(username) = ?", [trim($data['username'])])
                                    ->orWhere('email', '=', $data['username']);
                        })->first();

                if ($user) {

                    if ($user->is_active == 0) {
                        return Api::make_response(0, [], 'Inactive account! Please contact your administrator.');
                    }
                    if (Hash::check($data['password'], $user->password)) {
                        $user->is_login = 1;
                        $user->save();
                        ApiKey::make($user->id);
                        $subscription = array();
                        $subdetils = Subscription::where('user_id', $user->id)->first();
                        if (!empty($subdetils)) {
                            //$subscription['subdetails'] = $subdetils;
                            $subscription['Plansdetails']['plan'] = Plans::find($subdetils->package_id);
                            $subscription['remaining'] = (strtotime($subdetils->package_valide_to) - strtotime(date('Y-m-d'))) / (60 * 60 * 24);
                        }
                        $userApiKey = ApiKey::where('user_id', '=', $user->id)->first();
                        $user_account_settings = AccountSettings::where('user_id', '=', $user->id)->first();
                        if (!$user_account_settings) {
                            $user_account_settings = new AccountSettings;
                            $user_account_settings->user_id = $user->id;
                            $user_account_settings->save();
                        }
                        $user = User::with('accountSettings', 'userAddressSettings', 'designSettings', 'events', 'isFollowing', 'follows')->find($user->id);
                        $suggestedFollowers = User::getfollowerSuggetions($user->id, 5);
                        $suggestedFollowersIds = User::getfollowerSuggetionsIds($user->id, 5);
                        $UserCardDetails = UserCard::where('user_id', $user->id)->get();

                        //make login log of device
                        $token = md5(time() . '-' . rand(1000, 9999));
                        $login_log_arr = [
                            'device_id' => $request->input('fcm_id'),
                            'device_type' => $request->input('device_type'),
                            'user_id' => $user->id,
                            'auth_token' => $token,
                            'created_at' => date('Y-m-d H:i:s'),
                            'created_ip' => $request->ip()
                        ];
                        \App\Login_log::insert($login_log_arr);

                        $response = Api::make_response(1, ['auth_token' => $token, 'card_list' => $UserCardDetails, 'api_key' => $userApiKey->key, 'user_info' => $user, 'suggested_followers_ids' => $suggestedFollowersIds, 'suggested_followers' => $suggestedFollowers, 'subscription' => $subscription ? $subscription : null], 'Login Successfull');
                    } else {
                        $response = Api::make_response(0, [], 'Opps! Your credentials does not match.');
                    }
                } else {
                    $response = Api::make_response(0, [], 'Opps! Your credentials does not match.');
                }
            }
        } else {
            $response = Api::make_response(0, [], 'Opps! looks like a non categorized request!');
        }
        return $response;
    }

    // register api
    public function mobile_registerUser(Request $request) {

        $data = $data = $request->all();

        $validator = Validator::make($request->all(), [
                    //'secondary_email' => 'required',
                    'country_id' => 'required',
                    'region_id' => 'required',
                    'division_id' => 'required',
                    'state_id' => 'required',
                    'email' => 'required',
                    'username' => 'required',
                    'name' => 'required',
                    'birth_month' => 'required',
                    'birth_year' => 'required',
                    'password' => 'required',
                    'is_following' => 'required',
                    //'user_interests' => 'required',
                    //'social_uid' => 'required',
                    'user_type' => 'required',
                    //'registrationtype' => 'required',
                    //'finish_reg' => 'required',
                    //'plan_id' => 'required',
                    'device_type' => 'required',
                    'fcm_id' => 'required',
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }

        $response = [];

        if (User::where('email', '=', $data['email'])->exists()) {
            // user found
            $response = Api::make_response(0, [], 'User Email Already Exists');
        } elseif (User::where('username', '=', $data['username'])->exists()) {
            // user found
            $response = Api::make_response(0, [], 'Username Already Exists');
        } else {
            //User create
            $user = new User;
            $user->name = $data['name'];
            $user->email = $data['email'];
            if (isset($data['secondary_email'])) {
                $user->secondary_email = $data['secondary_email'];
            }
            $user->username = $data['username'];
            $user->birth_month = $data['birth_month'];
            $user->birth_year = $data['birth_year'];
            $user->password = bcrypt($data['password']);
            $user->registrationtype = $data['registrationtype'];
            $user->user_type = $data['user_type'];
            $user->is_login = 1;
            if ($user->save()) {


                if ($request->hasFile('cover_image')) {
                    // add cover image for user
                    /* $filename = $user->id . '-' . substr(md5($user->id . '-' . time()), 0, 15) . '.' . $request->file('cover_image')->getClientOriginalExtension();
                      $image = $request->file('cover_image');
                      $path = public_path('image_file/');
                      $img = \Image::make($image->getRealPath());
                      $img->orientate();
                      if ($img->save($path . '/' . $filename)) {
                      //User::where('id', $user->id)->update(['background_image' => $filename]);
                      $background_image = $filename;
                      } */
                    $upload_result = \App\Lib\Upload_file::upload_user_image_file($request,'cover_image');
                    if ($upload_result['status']) {
                        $background_image = $upload_result['storage_path'];
                    } else {
                        $background_image = "";
                    }
                } else {
                    $background_image = "";
                }

                if (isset($data['registrationtype']) && isset($data['profile_image'])) {
                    if ($data['registrationtype'] == 'facebook' || $data['registrationtype'] == 'twitter' || $data['registrationtype'] == 'linkedin') {
                        User::where('id', $user->id)->update(['user_image' => $request->input('profile_image')]);
                    }
                }

                //add profile image for user
                if ($request->hasFile('profile_image')) {
                    /* $filename = $user->id . '-' . substr(md5($user->id . '-' . time()), 0, 15) . '.' . $request->file('profile_image')->getClientOriginalExtension();
                      $image = $request->file('profile_image');
                      $path = public_path('image_file/');
                      $img = \Image::make($image->getRealPath());
                      $img->orientate();
                      if ($img->save($path . '/' . $filename)) {
                      User::where('id', $user->id)->update(['user_image' => $filename]);
                      } */
                    $upload_result = \App\Lib\Upload_file::upload_user_image_file($request,'profile_image');
                    if ($upload_result['status']) {
                        User::where('id', $user->id)->update(['user_image' => $upload_result['storage_path']]);
                        $userimage = new UserImage;
                        $userimage->user_id = $user->id;
                        $userimage->image_type = "Profile";
                        $userimage->user_image = $upload_result['storage_path'];
                        $userimage->save();
                    }
                }

                $interests = explode(',', $data['interests']);
                $followers = explode(',', $data['is_following']);
                //dd($interests);
                foreach ($interests as $key => $interest_id) {
                    $newInterest = new Interests;
                    $newInterest->user_id = $user->id;
                    $newInterest->interest_id = $interest_id;

                    if ($interest_id) {
                        $newInterest->save();
                    }

                    //insert in our new user_category_interest table, we will not use interest table
                    $user_category_interest_arr = [
                        'interest_cat_id' => $interest_id,
                        'user_id' => $user->id,
                        'created_ip' => $request->ip()
                    ];
                    $user_category_interest_check = User_category_interest::where($user_category_interest_arr)->get();
                    if ($user_category_interest_check->count() > 0) {
                        continue;
                    }
                    User_category_interest::insert($user_category_interest_arr);
                }
                foreach ($followers as $key => $follow_id) {
                    $newFollow = new Followers;
                    $newFollow->user_id = $user->id;
                    $newFollow->is_following = $follow_id;
                    if ($follow_id) {
                        $newFollow->save();
                        $user->increment('total_follows');
                    }


                    $follow_user = User::find($follow_id);
                    if ($follow_user) {
                        $follow_user->increment('total_followed_by');
                    }
                }

                $user_account_settings = new AccountSettings;
                $user_account_settings->user_id = $user->id;
                $user_account_settings->country_id = $data['country_id'];
                $user_account_settings->region_id = $data['region_id'];
                $user_account_settings->division_id = $data['division_id'];
                $user_account_settings->state_id = $data['state_id'];
                $user_account_settings->background_image = $background_image;

                if (isset($data['registrationtype'])) {
                    switch ($data['registrationtype']) {
                        case 'facebook':
                            $user_account_settings->social_link_facebook = "http://facebook.com/" . $data['social_uid'];
                            break;
                        case 'twitter':
                            $user_account_settings->social_link_twitter = "http://twitter.com/" . $data['social_uid'];
                            break;
                        case 'linkedin':
                            $user_account_settings->social_link_linkedin = $data['social_uid'];
                            break;
                        default:
                            break;
                    }
                }
                $user_account_settings->save();

                $apiKey = ApiKey::make($user->id);
                $user = User::find($user->id);
                $suggestedFollowers = User::getfollowerSuggetions($user->id, 5);
                $suggestedFollowersIds = User::getfollowerSuggetionsIds($user->id, 5);
                //Email::sendMail($user->id);

                if (isset($data['plan_id'])) {
                    $plan_dtails = Plans::find($data['plan_id']);
                    $user_id = $user->id;
                    $expired_date = date('Y-m-d', strtotime("+" . $plan_dtails['duration'] . " days"));

                    $addsubscription = new Subscription;
                    $addsubscription->user_id = $user->id;
                    $addsubscription->package_id = $data['plan_id'];
                    $addsubscription->package_valide_from = date('Y-m-d');
                    $addsubscription->package_valide_to = $expired_date;
                    $addsubscription->is_active = 1;
                } else {
                    $plan_dtails = Plans::where('type', 'free')->first();

                    $expired_date = date('Y-m-d', strtotime("+" . $plan_dtails['duration'] . " days"));

                    $addsubscription = new Subscription;
                    $addsubscription->user_id = $user->id;
                    $addsubscription->package_id = $plan_dtails['id']; //$data['plan_id'];
                    $addsubscription->package_valide_from = date('Y-m-d');
                    $addsubscription->package_valide_to = $expired_date;
                    $addsubscription->is_active = 1;
                }
                $addsubscription->save();
                $subscription['Plansdetails']['plan'] = $plan_dtails;

                //make login log of device
                $token = md5(time() . '-' . rand(1000, 9999));
                $login_log_arr = [
                    'device_id' => $request->input('fcm_id'),
                    'device_type' => $request->input('device_type'),
                    'user_id' => $user->id,
                    'auth_token' => $token,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_ip' => $request->ip()
                ];
                \App\Login_log::insert($login_log_arr);

                $response = Api::make_response(1, ['auth_token' => $token, 'is_temporary' => 0, 'api_key' => $apiKey->key, 'user_info' => $user, 'suggested_followers_ids' => $suggestedFollowersIds, 'suggested_followers' => $suggestedFollowers, 'subscription' => $subscription], 'User Data saved');
            } else {
                $response = Api::make_response(0, [], 'Data not saved');
            }
        }
        return $response;
    }

    // list of interest_category
    public function mobile_interest_category(Request $request) {
        $interestCategories = InterestCategory::where('is_active', 1)->get();
        $response = [];
        if ($interestCategories) {
            $response = Api::make_response(1, $interestCategories->toArray(), 'All Active Interest Category Found');
        } else {
            $response = Api::make_response(0, [], 'No Interest Category Found');
        }
        return Api::replace_null_with_empty_string($response);
    }

    /* To get single interest category by id     
     */

    public function mobile_interest_category_by_id(Request $request) {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
                    'id' => 'required',
        ]);

        $id = $data['id'];

        $interestCategory = InterestCategory::find($id);
        $response = [];
        if ($interestCategory) {
            $response = Api::make_response(1, $interestCategory->toArray(), 'Interest Category Found');
        } else {
            $response = Api::make_response(0, [], 'Interest Category Not Found');
        }
        return Api::replace_null_with_empty_string($response);
    }

    public function mobile_country_list(Request $request) {  //09/06/2020
        $data = $request->all();
        $response = [];
        if ($countries = Country::get()) {
            $response = Api::make_response(1, $countries, count($countries) . ' Countries Found');
        } else {
            $response = Api::make_response(0, [], 'No Countries Found');
        }
        return $response;
    }

    public function mobile_all_country_list(Request $request) {  //09/06/2020
        $data = $request->all();
        $response = [];
        if ($countries = Country::get()) {
            $response = Api::make_response(1, $countries, count($countries) . ' Countries Found');
        } else {
            $response = Api::make_response(0, [], 'No Countries Found');
        }
        return $response;
    }

    public function mobile_language_list(Request $request) {   //05/06/2020

        $request_data = $request->all();
        $language_list = DB::table('languages')->get(['id','name','code']);

        return Api::make_response(1, $language_list, 'USA language List.');
    }

    public function mobile_country_regions(Request $request) {  //nishit

        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
        }

        $data = $request->all();
        $validator = Validator::make($request->all(), [
                    'country_slug' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        if ($data) {
            if ($regions = Country::with('regions')->where('slug', '=', $data['country_slug'])->where('is_active', 1)->first()) {
                $response = Api::make_response(1, $regions, 'Regions Found');
            } else {
                $response = Api::make_response(0, [], 'No region found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param.');
        }
        return $response;
    }

    public function mobile_country_divisions(Request $request) {   //nishit

        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'region_slug' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $data = $request->all();
        $response = [];
        if ($data) {
            if ($divisions = Region::with('divisions')->where('slug', '=', $data['region_slug'])->where('is_active', 1)->first()) {
                $response = Api::make_response(1, $divisions, 'Division Found');
            } else {

                $response = Api::make_response(0, [], 'No Division found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param.');
        }
        return $response;
    }

    public function mobile_country_states(Request $request) {  //nishit
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'division_slug' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        if ($data) {
            if ($states = Devision::with('states')->where('slug', '=', $data['division_slug'])->where('is_active', 1)->first()) {
                $response = Api::make_response(1, $states, 'States Found');
            } else {
                $response = Api::make_response(0, [], 'No State found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param.');
        }
        return $response;
    }

    //================================ 28/05/2020 ===============================

    public function get_country_regions(Request $request) {

        $validator = Validator::make($request->all(), [
                    'country_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }
        $data = $request->all();
        $response = [];
        if ($data) {
            if ($regions = Country::with('regions')->where('id', '=', $data['country_id'])->where('is_active', 1)->first()) {
                $response = Api::make_response(1, $regions, 'Regions Found');
            } else {
                $response = Api::make_response(0, [], 'No region found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param.');
        }
        return $response;
    }

    public function get_country_divisions(Request $request) {

        $validator = Validator::make($request->all(), [
                    'region_id' => 'required',
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        if ($data) {
            if ($divisions = Region::with('divisions')->where('id', '=', $data['region_id'])->where('is_active', 1)->first()) {
                $response = Api::make_response(1, $divisions, 'Division Found');
            } else {

                $response = Api::make_response(0, [], 'No Division found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param.');
        }
        return $response;
    }

    public function get_country_states(Request $request) {
        
        $validator = Validator::make($request->all(), [
                    'division_id' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }
        $data = $request->all();
        $response = [];
        if ($data) {
            if ($states = Devision::with('states')->where('id', '=', $data['division_id'])->where('is_active', 1)->first()) {
                $response = Api::make_response(1, $states, 'States Found');
            } else {
                $response = Api::make_response(0, [], 'No State found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param.');
        }
        return $response;
    }

    //========================================== // ==========================================================

   
    public function mobile_forget_password(Request $request) {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'user_name_or_email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        if ($data['user_name_or_email']) {
            if (filter_var($data['user_name_or_email'], FILTER_VALIDATE_EMAIL)) {
                $user = User::where('email', '=', $data['user_name_or_email'])->first();
            } else {
                $user = User::where('username', '=', $data['user_name_or_email'])->first();
            }
            if ($user) {
                $customeKey = new CustomeKey;
                $resetPassword = new ResetPassword;
                $resetPassword->email = $user->email;
                $resetPassword->token = $customeKey;
                if ($resetPassword->save()) {
                    $user->remember_token = $customeKey;
                    $user->save();
                    //Email::sendForgetPasswordMail($user->id);
                    $response = Api::make_response(1, [], 'Forget Password Email Send');
                } else {
                    $response = Api::make_response(0, [], 'Error Occurred');
                }
            } else {
                $response = Api::make_response(0, [], 'Invalide Email or Username Given');
            }
        } else {
            $response = Api::make_response(0, [], 'Json Request Error');
        }

        return $response;
    }

    public function mobile_authenticate_user_key(Request $request) {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'key' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        if ($data['key']) {
            //dd(Carbon::now()->subHours(24));
            $token = ResetPassword::where('token', '=', $data['key'])
                    ->where('created_at', '>', Carbon::now()->subHours(24))
                    ->first();
            if ($token) {
                $user = User::where('email', '=', $token->email)->first();
                if ($user->remember_token == $token->token) {
                    $response = Api::make_response(1, ['name' => $user->name, 'email' => $user->email], 'Key Is Valide');
                } else {
                    $response = Api::make_response(0, [], 'Key Is Invalide');
                }
            } else {
                $response = Api::make_response(0, [], 'Key Is Invalide');
            }
        } else {
            $response = Api::make_response(0, [], 'Json Request Error');
        }

        return $response;
    }

    public function mobile_change_password(Request $request) {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'key' => 'required',
                    'new_password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        if ($data['key'] && $data['new_password']) {
            $token = ResetPassword::where('token', '=', $data['key'])
                    ->where('created_at', '>', Carbon::now()->subHours(24))
                    ->first();
            if ($token) {
                $user = User::where('email', '=', $token->email)->first();
                if ($user->remember_token == $token->token) {
                    //update user new password here
                    $user->password = bcrypt($data['new_password']);
                    if ($user->save()) {
                        $response = Api::make_response(1, ['name' => $user->name, 'email' => $user->email], 'New Password Updated');
                    } else {
                        $response = Api::make_response(0, [], 'Password not updated');
                    }
                } else {
                    $response = Api::make_response(0, [], 'Key Is Invalide');
                }
            } else {
                $response = Api::make_response(0, [], 'Key Is Invalide');
            }
        } else {
            $response = Api::make_response(0, [], 'Json Request Error');
        }

        return $response;
    }

    public function mobile_following_user_list(Request $request) {   //09/06/2020
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'page_number' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        $offset = ($data['page_number'] - 1) * $this->limit;
        // dd($data['username']);
        if ($data) {
            if (isset($data['search_keyword'])) {
                $search_keyword = $data['search_keyword'];
            } else {
                $search_keyword = "";
            }
            $user = Followers::join('users', 'users.id', '=', 'followers.is_following')
                    ->where('followers.user_id', $data['user_id'])
                    ->where('followers.is_active', 1)
                    ->where(function($q) use($search_keyword) {
                        if ($search_keyword != "") {
                            $q->where('users.name', 'like', $search_keyword . '%');
                        }
                    })
                    ->with('followingUser')
                    ->offset($offset)
                    ->limit($this->limit)
                    ->get(['user_id', 'is_following']);

            if ($user->count() == 0) {

                $response = Api::make_response(0, [], 'No record found');
            } else {

                $response = Api::make_response(1, $user, 'User followed list');
            }

            /* if ($user = User::with('isFollowing')->where('id', '=', $data['user_id'])->first()) {
              // dd(count($user['isFollowing']));

              $response = Api::make_response(1, $user->isFollowing, 'User following list');
              } else {
              $response = Api::make_response(0, [], 'User Not Exists');
              } */
        } else {
            $response = Api::make_response(0, [], 'Invalide Param . Data not saved');
        }
        return $response;
    }

    public function mobile_followed_user_list(Request $request) {    //09/06/2020
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'page_number' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        $offset = ($data['page_number'] - 1) * $this->limit;
        if ($data) {
            if (isset($data['search_keyword'])) {
                $search_keyword = $data['search_keyword'];
            } else {
                $search_keyword = "";
            }

            $user = Followers::join('users', 'users.id', '=', 'followers.user_id')
                    ->where('followers.is_following', $data['user_id'])
                    ->where('followers.is_active', 1)
                    ->where(function($q) use($search_keyword) {
                        if ($search_keyword != "") {
                            $q->where('users.name', 'like', $search_keyword . '%');
                        }
                    })
                    ->with('followsUser')
                    ->offset($offset)
                    ->limit($this->limit)
                    ->get(['user_id', 'is_following']);

            if ($user->count() == 0) {
                $response = Api::make_response(0, [], 'No record found');
            } else {
                foreach ($user as $key => $follow) {
                    //check if user is also following him
                    $check_result = Followers::where('user_id', $data['user_id'])
                                    ->where('is_following', $follow['user_id'])
                                    ->where('is_active', 1)
                                    ->get(['id'])->count();
                    if ($check_result > 0) {
                        $user[$key]->user_also_follow = 1;
                    } else {
                        $user[$key]->user_also_follow = 0;
                    }
                }
                $response = Api::make_response(1, $user, 'User followed list');
            }

            /* if ($user = User::with(['follows'])->where('id', '=', $data['user_id'])->first()) {

              foreach ($user->follows as $key => $follow) {
              //check if user is also following him
              $check_result = Followers::where('user_id', $data['user_id'])
              ->where('is_following', $follow['user_id'])
              ->where('is_active', 1)
              ->get(['id'])->count();
              if ($check_result > 0) {
              $user->follows[$key]->user_also_follow = 1;
              } else {
              $user->follows[$key]->user_also_follow = 0;
              }
              }

              $response = Api::make_response(1, $user->follows, 'User followed list');
              } else {
              $response = Api::make_response(0, [], 'User Not Exists');
              } */
        } else {
            $response = Api::make_response(0, [], 'Invalide Param . Data not saved');
        }
        return $response;
    }

    public function mobile_following_user_details(Request $request) {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        if ($data) {
            if (isset($data['user_id'])) {
                $user = User::where('id', '=', $data['user_id'])->first();
                $response = Api::make_response(1, $user, 'User following list');
            } else {
                $response = Api::make_response(0, [], 'User Not Exists');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param . Data not saved');
        }
        return $response;
    }

    public function mobile_logout(Request $request) {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'userid' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        if ($data) {
            if ($user = User::where('id', '=', $data['userid'])->first()) {
                $user->is_login = 0;
                $user->save();
                $userApiKey = ApiKey::where('user_id', '=', $user->id)->first();
                if (!empty($userApiKey)) {
                    $userApiKey->delete();
                }
                $user = User::where('id', '=', $data['userid'])->first();
                $response = Api::make_response(1, [], 'Logout Successfully');
            } else {
                $response = Api::make_response(0, [], 'User Not Exists');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalid Param . Data not saved');
        }
        return $response;
    }

    public function mobile_add_user_profile_image(Request $request) {
        //dd($_POST);
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'webcamimg' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $is_temporary = 0;

        $filename = $request->user_id . '-' . substr(md5($request->user_id . '-' . time()), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();
        $image = $request->file('image');
        $path = public_path('images/users/');
        $img = \Image::make($image->getRealPath());
        $img->orientate();

        if ($img->save($path . '/' . $filename)) {

            $user = User::where('id', '=', $request->user_id)->first();

            if ($user) {
                $user->user_image = $filename;
                $user->save();

                /* if (!intval($request->is_temporary)) {

                  $userimage = new UserImage;
                  $userimage->user_id = $request->user_id;
                  $userimage->image_type = "Profile";
                  $userimage->user_image = $filename;
                  $userimage->save();
                  } */

                $user = User::where('id', '=', $request->user_id)->first();

                $response = Api::make_response(1, ["is_temporary" => $is_temporary, "userDetails" => $user], 'Image Uploaded Successfully');
            } else {
                $response = Api::make_response(0, [], 'User Not Exists');
            }
        } else {
            $response = Api::make_response(0, [], 'Image Upload Faild. Please Try Again');
        }

        return $response;
    }

    public function mobile_map_location() {
        $response = [];

        $allCountry = Country::where('is_active', '=', 1)->get();
        foreach ($allCountry as $key => $country) {
            $getRegion = Region::where('is_active', '=', 1)->where('country_id', '=', $country->id)->get();
            $response[$country->slug] = ['id' => $country->id];
            foreach ($getRegion as $key => $region) {
                $response[$country->slug]['regions'][$region->slug] = ['id' => $region->id];
                //$response[$country->slug]['regions'][$region->slug] = ['poly' => $region->poly];
                $getDivision = Devision::where('is_active', '=', 1)->where('region_id', '=', $region->id)->get();
                foreach ($getDivision as $key => $division) {
                    $response[$country->slug]['regions'][$region->slug]['divisions'][$division->slug] = ['id' => $division->id];
                    $getStates = State::where('is_active', '=', 1)->where('division_id', '=', $division->id)->get();
                    foreach ($getStates as $key => $state) {
                        $response[$country->slug]['regions'][$region->slug]['divisions'][$division->slug]['states'][$state->name] = ['id' => $state->id, 'code' => $state->slug, 'poly' => $state->poly];
                    }
                }
            }
        }
        return response()->json($response);
    }

    public function mobile_user_profile_details(Request $request) {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
                    'current_user_id' => 'required',
                    'username' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }


        $response = [];
        if (isset($data['username']) && isset($data['current_user_id'])) {
            $current_user_id = $data['current_user_id'];
            if ($user = User::where('username', '=', urldecode($data['username']))->where('is_active', 1)->first()) {
                $user = User::with('accountSettings', 'designSettings', 'events', 'isFollowing', 'follows')->find($user->id);

                $following = Followers::where('is_following', $user->id)->where('user_id', $current_user_id)->first();

                $response = Api::make_response(1, ['is_following' => $following ? true : false, 'user_info' => $user], 'Profile Found');
            } else {
                $response = Api::make_response(0, [], 'Invalide Username');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param . Data not saved');
        }
        return $response;
    }

    public function mobile_get_address(Request $request) {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $user_setting_detail = UserAccountSetting::leftJoin('state', 'state.id', '=', 'user_account_settings.state_id')
                ->leftJoin('country', 'country.id', '=', 'user_account_settings.country_id')
                ->where('user_account_settings.user_id', $data['user_id'])
                ->get(['user_account_settings.id', 'user_account_settings.city', 'user_account_settings.street_name',
            'user_account_settings.street_number', 'user_account_settings.state_id', 'user_account_settings.address_verified',
            'user_account_settings.country_id', 'user_account_settings.zip_code', 'user_account_settings.address_proof',
            'state.name as state_name', 'country.name as country_name']);

        if ($user_setting_detail->count() == 0) {
            return Api::make_response(0, [], 'Error Occurred. Try Again!');
        }

        return Api::make_response(1, $user_setting_detail, 'Address Setting Found.');
    }

    public function mobile_get_user_image(Request $request) {

        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'image_type' => 'required',
                    'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        if ($data['user_id'] != '' && $data['image_type'] != '') {
            $userimagelist = UserImage::where("user_id", $data['user_id'])
                    ->where("image_type", $data['image_type'])
                    ->orderBy('id', 'desc')
                    //->skip(1)
                    //->take(999)
                    ->take(4)
                    ->get();

            if (count($userimagelist) > 0) {
                $response = Api::make_response(1, $userimagelist, 'All Image');
            } else {
                $response = Api::make_response(0, [], 'Image not found');
            }
        } else {
            $response = Api::make_response(0, [], 'Image not found');
        }
        return $response;
    }

    public function mobile_update_user_account_settings(Request $request) {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        $lastLang = '';
        $pageload = 0;
        if ($data) {
            if ($user = User::where('id', '=', $data['user_id'])->first()) {
                $userAccountSettings = AccountSettings::where('user_id', '=', $data['user_id'])->first();
                $lastLang = $userAccountSettings->language;
                if (!$userAccountSettings) {
                    $userAccountSettings = New AccountSettings;
                    $userAccountSettings->user_id = $data['user_id'];
                }
                $userAccountSettings->about = $data['about'];
                if ($data['country_id']) {
                    $userAccountSettings->country_id = $data['country_id'];
                }
                //$userAccountSettings->region_id = $data['region_id'];
                //$userAccountSettings->division_id = $data['division_id'];
                //$userAccountSettings->state_id = $data['state_id'];
                $userAccountSettings->language = $data['language'];
                $userAccountSettings->time_zone = $data['time_zone'];
                $userAccountSettings->social_link_facebook = $data['social_link_facebook'];
                $userAccountSettings->social_link_twitter = $data['social_link_twitter'];
                $userAccountSettings->social_link_linkedin = $data['social_link_linkedin'];
                $userAccountSettings->social_link_youtube = $data['social_link_youtube'];
                $userAccountSettings->social_link_pinterest = $data['social_link_pinterest'];
                $userAccountSettings->social_link_instagram = $data['social_link_instagram'];
                //$userAccountSettings->show_sensitive_media = $data['show_sensitive_media'];
                //$userAccountSettings->my_media_is_sensitive = $data['my_media_is_sensitive'];
                //$userAccountSettings->show_tile_background = $data['show_tile_background'];
                //$userAccountSettings->background_position = $data['background_position'];
                //$userAccountSettings->background_color_code = $data['background_color_code'];

                if ($userAccountSettings->save()) {
                    $user = User::with('accountSettings')->where('id', '=', $data['user_id'])->first()->toArray();

                    $response = Api::make_response(1, $user, 'Account Settings Updated Successfully');
                } else {
                    $response = Api::make_response(0, [], 'Account Settings Not Updated');
                }
            } else {
                $response = Api::make_response(0, [], 'User Not Exists');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param . Data not saved');
        }
        return $response;
    }

    public function update_user_design_settings(Request $request) {

        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'dark_mode' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response_data = [];

        if ($user = User::where('id', '=', $request->user_id)->first()) {
            $userAccountSettings = AccountSettings::where('user_id', '=', $request->user_id)->first();
            if (!$userAccountSettings) {
                $userAccountSettings = New AccountSettings;
                $userAccountSettings->user_id = $request->user_id;
            }
            //$userAccountSettings->show_tile_background = (int) $request->show_tile_background;
            //$userAccountSettings->background_position = $request->background_position;
            //$userAccountSettings->background_color_code = $request->background_color_code;
            if ($request->hasFile('profile_image_file')) {
                $profile_upload_result = \App\Lib\Upload_file::upload_user_image_file($request, 'profile_image_file');
                if ($profile_upload_result['status']) {
                    $user_image_arr = [
                        'user_id' => $request->input('user_id'),
                        'image_type' => 'Profile',
                        'user_image' => $profile_upload_result['storage_path'],
                        'is_active' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];
                    UserImage::insert($user_image_arr);
                    $user->user_image = $profile_upload_result['storage_path'];
                    $user->save();
                }
            }
            $user = User::where('id', '=', $request->user_id)->first();
           
            $response_data['user_image_url'] = $user->user_image_url;

            if ($request->hasFile('cover_image_file')) {
                $profile_upload_result = \App\Lib\Upload_file::upload_user_image_file($request, 'cover_image_file');
                if ($profile_upload_result['status']) {
                    $user_image_arr = [
                        'user_id' => $request->input('user_id'),
                        'image_type' => 'Cover',
                        'user_image' => $profile_upload_result['storage_path'],
                        'is_active' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];
                    UserImage::insert($user_image_arr);
                    $userAccountSettings->background_image = $profile_upload_result['storage_path'];
                }
            }

            $userAccountSettings->dark_mode = $request->input('dark_mode');
            $response_data['dark_mode'] = $request->input('dark_mode');
            $userAccountSettings->save();
            $userAccountSettings = AccountSettings::where('user_id', '=', $request->user_id)->first();
            $response_data['background_image_url'] = $userAccountSettings->background_image_url;
            $response = Api::make_response(1, $response_data, "Record successfully updated.");
        } else {
            $response = Api::make_response(0, [], 'User Not Exists');
        }
        return $response;
    }

    public function mobile_update_address(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'city' => 'required',
                    'country_id' => 'required',
                    'state_id' => 'required',
                    'street_name' => 'required',
                    'street_number' => 'required',
                    'zip_code' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $user_id = $request->input('user_id');
        $city = $request->input('city');
        $country_id = $request->input('country_id');
        $state_id = $request->input('state_id');

        $setting_arr = [
            'city' => $city,
            'country_id' => $country_id,
            'state_id' => $state_id,
            'street_name' => $request->input('street_name'),
            'zip_code' => $request->input('zip_code'),
            'street_number' => $request->input('street_number')
        ];

        if ($request->hasFile('address_proff')) {
            $upload_result = \App\Lib\Upload_file::upload_address_proff_image_file($request);

            if ($upload_result['status']) {
                $setting_arr['address_proof'] = $upload_result['storage_path'];
            }
        }

        if (UserAccountSetting::where('user_id', $user_id)->update($setting_arr)) {
            $state_detail = \App\State::where('id', $state_id)->get();
            $setting_arr['state_name'] = $state_detail[0]->name;
            $setting_arr['country_details'] = Country::where('id', $country_id)->get();
            $setting_arr['state_details'] = State::where('id', $state_id)->get();

            $user_setting = UserAccountSetting::where('user_id', $user_id)->get();
            $setting_arr['address_proof_url'] = $user_setting[0]->address_proof_url;
            return Api::make_response(1, $setting_arr, "Address updated successfully.");
        } else {
            return Api::make_response(0, [], "Error occurred during update. Try again!");
        }
    }

    public function mobile_verify_shuftipro(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'id_image' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $user_id = $request->input('user_id');
        $id_image = $request->input('id_image');

        $account_setting = UserAccountSetting::where('user_account_settings.user_id', $user_id)->join('state', 'state.id', '=', 'user_account_settings.state_id')->get(['user_account_settings.id', 'user_account_settings.full_address', 'user_account_settings.address_verified', 'state.name']);

        if ($account_setting->count() == 0) {
            return Api::make_response(0, [], 'Error Occurred. Try Again!');
        }

        $user_data = User::where('id', $user_id)->get(['id', 'email']);

        $full_address = $account_setting[0]->full_address;
        $state_name = $account_setting[0]->name;

        $reference = time() . rand(1000, 9999);
        $shufti_pro_arr = [
            'reference' => $reference,
            'callback_url' => route('get_shufti_pro_response'),
            'email' => $user_data[0]->email,
            'country' => 'US',
            'language' => 'EN',
            'verification_mode' => 'any',
            'address' => ['proof' => $id_image, 'supported_types' => ['id_card', 'passport', 'driving_license', 'utility_bill',
                    'bank_statement', 'rent_agreement', 'employer_letter', 'insurance_agreement', 'tax_bill'],
                'full_address' => $full_address . ', ' . $state_name]
        ];
        $client_id = "dLYNl2OCsMThJuGA6VZ1Y4jfgf8iDX9ZEugJRd0fYZAdksQvjz1554122403";
        $client_secret = '$2y$10$ipse4f/MOsCTwDtejMK.luolVc8HxPdqWYgU0X9fS2XrbumOUbR76';
        $auth_token = base64_encode($client_id . ':' . $client_secret);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://shuftipro.com/api/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($shufti_pro_arr),
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic $auth_token",
                "Content-Type: application/json",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return Api::make_response(0, [], "Error Occurred. Try Again!");
        } else {
            $result = json_decode($response, true);
            if ($result['event'] == 'verification.declined') {
                return Api::make_response(0, [], $result['declined_reason']);
            } elseif ($result['event'] == 'request.pending' || $result['event'] == 'request.received') {
                $address_arr = [
                    'ref_id' => $reference,
                    'shuftipro_status' => $result['event']
                ];
                UserAccountSetting::where('user_id', $user_id)->update($address_arr);
                return Api::make_response(0, [], 'Your request is pending. Your verification will be complete soon.');
            } elseif ($result['event'] == 'verification.accepted') {
                $address_arr = [
                    'ref_id' => $reference,
                    'shuftipro_status' => $result['event'],
                    'address_verified' => 1,
                ];
                UserAccountSetting::where('user_id', $user_id)->update($address_arr);
                return Api::make_response(1, [], "Address verified successfully.");
            } else {
                return Api::make_response(0, [], 'Error Occurred. Try Again!');
            }
        }
    }

    public function mobile_saveboostsetting(Request $request) {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $response = [];
        $uaerdtails = array();

        if ($data['user_id'] != '') {
            $uaerdtails = AccountSettings::where('user_id', $data['user_id'])->first();

            $AccountSettingsUpdate = AccountSettings::find($uaerdtails->id);

            if ($data['selectcountry'] != '') {
                $AccountSettingsUpdate->boosted_country = $data['selectcountry'];
            } else {
                $AccountSettingsUpdate->boosted_country = 0;
            }
            if ($data['selectregion'] != '') {
                $AccountSettingsUpdate->boosted_region = $data['selectregion'];
            } else {
                $AccountSettingsUpdate->boosted_region = 0;
            }
            if ($data['selectdivision'] != '') {
                $AccountSettingsUpdate->boosted_division = $data['selectdivision'];
            } else {
                $AccountSettingsUpdate->boosted_division = 0;
            }
            if ($data['selectstate'] != '') {
                $AccountSettingsUpdate->boosted_state = $data['selectstate'];
            } else {

                $AccountSettingsUpdate->boosted_state = 0;
            }

            if ($AccountSettingsUpdate->save()) {
                $response = Api::make_response(1, $AccountSettingsUpdate, 'Boost setting updated successfully');
            } else {
                $response = Api::make_response(0, [], 'Something went wrong');
            }
        } else {
            $response = Api::make_response(0, [], 'Something went wrong');
        }

        return $response;
    }

    public function mobile_profile_posts(Request $request) {    // vishal
        $data = $request->all();
        $validator = Validator::make($request->all(), [
                    'current_user_id' => 'required',
                    'username' => 'required',
                    'itemsPerPage' => 'required',
                    'page' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }


        $response = [];
        $page = $data['page'];
        $itemsPerPage = $data['itemsPerPage'];
        if (isset($data['username']) && $user = User::where('username', '=', $data['username'])->first()) {

            if ($user->id != $data['current_user_id']) {

                $isuserFollower = Followers::where('is_following', $user->id)->where('user_id', $data['current_user_id'])->where('is_active', 1)->first();

                if ($isuserFollower) {

                    //$getPosts = Posts::where('user_id','=',$user->id)->where('is_active','=',1)->whereIn('is_public', [1, 2])->orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();
                    $getPosts = DB::select("SELECT posts.*,posts.created_at AS abc FROM  posts WHERE is_active = 1 AND is_public IN(1, 2) AND user_id = '" . $user->id . "' ORDER BY  abc DESC");
                } else {

                    //$getPosts = Posts::where('user_id','=',$user->id)->where('is_active','=',1)->where('is_public','=',2)->orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();
                    $getPosts = DB::select("SELECT posts.*,posts.created_at AS abc FROM  posts WHERE is_active = 1 AND is_public = 2 AND user_id = '" . $user->id . "' ORDER BY  abc DESC");
                }
            } else {

                $getPosts = DB::select("(SELECT posts. * , post_boost.created_at AS abc FROM  posts LEFT JOIN  post_boost ON  posts.id =  post_boost.post_id WHERE  post_boost.user_id = '" . $user->id . "' GROUP BY  post_boost.post_id ,  post_boost.created_at )UNION(SELECT posts.*,posts.created_at AS abc FROM  posts WHERE is_active = 1 AND user_id = '" . $user->id . "')ORDER BY  abc DESC");
                //$getPosts = Posts::where('user_id','=',$user->id)->where('is_active','=',1)->orderBy('updated_at','DESC')->orderBy('id','DESC')->paginate($itemsPerPage)->toArray();
            }

            $getPosts = $this->arrayPaginator($getPosts, $request)->toArray();
            $getPosts = json_decode(json_encode($getPosts), true);

            $finalArray = [];
            if (isset($getPosts['data']) && !empty($getPosts['data'])) {
                $i = 0;
                foreach ($getPosts['data'] as $key => $value) {



                    $finalArray[$i] = $value;
                    $userdetails = User::find($value['user_id']);
                    if ($userdetails) {
                        $finalArray[$i]['created_by_img'] = $userdetails['user_image_url'];
                        $finalArray[$i]['created_by_name'] = $userdetails['name'];
                        $finalArray[$i]['created_by_user_id'] = $userdetails['id'];
                        $finalArray[$i]['created_by_user_name'] = $userdetails['username'];
                    }
                    if ($value['is_shared'] == 0) {
                        if ($value['user_id'] == $data['current_user_id']) {
                            if ($value['is_boost'] == 1) {
                                $finalArray[$i]['display_text'] = "Boosted a new post";
                            } else {
                                $finalArray[$i]['display_text'] = "Added a new post";
                            }
                        } else {
                            //$finalArray[$i]['display_text'] = $userdetails['name']." added a new post";
                            $finalArray[$i]['display_text'] = " Added a new post";
                        }
                    } else {
                        $sharedname = User::find($value['shared_id'])->name;
                        $finalArray[$i]['display_text'] = "shared " . $sharedname . "'s post";
                    }

                    $finalArray[$i]['post_id'] = $key . $value['id'];

                    if ($value['video_name'] != '') {

                        $finalArray[$i]['video_url'] =  Upload_file::get_post_file_path('Video', $value['video_name'], 0);
                        $finalArray[$i]['media_type'] = "video";
                        $finalArray[$i]['audio_url'] = "";
                    } else if ($value['audio_name'] != '') {
                        $finalArray[$i]['audio_url'] = Upload_file::get_post_file_path('Audio', $value['audio_name'], 0);
                        $finalArray[$i]['video_url'] = "";
                        $finalArray[$i]['media_type'] = "audio";
                    } else {
                        $finalArray[$i]['media_type'] = "";
                        $finalArray[$i]['video_url'] = "";
                        $finalArray[$i]['audio_url'] = "";
                    }
                //===================
                    if ($value['video_thumb'] != '') {
                        $finalArray[$i]['video_thumb'] = Upload_file::get_video_thumb_path($value['video_thumb']);
                    } 
                //===================

                    if (PostLikeLog::where('post_id', '=', $value['id'])->where('liked_by', '=', $data['current_user_id'])->first()) {
                        $finalArray[$i]['is_liked'] = true;
                    } else {
                        $finalArray[$i]['is_liked'] = false;
                    }
                    if (PostShareLog::where('post_id', '=', $value['id'])->where('shared_by', '=', $data['current_user_id'])->first()) {
                        $finalArray[$i]['is_shared'] = true;
                    } else {
                        $finalArray[$i]['is_shared'] = false;
                    }

                    $finalArray[$i]['time_ago'] = $this->timeAgo($value['abc']);
                    $finalArray[$i]['total_vote'] = $value['total_like'];

                    $i++;
                }
                $getPosts["data"] = $finalArray;
                $response = Api::make_response(1, [$getPosts], 'Posts Found');
            } else {
                $response = Api::make_response(0, [], 'Posts not Found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalid user name');
        }
        return $response;
    }

    public function arrayPaginator($array, $request) {
        $page = Input::get('page', 1);
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
                ['path' => $request->url(), 'query' => $request->query()]);
    }

    public function timeAgo($time_ago) {
        $time_ago = strtotime($time_ago);
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        }
        //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        }
        //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        }
        //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        }
        //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "$months months ago";
            }
        }
        //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }

    //================================ 29/05/2020 nishit =========================================================//
  
    public function get_currency_list(Request $request) {   //this

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',       
                    
        ]);
        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }

        $curreny_list = [
                            ['currency_name'=> 'USD',
                             'currency_code' => 'USD'],
                            ['currency_name'=> 'Bitcoin',
                             'currency_code' => 'BTC'],
                            ['currency_name'=> 'Ethereum',
                             'currency_code' => 'ETH'],
                            ['currency_name'=> 'Litecoin',
                             'currency_code' => 'LTC'],
                            ['currency_name'=> 'XRP',
                             'currency_code' => 'XRP']
                        ];

        return Api::make_response(1, $curreny_list, 'All Currency Data.');
    }

    public function my_wallet_setting(Request $request) {   //this

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'currency_code' => 'required',  
                    'address' => 'required',               
        ]);
        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();

        $insert_crypto = [
        	'user_id' => $request_data['user_id'],
            'coin_code' => $request_data['currency_code'],
            'address' => $request_data['address'],
            'created_at' => date('Y-m-d H:i:s'),
            'created_ip' => $request->ip(),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_ip' => $request->ip()
        	
        ];
 
        UserCryptoAddress::insert($insert_crypto);
    
        return Api::make_response(1, [], 'Wallet Setting successfully!.');
    }

    public function mobile_get_address_list(Request $request) {  //this
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'currency_code' => 'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }
        $request_data = $request->all();

        $user_crypto_data = UserCryptoAddress::where('user_id', $request_data['user_id'])
                        ->where('coin_code',$request_data['currency_code'])
                        ->get();

        if ($user_crypto_data->count() == 0) {
            return Api::make_response(0, [], 'No user wallet address available');
        }

        return Api::make_response(1, $user_crypto_data, 'User crypto wallet addresses found.');
    }
    //=========================================================================================================

    // check email exits or not
    public function check_email_exits(Request $request) {

        $validator = Validator::make($request->all(), [
                    'email' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }

        $email = $request->input('email');

        $user_email_data = User::where('email', $email)->get();

        if ($user_email_data->count() == 0) {
            return Api::make_response(0, [], 'No user email address available');
        }

        return Api::make_response(1, $user_email_data, 'User email addres addresses found.');
    }

    // get follow user suggestion  list
    public function get_user_follower_list(Request $request) {

        $validator = Validator::make($request->all(), [
                    'interests' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Please follow validation rules.', 'data' => []]);
        }
        $request_data = $request->all();
        $interests = $request->input('interests');
        $logged_user_id = 0;

        if ( isset($request_data['user_id']) && $request_data['user_id'] ) {
            $logged_user_id = $request_data['user_id'];
        } 
        
        if ($interests != 'all') {
            $interests = $interests;
            
        } else {
            //$allInterests = SubInterest::where('is_active', '=', 1)->select('id')->get()->toArray();
            $allInterests = InterestCategory::where('is_active', '=', 1)->select('id')->get()->toArray();
            $allInterestsArray = [];
            foreach ($allInterests as $key => $interestId) {
                $allInterestsArray[] = $interestId['id'];
            }
            //dd($allInterestsArray);
            $interests = implode(',', $allInterestsArray);
        }
        
        $suggestedFollowers = User::getfollowerSuggetionsFromTempUser($interests,$logged_user_id, 5);
        return Api::make_response(1, ['is_temporary' => 1, 'api_key' => '', 'suggested_followers' => $suggestedFollowers], 'User Data saved');
    }
    
    public function mobile_follow_user(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'follow_user_id' => 'required'
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();

        //check if already following
        $already_follow = Followers::where('user_id', $request_data['user_id'])
                ->where('is_following', $request_data['follow_user_id'])
                ->where('is_active', 1)
                ->get(['id']);
        if ($already_follow->count() > 0) {
            return Api::make_response(0, [], "You are already following this member.");
        }
        $insert_arr = [
            'user_id' => $request_data['user_id'],
            'is_following' => $request_data['follow_user_id'],
            'is_active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        Followers::insert($insert_arr);
        return Api::make_response(1, [], "Followed successfully.");
    }

    public function mobile_unfollow_user(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'follow_user_id' => 'required'
        ]);

        if ($validator->fails()) {

            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();
        Followers::where('user_id', $request_data['user_id'])
                ->where('is_following', $request_data['follow_user_id'])
                ->delete();

        return Api::make_response(1, [], "Successfully unfollowed");
    }

    public function delete_user(Request $request) {
        User::where('email', $request->input('email'))->delete();
        return Api::make_response(1, [], "user deleted");
    }

    public function get_state_by_country(Request $request) {
        $country_id = $request->input('country_id');

        $state_list = State::whereIn('division_id', function($query) use($country_id) {
                    $query->select(['id'])->from('division')->whereIn('region_id', function($query1) use($country_id) {

                        $query1->select(['id'])->from('region')->where('country_id', $country_id)->pluck('id');
                    })->pluck('id');
                })->get();

        if ($state_list->count() == 0) {
            return Api::make_response(0, [], "No record found");
        }
        return Api::make_response(1, $state_list, "Record Found");
    }

    public function get_division_by_country(Request $request) {
        $country_id = $request->input('country_id');

        $divison_list = Devision::whereIn('region_id', function($query) use($country_id) {
                    $query->select(['id'])->from('region')->where('country_id', $country_id)->pluck('id'); 
                })->get();

        if ($divison_list->count() == 0) {
            return Api::make_response(0, [], "No record found");
        }
        return Api::make_response(1, $divison_list, "Record Found");
    }

    public function deactive_user_account(Request $request) {
        $data = $request->all();
        $userid = $data['user_id'];
        $response = [];
        $usertble = User::find($data['user_id']);
        $usertble->is_active = 0;
        if ($usertble->save()) {
            $postsmstr = Posts::where("user_id", $data['user_id'])->get();
            foreach ($postsmstr as $key => $value) {
                $updateposts = Posts::find($value['id']);
                $updateposts->is_active = 0;
                $updateposts->save();

                $postLogsMstr = PostLogs::where("post_id", $value['id'])->get();
                if (count($postLogsMstr) > 0) {
                    foreach ($postLogsMstr as $key1 => $value1) {
                        $updatepostLog = PostLogs::find($value1['id']);
                        $updatepostLog->updated_at = $value1['updated_at'];
                        $updatepostLog->is_active = 0;
                        $updatepostLog->save();
                    }
                }
            }

            $allAdBid = AdBid::where("user_id", $data['user_id'])->get();
            if (count($allAdBid) > 0) {
                foreach ($allAdBid as $key2 => $value2) {
                    $updateAdBid = AdBid::find($value2['id']);
                    $updateAdBid->is_active = 0;
                    $updateAdBid->save();
                }
            }

            $allleaderboardLog = LeaderboardLog::where('user_id', $data['user_id'])->get();
            if (count($allleaderboardLog) > 0) {
                foreach ($allleaderboardLog as $key3 => $value3) {
                    $updateLeaderboardLog = LeaderboardLog::find($value3['id']);
                    $updateLeaderboardLog->is_active = 0;
                    $updateLeaderboardLog->save();
                }
            }

            $allFollowers = Followers::where('user_id', $data['user_id'])->orWhere('is_following', $data['user_id'])->get();
            if (count($allFollowers) > 0) {
                foreach ($allFollowers as $key4 => $value4) {
                    $updateFollowers = Followers::find($value4['id']);
                    $updateFollowers->is_active = 0;

                    if ($updateFollowers->user_id == $data['user_id']) {

                        $userupdate = User::find($updateFollowers->is_following);
                        if ($userupdate) {
                            $userupdate->decrement('total_follows');
                            $userupdate->save();
                        }
                    }

                    $updateFollowers->save();
                }
            }
            $allEvents = Events::where('user_id', $data['user_id'])->get();
            if (count($allEvents) > 0) {
                foreach ($allEvents as $key5 => $value5) {
                    $updateEvents = Followers::find($value5['id']);
                    if ($updateEvents) {
                        $updateEvents->is_active = 0;
                        $updateEvents->save();
                    }
                }
            }

            $response = Api::make_response(1, $usertble, 'You account has been deactived! We are waiting to hear from yu soon.');
        } else {
            $response = Api::make_response(0, [0], 'Oops!!!You are not able to deactive your account.');
        }

        return $response;
    }

    public function get_compete_states(Request $request) {

        $country_id = $request->input('country_id');

        $state_list = State::whereIn('division_id', function($query) use($country_id) {
                    $query->select(['id'])->from('division')->whereIn('region_id', function($query1) use($country_id) {

                        $query1->select(['id'])->from('region')->where('country_id', $country_id)->pluck('id');
                    })->pluck('id');
                })->where('is_active', 1)->where('state_compete', 'Yes')->get();

        if ($state_list->count() == 0) {
            return Api::make_response(0, [], "No record found");
        }
        return Api::make_response(1, $state_list, "Record Found");
    }

    public function get_user_boost_location_details(Request $request) {
        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Please follow validation rules.');
        }
        $request_data = $request->all();

        $user_boost_location_setting_setting = UserAccountSetting::leftJoin('country', 'country.id', '=', 'user_account_settings.country_id')
                ->leftJoin('region', 'region.id', '=', 'user_account_settings.region_id')
                ->leftJoin('division', 'division.id', '=', 'user_account_settings.division_id')
                ->leftJoin('state', 'state.id', '=', 'user_account_settings.state_id')
                ->where('user_id', $request_data['user_id'])
                ->get(['user_account_settings.country_id', 'user_account_settings.region_id',
            'user_account_settings.division_id', 'user_account_settings.state_id', 'country.name as country_name',
            'region.name as region_name', 'division.name as division_name', 'state.name as state_name']);

        return Api::make_response(1, $user_boost_location_setting_setting, 'record found');
    }

}
