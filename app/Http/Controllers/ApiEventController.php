<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Illuminate\Pagination\Paginator;
use App\Http\Requests;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
//load Models
use App\Api;
use App\User;
use App\Events;
use App\Country;
use App\Region;
use App\Devision;
use App\State;
use DB;

class ApiEventController extends ApiGuardController
{
    protected $apiMethods = [
        'addEvent' => [
            'keyAuthentication' => true
        ], 
        'getEventList' => [
            'keyAuthentication' => true
        ],        
        'getEventDateList' => [
            'keyAuthentication' => false
        ],  

        'getDate' =>[
           'keyAuthentication' => false
        ] ,             
    ];

    /*
        Add Event
        Debasis Chakraborty
        20/3/2017
    */
    public function addEvent(Request $request){
        $data = $request->all();

        $response =  [];
        if ($data) {
            //dd($data['start_time']);
            $event = New Events;
            $event->user_id = $data['user_id'];
            $event->title = $data['title'];
            $event->country_id = Api::getLocationId('Country',$data['country_slug']);
            $event->region_id = Api::getLocationId('Region',$data['region_slug']);
            $event->division_id = Api::getLocationId('Devision',$data['division_slug']);
            $event->state_id = Api::getLocationId('State',$data['state_slug']);
            $event->name_of_place = $data['name_of_place'];
            $event->full_address = $data['full_address'];
            $event->start_date = $data['start_date'];
            $event->start_time = $data['start_time'];
            // $event->start_time = date("H:i:s", strtotime($data['start_time']));
            $event->city = $data['city'];
         
            $event->event_description = $data['event_description'];
            if($event->save()){
              $events = Events::with('userInfoWithEvent')->find($event->id);
              $response =  Api::make_response(1,$events,'New Event Created');
            }
            else{
              $response =  Api::make_response(0,[],'Something went wrong .  Data Not Saved');
            }                       
        }else{
            $response =  Api::make_response(0,[],'Invalide Param . Data not saved');
        }
        return $response; 
    }
    /*
        Get Event List with pagination
        Debasis Chakraborty
        20/3/2017
    */
    public function getEventList(Request $request){
      date_default_timezone_set("Asia/Kolkata");
        $data = $request->json()->all();
        $img_url = Country::find(1)->img_url;
        $filetername = "UNITED STATES OF AMERICA'S";
        $response =  [];
        if ($data) {

        $page = $data['page'];
        $itemsPerPage = $data['itemsPerPage'];
       // protected $currentPage;
       $condition =[];
        $condition['is_active'] = 1;
       if(!empty($data['country_slug'])){
        $condition['country_id'] = Api::getLocationId('Country',$data['country_slug']);
        $img_url= Country::find($condition['country_id'])->img_url;
        $filetername = Country::find($condition['country_id'])->name;
        $filetername = $filetername."'s Country";
        //$condition['country_name'] = Api::getLocationName('Country',$data['country_slug']);
       } 
       if(!empty($data['region_slug'])){
        $condition['region_id'] = Api::getLocationId('Region',$data['region_slug']);
        $img_url= Region::find(Api::getLocationId('Region',$data['region_slug']))->img_url;
        $filetername = Region::find(Api::getLocationId('Region',$data['region_slug']))->name;
        $filetername = $filetername." Region's";
        //$condition['region_name'] = Api::getLocationName('Region',$data['region_slug']);
       } 
       if(!empty($data['division_slug'])){
        $condition['division_id'] = Api::getLocationId('Devision',$data['division_slug']);
        $img_url= Devision::find(Api::getLocationId('Devision',$data['division_slug']))->img_url;
        $filetername = Devision::find(Api::getLocationId('Devision',$data['division_slug']))->name;
        $filetername = $filetername." Division's";
        //$condition['division_name'] = Api::getLocationName('Devision',$data['division_slug']);
       } 
       if(!empty($data['state_slug'])){
        $condition['state_id'] = Api::getLocationId('State',$data['state_slug']);
        $img_url= State::find(Api::getLocationId('State',$data['state_slug']))->img_url;
        $filetername = State::find(Api::getLocationId('State',$data['state_slug']))->name;
        $filetername = $filetername."'s";
        //$condition['state_name'] = Api::getLocationName('State',$data['state_slug']);
       } 
        

       if($data['user_id']!=0){
        if (!User::where('id', '=', $data['user_id'])->first()){
           return $response =  Api::make_response(0,[],'User Not Exists');
        }else{
           $condition['user_id'] = $data['user_id'];
        }
       } 
          
         $id=$data['user_id'];
        
         if(isset($data['start_date']) && $data['start_date']){
           $date=date('Y-m-d',$data['start_date']);
           $condition['start_date'] = $date;
         }
           $events = Events::with('userInfo')->where($condition)->get();

           if (count($events)>0){
        $events_info = Events::with('userInfo')->where($condition)->where('is_active', 1)->orderBy('id','DESC')->paginate($itemsPerPage);

        $response =  Api::make_response(1,["events"=>$events_info],'Events Found',['img_url'=>$img_url,'filetername'=>$filetername]);            
           }else{
             $response =  Api::make_response(0,[],'No Event Found',['img_url'=>$img_url,'filetername'=>$filetername]);               
           }
        }
        else
        {
            $response =  Api::make_response(0,[],'Invalide Param . Data not saved');
        }
        return $response;    	
    }

    public function getEventDateList(Request $request){
        $data = $request->json()->all();
        $response =  []; 
        if (isset($data['user_name'])) {
            $user = User::with('isFollowing')->where('username','=',$data['user_name'])->first()->toArray();
            return $user['is_following'];
         } else{
          $response =  Api::make_response(0,[],'Invalide Param . Data not saved');
         }
        return $response;    
    }

    public function getDate()
    {
       // $data = $request->json()->all();
        $response=[];
         // if(isset($data['start_date']))
         // {
         //  
        //$events_date=Events::select('start_date')->groupBy('start_date')->get();//->makeHidden(['time','start_date']);
        $events_date=Events::groupBy('start_date')->pluck('start_date');
        $datefrmt = [];
        foreach ($events_date as $key => $value) {
            $date = date_create($value);
            
            $datefrmt[$key]= date_format($date,"Y-n-j");
            
        }
          $response =Api::make_response(1,["data"=>$datefrmt],'Events date Found');
         // }
         // else
         // {
         //     $response =  Api::make_response(0,[],'Invalide Param . Data not saved');

         // }
       
        return $response;    

    }  

}
