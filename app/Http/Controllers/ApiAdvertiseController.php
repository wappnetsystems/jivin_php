<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\Http\Requests;
use Image;
use File;
use App\Api;
use App\User;
use App\Posts;
use App\Plans;
use App\PostLogs;
use App\PostLikeLog;
use App\PostShareLog;
use App\LeaderboardLog;
use App\NotificationLog;
use App\Subscription;
use App\Email;
use App\AdRegions;
use App\AdPages;
use App\Advertisement;
use App\AdBid;
use App\AdPriceMaster;
use App\Country;
use App\Region;
use App\Devision;
use App\State;
use App\AdBids;
use App\AdCreative;
use \Validator;
use Illuminate\Validation\Rule;
use DB;
use App\AccountSettings;
use App\UserTransaction;
use Carbon\Carbon;
//use Carbon;
class ApiAdvertiseController extends Controller
{
    public function getregions(){
		$response = array();
    	// $regionList = AdRegions::where('status',1)->get();
    	$regionList = AdRegions::with('getalladpages')
    	->where('status',1)
    	->get();

    	if(!empty($regionList)){
    		$response =  Api::make_response(1,$regionList,'Region list');
		}else{
         $response =  Api::make_response(0,[],'Region not found');
	   	}
	   	return $response; 
    }

    public function getpages(Request $request){
		$response = array();
		$data = $request->json()->all();

		if($data['region_id']!=''){
	    	$regionList = AdPages::where('ad_region_id',$data['region_id'])->get();

	    	if(!empty($regionList) && count($regionList)>0){
	    		$response =  Api::make_response(1,$regionList,'Page list');
			}else{
	         $response =  Api::make_response(0,[],'Page not found');
		   	}
		}else{
			$response =  Api::make_response(0,[],'Region not found');
		}

	   	return $response; 
    }

	public function pastcreative(Request $request){

		$response = array();
		$data = $request->json()->all();
		$user_id = $data['user_id'];
		$type = isset($data['type']) ? $data['type'] : null;
		$ad_region_id = isset($data['ad_region_id']) ? $data['ad_region_id'] : null;

		if($type == "videoaudio"){
			$creative = AdCreative::where('user_id',$user_id)->where('ad_region_id',$ad_region_id)->wherein('type',['Video','Audio'])->where('status',1)->get();
		}else{
			$creative = AdCreative::where('user_id',$user_id)->where('ad_region_id',$ad_region_id)->where('type','Image')->where('status',1)->get();
		}
		
		//echo json_encode($creative);die;
		if(count($creative)>0){
			$response =  Api::make_response(1,$creative,'Creative list');
		}else{
			$response =  Api::make_response(0,[],'Page not found');
		}
		return $response; 

	}


	public function postcreative(Request $request){

		$response = array();
		$chkwidth = $chkheight = 0;
		$region_id = $request->ad_region_id;

		if($region_id!=''){
			$adregions = AdRegions::find($region_id);
			$chkwidth = $adregions->width;
			$chkheight = $adregions->height;

		}

	    Validator::extend('checkdimensions', function($attribute, $value, $parameters, $validator) use ($region_id,$request,$chkwidth,$chkheight){
				
				if($request->filetype !="other"){
					
					$data = getimagesize($value);

					$width = (float)$data[0];
					$height = (float)$data[1];
					if ($chkwidth > $width || $chkheight > $height) {
					
						return false;
					}
					return true;
				}else{
					return true;
				}

	    });
	    Validator::extend('checkSameAd', function($attribute, $value, $parameters, $validator) use ($request){
				
					$CheckAdvertisementlist = AdCreative::where('ad_region_id',$request->ad_region_id)
											//->where('ad_page_id',$request->ad_page_id)
											// ->where('ad_date',$request->ad_date)
											// ->where('start_time', '>=', $request->start_time)
											// ->where('end_time', '<=', $request->end_time)
											->where('user_id', $request->user_id)
											->get();

	            if (count($CheckAdvertisementlist)>0) {
	                return false;
	            }
	            return true;
	    });

    	$validator = Validator::make($request->all(), [
    		'image' => 'required|checkdimensions', 
        	'ad_region_id'=> 'required',
        	//'ad_page_id'=> 'required',
        	'user_id'=> 'required'//|checkSameAd',
        ],[
        	 'image.required' => 'Image required',
        	 'image.checkdimensions' => 'Image dimensions not matched',
        	 'ad_region_id.required' => 'Region details required',
        	 //'ad_page_id.required' => 'Page details required',
        	 'user_id.required' => 'Region details required',
			 'user_id.checkSameAd' => 'Sorry You already added an Advertisement on same date and time',
        ]);

        if($validator->fails()){

            $messages = $validator->messages();
            $response =  Api::make_response(0,[],$messages);

        }else{

	    	if($request->image) {

				$file = $request->image;
				$mime = ucwords(explode("/",File::mimeType($_FILES['image']['tmp_name']))[0]);

	    		$filename = substr( md5( time() ), 0, 15);
	    		$upload_response = false;

				if($request->filetype !="other") {	

					$filename = $filename . '.jpg'; 
					$path = public_path('adcreative/' . $filename);
					$upload_response = move_uploaded_file($_FILES["image"]["tmp_name"],$path);
					if($upload_response) {
						$image_resize = Image::make($path);
						$image_resize->fit($chkwidth, $chkheight, function ($constraint) {
						    $constraint->upsize();
						});
						// $image_resize->resize($chkwidth, $chkheight);
						$image_resize->save($path);
					}

				}else{
					
					$is_audio_video = false;
					if(preg_match('/video\/*/',$_FILES['image']['type'])) {
						$filename = $filename.'.mp4';
						$is_audio_video = true;
					}elseif(preg_match('/audio\/*/',$_FILES['image']['type'])) {
						$filename = $filename.'.mp3';
						$is_audio_video = true;
					}/*else {
						$filename = $filename.'.'.$file->getClientOriginalExtension();	
					}*/
					if($is_audio_video) {

			    		$temp_file = $_FILES['image']['tmp_name'];
			    		$time = exec("ffmpeg -i $temp_file 2>&1 | grep Duration | cut -d ' ' -f 4 | sed s/,//");

			    		$parsed = date_parse($time);
			    		$seconds = (isset($parsed['hour']) ? ($parsed['hour'] * 3600) : 0) + (isset($parsed['minute']) ? ($parsed['minute'] * 60) : 0) + $parsed['second'];
			    		if($seconds > 181) {
			    		    return Api::make_response(0,[],'Your file duration is greter than 3 minutes. Please upload a file within 3 minutes.');
			    		}
						$path = public_path('adcreative/' . $filename);
						exec("ffmpeg -i $temp_file $path");

						$upload_response = file_exists($path);
					}

				}
				

				if ($upload_response) {

		    		$insertadvertisement = new AdCreative;
		    		$insertadvertisement->ad_region_id = $request->ad_region_id;
		    		//$insertadvertisement->ad_page_id = $request->ad_page_id;
		    		$insertadvertisement->user_id = $request->user_id;
		    		$insertadvertisement->type = $mime;
		    		$insertadvertisement->content = $filename;
					$insertadvertisement->name = $request->name;
					$insertadvertisement->user_link = isset($request->url) ? $request->url : '';
					// $insertadvertisement->ad_date = $request->ad_date;
					// $insertadvertisement->start_time = $request->start_time;;
					// $insertadvertisement->end_time = $request->end_time;
					$insertadvertisement->status = 1;
					
		    		if($insertadvertisement->save()) {
		    			$response =  Api::make_response(1,$insertadvertisement,'Advertisement post successfully');
		    		}
				}
	    	}
        }
    	return $response; 		

	}


    public function postadvertisement(Request $request){

		$response = array();
		$chkwidth = $chkheight = 0;
		$region_id = $request->ad_region_id;
		//die;
		//echo json_encode($_FILES["image"]);	die;
	    Validator::extend('checkdimensions', function($attribute, $value, $parameters, $validator) use ($region_id){
				
				if($region_id!=''){
					$adregions = AdRegions::find($region_id);
					$chkwidth = $adregions->width;
					$chkheight = $adregions->height;

				}
	    		$data = getimagesize($value);
				$width = (float)$data[0];
				$height = (float)$data[1];
				//echo "pre"; print_r(array(['sent']=>array($height, $width), ['region']=>array()

	            if ($chkwidth == $width && $chkheight == $height) {
	                return true;
	            }
	            return false;
	    });
	    Validator::extend('checkSameAd', function($attribute, $value, $parameters, $validator) use ($request){
				
					$CheckAdvertisementlist = Advertisement::where('ad_region_id',$request->ad_region_id)
											->where('ad_page_id',$request->ad_page_id)
											->where('ad_date',$request->ad_date)
											->where('start_time', '>=', $request->start_time)
											->where('end_time', '<=', $request->end_time)
											->where('user_id', $request->user_id)
											->get();

	            if (count($CheckAdvertisementlist)>0) {
	                return false;
	            }
	            return true;
	    });
    	$validator = Validator::make($request->all(), [
    		'image' => 'required|checkdimensions', 
        	'ad_region_id'=> 'required',
        	'ad_page_id'=> 'required',
        	'user_id'=> 'required|checkSameAd',
        ],[
        	 'image.required' => 'Image required',
        	 'image.checkdimensions' => 'Image dimensions not match',
        	 'ad_region_id.required' => 'Region details required',
        	 'ad_page_id.required' => 'Page details required',
        	 'user_id.required' => 'Region details required',
			 'user_id.checkSameAd' => 'Sorry You already added an Advertisement on same date and time',
        ]);
        if($validator->fails()){
            $messages = $validator->messages();
            //return Redirect::back()->withErrors($validator)->withInput();
            $response =  Api::make_response(0,[],$messages);
        }else{

	    	if($request->image){
	    		
	    		/*
				echo $request->ad_region;
	    		echo json_encode($_FILES["image"]);
				die;
				*/

				$filename = $request->user_id.'-'.substr( md5( $request->user_id . '-' . time() ), 0, 15) . '.jpg'; 
				$path = public_path('advertisement/' . $filename);

				if (move_uploaded_file($_FILES["image"]["tmp_name"],$path)){

		    		$insertadvertisement = new Advertisement;
		    		$insertadvertisement->ad_region_id = $request->ad_region_id;
		    		$insertadvertisement->ad_page_id = $request->ad_page_id;
		    		$insertadvertisement->user_id = $request->user_id;
		    		$insertadvertisement->type = "Image";
		    		$insertadvertisement->content = $filename;
					$insertadvertisement->ad_date = $request->ad_date;
					$insertadvertisement->start_time = $request->start_time;;
					$insertadvertisement->end_time = $request->end_time;;
					$insertadvertisement->status = 1;
					
		    		if($insertadvertisement->save()){
		    			$response =  Api::make_response(1,$insertadvertisement,'Advertisement post successfully');
		    		}
				}
	    	}
        }
    	return $response; 
    }

	public function useraddlist(Request $request){
		$response = array();
		$data = $request->json()->all();
		if($data['user_id']!=''){
			$advertisement = Advertisement::with('getAdRegions','getAdpages')->where('user_id',$data['user_id'])->get();
			$advertisementChecking = [];
			if(count($advertisement)>0){
				//echo json_encode($advertisement);die;

				foreach($advertisement as $key => $value){
					$value["content"] = env('IMG_URL').'advertisement/'.$value->content;
					//echo "<br>".$value->ad_page_id;

					/*$advertisementChecking[$key]["id"] = $value->id;
					$advertisementChecking[$key]["ad_region_id"] = $value->ad_region_id;
					$advertisementChecking[$key]["ad_page_id"] = $value->ad_page_id;
					$advertisementChecking[$key]["user_id"] = $value->user_id;
					$advertisementChecking[$key]["content"] = $value->content;
					$advertisementChecking[$key]["type"] = $value->type;
					$advertisementChecking[$key]["ad_date"] = $value->ad_date;
					$advertisementChecking[$key]["start_time"] = $value->start_time;
					$advertisementChecking[$key]["end_time"] = $value->end_time;
					$advertisementChecking[$key]["status"] = $value->status;
					*/
					$sameadvertisementlist = Advertisement::where('ad_region_id',$value->ad_region_id)
											->where('ad_page_id',$value->ad_page_id)
											->where('ad_date',$value->ad_date)
											->where('start_time', '>=', $value->start_time)
											->where('end_time', '<=', $value->end_time)
											->where('user_id', '!=', $value->user_id)
											->get();
											
					
					if(count($sameadvertisementlist)>0){
						/*foreach($sameadvertisementlist as $key1 => $value1){
							
						}
						$advertisementChecking[$key]["flag"] = count($sameadvertisementlist);*/
						$value["flagBid"] = true;
					}else{
						$value["flagBid"] = false;
					}
				}
				
				if(!empty($advertisement)){
					$response =  Api::make_response(1,$advertisement,'User advertisement');
				}else{
					$response =  Api::make_response(0,[],'Something wrong');
				}
				//echo json_encode($advertisementChecking);
				
			}
		}
		return $response; 
	}
    public function postbids(Request $request){

		$response = array();
		$data = $request->json()->all();
		//echo json_encode($data);die;
		$checkAdBid = AdBid::where('user_id',$data['user_id'])->where('advertisement_id',$data['advertisement_id'])->first();
		if(!empty($checkAdBid)){
			$AdBidEdit = AdBid::find($checkAdBid->id);
			$AdBidEdit->user_id = $data['user_id'];
			$AdBidEdit->advertisement_id = $data['advertisement_id'];
			$AdBidEdit->ad_page_id = $data['ad_page_id'];
			$AdBidEdit->state_id = $data['state_id'];
			$AdBidEdit->from_time = $data['from_time'];
			$AdBidEdit->to_time = $data['to_time'];
			$AdBidEdit->publish_date = $data['publish_date'];
			$AdBidEdit->bid_amount = $data['bid_amount'];
			$AdBidEdit->is_highest_bid = $data['is_highest_bid'];
			$AdBidEdit->current_highest_bid = $data['current_highest_bid'];
			$AdBidEdit->status = $data['status'];
			if($AdBidEdit->save()){
				$response =  Api::make_response(1,$AdBidEdit,'Bid updated successfully');
			}
		}else{
			$AdBidInsert = new AdBid;
			$AdBidInsert->user_id = $data['user_id'];
			$AdBidInsert->advertisement_id = $data['advertisement_id'];
			$AdBidInsert->ad_page_id = $data['ad_page_id'];
			$AdBidInsert->state_id = $data['state_id'];
			$AdBidInsert->from_time = $data['from_time'];
			$AdBidInsert->to_time = $data['to_time'];
			$AdBidInsert->publish_date = $data['publish_date'];
			$AdBidInsert->bid_amount = $data['bid_amount'];
			$AdBidInsert->is_highest_bid = $data['is_highest_bid'];
			$AdBidInsert->current_highest_bid = $data['current_highest_bid'];
			$AdBidInsert->status = $data['status'];
			if($AdBidInsert->save()){
				$response =  Api::make_response(1,$AdBidInsert,'Bid added successfully');
			}
		}
		return $response; 
    }
	public function bidlisting(Request $request){

/*
use App\Country;
use App\Region;
use App\Devision;
use App\State;
*/
		$response = array();
		$data = $request->json()->all();


		$stateList = [];

		if($data['countryId']!=''){

			if($data['regionId']!=''){

					if($data['divisionId']!=''){


						if(!empty($data['stateId'])){

							foreach($data['stateId'] as $key => $value1 ){
								$stateList[] = State::where('slug', '=', $value1)->first()->id;
							}
						}else{
							
							$devisionlist = Devision::with('states')->where('slug', '=', $data['divisionId'])->get();
							
							foreach ($devisionlist as $key => $value1) {
								foreach ($value1['states'] as $key => $statesvalue) {
									$stateList[]= $statesvalue->id;
								}
							}

						}

					}else{

						$regionlist = Region::with('divisions.states')->where('slug', '=', $data['regionId'])->get();

						foreach ($regionlist as $key => $regionsvalue) {
								foreach ($regionsvalue['divisions'] as $key => $value1) {
									foreach ($value1['states'] as $key => $statesvalue) {
										$stateList[]= $statesvalue->id;
									}
								}
							}
					}

			}else{

				$countryId = Country::with('regions.divisions.states')->where('slug',$data['countryId'])->get();

				foreach ($countryId as $key => $value) {
					foreach ($value['regions'] as $key => $regionsvalue) {
						foreach ($regionsvalue['divisions'] as $key => $value1) {
							foreach ($value1['states'] as $key => $statesvalue) {
								$stateList[]= $statesvalue->id;
							}
						}
					}
				}
				
			}
			
		}else{
			$response =  Api::make_response(0,[],'Bid not found');
		}
		$pageValue = AdPages::find($data['ad_page_id'])->first();
		

		$addStartTime = $data['ad_start_time'];
		$addEndTime = $data['ad_end_time'];
		$adTimeDiff = (strtotime($data['ad_end_time']) - strtotime($data['ad_start_time']))/3600;
        $pageValueminbid = $adTimeDiff * $pageValue->min_bid;
		$pageValuebuynowprice = $adTimeDiff * $pageValue->buy_now_price;

		$bidadpricemaster = AdPriceMaster::where('ad_page_id',$data['ad_page_id'])
							->whereIn('state_id',$stateList)
							// ->where('to_time', '<=', $addStartTime)
                           	// ->where('from_time', '>=', $addEndTime)
							->whereBetween('to_time', [$addStartTime, $addEndTime])
							->whereBetween('from_time', [$addStartTime, $addEndTime])
							->get();
		$totalPrice = '';
		foreach ($bidadpricemaster as $key => $value) {
			$value['adTimeDiff'] = $adTimeDiff;
			$value['page_min_bid'] = $pageValue->min_bid;
			$value['page_buy_now_price'] = $pageValue->buy_now_price;
			$value['calculate_page_min_bid'] = $pageValueminbid;
			$value['calculate_page_buy_now_price'] = $pageValuebuynowprice;			
			$value['calculate_min_bid'] = ($value['min_bid'] * $adTimeDiff) + $pageValueminbid;
			$value['calculate_buy_now_price'] = ($value['buy_now_price'] * $adTimeDiff) + $pageValuebuynowprice;
			$totalPrice = $totalPrice + $value['calculate_buy_now_price'];
		}


		//$AdPriceMasterlist = AdPriceMaster::whereIn('id', [1, 2, 3])-get();
		
		if(count($bidadpricemaster)>0){
			$response =  Api::make_response(1,['bidadpricemaster'=>$bidadpricemaster,'totalPrice'=>$totalPrice],'Bid list');
		}
		return $response;
	}

    public function getAddRegions(Request $request){
	 	$data = $request->json()->all();
	 	$response =  [];
	 	if ($data) {
           if ($regions = Country::with('regions')->where('slug', '=', $data['country_slug'])->where('is_active', 1)->first()){
            $response =  Api::make_response(1,$regions,'Regions Found');			
           }else{
             $response =  Api::make_response(0,[],'No region found');           	
           }
	 	}else{
	 		$response =  Api::make_response(0,[],'Invalide Param.');
	 	}
	 	return $response;  
    }

    public function getDivisions(Request $request){
	 	$data = $request->json()->all();

		 //echo json_encode($data['region_slug']);die;
	 	$response =  [];
	 	if ($data) {
           if ($divisions_id = Region::wherein('slug', $data['region_slug'])->where('is_active', 1)->pluck('id')){
			   $divisions = Devision::wherein('region_id',$divisions_id)->get();
            $response =  Api::make_response(1,$divisions,'Division Found');			
           }else{
             $response =  Api::make_response(0,[],'No Division found');           	
           }
	 	}else{
	 		$response =  Api::make_response(0,[],'Invalide Param.');
	 	}
	 	return $response;  
    } 

    public function getStates(Request $request){
	 	$data = $request->json()->all();
	 	$response =  [];
	 	if ($data) {
           if ($states_id = Devision::wherein('slug',$data['division_slug'])->where('is_active', 1)->pluck('id')){
			   $states = State::wherein('division_id',$states_id)->get();
            $response =  Api::make_response(1,$states,'States Found');			
           }else{
             $response =  Api::make_response(0,[],'No State found');           	
           }
	 	}else{
	 		$response =  Api::make_response(0,[],'Invalide Param.');
	 	}
	 	return $response;  
    }

/*
	"user_id":6,
	"creative_id":12,
	"ad_page_id":21,
	"publish_date":"2017-07-14",
	"bid_amount":"50.00",
	"is_highest_bid":"1",
	"current_highest_bid":"50.50",
	"status":"1",

	*/

	public function bidpostfinal(Request $request){

		$data = $request->json()->all();
		$response =  [];
		$flag = 1;
		foreach ($data['state_time'] as $key => $value) {
			
			$alredyentry = AdBid::where('ad_page_id',$data['ad_page_id'])
								->where('user_id',$data['user_id'])
								->where('publish_date',$data['publish_date'])
								->where('state_id',$value['state_id'])
								->where('from_time',$value['slot'][0]['time'])
								->get();

			
			if(count($alredyentry)>0){
				$flag = 2;
			}else{
				$insertAdBid = new AdBid;
				$insertAdBid->user_id = $data['user_id'];
				$insertAdBid->ad_page_id = $data['ad_page_id'];	
				$insertAdBid->publish_date = $data['publish_date'];
				$insertAdBid->creative_id = $data['cretive_id'];	
				$insertAdBid->user_id = $data['user_id'];
				$insertAdBid->status = $data['status'];	
				$insertAdBid->state_id = $value['state_id'];

				$insertAdBid->from_time = $value['slot'][0]['time'];
				$insertAdBid->to_time = date('H:i', strtotime($value['slot'][0]['time'].'+1 hour'));
				$insertAdBid->bid_amount = $value['slot'][0]['price'];
				$insertAdBid->is_highest_bid = '1';
				$insertAdBid->current_highest_bid = $value['slot'][0]['price'];
				$insertAdBid->buynowprice = $value['slot'][0]['buynow'];

				if(!$insertAdBid->save()){
					$flag = 0;

					break;
				}else{
					$oldentry = AdBid::where('ad_page_id',$data['ad_page_id'])
								->where('publish_date',$data['publish_date'])
								->where('state_id',$value['state_id'])
								->where('user_id','!=',$data['user_id'])
								->where('from_time',$value['slot'][0]['time'])
								->get();
					if(count($oldentry)>0){

						foreach ($oldentry as $keyold => $valueold) {
							
							$updateAdBid = AdBid::find($valueold['id']);

							if($insertAdBid->current_highest_bid > $valueold['bid_amount'])
							{
								$updateAdBid->is_highest_bid = 0;
								$updateAdBid->current_highest_bid = $value['slot'][0]['price'];
								$updateAdBid->is_active = 0;
							}
							$updateAdBid->save();
						}

					}
				}
			}
			
		}

		if($flag == 1){
 		//$response =  Api::make_response(1,["bit submit successfully"],'Successfully.');	
		$response =  Api::make_response(1,$oldentry,'Successfully.');	

	 	}else if($flag == 2){
			$response =  Api::make_response(0,[],'Sorry you have already bid on this slot');
		}else{
	 		$response =  Api::make_response(0,[],'Invalide Param.');
	 	}
	 	return $response;

	}

	public function mybidupdate(Request $request){
		$data = $request->json()->all();
	 	$response =  [];
		$myAdBid = AdBid::find($data['mybidid']);
		$myAdBid->bid_amount = $data['bid_amount'];
		$myAdBid->current_highest_bid = $data['bid_amount'];
		$myAdBid->is_active = 1;
		$myAdBid->is_highest_bid = 1;

		//$response =  Api::make_response(1,explode(",",rtrim($data['otherbidid'],',')),'States Found');		
		//return $response;
		if($myAdBid->save()){

			foreach (explode(",",rtrim($data['otherbidid'],',')) as $key => $value) {
				//echo $value;
			$AdBidlist = AdBid::find($value);
			$AdBidlist->is_active = 0;
			$AdBidlist->is_highest_bid = 0;
			$AdBidlist->save();

			}
        $response =  Api::make_response(1,$myAdBid,'States Found');		


	 	}else{
	 		$response =  Api::make_response(0,[],'Invalide Param.');
	 	}
	 	return $response; 
	}
	public function bidpost(Request $request){
		$data = $request->json()->all();
	 	$response =  [];
		if(count($data['state_time']>0)){


			foreach ($data['state_time'] as $key => $value) {

				
				foreach ($value['slot'] as $key => $value1) {

					$insertAdBid = new AdBid;
					$insertAdBid->user_id = $data['user_id'];
					$insertAdBid->ad_page_id = $data['ad_page_id'];	
					$insertAdBid->publish_date = $data['publish_date'];
					$insertAdBid->creative_id = $data['creative_id'];	
					$insertAdBid->user_id = $data['user_id'];
					$insertAdBid->status = $data['status'];	
					$insertAdBid->state_id = $value['state_id'];

					$insertAdBid->from_time = $value1['time'];
					$insertAdBid->to_time = date('H:i', strtotime($value1['time'].'+1 hour'));
					if($value1['price']!=''){
						$insertAdBid->bid_amount = $value1['price'];
						$insertAdBid->is_highest_bid = '1';
						$insertAdBid->current_highest_bid = $value1['price'];
					}
					/*else{
						$adpricemaster = AdPriceMaster::where('ad_page_id',$data['ad_page_id'])
										//->where('to_time',$value1['time'])
										->where('to_time', '<=', $value1['time'])
										->where('to_time', '>=', $value1['time'])
										->where('state_id',$value['state_id'])
										->first()->buy_now_price;
					}*/

					$insertAdBid->save();

				}
			}
		}
die;
		echo json_encode($data['state_time']);

	}



	public function bidpostlist(Request $request){
		$data = $request->json()->all();
	 	$response =  [];

		$state_id = $data['stateIdDetails'];//["5","12"];
		$ad_page_id = $data['ad_page_id'];//["5","12"];
		$user_id = 7;//$data['user_id'];
		$datevalue = date($data['myDateForm'], strtotime("+1 month"));//$data['myDateForm'];//"2017-07-14";
		$statearray = array();
		foreach($state_id as $key => $value){
			$adBidyellow = AdBid::where('state_id', $value)->where('ad_page_id',$ad_page_id)->where('publish_date', $datevalue)->where('status', 1)->pluck('from_time');
			$adBidbid_amount = AdBid::where('state_id', $value)->where('ad_page_id',$ad_page_id)->where('publish_date', $datevalue)->where('status', 1)->pluck('bid_amount');
			$adBidbid_user = AdBid::where('state_id', $value)->where('ad_page_id',$ad_page_id)->where('publish_date', $datevalue)->where('status', 1)->pluck('user_id');
			//$adBidgreen = AdBid::where('state_id', $value)->where('user_id',$user_id)->where('ad_page_id',$ad_page_id)->where('publish_date', $datevalue)->where('status', 1)->pluck('from_time');
			//$adBidbid_amountgreen = AdBid::where('state_id', $value)->where('user_id',$user_id)->where('ad_page_id',$ad_page_id)->where('publish_date', $datevalue)->where('status', 1)->pluck('bid_amount');
			
			$adBidred = AdBid::where('state_id', $value)->where('ad_page_id',$ad_page_id)->where('publish_date', $datevalue)->where('status', 2)->pluck('from_time');


			$statearray[$key]['state_name'] = State::where('id','=',$value)->first()->name;
			$statearray[$key]['state_id'] = $value;

			//$statearray[$key]['time']['green']['start_time'] = $adBidgreen;
			//$statearray[$key]['time']['green']['bid_amount'] = $adBidbid_amountgreen;

			$statearray[$key]['time']['yellow']['start_time'] = $adBidyellow;
			$statearray[$key]['time']['yellow']['bid_amount'] = $adBidbid_amount;
			$statearray[$key]['time']['yellow']['user_id'] = $adBidbid_user;
			$statearray[$key]['time']['red'] = $adBidred;
		}
		
		if(count($statearray)>0){
        $response =  Api::make_response(1,$statearray,'States Found');			
	 	}else{
	 		$response =  Api::make_response(0,[],'Invalide Param.');
	 	}
	 	return $response; 
	}

	public function bidcalculation(Request $request){
		$data = $request->json()->all();
		$calculate = array();
		$response =  [];
		$amount = 0;
		foreach ($data['data'] as $key => $value) {
			$calculate[$key]['calculate']['id'] = $key + 1;
			$calculate[$key]['state_name'] = State::where('id','=',$value['state_id'])->first()->name;
			$calculate[$key]['calculate']['time'] = $value['slot'];
			$calculate[$key]['calculate']['datetime'] = $data['datetime'];
			$calculate[$key]['calculate']['state_id'] = $value['state_id'];

			$adpricemaster = AdPriceMaster::where('ad_page_id',$value['ad_page_id'])
								->where('to_time', '<=', $value['slot'])
								->where('from_time', '>=', $value['slot'])
								->where('state_id',$value['state_id'])
								->first();

			if($value['price'] && $value['price']!='0') {
				
				$amount = $amount + $value['price'];
				$calculate[$key]['calculate']['min_bid'] = $value['price'];


				if(count($adpricemaster)>0){
					$amount = $amount + $adpricemaster->buy_now_price;
					$calculate[$key]['calculate']['buy_now_price'] = $adpricemaster->buy_now_price;

				}else{
					$price = AdPages::where('id',$value['ad_page_id'])->first()->buy_now_price;
					$calculate[$key]['calculate']['buy_now_price'] = $price;
					$amount = $amount + $price;
				}

			}else{
				
				if(count($adpricemaster)>0){
					$amount = $amount + $adpricemaster->buy_now_price;
					//$calculate[$key]['calculate']['price'] = $adpricemaster->buy_now_price;
					$calculate[$key]['calculate']['buy_now_price'] = $adpricemaster->buy_now_price;
					$calculate[$key]['calculate']['min_bid'] = $adpricemaster->min_bid;

				}else{
					$price = AdPages::where('id',$value['ad_page_id'])->first()->buy_now_price;
					$amount = $amount + $price;
					//$calculate[$key]['calculate']['price'] = $price;
					$calculate[$key]['calculate']['buy_now_price'] = $price;
					$calculate[$key]['calculate']['min_bid'] = AdPages::where('id',$value['ad_page_id'])->first()->min_bid;
				}
			}
		}

		if(count($calculate)>0){
        $response =  Api::make_response(1,["calculation"=>$calculate,"totalprice"=>$amount],'Calculate Found');			
	 	}else{
	 		$response =  Api::make_response(0,[],'Invalide Param.');
	 	}
	 	return $response; 
	 	
	}

	public function winbidcron(){

		$past_three_day = Carbon::now()->addDays(3);
		$past_three_date = $past_three_day->toDateString();
		$past_three_date_time = $past_three_day->toDateTimeString();

		/*$quesryStr = "SELECT *  FROM  `ad_bids`  WHERE  `publish_date` =  '$past_three_date' AND  `is_highest_bid` =1 GROUP BY  `publish_date` ,  `from_time` ,  `state_id` ,  `ad_page_id` ";
		$AdBidWin = DB::select( $quesryStr );*/

		$AdBidWin = AdBids::with('state')
					->where('publish_date',$past_three_date)
					->where('is_highest_bid',1)
					->where('is_msg_sent',0)
					->groupby('publish_date','from_time','state_id','ad_page_id')
					->get();

		//dd($AdBidWin);die;
		if(count($AdBidWin)>0) {
			foreach ($AdBidWin as $key => $value) {

				$value->is_msg_sent = 1;
				$value->update();

				$notificationLog = new NotificationLog;
				$notificationLog->activity_by = $value->user_id;
				$notificationLog->visible_by = $value->user_id;
				$notificationLog->activity_type = "bid_win";
				$notificationLog->msg_text = "";
				$sate_name = isset($value['state']->name) ? $value['state']->name : '';
				if( $value->status == 1) {
					$notificationLog->notification_text = "You have a chance to win a Bid Location : $sate_name ".date('jS M, Y',strtotime($value->publish_date))." (".date('ga',strtotime($value->from_time))." - ".date('ga',strtotime($value->to_time)).")";
					// $notificationLog->msg_text = "You have to pay $".$value->bid_amount." to win a Bid Slot with in 6 hours";
					$notificationLog->msg_text = "You have to pay $".$value->bid_amount." to win the Bid slot of ".date('ga',strtotime($value->from_time))." - ".date('ga',strtotime($value->to_time))." for the date of ".date('jS M, Y',strtotime($value->publish_date))." for the location  $sate_name within 6 hour. After your successful payment you will win the slot for your Advertisement and it will display on the day of ".date('jS M, Y',strtotime($value->publish_date))." at your selected time.";
					$notificationLog->adbid_id = $value->id;
				} else if( $value->status == 2) {

					$notificationLog->notification_text = "You have won a Bid Location : $sate_name ".date('jS M, Y',strtotime($value->publish_date))." (".date('ga',strtotime($value->from_time))." - ".date('ga',strtotime($value->to_time)).")";
					$notificationLog->msg_text = "Your payment has been successfully made. You have won the Bid for the time slot of ".date('ga',strtotime($value->from_time))." - ".date('ga',strtotime($value->to_time))." for the date of ".date('jS M, Y',strtotime($value->publish_date))." for the location $sate_name";
					$notificationLog->adbid_id = "";
				}				
				$notificationLog->save();
				//echo $value->id." ".$value->user_id;
				//echo "<br>";
			}
			echo "Bid cron run successfully";die;
		}
		echo "Biding data not found";die;
	}

	public function mybid(Request $request){
		$data = $request->json()->all();
		$response =  [];
		$AdBidMstrt = [];
		$AdBidMstrtNew = array();
		$arrayStatus =[];
		$statarray = array();
		$statarraynew = array();
		//$data['state_id'] = 12;
		//$data['ad_page_id'] = 3;
		if(!empty($data['user_id']) && !empty($data['datepicker'])){
		$ad_page_id = $data['ad_page_id'];
		$state_id = $data['state_id'];
		$user_id = $data['user_id'];
		$datepicker = $data['datepicker'];
		//$page_id = $data['ad_page_id'];
		//$state_id = $data['state_id'];
//select ab . * , my_bid.id AS prev_id FROM  ad_bids AS ab LEFT JOIN ad_bids AS m' at line 1 (SQL: 1select ab . * , my_bid.id AS prev_id FROM  ad_bids AS ab LEFT JOIN ad_bids AS my_bid ON ab.`from_time` = my_bid.`from_time` AND ab.publish_date = my_bid.publish_date AND my_bid.is_active = 0 AND my_bid.user_id = 7 WHERE ab.publish_date =  '2017-09-05' AND ab.is_active = 1 GROUP BY ab.id order by ab.state_id)
		//$quesryStr = "select ab . * , my_bid.id AS prev_id FROM  ad_bids AS ab LEFT JOIN ad_bids AS my_bid ON ab.`from_time` = my_bid.`from_time` AND ab.publish_date = my_bid.publish_date AND my_bid.is_active = 0 AND my_bid.user_id = $user_id WHERE ab.publish_date =  '".$datepicker."' AND ab.state_id = $state_id AND ab.ad_page_id =2 AND ab.is_active = 1 GROUP BY ab.id";
			$quesryStr = "select ab . * , my_bid.id AS prev_id FROM  ad_bids AS ab LEFT JOIN ad_bids AS my_bid ON ab.`from_time` = my_bid.`from_time` AND ab.publish_date = my_bid.publish_date AND my_bid.is_active = 0 AND my_bid.user_id = $user_id WHERE ab.ad_page_id = '".$ad_page_id."' AND ab.publish_date =  '".$datepicker."' AND ab.state_id= '".$state_id."' AND ab.is_active = 1 GROUP BY ab.id order by ab.state_id";
			$AdBidMstrt = DB::select( $quesryStr );
			$color = '';
			foreach ($AdBidMstrt as $key => $value) {
				

				if(!isset($statarray[$value->state_id])){
					$statarray[$value->state_id] = [];
				}


				if($value->status == 1 && $value->user_id == $user_id){
					$color = "green";
				}
				if($value->status == 1 && $value->user_id != $user_id){
					$color = "yellow";
				}
				if($value->status == 2 && $value->user_id != $user_id){
					$color = "red";
				}
				if($value->state_id!=''){
					$value->state_name = State::where('id',$value->state_id)->first()->name;
				}
				if($value->status == 2 && $value->user_id == $user_id){
					$color = "green";
				}
				$value->color = $color;
				//$dataBidArray = array('abcd' => $value);

				array_push($statarray[$value->state_id],$value);
				if(!in_array($value->state_id,$statarraynew)){
					array_push($statarraynew,$value->state_id);

					//$AdBidMstrtNew['id'][$value->state_id] = State::where('id',$value->state_id)->first()->name;
					$dataArray = array(
						'id' => $value->state_id,
						'name' => State::where('id',$value->state_id)->first()->name
					);

					array_push($AdBidMstrtNew,$dataArray);
				}
			}


			if(count($statarray)>0){
	        $response =  Api::make_response(1,['bid_array'=>$statarray,'state_array'=>$AdBidMstrtNew],'My bid Found');			
		 	}else{
		 		$response =  Api::make_response(0,[],'No Data found!');
		 	}


		}else{
			$response =  Api::make_response(0,[],'Missing required field!');
		}

		
		return $response;
	}

	public function mybidmodal(Request $request){
		$data = $request->json()->all();

		if($data["user_id"]!='' && $data["datepicker"]!='' && $data["from_time"]!='' && $data["state_id"]!='' ){
				$mybid = AdBid::where('state_id', $data["state_id"])
				->where('publish_date',$data["datepicker"])
				->where('from_time',$data["from_time"])
				->get();

				

				foreach ($mybid as $key => $value) {
					$my_status = "";
					if($value->status == 2 && $value->is_active == 1 && $value->user_id != $data["user_id"]){
						$my_status = "Other win";
					}
					if($value->status == 2 && $value->is_active == 1 && $value->user_id == $data["user_id"]){
						$my_status = "Me win";
					}
					if($value->status == 1 && $value->is_active == 1 && $value->user_id == $data["user_id"]){
						$my_status = "Me highest bid";
					}
					if($value->status == 1 && $value->is_active == 0 && $value->user_id == $data["user_id"]){
						$my_status = "Me lowest bid";
					}
					$value->val = $my_status;
					$value->user_name = User::find($value->user_id)->name;
				}
		}
		//echo "<pre>";print_r($mybid);die;
		if(count($mybid)>0){
        $response =  Api::make_response(1,$mybid,'My bid Found');			
	 	}else{
	 		$response =  Api::make_response(0,[],'Invalid Param.');
	 	}
		return $response;
	}
	public function showvideoadd(Request $request){
		$data = $request->json()->all();
		$State_ID = '';
		if($data['state_id']!=''){
			$State_dtls = State::where('slug',$data['state_id'])->where('is_active',1)->first();
			if(count($State_dtls)>0){
				$State_ID =  $State_dtls['id'];
			}
		}



		$response =  [];
		// $videoaudio = AdBid::with("creativedetails")
		// ->where("state_id",$State_ID)
		// ->where("publish_date",$data['date'])
		// ->where("is_active",1)
		// ->where('from_time','<',$data['time'] )
		// ->where('to_time','>',$data['time'])		
		// ->where("status",2)
		// ->first();

		$queryOne = AdCreative::leftJoin('ad_bids', 'ad_bids.creative_id', '=', 'ad_creative.id')
			->select('ad_creative.*')
			->where("ad_bids.state_id",$State_ID)
			->where("ad_bids.publish_date",$data['date'])
			->where("ad_bids.is_active",1)
			->where('ad_bids.from_time','<',$data['time'] )
			->where('ad_bids.to_time','>',$data['time'])		
			->where("ad_bids.status",2)
			->where('ad_creative.type','Video')
			->first();
		$quesryStr = "";
		//$AdBidMstrt = DB::select( "SELECT ad_creative.* FROM ad_creative LEFT JOIN ad_bids ON ad_creative.id = ad_bids.creative_id WHERE ad_bids.`state_id` = ".$State_ID." AND  ad_bids.publish_date =  '".$data['date']."' AND  ad_bids.is_active =1 AND  ad_bids.from_time <  '".$data['time']."' AND  ad_bids.to_time >  '".$data['time']."' AND  ad_bids.status = 2 AND  ad_creative.type = 'Video' ");

		if(count($queryOne)>0){
		//if(count($AdBidMstrt)>0){
        //$response =  Api::make_response(1,$videoaudio->creativedetails,'Video found');	
		//$response =  Api::make_response(1,$AdBidMstrt[0],'Video found');			
		$response =  Api::make_response(1,$queryOne,'Video found');			
	 	}else{
	 		$response =  Api::make_response(0,[],'No video found');
	 	}
		return $response;
	}

	public function showimageadd(Request $request){
		$data = $request->json()->all();
		//echo json_encode($data);die;
		$State_ID = '';
		if($data['state_id']!=''){
			$State_dtls = State::where('slug',$data['state_id'])->where('is_active',1)->first();
			if(count($State_dtls)>0){
				$State_ID =  $State_dtls['id'];
			}
		}
		// if($data['ad_page_id']!=''){
		// 	$AdPages_dtls = AdPages::where('page_url',$data['ad_page_id'])->where('status',1)->first();
		// 	if(count($AdPages_dtls)>0){
		// 		$AdPages_ID =  $AdPages_dtls['id'];
		// 	}
		// }

		//echo $AdPages_ID." ".$State_ID;die;

		$response =  [];
		$videoaudio = AdBid::with("creativedetails")
		->where("state_id",$State_ID)
		->where("publish_date",$data['date'])
		->where("is_active",1)
		->where('from_time','<',$data['time'] )
		->where('to_time','>',$data['time'])		
		->where("status",2)
		->get();

		// $videoaudio = AdCreative::leftJoin('ad_bids', 'ad_bids.creative_id', '=', 'ad_creative.id')
		// 	->select('ad_creative.*')
		// 	->where("ad_bids.state_id",$State_ID)
		// 	->where("ad_bids.publish_date",$data['date'])
		// 	->where("ad_bids.is_active",1)
		// 	->where('ad_bids.from_time','<',$data['time'] )
		// 	->where('ad_bids.to_time','>',$data['time'])		
		// 	->where("ad_bids.status",2)
		// 	->where('ad_creative.type','Image')
		// 	->get();
		if(count($videoaudio)>0){
        $response =  Api::make_response(1,$videoaudio,'Image found');			
	 	}else{
	 		$response =  Api::make_response(0,[],'No image found');
	 	}
		return $response;
	}
	public function buynowbid(Request $request){
		$data = $request->json()->all();
		$response =  [];
		$totalamout = 0;
		if(count($data)>0){

			foreach ($data['plan_id']['totalbynowdata'] as $key => $value) {

				$adAdBid = new AdBid;
				$adAdBid->user_id = $value['user_id'];
				$adAdBid->buynowprice = $value['buynowprice'];
				$adAdBid->creative_id = $value['cretive_id'];
				$adAdBid->from_time = $value['from_time'];
				$adAdBid->publish_date = $value['publish_date'];
				$adAdBid->ad_page_id = $value['ad_page_id'];
				$adAdBid->state_id = $value['state_id'];
				$adAdBid->$totalamout + $value['buynowprice'];
				$adAdBid->bid_amount = $value['buynowprice'];
				$adAdBid->current_highest_bid = $value['buynowprice'];
				$adAdBid->is_highest_bid = 1;
				$adAdBid->status = 2;
				$adAdBid->is_active = 1;
				$adAdBid->to_time = date('H:i', strtotime($value['from_time'].'+1 hour'));

				$transaction=array("transaction_status"=>"approved");

				if($adAdBid->save()){
					$instUserTransaction = new UserTransaction;
					$instUserTransaction->user_id = $value['user_id'];
					$instUserTransaction->transaction_details = json_encode($transaction);
					$instUserTransaction->transaction_for = "Bid Payment";
					$instUserTransaction->amount = $value['buynowprice'];
					$instUserTransaction->save();
					
					// Email::sendsubScriptionMail($value['user_id']);
				}

			}

        	$response =  Api::make_response(1,["msg"=>'Payment successfully'],'Payment successfully');			

	 	}else{
	 		$response =  Api::make_response(0,[],'Payment not successfully');
	 	}
		return $response;

	}

	public function getmylastbid(Request $request){
		$data = $request->json()->all();
		$response =  [];
		$currentdate = date("Y-m-d");
		if($data['user_id']!='' && $data['limit']!='' && !isset($data['is_purchase'])){
			$adAdBid = AdBid::where('user_id',$data['user_id'])->where('status',1)->whereDate('publish_date', '>=', date("Y-m-d"))->skip(0)->take($data['limit'])->get();
		}elseif($data['user_id']!='' && $data['limit']!='' && isset($data['is_purchase'])) {
			$adAdBid = AdBid::where('user_id',$data['user_id'])->where('status',2)->whereDate('publish_date', '>=', date("Y-m-d"))->skip(0)->take($data['limit'])->get();
		}

		if($adAdBid){
			$response =  Api::make_response(1,$adAdBid,'Bid found successfully');	
		}else{
			$response =  Api::make_response(0,[],'Bid Not Found');
		}
		return $response;
	}

	public function saveboostsetting(Request $request){
		$data = $request->json()->all();
		$response =  [];
		$uaerdtails = array();



		if($data['user_id']!=''){
			$uaerdtails = AccountSettings::where('user_id',$data['user_id'])->first();

			$AccountSettingsUpdate = AccountSettings::find($uaerdtails->id);

			if($data['selectcountry']!=''){
				$AccountSettingsUpdate->boosted_country = Country::where('slug',$data['selectcountry'])->first()->id;
			}else{
				$AccountSettingsUpdate->boosted_country = 0;
			}
			if($data['selectregion']!=''){
				$AccountSettingsUpdate->boosted_region = Region::where('slug',$data['selectregion'])->first()->id;
			}else{
				$AccountSettingsUpdate->boosted_region = 0;
			}
			if($data['selectdivision']!=''){
				$AccountSettingsUpdate->boosted_division = Devision::where('slug',$data['selectdivision'])->first()->id;
			}else{
				$AccountSettingsUpdate->boosted_division = 0;
			}
			if($data['selectstate']!=''){
				$AccountSettingsUpdate->boosted_state = State::where('slug',$data['selectstate'])->first()->id;
			}else{
				$AccountSettingsUpdate->boosted_state = 0;
			}

			if($AccountSettingsUpdate->save()){
				$response =  Api::make_response(1,$AccountSettingsUpdate,'Boost setting updated successfully');	
			}else{
				$response =  Api::make_response(0,[],'Something went wrong');
			}			
		}else{
			$response =  Api::make_response(0,[],'Something went wrong');
		}

		return $response;
	}	

}
