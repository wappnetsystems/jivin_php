<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Jivin_video;
class AdminJivinVideoController extends Controller
{
    public function index(){
        $video_detail= Jivin_video::where('id',1)->get();
        return view('admin.jivinvideo.index',['video_detail'=>$video_detail]);
    }
    
    public function edit_jivin_video($id){
        $video_detail= Jivin_video::where('id',1)->get();
        return view ('admin.jivinvideo.edit_jivin_video',['id'=>$id,'video_detail'=>$video_detail]);
    }
    
    public function update_jivin_video(Request $request){
        $video_title=$request->input('video_title');
        $update_arr=[
            'video_title'=>$video_title
        ];
        if($request->hasFile('video_file')){

            $file =$request->file('video_file');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/uploads/';
            $file->move($path, $filename);
            $update_arr['video_file']=$filename;
        }
        Jivin_video::where('id',$request->input('id'))->update($update_arr);
        return redirect()->route('jivin_video')->with('success','Update successfully completed.');
    }
    
}
