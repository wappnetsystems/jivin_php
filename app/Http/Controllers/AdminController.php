<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
/*Models*/
use App\State;
use App\AdPages;
use App\AdRegions;
use App\AdPriceMaster;

class AdminController extends Controller
{
    public function __construct(){
    	$this->middleware('admin');
    }

    public function index(){
    	// return Auth::guard('admin')->user();
        exec(" ffmpeg -i /var/www/html/dtswork/jivin/development/jivin/services/public/uploads/sample_video.mp4 /var/www/html/dtswork/jivin/development/jivin/services/public/uploads/sample_video_convert.mp4");
    	return view('admin.dashboard');
    }

    public function priceMasterScript() {

    	$all_state = State::where('is_active',1)->whereRaw('deleted_at is null')->get();
    	$all_pages = AdPages::where('status',1)->get();
    	
    	$index = 0;
    	$priceArr = array();
    	foreach ($all_pages as $keyPage => $eachPage) {

    		foreach ($all_state as $keyState => $eachState) {

    			$hour = 0;

    			while($hour++ < 24) {

    			    $timetoprint = date('H:i:s',mktime($hour,0,0,1,1,2011));
    			    $next_timetoprint = date('H:i:s',mktime(($hour+1),0,0,1,1,2011));
    			    
    			    $priceArr['ad_page_id'] = $eachPage->id;
    			    $priceArr['state_id'] = $eachState->id;
    			    $priceArr['to_time'] = $timetoprint;
    			    $priceArr['from_time'] = $next_timetoprint;
    			    $priceArr['min_bid'] = 60;
    			    $priceArr['buy_now_price'] = 150;
    			    $priceArr['status'] = 1;
    			    // $index++;
    			    \DB::table('ad_price_master')->insert($priceArr);
    			}
    		}

    	}
    	echo "Success";die;
    	// dd($priceArr);
    }

    public function priceManagement($page_id=null) {

        if($page_id) {
            $data = AdPriceMaster::where('ad_page_id',$page_id)->orderby('id','asc')->get();
        }else{
            $data = AdPriceMaster::where('ad_page_id',1)->orderby('id','asc')->get();
            $page_id=1;
        }
        $all_state = State::where('is_active',1)->whereRaw('deleted_at is null')->get();
        $all_ad_regions = AdRegions::where('status',1)->get();
        return view('admin.Price.index',compact('data','page_id','all_state','all_ad_regions'));
    }

    public function ajaxSavePrice(Request $request) {

        $update_data = $request->only(['buy_now_price','min_bid']);

        try{
            $data = AdPriceMaster::where('state_id',$request->state_id)
                            ->where('ad_page_id',$request->page_id)
                            ->where('to_time',$request->to_time)
                            ->where('from_time',$request->from_time)
                            ->update($update_data);

            return response()->json(['success'=>1]);

        }catch(\Exception $e){
            return response()->json(['success'=>0]);
        }

    }
}
