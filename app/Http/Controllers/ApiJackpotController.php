<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Illuminate\Http\Request;
use App\Api;
use App\Crypto_prices;
use App\Jackpot_amount;
use App\SiteSetting;
use Illuminate\Support\Facades\Validator;

class ApiJackpotController extends ApiGuardController {

    protected $apiMethods = [
        'get_jackpot_amount' => [
            'keyAuthentication' => true
        ]
        
    ];

    //this api function will return jackpot amount in different currency
    public function get_jackpot_amount(Request $request) {
        
        $validator = Validator::make($request->all(), [
                    'area_type' => 'required',
                    'location_id' => 'required|numeric',
                    
        ]);

        if ($validator->fails()) {
            return Api::make_response(0, [], 'Validation errors. Try Again!');
        }
        
        $area_type=$request->input('area_type');
        $location_id=$request->input('location_id');
        
        $crypto_prices = Crypto_prices::where('status', 'Enabled')->get();

        if ($crypto_prices->count() == 0) {
            return Api::make_response(0, [], 'No currency found.');
        }
        $amount_arr = [];
        $jackpot_amt_obj = Jackpot_amount::where(['area_type'=>$area_type,'location_id'=>$location_id])->get();
        if ($jackpot_amt_obj->count() == 0) {
            return Api::make_response(0, [], 'No jackpot found.');
        }
        $jackpot_amt = $jackpot_amt_obj[0]->amount;
        foreach ($crypto_prices as $key => $crypto) {

            $amount_arr[0][$crypto->coin_code] = number_format($jackpot_amt / $crypto->coin_price_usd, 2);
        }
        $amount_arr[0]['USD'] = $jackpot_amt;
        if (empty($amount_arr)) {
            return Api::make_response(0, [], 'No record found.');
        }

        //send jackpot text together in msg
        $site_setting = SiteSetting::where('id', 1)->get();
        if ($site_setting->count() == 0) {
            $msg = 'NA';
        } else {
            $msg = $site_setting[0]->jackpot_text;
        }
        return Api::make_response(1, $amount_arr, $msg);
    }

}
