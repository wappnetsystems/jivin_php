<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use App\Country;
use App\Region;
use App\Devision;
use App\State;

class AdminDevisionController extends Controller
{    
	public function __construct(){
	$this->middleware('admin');
}
      public function listDevision(){
      	$region = Region::where('is_active','=',1)->get()->toArray();
        return view('admin.location.devision')->with('region', $region);
      }
      public function getList(){
      	    $output['aaData'] = [];
			$aColumns = array(
				                 'id'
				                ,'region_id'
				                ,'region_name'
			                    ,'name'
			                    ,'is_active'
								,'id'
							);
			$devisions = Devision::get()->toArray();
			// /dd($devisions);
			foreach ($devisions as $key => $devision) {
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if($devision[ $aColumns[$i]]=== NULL){$devision[ $aColumns[$i]]= "";}
						$row[] = $devision[ $aColumns[$i] ];

				}
				$output['aaData'][] = $row;
			}
			$output = array(
			    "draw"            => intval(1),
			    "data"            => $output['aaData']
			); 	
      	return $output;
      }
      public function postAddNewDevision(Request $request){
       // echo $request->name;exit();
      	if ($request->id) {
      		$devision = Devision::where('id','=',$request->id)->first();
	      	if (Devision::where('name','=',$request->name)->where('id','!=',$request->id)->first()) {
	      		return redirect('admin/devision/list')->with('alert-danger', 'Devision Already Exists');
	      	}  	
	      	$devision->region_id = $request->region_id;
	      	$devision->name = str_replace(' ', '_', $request->name);;
	      	$devision->slug = str_replace(' ', '_', $request->name);;
	      	$devision->is_active = $request->is_active;
	      	if ($devision->save()) {
	      		return redirect('admin/devision/list')->with('alert-success', 'Devision Updated!');
	      	}else{
      		   return redirect('admin/devision/list')->with('alert-danger', 'Devision Updation faild');	      		
	      	}      		
      	}else{
	      	$devision = new Devision;
	      	if ($devision::where('name','=',$request->name)->first()) {
	      		return redirect('admin/devision/list')->with('alert-danger', 'Devision Already Exists');
	      	}else{
	      		// echo $request->name;exit();
	      	$devision->region_id = $request->region_id;
	      	$devision->name = str_replace(' ', '_', $request->name);;
	      	$devision->slug = str_replace(' ', '_', $request->name);;
	      	$devision->is_active = $request->is_active;
		      	if ($devision->save()) {
		      		return redirect('admin/devision/list')->with('alert-success', 'New Devision Created!');
		      	}else{
	      		   return redirect('admin/devision/list')->with('alert-danger', 'Devision Creation faild');	      		
		      	}
	      	}
        }
      }      
      public function updateStatus(Request $request){
      	$devision = Devision::where('id','=',$request->id)->first();
      	$devision->is_active = $request->status;
      	if ($devision->save()) {
      		return 'true';
      	}else{
  		   return 'false';	      		
      	}      	
      } 
}
