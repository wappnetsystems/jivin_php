<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Posts;
use App\ReportPost;
use App\PostLogs;
use App\User;
use App\PostBoost;
class AdminPostController extends Controller
{
    public function __construct(){
    	$this->middleware('admin');
    }
    public function details()
    {
 
       $post=ReportPost::with('users')->orderBy('created_at','DESC')->paginate(10);
        /*$post=Posts::whereHas('posts', function($q)
            {
                $q->where('post_id', '<>', '');

            })->orderBy('created_at')->get();
*/
     //  foreach($post as $posts)
     //  {
     // dd($posts['users']['name']);
     // //  }
      // dd($post);
            //echo json_encode($post);die;
  
    return view('admin.post.details')
    ->with('post',$post);
    }
    
    public function delete(Request $request){
      $postsmstr = Posts::find($request['postid']);
      //return $postsmstr;
      //$returnVar = 0;
      if($postsmstr){
         
      //echo json_encode($postsmstr);die;
         $postdelete = Posts::find($request['postid']);
         $postdelete->is_active = 0;
         if($postdelete->save()){
            $ReportPost = ReportPost::find($request['id']);
            $ReportPost->is_active = 0;
            $ReportPost->save();
            
            $returnVar = 1;
            $postLogsMstr = PostBoost::where("post_id",$request['postid'])->get();
               if(count($postLogsMstr)>0){
                  foreach ($postLogsMstr as $key1 => $value1) {
                  $updatepostLog = PostBoost::find($value1['id']);
                  $updatepostLog->is_active = 0;
                  $updatepostLog->save();
               }
            }
            // $postLogsMstr = PostLogs::where("post_id",$request['postid'])->get();
            //    if(count($postLogsMstr)>0){
            //       foreach ($postLogsMstr as $key1 => $value1) {
            //       $updatepostLog = PostLogs::find($value1['id']);
            //       $updatepostLog->updated_at = $value1['updated_at'];
            //       $updatepostLog->is_active = 0;
            //       $updatepostLog->save();
            //    }
            // }
         }
      }else{
            $ReportPost = ReportPost::find($request['id']);
            $ReportPost->is_active = 0;
            if($ReportPost->delete()){
               $returnVar = 1;
            }
      }
      return $returnVar;
    }
}
