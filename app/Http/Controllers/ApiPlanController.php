<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\Http\Requests;

use App\Api;
use App\User;
use App\Posts;
use App\Plans;
use App\PostLogs;
use App\PostLikeLog;
use App\PostShareLog;
use App\LeaderboardLog;
use App\NotificationLog;
use App\Subscription;
use App\Email;
use App\UserCard;
use App\Payeezy;
use App\UserTransaction;
use App\UserSubcriptionHistory;
use App\BoostPlans;
use App\UserBoost;
use App\PostBoost;
use App\Country;
use App\Region;
use App\Devision;
use App\State;
use App\AdBid;
use App\AccountSettings;
use Illuminate\Support\Facades\DB;
class ApiPlanController extends Controller
{
    protected $apiMethods = [
        'getplan' => [
            'keyAuthentication' => false
        ],
		'postyourboost' => [
            'keyAuthentication' => false
		],
		'sendmsg' => [
			'keyAuthentication' => false
		],   		
		'bidwinpayment' => [
			'keyAuthentication' => false
		]            
	];		
	
	public function sendmsg(Request $request){
		$data = $request->json()->all();
		$response =  [];
		$notificationLogAdd = new NotificationLog;
		$notificationLogAdd->activity_type = "contact_msg";
		$notificationLogAdd->activity_by =$data['activity_by'];
		$notificationLogAdd->visible_by = $data['visible_by'];
		$notificationLogAdd->related_id = 0;
		$notificationLogAdd->notification_text = "You have new message";
		$notificationLogAdd->msg_text = $data['msg'];
		$notificationLogAdd->is_read = 0;		

		if($notificationLogAdd->save()){
		//if($data){
			$response =  Api::make_response(1,$data,'Plans get successfully');
		}else{
			$response =  Api::make_response(0,[],'Plans not found');		
		}
		return $response;  
	}

    public function getplan() {

    	$response = array();
    	$plans = Plans::where('is_active',1)->get()->toArray();
    	

    	foreach ($plans as $key => $eachPlan) {

    		$all_boost_arr = array();
    		$all_boost_location = array();

    		if($eachPlan['allow_boost_count']) {
    			array_push($all_boost_arr,$eachPlan['allow_boost_count']);
    			array_push($all_boost_location,'Country');
    		}if($eachPlan['allow_boost_count_region']) {
    			array_push($all_boost_arr,$eachPlan['allow_boost_count_region']);
    			array_push($all_boost_location,'Region');
    		}if($eachPlan['allow_boost_count_division']) {
    			array_push($all_boost_arr,$eachPlan['allow_boost_count_division']);
    			array_push($all_boost_location,'Division');
    		}if($eachPlan['allow_boost_count_state']) {
    			array_push($all_boost_arr,$eachPlan['allow_boost_count_state']);
    			array_push($all_boost_location,'State');
    		}
    		$plans[$key]['all_boost_arr'] = $all_boost_arr;
    		$plans[$key]['all_boost_location'] = $all_boost_location;
    	}
    		// echo '<pre>';print_r($plans);die;


    	if(!empty($plans)){
    		$response =  Api::make_response(1,$plans,'Plans list');
		}else{
         $response =  Api::make_response(0,[],'Plans not found');
	   }
        
	   return $response;      
    }


	public function subscribeboost(Request $request){
	 	$data = $request->json()->all();
	 	$response =  [];
		$userCardlist = [];
		$num_of_boost = 0;
		$boosttype = '';

		// $response =  Api::make_response(1,$num_of_boost,$boosttype);	
		// return $response;  

		if($data['plan_id']!='' && $data['user_id']!=''){

			if($data['plan_id']!='' && $data['user_id']!='' && $data['save_card']='1'){
				
				$checkUserCard = UserCard::where('user_id',$data['user_id'])->where('card_no',$data['credit_card']['card_number'])->first();
				if(!empty($checkUserCard)){
					$checkUserCard->user_id = $data['user_id'];
					$checkUserCard->card_type = $data['credit_card']['type'];
					$checkUserCard->card_exp_date = $data['credit_card']['exp_date'];
					$checkUserCard->cardholder_name = $data['credit_card']['cardholder_name'];			
					$checkUserCard->card_no = $data['credit_card']['card_number'];			
					$checkUserCard->status = 1;
					$checkUserCard->save();
					
				}else{
	
					$checkUserCard = new UserCard;
					$checkUserCard->user_id = $data['user_id'];
					$checkUserCard->card_type = $data['credit_card']['type'];
					$checkUserCard->card_exp_date = $data['credit_card']['exp_date'];
					$checkUserCard->cardholder_name = $data['credit_card']['cardholder_name'];			
					$checkUserCard->card_no = $data['credit_card']['card_number'];			
					$checkUserCard->status = 1;
					$checkUserCard->save();
				}
				$plan_dtails = BoostPlans::find($data['plan_id']);
				$price = $plan_dtails->amount;
				$country_boost_count = $plan_dtails->country_boost_count;
				$region_boost_count = $plan_dtails->region_boost_count;
				$division_boost_count = $plan_dtails->division_boost_count;
				$state_boost_count = $plan_dtails->state_boost_count;
				/*
				$boost_details = explode("***",$data['boost_type']);
				$boost_details = explode("***",$data['boost_type']);

				if($boost_details[0]!='' && $boost_details[0]>0){
					$num_of_boost = $boost_details[0];
					$boosttype = $boost_details[1];
				}
				*/
				

				$card_details = array("amount"=> $price*100,
										"card_number"=> $data['credit_card']['card_number'],
										"card_type"=> $data['credit_card']['type'],
										"card_holder_name"=> $data['credit_card']['cardholder_name'],
										"card_cvv"=> $data['credit_card']['cvv'],
										"card_expiry"=> $data['credit_card']['exp_date'],
										"merchant_ref"=> "Astonishing-Sale",
										"currency_code"=> "USD",
										"method"=> "credit_card");

				$payeezy = new Payeezy;
				$transaction = json_decode($payeezy->purchase($card_details));
				 
				if(isset($transaction->code) && $transaction->code) {
					return Api::make_response(0,$transaction->code, 'Unauthorized Access! Please check your credentials.');
				}

				/*if($transaction->validation_status!='success' && $transaction->transaction_status!='approved') {
					return Api::make_response(0,$transaction->code, 'Unauthorized Access! Please check your credentials.');
				}*/
			/*if($transaction->code != ''){
				$response =  Api::make_response(0,$transaction->code,$transaction->message);
			}else{
				
				
			if($transaction->transaction_status == "approved"){*/
				
			
			//$transaction=array("transaction_status"=>"approved");
			//echo "santosh<pre>";print_r($transaction);die;
			if($transaction->transaction_status=='approved'){
				$status=1;
			}else{
				$status=0;
			}
			$instUserTransaction = new UserTransaction;
			$instUserTransaction->user_id = $data['user_id'];
			$instUserTransaction->transaction_details = json_encode($transaction);			
			$instUserTransaction->transaction_for = "Boost Plan Subscription";
			$instUserTransaction->transaction_id = isset($transaction->transaction_id) ? $transaction->transaction_id : NULL;
			$instUserTransaction->user_card_id = !empty($checkUserCard->id) ? $checkUserCard->id : NULL;
			$instUserTransaction->method = $transaction->method;
			$instUserTransaction->transaction_status = $transaction->transaction_status;
			$instUserTransaction->bank_resp_code = isset($transaction->bank_resp_code) ? $transaction->bank_resp_code : NULL;
			$instUserTransaction->amount = $price;
			$instUserTransaction->status =  $status;
			$instUserTransaction->save();
		
			//Email::sendsubScriptionMail($data['user_id']);	

			$userCardlist = UserCard::where('user_id',$data['user_id'])->get();

			
			$userboost = new UserBoost;
			$userboost->user_id = $data['user_id'];
			$userboost->boost_plan = $data['plan_id'];
			$userboost->is_active = 1;
				if($userboost->save()){
				//****** boost update start ************//
				//$user_boost = User::where("id",$data['user_id'])->first()->total_boost;
				
				$user_boost = User::where("id",$data['user_id'])->first();
				$user_boost_update = User::find($data['user_id']);
				/*
				$user_boost_update = User::find($data['user_id']);
				if($boosttype == 'country'){
					$user_boost_update->country_boost_avaliable = $user_boost['country_boost_avaliable']+$num_of_boost;
					$user_boost_update->country_boost = $user_boost['country_boost']+$num_of_boost;

				}else if($boosttype == 'region'){
					$user_boost_update->region_boost_avaliable = $user_boost['region_boost_avaliable']+$num_of_boost;
					$user_boost_update->region_boost = $user_boost['region_boost']+$num_of_boost;

				}else if($boosttype == 'division'){
					$user_boost_update->division_boost_avaliable = $user_boost['division_boost_avaliable']+$num_of_boost;
					$user_boost_update->division_boost = $user_boost['division_boost']+$num_of_boost;

				}else if($boosttype == 'state'){
					$user_boost_update->state_boost_avaliable = $user_boost['state_boost_avaliable']+$num_of_boost;
					$user_boost_update->state_boost = $user_boost['state_boost']+$num_of_boost;

				}
				*/
				//$user_boost_update->total_boost = $user_boost+$num_of_boost;

				$user_boost_update->country_boost_avaliable = $user_boost['country_boost_avaliable']+$country_boost_count;
				$user_boost_update->country_boost = $user_boost['country_boost']+$country_boost_count;

				$user_boost_update->region_boost_avaliable = $user_boost['region_boost_avaliable']+$region_boost_count;
				$user_boost_update->region_boost = $user_boost['region_boost']+$region_boost_count;

				$user_boost_update->division_boost_avaliable = $user_boost['division_boost_avaliable']+$division_boost_count;
				$user_boost_update->division_boost = $user_boost['division_boost']+$division_boost_count;

				$user_boost_update->state_boost_avaliable = $user_boost['state_boost_avaliable']+$state_boost_count;
				$user_boost_update->state_boost = $user_boost['state_boost']+$state_boost_count;

				$user_boost_update->save();
				//****** boost update end ************//
				
				$response =  Api::make_response(1,['plan'=>$plan_dtails,'userboost'=>$userboost,'transaction'=>$transaction,'user_card_list'=>$userCardlist],'Plans get successfully');
				}else{
				$response =  Api::make_response(0,[],'Plans not found');		
				}
			}

		}
		return $response;  

	}

    public function subscription(Request $request){
	 	$data = $request->json()->all();
	 	$response =  [];
		$userCardlist = [];

	 	if($data['plan_id']!='' && $data['user_id']!=''){
	 		$plan_dtails = Plans::find($data['plan_id']);
			$boost_for_location = $plan_dtails->boost_for_location;
			$country_boost = ($plan_dtails->allow_boost_count) ? $plan_dtails->allow_boost_count : 0;
			$region_boost = ($plan_dtails->allow_boost_count_region) ? $plan_dtails->allow_boost_count_region : 0;
			$division_boost = ($plan_dtails->allow_boost_count_division) ? $plan_dtails->allow_boost_count_division : 0;
			$state_boost = ($plan_dtails->allow_boost_count_state) ? $plan_dtails->allow_boost_count_state : 0;
			
			$price = $plan_dtails->price;
			$plantype = $plan_dtails->type;
	 		$user_id = $data['user_id'];
	 		$expired_date = date('Y-m-d', strtotime("+".$plan_dtails['duration']." days"));

	 		$checksubscription = Subscription::where('user_id',$data['user_id'])->first();
			
			
			/******* 20-07-2017 added for Subcription history start ****/
			$usersubcription = new UserSubcriptionHistory;
			$usersubcription->user_id = $data['user_id'];
			$usersubcription->package_id = $data['plan_id'];
			$usersubcription->save();
			/******* 20-07-2017 added for Subcription history end ****/

			//echo json_encode($card_details);die;
			if($plantype != "Free"){
				
				if($data['plan_id']!='' && $data['user_id']!='' && $data['save_card']=='1'){
					
					$checkUserCard = UserCard::where('user_id',$data['user_id'])->where('card_no',$data['credit_card']['card_number'])->first();
					//echo json_encode($checkUserCard);die;
					if(!empty($checkUserCard)){
						$instertUserCard = UserCard::find($checkUserCard->id);
						$instertUserCard->user_id = $data['user_id'];
						$instertUserCard->card_type = $data['credit_card']['type'];
						$instertUserCard->card_exp_date = $data['credit_card']['exp_date'];
						$instertUserCard->cardholder_name = $data['credit_card']['cardholder_name'];			
						$instertUserCard->card_no = $data['credit_card']['card_number'];			
						$instertUserCard->status = 1;
						$instertUserCard->save();
						
					}else{
		
						$instertUserCard = new UserCard;
						$instertUserCard->user_id = $data['user_id'];
						$instertUserCard->card_type = $data['credit_card']['type'];
						$instertUserCard->card_exp_date = $data['credit_card']['exp_date'];
						$instertUserCard->cardholder_name = $data['credit_card']['cardholder_name'];			
						$instertUserCard->card_no = $data['credit_card']['card_number'];			
						$instertUserCard->status = 1;
						$instertUserCard->save();
					}
				}
			
				
			$card_details = array("amount"=> $price*100,
									"card_number"=> $data['credit_card']['card_number'],
									"card_type"=> $data['credit_card']['type'],
									"card_holder_name"=> $data['credit_card']['cardholder_name'],
									"card_cvv"=> $data['credit_card']['cvv'],
									"card_expiry"=> $data['credit_card']['exp_date'],
									"merchant_ref"=> "Astonishing-Sale",
									"currency_code"=> "USD",
									"method"=> "credit_card");
			/*
				$transaction = json_decode(Payeezy::purchase($card_details));
				
		//echo json_encode($transaction);die;
				
			if($transaction->code != ''){
				$response =  Api::make_response(0,$transaction->code,$transaction->message);
			}else{
				
				
			if($transaction->transaction_status == "approved"){
				*/
			
			$transaction=array("transaction_status"=>"approved");
			
				$userCardlist = UserCard::where('user_id',$data['user_id'])->get();
				
				
					$instUserTransaction = new UserTransaction;
					$instUserTransaction->user_id = $data['user_id'];
					$instUserTransaction->transaction_details = json_encode($transaction);
					$instUserTransaction->transaction_for = "Plan Subscription";
					$instUserTransaction->amount = $price;
					$instUserTransaction->save();
				
					Email::sendsubScriptionMail($data['user_id']);	
					if(!empty($checksubscription)){
		
						//echo $checksubscription['id'];die;
						$addsubscription = Subscription::find($checksubscription['id']);
						$addsubscription->user_id = $user_id;
						$addsubscription->package_id = $data['plan_id'];
						$addsubscription->package_valide_from = date('Y-m-d');
						$addsubscription->package_valide_to = $expired_date;
						$addsubscription->is_active = 1;
						if($addsubscription->save()){
							//****** boost update start ************//
							/*
							$user_boost = User::where("id",$data['user_id'])->first()->total_boost;
							$user_boost_update = User::find($data['user_id']);
							$user_boost_update->total_boost = $user_boost+$allow_boost_count;
							$user_boost_update->save(); // boost_for_location
							*/
							$user_boost = User::where("id",$data['user_id'])->first();

							$user_boost_update = User::find($data['user_id']);
							if($plan_dtails->allow_boost_count){
								$user_boost_update->country_boost_avaliable = $user_boost['country_boost_avaliable']+$country_boost;
								$user_boost_update->country_boost = $user_boost['country_boost']+$country_boost;

							}
							if($plan_dtails->allow_boost_count_region){
								$user_boost_update->region_boost_avaliable = $user_boost['region_boost_avaliable']+$region_boost;
								$user_boost_update->region_boost = $user_boost['region_boost']+$region_boost;

							}
							if($plan_dtails->allow_boost_count_division){
								$user_boost_update->division_boost_avaliable = $user_boost['division_boost_avaliable']+$division_boost;
								$user_boost_update->division_boost = $user_boost['division_boost']+$division_boost;

							}
							if($plan_dtails->allow_boost_count_state){
								$user_boost_update->state_boost_avaliable = $user_boost['state_boost_avaliable']+$state_boost;
								$user_boost_update->state_boost = $user_boost['state_boost']+$state_boost;

							}
							$user_boost_update->save();


							//****** boost update end ************//
							$response =  Api::make_response(1,array('plan'=>$plan_dtails,'transaction'=>$transaction,'user_card_list'=>$userCardlist),'Plans details');
						}else{
							$response =  Api::make_response(0,[],'Somethings wrong');
						}
		
					}else{
						$addsubscription = new Subscription;
						$addsubscription->user_id = $user_id;
						$addsubscription->package_id = $data['plan_id'];
						$addsubscription->package_valide_from = date('Y-m-d');
						$addsubscription->package_valide_to = $expired_date;
						$addsubscription->is_active = 1;
						if($addsubscription->save()){
							//****** boost update start ************//

							$user_boost = User::where("id",$data['user_id'])->first()->total_boost;
							$user_boost_update = User::find($data['user_id']);
							$user_boost_update->total_boost = $user_boost+$country_boost+$region_boost+$division_boost+$state_boost;
							$user_boost_update->save();
							//****** boost update end ************//
							$response =  Api::make_response(1,array('plan'=>$plan_dtails,'transaction'=>$transaction,'user_card_list'=>$userCardlist),'Plans details');
						}else{
							$response =  Api::make_response(0,[],'Somethings wrong');
						}
					}
					
					/*
				}else{
					$response =  Api::make_response(0,$transaction,'Transaction error');
				}
			}*/
				
				//$response =  Api::make_response(0,$instertUserCard,'Transaction error');
				
			}else{
					
				if($plantype == "Free"){
					
					$userCardlist = UserCard :: where('user_id',$data['user_id'])->get();
					
					Email::sendsubScriptionMail($data['user_id']);	
					if(!empty($checksubscription)){
						//echo $checksubscription['id'];die;
						$addsubscription = Subscription::find($checksubscription['id']);
						$addsubscription->user_id = $user_id;
						$addsubscription->package_id = $data['plan_id'];
						$addsubscription->package_valide_from = date('Y-m-d');
						$addsubscription->package_valide_to = $expired_date;
						$addsubscription->is_active = 1;
						if($addsubscription->save()){
							$response =  Api::make_response(1,array('plan'=>$plan_dtails,'user_card_list'=>$userCardlist),'Plans details');
						}else{
							$response =  Api::make_response(0,[],'Somethings wrong');
						}
		
					}else{
						$addsubscription = new Subscription;
						$addsubscription->user_id = $user_id;
						$addsubscription->package_id = $data['plan_id'];
						$addsubscription->package_valide_from = date('Y-m-d');
						$addsubscription->package_valide_to = $expired_date;
						$addsubscription->is_active = 1;
						if($addsubscription->save()){
							$response =  Api::make_response(1,array('plan'=>$plan_dtails,'user_card_list'=>$userCardlist),'Plans details');
						}else{
							$response =  Api::make_response(0,[],'Somethings wrong');
						}
					}
				}else{
					$response =  Api::make_response(0,$transaction,'Transaction error');
				}
				
				
			}
			

			//echo json_encode($transaction->transaction_status);die;

	 	}
	 	return $response;     
    }

	public function postyourboostold(Request $request){
	 	$data = $request->json()->all();
	 	$response =  [];
		$boostCount = User::where("id",$data['loginuserid'])->first()->total_boost;
		if($boostCount>0){
			$userUpdate = User::find($data['loginuserid']);
			$userUpdate->total_boost = $boostCount - 1;
			if($userUpdate->save()){
				$postLogstbl = PostLogs::where("post_id",$data['postid'])->get();
				foreach ($postLogstbl as $key => $value) {
					//$data = ['boost_order' => 1, "updated_at" =>date("Y-m-d H:i:s")];
					//$postLogsupdate = PostLogs::find($value['id'])->update($data);
					$postLogsupdate = PostLogs::find($value['id']);
					$postLogsupdate->boost_order = 1;
					$postLogsupdate->updated_at = date("Y-m-d H:i:s");
					$postLogsupdate->save();
				}
			}

			$response =  Api::make_response(1,[],'You have successfully boosted');

		}else{
			$response =  Api::make_response(0,[],'Sorry!!! You do not have any boost in your account');
		}
	   return $response; 
	}

// use App\PostBoost;
// use App\Country;
// use App\Region;
// use App\Devision;
// use App\State;
	public function postyourboost(Request $request){
	 	$data = $request->json()->all();
	 	$response =  [];
		if($data>0){
				
			if($data['boost_location']!='' && $data['boost_location_type']!='' && $data['postid']!='' && $data['loginuserid']!=''){
				
				$userfind = User::find($data['loginuserid']);
				if($data['boost_location_type'] == "Country"){

				//$stateslist = Country::with("regions.divisions.states")->where("slug",$data['boost_location'])->get();
				$sqlsatelist = "SELECT id FROM state WHERE division_id IN (SELECT id FROM division WHERE region_id IN(SELECT id FROM region WHERE country_id IN(SELECT id FROM country where slug = '".$data['boost_location']."' and is_active = 1) and is_active = 1) and is_active = 1)and is_active = 1";
				$userfind->decrement('country_boost_avaliable');

				}elseif ($data['boost_location_type'] == "Region") {
				$sqlsatelist = "SELECT id FROM state WHERE division_id IN (SELECT id FROM division WHERE region_id IN(SELECT id FROM region WHERE slug =  '".$data['boost_location']."' and is_active = 1)and is_active = 1) and is_active = 1";
				
				$userfind->decrement('region_boost_avaliable');

				}elseif ($data['boost_location_type'] == "Division") {
					$sqlsatelist = "SELECT id FROM state WHERE division_id IN (SELECT id FROM division WHERE slug ='".$data['boost_location']."' and is_active = 1) and is_active = 1";
				$userfind->decrement('division_boost_avaliable');
				}elseif ($data['boost_location_type'] == "State") {
					$sqlsatelist = "SELECT id FROM state WHERE slug ='".$data['boost_location']."' and is_active = 1";
				$userfind->decrement('state_boost_avaliable');
				}
				if($userfind->save()){
					$postUpdate = Posts::find($data['postid']);
					$postUpdate->is_boost = 1;
					$postUpdate->save();


				}
			}

			$stateslist =  DB::select($sqlsatelist);
			if(count($stateslist)){
				foreach ($stateslist as $key => $value) {

					$addpostBoost = new PostBoost;
					$addpostBoost->post_id = $data['postid'];
					$addpostBoost->user_id = $data['loginuserid'];
					$addpostBoost->boost_location_id = $value->id;
					$addpostBoost->save();
					//echo $value->id;
				}
			}

			$response =  Api::make_response(1,$userfind,'You have successfully boosted');

		}else{
			$response =  Api::make_response(0,[],'Sorry!!! You do not have any boost in your account');
		}
	   return $response; 
	}	

	public function postyourboostcheck(Request $request){
	 	$data = $request->json()->all();
	 	$response =  [];
		if($data>0){
			//$loginuser = User::find($data['loginuserid']);
			if($data['location_type'] == "Country"){
				$loginuser = User::find($data['loginuserid'])->country_boost_avaliable;
				if($loginuser < 0) {
					$loginuser = 0;	
				}
				$loginusertest = "You have ".$loginuser." Country boost avaliable";
				$type = "Country";
			}elseif($data['location_type'] == "Region"){
				$loginuser = User::find($data['loginuserid'])->region_boost_avaliable;
				if($loginuser < 0) {
					$loginuser = 0;	
				}
				$loginusertest = "You have ".$loginuser." Region boost avaliable";
				$type = "Region";
			}elseif($data['location_type'] == "Division"){
				$loginuser = User::find($data['loginuserid'])->division_boost_avaliable;
				if($loginuser < 0) {
					$loginuser = 0;	
				}
				$loginusertest = "You have ".$loginuser." Division boost avaliable";
				$type = "Division";
			}elseif($data['location_type'] == "State"){
				$loginuser = User::find($data['loginuserid'])->state_boost_avaliable;
				if($loginuser < 0) {
					$loginuser = 0;	
				}
				$loginusertest = "You have ".$loginuser." State boost avaliable";
				$type = "State";
			}
			//$loginuser = User::where('id',$data['loginuserid'])->where('')->first();

			$response =  Api::make_response(1,["userdtls"=>$loginuser, "type"=>$type, "boosttext"=>$loginusertest],'You have successfully boosted');

		}else{
			$response =  Api::make_response(0,[],'Sorry!!! You do not have any boost in your account');
		}
	   return $response; 
	}

	public function bidwinpayment(Request $request){
		$data = $request->json()->all();
		$response =  [];
	   	$userCardlist = [];

		if($data['plan_id']!='' && $data['user_id']!=''){
			$plan_dtails = AdBid::find($data['plan_id']);

			$price = $plan_dtails->bid_amount;
			$user_id = $data['user_id'];

			   
			if($data['plan_id']!='' && $data['user_id']!='' && $data['save_card']=='1'){
				
				$checkUserCard = UserCard::where('user_id',$data['user_id'])->where('card_no',$data['credit_card']['card_number'])->first();
				if(!empty($checkUserCard)){
					$instertUserCard = UserCard::find($checkUserCard->id);
					$instertUserCard->user_id = $data['user_id'];
					$instertUserCard->card_type = $data['credit_card']['type'];
					$instertUserCard->card_exp_date = $data['credit_card']['exp_date'];
					$instertUserCard->cardholder_name = $data['credit_card']['cardholder_name'];			
					$instertUserCard->card_no = $data['credit_card']['card_number'];			
					$instertUserCard->status = 1;
					$instertUserCard->save();
					
				}else{
	
					$instertUserCard = new UserCard;
					$instertUserCard->user_id = $data['user_id'];
					$instertUserCard->card_type = $data['credit_card']['type'];
					$instertUserCard->card_exp_date = $data['credit_card']['exp_date'];
					$instertUserCard->cardholder_name = $data['credit_card']['cardholder_name'];			
					$instertUserCard->card_no = $data['credit_card']['card_number'];			
					$instertUserCard->status = 1;
					$instertUserCard->save();
				}
			}
		
			
			$card_details = array("amount"=> $price*100,
								"card_number"=> $data['credit_card']['card_number'],
								"card_type"=> $data['credit_card']['type'],
								"card_holder_name"=> $data['credit_card']['cardholder_name'],
								"card_cvv"=> $data['credit_card']['cvv'],
								"card_expiry"=> $data['credit_card']['exp_date'],
								"merchant_ref"=> "Astonishing-Sale",
								"currency_code"=> "USD",
								"method"=> "credit_card");

			$transaction=array("transaction_status"=>"approved");
		
			$userCardlist = UserCard::where('user_id',$data['user_id'])->get();
			
			// echo json_encode($userCardlist);
			// die;
			$instUserTransaction = new UserTransaction;
			$instUserTransaction->user_id = $data['user_id'];
			$instUserTransaction->transaction_details = json_encode($transaction);
			$instUserTransaction->transaction_for = "Bid Payment";
			$instUserTransaction->amount = $price;
			$instUserTransaction->save();
			
			Email::sendsubScriptionMail($data['user_id']);	

			$AdBid_update = AdBid::with('state')->find($data['plan_id']);

			if($transaction['transaction_status'] == "approved"){
				if(count($AdBid_update)>0){
					$AdBid_update->status = 2;
					$AdBid_update->is_msg_sent = 1;
					if($AdBid_update->save()){
						$checknotificationLog = NotificationLog::where('adbid_id',$data['plan_id'])->first();
						if(count($checknotificationLog)>0) {
							$dltNotificationLog = NotificationLog::find($checknotificationLog['id']);
							$dltNotificationLog->delete();
							/*find state name*/
							$sate_name = isset($AdBid_update['state']->name) ? $AdBid_update['state']->name : '';

							$notificationLog = new NotificationLog;
							$notificationLog->activity_by = $data['user_id'];
							$notificationLog->visible_by = $data['user_id'];
							$notificationLog->activity_type = "bid_win";
							$notificationLog->notification_text = "You have won a Bid Location : $sate_name ".date('jS M, Y',strtotime($AdBid_update->publish_date))." (".date('ga',strtotime($AdBid_update->from_time))." - ".date('ga',strtotime($AdBid_update->to_time)).")";;
							$notificationLog->msg_text = "Your payment has been successfully made. You have won the Bid for the time slot of ".date('ga',strtotime($AdBid_update->from_time))." - ".date('ga',strtotime($AdBid_update->to_time))." for the date of ".date('jS M, Y',strtotime($AdBid_update->publish_date))." for the location $sate_name";
							$notificationLog->adbid_id = "";
							$notificationLog->save();
						}

						$response =  Api::make_response(1,array('transaction'=>$transaction,'user_card_list'=>$userCardlist),'Payment successfully');
						
					}else{
						$response =  Api::make_response(0,[],'Somethings wrong');
						
					}
				}
			}else{
				$response =  Api::make_response(0,[],'Somethings wrong');
			}
		}
		return $response;     
   }
   public function getcurrentboostedlocation(Request $request){
	$data = $request->json()->all();
	$response =  [];
	if($data['userid']!=''){
		$getUser = AccountSettings::where('user_id',$data['userid'])->first();
		if(count($getUser)>0){
			$response =  Api::make_response(1,$getUser,'User Boost');
		}else{
			$response =  Api::make_response(0,[],'Somethings wrong');
		}
	}else{
		$response =  Api::make_response(0,[],'Somethings wrong');
		
	}

	return $response;     
   }
}
