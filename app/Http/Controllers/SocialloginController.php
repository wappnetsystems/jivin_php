<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Api;
use Laravel\Socialite\Facades\Socialite;

class SocialloginController extends Controller {

    public $linkedin_return_url = "https://www.jivin.com/development/jivin/services/public/linkedin_auth";
    public $login_linkedin_return_url = "https://www.jivin.com/development/jivin/services/public/login_linkedin_auth";
    public $instagram_return_url = "https://www.jivin.com/development/jivin/services/public/instagram_auth";
    public $login_instagram_return_url = "https://www.jivin.com/development/jivin/services/public/login_instagram_auth";
    public $twitter_return_url = "https://www.jivin.com/development/jivin/services/public/twitter_auth";
    public $snapchat_return_url = "https://www.jivin.com/development/jivin/services/public/snapchat_auth";

    public function linkedin_login($flag = '') {
        $client_id = "81lq3vo74v13dv";
        if ($flag == '') {
            return redirect()->to("https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id={$client_id}&redirect_uri={$this->linkedin_return_url}&state=fooobar&scope=r_liteprofile%20r_emailaddress%20w_member_social");
        } else {
            return redirect()->to("https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id={$client_id}&redirect_uri={$this->login_linkedin_return_url}&state=fooobar&scope=r_liteprofile%20r_emailaddress%20w_member_social");
        }
    }

    public function linkedin_auth() {
        if (isset($_GET['code'])) {
            return view('sociallogin.linkedin_auth', ['auth_code' => $_GET['code'], 'status' => true]);
        } else {
            return view('sociallogin.linkedin_auth', ['status' => false]);
        }
    }

    public function login_linkedin_auth() {
        if (isset($_GET['code'])) {
            return view('sociallogin.login_linkedin_auth', ['auth_code' => $_GET['code'], 'status' => true]);
        } else {
            return view('sociallogin.login_linkedin_auth', ['status' => false]);
        }
    }

    public function get_access_token_linkedin(Request $request) {

        $linked_in_arr = [
            'grant_type' => $request->input('grant_type'),
            'code' => $request->input('code'),
            'redirect_uri'=> $this->linkedin_return_url,
            'client_id' => $request->input('client_id'),
            'client_secret' => $request->input('client_secret'),
        ];
        if($request->input('flag')){
            $linked_in_arr['redirect_uri']= $this->login_linkedin_return_url;
        }
        

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.linkedin.com/oauth/v2/accessToken",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POSTFIELDS => http_build_query($linked_in_arr),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                "content-type:  application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return Api::make_response(0, [], "Error Occurred In API call.");
        } else {
            $result = json_decode($response, true);
            return Api::make_response(1, $result, "");
        }
    }

    public function get_user_profile_linkedin(Request $request) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer {$request->input('access_token')}",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return Api::make_response(0, [], "Error Occurred In API call.");
        } else {
            $result = json_decode($response, true);
            return Api::make_response(1, $result, "");
        }
    }

    public function get_user_email_linkedin(Request $request) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer {$request->input('access_token')}",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return Api::make_response(0, [], "Error Occurred In API call.");
        } else {
            $result = json_decode($response, true);
            return Api::make_response(1, $result, "");
        }
    }

    public function instagram_login($flag = '') {
        $client_id = "2fb5b86517134bd7a9a0cb8e0d8e2792";
        if ($flag == '') {
            return redirect()->to('https://api.instagram.com/oauth/authorize/?client_id=' . $client_id . '&redirect_uri=' . $this->instagram_return_url . '&response_type=code');
        } else {
            return redirect()->to('https://api.instagram.com/oauth/authorize/?client_id=' . $client_id . '&redirect_uri=' . $this->login_instagram_return_url . '&response_type=code');
        }
    }

    public function login_instagram_auth() {
        if (isset($_GET['code'])) {

            return view('sociallogin.login_instagram_auth', ['auth_code' => $_GET['code'], 'status' => true]);
        } else {
            return view('sociallogin.login_instagram_auth', ['status' => false]);
        }
    }

    public function instagram_auth() {

        if (isset($_GET['code'])) {

            return view('sociallogin.instagram_auth', ['auth_code' => $_GET['code'], 'status' => true]);
        } else {
            return view('sociallogin.instagram_auth', ['status' => false]);
        }
    }

    public function get_access_token_instagram(Request $request) {
        $linked_in_arr = [
            'grant_type' => $request->input('grant_type'),
            'code' => $request->input('code'),
            'redirect_uri' => $this->instagram_return_url,
            'client_id' => $request->input('client_id'),
            'client_secret' => $request->input('client_secret'),
        ];
        if($request->input('flag')){
            $linked_in_arr['redirect_uri']= $this->login_instagram_return_url;
        }
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.instagram.com/oauth/access_token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POSTFIELDS => http_build_query($linked_in_arr),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                "content-type:  application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return Api::make_response(0, [], "Error Occurred In API call.");
        } else {
            $result = json_decode($response, true);
            return Api::make_response(1, $result, "");
        }
    }

    public function get_access_token_twitter($flag = '') {
        if ($flag == '') {
            return Socialite::driver('twitter')->redirect();
        } else {
            session('login_redriect',1);
            return Socialite::driver('twitter')->redirect();
        }
        $twitter_arr = [
            'grant_type' => 'client_credentials',
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.twitter.com/oauth2/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POSTFIELDS => http_build_query($twitter_arr),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                "content-type:  application/x-www-form-urlencoded",
                "Authorization: Basic " . base64_encode("EOoOAJsMuX2DvR3kbewLAowuk:MzPBmAJYPNzLP4jz347OBKMuaPBIddEoepXkhWjx69YM2iVW6Z")
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return Api::make_response(0, [], "Error Occurred In API call.");
        } else {
            $result = json_decode($response, true);
            print_r($result);
            die();
            return Api::make_response(1, $result, "");
        }
    }

    public function twitter_auth() {
        $user_detail = Socialite::driver('twitter')->user();

        if ($user_detail->id) {
            $id = $user_detail->id;
            $email = $user_detail->email;
            $profile_image = $user_detail->avatar_original;
            $background_image = $user_detail->user['profile_banner_url'];
            $temp_id = time() . $id;
            $twitter_arr = [
                'twitter_id' => $id,
                'profile_image' => $profile_image,
                'banner_image' => $background_image,
                'temp_id' => $temp_id,
                'email' => $email,
                'name' => $user_detail->name
            ];
            \App\Twitter_temp::insert($twitter_arr);
            if(session('login_redriect')){
            return view('sociallogin.twitter_auth', ['temp_id' => $temp_id, 'status' => true]);
            }
            else{
                return view('sociallogin.login_twitter_auth', ['temp_id' => $temp_id, 'status' => true]);
            }
        } else {
            return view('sociallogin.twitter_auth', ['status' => false]);
        }
    }

    public function login_twitter_auth() {
        $user_detail = Socialite::driver('twitter')->user();

        if ($user_detail->id) {
            $id = $user_detail->id;
            $email = $user_detail->email;
            $profile_image = $user_detail->avatar_original;
            $background_image = $user_detail->user['profile_banner_url'];
            $temp_id = time() . $id;
            $twitter_arr = [
                'twitter_id' => $id,
                'profile_image' => $profile_image,
                'banner_image' => $background_image,
                'temp_id' => $temp_id,
                'email' => $email,
                'name' => $user_detail->name
            ];
            \App\Twitter_temp::insert($twitter_arr);

            return view('sociallogin.login_twitter_auth', ['temp_id' => $temp_id, 'status' => true]);
        } else {
            return view('sociallogin.login_twitter_auth', ['status' => false]);
        }
    }

    public function get_user_profile_twitter(Request $request) {
        $temp_id = $request->input('temp_id');

        $temp_user = \App\Twitter_temp::where('temp_id', $temp_id)->get();
        $response_data = ['email' => $temp_user[0]->email, 'twitter_id' => $temp_user[0]->twitter_id,
            'profile_image' => $temp_user[0]->profile_image, 'banner_image' => $temp_user[0]->banner_image, 'name' => $temp_user[0]->name
        ];
        return Api::make_response(1, $response_data, "");
    }

    public function signup_snapchat() {
        return view('sociallogin.signup_snapchat');
    }

}
