<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Plans;
use App\Subscription;

class AdminPlanController extends Controller
{
    /* 
      Author : Debasis Chakraborty

      This Contoller is for Manage Membership plans 
    */
    public function __construct(){
    	$this->middleware('admin');
    }
	
      # To List Interest Category
      public function listPlans(){
        return view('admin.Plan.index');
      }
      public function postAddNewPlan(Request $request){
      	// dd($request->all());
      	if ($request->id) {
      		$plan = Plans::where('id','=',$request->id)->first();
	      	if (Plans::where('plan_name','=',$request->plan_name)->where('id','!=',$request->id)->first()) {
	      		return redirect('admin/memership_plans/list')->with('alert-danger', 'Plan Already Exists');
	      	}else{
		      	$plan->plan_name = $request->plan_name;
		      	$plan->type = $request->plan_type;
		      	if ($request->plan_type == "Paid") {
			      	$plan->duration = $request->plan_duration;
			      	$plan->allow_boost = $request->boost_allowed;
			      	$loc_arr = ['Country','Region','Division','State'];
			      	$diff_arr = array_diff($loc_arr, $request->boost_for_location);

			      	if ($request->boost_allowed == 1) {

			      		if(count($request->boost_for_location)>0) {

			      			foreach ($request->boost_for_location as $key => $eachBoost) {

			      				switch ($eachBoost) {
			      					case 'Country':
			      						$plan->allow_boost_count = $request->boost_count[$key];
			      						break;
			      					case 'Region':
			      						$plan->allow_boost_count_region = $request->boost_count[$key];
			      						break;
			      					case 'Division':
			      						$plan->allow_boost_count_division = $request->boost_count[$key];
			      						break;
			      					case 'State':
			      						$plan->allow_boost_count_state = $request->boost_count[$key];
			      						break;
			      				}
			      			}	

			      			foreach ($diff_arr as $eachDiffArr) {
			      				switch ($eachDiffArr) {
			      					case 'Country':
			      						$plan->allow_boost_count = NULL;
			      						break;
			      					case 'Region':
			      						$plan->allow_boost_count_region = NULL;
			      						break;
			      					case 'Division':
			      						$plan->allow_boost_count_division = NULL;
			      						break;
			      					case 'State':
			      						$plan->allow_boost_count_state = NULL;
			      						break;
			      				}
			      			}

			      		}else {
			      			$plan->allow_boost_count = 0;
			      			$plan->allow_boost_count_region = 0;
			      			$plan->allow_boost_count_division = 0;
			      			$plan->allow_boost_count_state = 0;
			      			$plan->boost_for_location = NULL;
			      		}

				      	$plan->allow_boost_count_per_month_day = $request->boost_count_duration_type;
				      	

			      	}else{
						$plan->allow_boost_count = 0;
						$plan->allow_boost_count_region = 0;
			      		$plan->allow_boost_count_division = 0;
			      		$plan->allow_boost_count_state = 0;
						$plan->allow_boost_count_per_month_day = "";
						$plan->boost_for_location = "";
					  }
			      	$plan->allow_advertisement = $request->add_post_allowed;
			      	$plan->price = $request->plan_price;
		      	}
		      	$plan->is_active = $request->is_active;

		      	if ($plan->save()) {
		      		return redirect('admin/memership_plans/list')->with('alert-success', 'Plan Updated!');
		      	}else{
	      		   return redirect('admin/memership_plans/list')->with('alert-danger', 'Plan updation faild');	      		
		      	}
	      	}      		
      	}else{
	      	$plan = new Plans;
	      	if ($plan::where('plan_name','=',$request->plan_name)->first()) {
	      		return redirect('admin/memership_plans/list')->with('alert-danger', 'Plan Already Exists');
	      	}else{
		      	$plan->plan_name = $request->plan_name;
		      	$plan->type = $request->plan_type;

		      	if ($request->plan_type == "Paid") {

			      	$plan->duration = $request->plan_duration;
			      	$plan->allow_boost = $request->boost_allowed;

			      	if ($request->boost_allowed == 1) {
				      	if(count($request->boost_for_location)>0) {

				      		foreach ($request->boost_for_location as $key => $eachBoost) {

				      			switch ($eachBoost) {
				      				case 'Country':
				      					$plan->allow_boost_count = $request->boost_count[$key];
				      					break;
				      				case 'Region':
				      					$plan->allow_boost_count_region = $request->boost_count[$key];
				      					break;
				      				case 'Division':
				      					$plan->allow_boost_count_division = $request->boost_count[$key];
				      					break;
				      				case 'State':
				      					$plan->allow_boost_count_state = $request->boost_count[$key];
				      					break;
				      			}
				      		}	

				      	}else {
				      		$plan->allow_boost_count = 0;
				      		$plan->allow_boost_count_region = 0;
				      		$plan->allow_boost_count_division = 0;
				      		$plan->allow_boost_count_state = 0;
				      		$plan->boost_for_location = NULL;
				      	}

				      	$plan->allow_boost_count_per_month_day = $request->boost_count_duration_type;
			      	}

			      	$plan->allow_advertisement = $request->add_post_allowed;
			      	$plan->price = $request->plan_price;
		      	}
		      	$plan->is_active = $request->is_active;

		      	if ($plan->save()) {
		      		return redirect('admin/memership_plans/list')->with('alert-success', 'New Plan Created!');
		      	}else{
	      		   return redirect('admin/memership_plans/list')->with('alert-danger', 'Plan Creation faild');	      		
		      	}
	      	}
        }
      }
		public function updateStatus(Request $request){

		$subscriptionCount = Subscription::where('package_id',$request->id)->count();
			if($subscriptionCount>0){
				return 'false';	
			}else{
				$plan = Plans::where('id','=',$request->id)->first();
				$plan->is_active = $request->status;
				if ($plan->save()) {
					return 'true';
				}else{
				return 'false';	      		
				}
			}

		
		}
      public function delete(Request $request){
      	// $interestCategory = InterestCategory::where('id','=',$request->id)->first();
      	// if ($interestCategory->delete()) {
      	// 	return 'true';
      	// }else{
  		   // return 'false';	      		
      	// }      	
      }      
      public function getList(){
      	    $output['aaData'] = [];
			$aColumns = array(
				                 'id'
			                    ,'plan_name'
			                    ,'type'
			                    ,'duration'
			                    ,'price'
			                    ,'allow_boost_count'
			                    ,'allow_boost_count_region'
			                    ,'allow_boost_count_division'
			                    ,'allow_boost_count_state'
			                    ,'allow_boost'
			                    ,'allow_boost_count_per_month_day'
			                    ,'allow_advertisement'
			                    ,'is_active'
								,'subscription_count'
								
							);
			$plans = Plans::get()->toArray();
			foreach ($plans as $key => $plan) {
				$row = array();
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					if($plan[ $aColumns[$i]]=== NULL){$plan[ $aColumns[$i]]= "";}
						$row[] = $plan[ $aColumns[$i] ];

				}
				$output['aaData'][] = $row;
			}
			$output = array(
			    "draw"            => intval(1),
			    "data"            => $output['aaData']
			); 	
      	return $output;
      }
}
