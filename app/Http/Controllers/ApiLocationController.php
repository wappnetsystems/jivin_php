<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\Http\Requests;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
//load Models
use App\Api;
use App\User;
use App\Country;
use App\Region;
use App\Devision;
use App\State;

class ApiLocationController extends ApiGuardController {

    protected $apiMethods = [
        'getCountryList' => [
            'keyAuthentication' => false
        ],
        'getRegions' => [
            'keyAuthentication' => false
        ],
        'getDivisions' => [
            'keyAuthentication' => false
        ],
        'getStates' => [
            'keyAuthentication' => false
        ],
        'saveUsCountry' => [
            'keyAuthentication' => false
        ],
        'getMapLocations' => [
            'keyAuthentication' => false
        ],
        'getMapLocationLeaderboard'=>[
            'keyAuthentication' => false
        ],
        'get_state_list'=>[
            'keyAuthentication' => false
        ]
    ];

    /*
      getCountryList
      Debasis Chakraborty
      20/3/2017
     */

    public function getCountryList(Request $request) {
        $data = $request->json()->all();
        $response = [];
        if ($countries = Country::where('is_active', 1)->get()) {
            $response = Api::make_response(1, $countries, count($countries) . ' Countries Found');
        } else {
            $response = Api::make_response(0, [], 'No Countries Found');
        }
        return $response;
    }

    /*
      getRegions
      Debasis Chakraborty
      20/3/2017
     */

    public function getRegions(Request $request) {
        $data = $request->json()->all();
        $response = [];
        if ($data) {
            if ($regions = Country::with('regions')->where('slug', '=', $data['country_slug'])->where('is_active', 1)->first()) {
                $response = Api::make_response(1, $regions, 'Regions Found');
            } else {
                $response = Api::make_response(0, [], 'No region found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param.');
        }
        return $response;
    }

    /*
      getDivisions
      Debasis Chakraborty
      20/3/2017
     */

    public function getDivisions(Request $request) {
        $data = $request->json()->all();
        $response = [];
        if ($data) {
            if ($divisions = Region::with('divisions')->where('slug', '=', $data['region_slug'])->where('is_active', 1)->first()) {
                $response = Api::make_response(1, $divisions, 'Division Found');
            } else {

                $response = Api::make_response(0, [], 'No Division found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param.');
        }
        return $response;
    }

    /*
      getStates
      Debasis Chakraborty
      20/3/2017
     */

    public function getStates(Request $request) {
        $data = $request->json()->all();
        $response = [];
        if ($data) {
            if ($states = Devision::with('states')->where('slug', '=', $data['division_slug'])->where('is_active', 1)->first()) {
                $response = Api::make_response(1, $states, 'States Found');
            } else {
                $response = Api::make_response(0, [], 'No State found');
            }
        } else {
            $response = Api::make_response(0, [], 'Invalide Param.');
        }
        return $response;
    }

    public function getMapLocations() {
        $response = [];
        $allCountry = Country::where('is_active', '=', 1)->get();
        foreach ($allCountry as $key => $country) {
            $getRegion = Region::where('is_active', '=', 1)->where('country_id', '=', $country->id)->get();
            $response[$country->slug] = ['id' => $country->id];
            foreach ($getRegion as $key => $region) {
                $response[$country->slug]['regions'][$region->slug] = ['id' => $region->id];
                //$response[$country->slug]['regions'][$region->slug] = ['poly' => $region->poly];
                $getDivision = Devision::where('is_active', '=', 1)->where('region_id', '=', $region->id)->get();
                foreach ($getDivision as $key => $division) {
                    $response[$country->slug]['regions'][$region->slug]['divisions'][$division->slug] = ['id' => $division->id];
                    $getStates = State::where('is_active', '=', 1)->where('division_id', '=', $division->id)->get();
                    foreach ($getStates as $key => $state) {
                        $response[$country->slug]['regions'][$region->slug]['divisions'][$division->slug]['states'][$state->name] = ['id' => $state->id, 'code' => $state->slug, 'poly' => $state->poly];
                    }
                }
            }
        }
        return response()->json($response);
    }
    
    public function getMapLocationLeaderboard(){
        $response = [];
        $allCountry = Country::where('is_active', '=', 1)->get();
        foreach ($allCountry as $key => $country) {
            $getRegion = Region::where('is_active', '=', 1)->where('country_id', '=', $country->id)->get();
            $response[$country->slug] = ['id' => $country->id];
            foreach ($getRegion as $key => $region) {
                //$response[$country->slug]['regions'][$region->slug] = [];
                $response[$country->slug]['regions'][$region->slug] = ['poly' => $region->poly,'id' => $region->id];
                $getDivision = Devision::where('is_active', '=', 1)->where('region_id', '=', $region->id)->get();
                foreach ($getDivision as $key => $division) {
                    //$response[$country->slug]['regions'][$region->slug]['divisions'][$division->slug] = [];
                    $response[$country->slug]['regions'][$region->slug]['divisions'][$division->slug] = ['poly' => $division->poly,'id' => $division->id];
                    $getStates = State::where('is_active', '=', 1)->where('division_id', '=', $division->id)->get();
                    foreach ($getStates as $key => $state) {
                        $response[$country->slug]['regions'][$region->slug]['divisions'][$division->slug]['states'][$state->name] = ['id' => $state->id, 'code' => $state->slug, 'poly' => $state->poly];
                    }
                }
            }
        }
        return response()->json($response);
    }

    public function saveUsCountry() {
        $alllocationJson = '{"US":{"regions":{"WEST":{"divisions":{"Pacific":{"id":65786,"states":{"California":{"id":454,"code":"CA","poly":"532,414,656,452,624,582,761,793,775,830,764,842,757,858,751,870,748,881,752,895,658,891,558,780,530,684,503,552,501,478"},"Oregon":{"id":454,"code":"OR","poly":"529,408,533,374,577,289,604,235,616,240,633,269,645,278,671,284,708,285,748,291,795,306,806,319,778,352,770,375,773,387,751,470,643,443"},"Washington":{"id":454,"code":"WA","poly":"602,222,608,194,610,161,619,136,678,129,829,176,796,300,742,284,720,282,701,281,681,280,656,271,634,263,633,248,620,237"},"Alaska":{"id":454,"code":"AK","poly":"292,394,310,571,335,568,349,583,369,575,430,629,444,639,446,669,421,662,387,613,368,613,339,603,307,592,273,583,258,571,250,571,244,579,236,588,218,595,163,636,123,660,78,668,68,658,111,638,135,620,142,609,112,600,96,588,101,572,82,562,77,543,77,528,90,511,104,498,118,495,121,485,105,485,87,469,84,451,86,442,101,437,116,435,124,441,120,419,107,402,112,390,125,385,137,369,158,362,177,362,198,365,247,380"},"Hawaii":{"id":454,"code":"HI","poly":"505,815,517,820,528,822,538,825,544,836,552,842,559,848,556,859,540,867,530,872,524,878,514,884,506,882,500,876,499,868,500,860,499,854,496,847,493,838,448,806,439,797,426,784,412,770,364,757,340,758,324,745,334,738,354,736,370,733,388,736,406,739,420,744,426,750,438,762,444,773,472,781,492,794,498,805"}}},"Mountain":{"id":545445,"states":{"Colorado":{"id":454,"code":"CO","poly":"996,572,1212,595,1204,761,1058,750,974,740"},"New_Mexico":{"id":454,"code":"NM","poly":"975,743,1172,763,1160,976,1032,968,1029,975,975,972,972,990,944,986,962,842"},"Arizona":{"id":454,"code":"AZ","poly":"741,907,756,897,752,883,751,874,760,861,779,836,766,797,778,749,792,751,806,713,969,743,938,982,874,978"},"Utah":{"id":454,"code":"UT","poly":"850,500,935,516,929,553,990,570,971,734,807,708"},"Nevada":{"id":454,"code":"NV","poly":"663,455,843,499,794,745,778,741,771,760,765,786,632,580"},"Wyoming":{"id":454,"code":"WY","poly":"960,390,1166,419,1148,581,937,553,959,413,960,400"},"Idaho":{"id":454,"code":"ID","poly":"835,178,851,184,842,228,844,254,863,289,873,303,868,312,863,328,858,343,872,349,881,346,888,367,890,377,897,394,909,403,931,403,954,409,938,511,848,495,758,473,776,397,777,369,794,348,809,323,802,301,817,238"},"Montana":{"id":454,"code":"MT","poly":"856,186,848,225,851,245,860,271,879,293,873,326,873,338,894,368,901,385,928,399,954,390,967,383,1164,410,1182,248,1002,220"}}}}},"MIDWEST":{"divisions":{"West_North_central":{"id":65786,"states":{"South_Dakota":{"id":215,"code":"SD","poly":"1258,330,1470,343,1473,346,1471,349,1467,351,1467,355,1469,358,1468,362,1472,365,1476,365,1476,444,1476,450,1473,452,1474,456,1476,460,1478,465,1476,470,1472,473,1472,478,1474,482,1476,485,1475,489,1471,489,1468,484,1464,481,1459,478,1454,478,1451,475,1448,472,1443,472,1438,472,1434,474,1430,475,1425,473,1421,470,1416,468,1410,468,1243,456"},"Kansas":{"id":454,"code":"KS","poly":"1295,590,1510,595,1523,600,1514,607,1521,620,1532,631,1532,711,1291,709"},"Nebraska":{"id":454,"code":"NE","poly":"1245,458,1412,469,1447,474,1475,494,1480,517,1492,541,1492,566,1504,583,1467,589,1296,584,1299,544,1238,533,1241,493"},"Iowa":{"id":454,"code":"IA","poly":"1478,449,1480,462,1476,476,1481,488,1486,502,1489,521,1497,533,1497,552,1500,563,1535,565,1625,563,1637,561,1644,551,1643,533,1662,516,1669,502,1659,488,1641,472,1632,457,1626,445,1581,445,1530,446,1500,446"},"Missouri":{"id":454,"code":"MO","poly":"1626,571,1636,600,1651,617,1661,633,1676,637,1670,657,1694,678,1703,698,1714,716,1703,733,1701,750,1692,742,1672,730,1615,735,1581,734,1539,735,1537,628,1518,610,1522,592,1564,569"},"Minnesota":{"id":454,"code":"MN","poly":"1461,211,1509,210,1547,224,1580,224,1601,235,1635,239,1670,247,1636,268,1606,293,1594,304,1594,328,1579,348,1580,386,1605,405,1630,426,1631,439,1478,440,1477,365,1468,353,1475,336,1477,320,1472,298,1465,259,1462,232"},"North_Dakota":{"id":454,"code":"ND","poly":"1271,198,1347,204,1456,209,1457,239,1460,264,1463,290,1466,319,1470,335,1414,333,1295,328,1259,322,1265,256"}}},"East_North_central":{"id":65786,"states":{"Illinois":{"id":215,"code":"IL","poly":"1661,483,1744,477,1753,496,1757,513,1761,561,1763,608,1765,634,1750,659,1749,685,1726,697,1707,699,1706,680,1685,668,1681,648,1684,633,1662,624,1645,604,1636,582,1644,565,1651,543,1647,528,1668,520,1675,507,1666,490"},"Wisconsin":{"id":215,"code":"WI","poly":"1598,308,1604,302,1616,305,1637,291,1638,308,1644,302,1669,296,1702,274,1725,259,1733,288,1763,299,1809,281,1833,285,1846,307,1828,316,1799,319,1779,329,1766,353,1749,392,1748,415,1748,452,1746,470,1662,478,1645,469,1642,449,1635,423,1615,403,1583,388,1589,361,1582,349,1591,336"},"Michigan":{"id":215,"code":"MI","poly":"1788,495,1797,478,1795,470,1795,458,1792,437,1784,424,1783,406,1788,380,1790,367,1800,357,1810,349,1817,344,1817,333,1828,323,1848,331,1871,343,1875,359,1877,379,1872,389,1863,410,1877,404,1894,392,1906,416,1914,443,1902,458,1892,473,1886,486,1841,493"},"Indiana":{"id":215,"code":"IN","poly":"1763,512,1789,502,1838,497,1849,573,1851,614,1857,620,1837,624,1834,636,1829,647,1821,655,1809,655,1790,661,1773,663,1762,655,1769,633,1766,616,1770,597,1766,559"},"Ohio":{"id":215,"code":"OH","poly":"1844,497,1859,604,1878,613,1895,620,1915,616,1934,622,1944,608,1952,596,1962,583,1976,571,1976,468,1958,478,1936,494,1907,499,1883,491"}}}}},"NORTHEAST":{"divisions":{"Middle_Atlantic":{"id":65786,"states":{"New_York":{"id":215,"code":"NY","poly":"2042,488,2060,470,2057,445,2076,432,2099,435,2136,416,2130,393,2151,352,2208,329,2214,349,2215,372,2223,380,2229,393,2234,415,2236,444,2239,472,2246,495,2238,507,2204,493,2191,478,2179,473,2044,499"},"New_Jersey":{"id":215,"code":"NJ","poly":"2206,498,2232,504,2237,514,2245,506,2264,500,2281,488,2299,490,2273,507,2248,522,2236,524,2242,535,2245,554,2240,570,2231,588,2228,598,2209,588,2197,574,2203,564,2214,557,2218,547,2203,532,2201,522,2204,507"},"Pennsylvania":{"id":215,"code":"PA","poly":"2038,495,2040,503,2180,477,2188,482,2192,492,2201,496,2198,509,2196,516,2192,525,2202,538,2213,544,2210,554,2207,561,2196,562,2186,568,2166,575,2103,586,2040,599,2030,546,2023,506"}}},"New_England":{"id":65786,"states":{"Connecticut":{"id":215,"code":"CT","poly":"2249,496,2266,484,2276,482,2284,477,2294,472,2288,450,2243,463"},"Rhode_Island":{"id":215,"code":"RI","poly":"2294,449,2305,448,2307,455,2308,464,2303,471,2296,459"},"Massachusetts":{"id":215,"code":"MA","poly":"2240,429,2240,445,2240,460,2302,442,2312,445,2318,455,2326,449,2336,451,2353,441,2344,425,2344,436,2335,440,2328,432,2320,425,2316,416,2324,408,2312,402,2300,410,2267,420"},"New_Hampshire":{"id":215,"code":"NH","poly":"2264,416,2286,410,2300,401,2308,395,2300,381,2270,301,2265,310,2267,329,2270,339,2258,351,2260,373,2260,395"},"Vermont":{"id":215,"code":"VT","poly":"2214,331,2260,319,2263,337,2252,349,2254,372,2256,400,2256,418,2240,424,2232,392,2221,377,2218,350"},"Maine":{"id":215,"code":"ME","poly":"2275,300,2282,247,2292,192,2321,185,2356,236,2384,268,2393,279,2381,303,2360,319,2340,348,2321,373,2313,386"}}}}},"SOUTH":{"divisions":{"West_South_central":{"id":65786,"states":{"Texas":{"id":215,"code":"TX","poly":"1268,843,1254,1036,1123,1028,1159,1076,1184,1123,1222,1154,1244,1160,1256,1134,1275,1132,1318,1147,1348,1201,1371,1231,1392,1275,1427,1295,1456,1300,1448,1268,1448,1250,1456,1225,1462,1214,1473,1199,1484,1193,1498,1182,1528,1164,1540,1156,1536,1135,1550,1130,1553,1148,1575,1136,1584,1117,1584,1082,1573,1048,1564,983,1532,963,1497,966,1476,964,1456,965,1439,964,1429,958,1417,953,1397,948,1379,943,1361,930,1362,848"},"Louisiana":{"id":215,"code":"LA","poly":"1572,999,1572,1033,1587,1059,1592,1084,1588,1107,1581,1132,1600,1120,1612,1135,1636,1139,1652,1128,1672,1138,1690,1152,1702,1148,1712,1152,1711,1139,1736,1148,1744,1160,1752,1156,1744,1144,1739,1135,1750,1131,1749,1120,1734,1116,1718,1116,1714,1108,1728,1101,1716,1084,1673,1084,1654,1080,1655,1063,1667,1038,1672,1024,1668,996"},"Arkansas":{"id":215,"code":"AR","poly":"1552,850,1559,968,1571,969,1572,992,1667,989,1665,962,1682,931,1696,901,1704,870,1694,883,1689,863,1693,844"},"Oklahoma":{"id":215,"code":"OK","poly":"1269,820,1268,833,1365,840,1368,924,1388,935,1409,948,1432,947,1440,961,1459,960,1480,953,1497,960,1512,956,1532,956,1551,966,1553,897,1543,851,1544,828,1404,823"}}},"East_South_central":{"id":65786,"states":{"Mississippi":{"id":215,"code":"MS","poly":"1738,1109,1752,1101,1775,1101,1768,1008,1771,908,1704,908,1696,922,1691,937,1679,954,1674,976,1675,1005,1676,1028,1670,1044,1665,1061,1660,1074,1728,1082,1731,1096"},"Alabama":{"id":215,"code":"AL","poly":"1777,906,1775,974,1774,1040,1780,1099,1788,1102,1795,1083,1804,1107,1809,1098,1801,1079,1806,1071,1894,1064,1897,1051,1892,1032,1894,1012,1888,993,1861,899"},"Tennessee":{"id":215,"code":"TN","poly":"1704,904,1904,889,1905,879,1916,866,1942,848,1956,836,1976,830,1987,814,1782,832,1772,831,1773,837,1724,842,1720,863,1713,880"},"Kentucky":{"id":215,"code":"KY","poly":"1738,833,1765,835,1774,825,1817,824,1880,816,1928,813,1944,798,1964,776,1956,763,1944,741,1930,732,1911,733,1885,723,1876,717,1875,730,1861,737,1853,747,1848,758,1836,769,1824,764,1809,772,1799,782,1786,776,1777,777,1769,787,1769,795,1756,802,1759,812,1739,809,1737,816"}}},"South_Atlantic":{"id":65786,"states":{"Florida":{"id":215,"code":"FL","poly":"1807,1077,1898,1068,1905,1082,1919,1078,2001,1077,2008,1085,2016,1076,2020,1066,2031,1077,2063,1138,2076,1158,2104,1215,2108,1280,2097,1301,2067,1305,2044,1272,2028,1249,2006,1220,1993,1193,1993,1168,1988,1142,1970,1132,1948,1111,1932,1108,1920,1118,1903,1122,1885,1109,1864,1103,1847,1100,1829,1100,1817,1104,1815,1088"},"Georgia":{"id":215,"code":"GA","poly":"1868,897,1896,991,1903,1008,1900,1030,1903,1052,1908,1075,2002,1072,2011,1079,2012,1064,2027,1055,2028,1028,2034,1015,2039,1006,2023,983,2007,968,1988,947,1968,925,1954,909,1940,899,1940,889"},"South_Carolina":{"id":215,"code":"SC","poly":"2041,999,2056,991,2078,971,2096,952,2115,914,2071,877,2032,882,2020,872,1975,878,1944,897,1964,908,1977,930,2004,946,2012,963,2029,980"},"North_Carolina":{"id":215,"code":"NC","poly":"1910,888,1947,884,2018,869,2036,876,2070,873,2116,908,2133,907,2172,867,2190,857,2200,833,2208,803,2192,783,2176,779,1996,810,1984,830,1967,840,1946,853,1928,866,1917,877"},"Virginia":{"id":215,"code":"VA","poly":"2184,772,2170,754,2164,733,2155,720,2136,712,2120,706,2124,688,2112,681,2101,672,2090,676,2080,677,2080,691,2074,701,2063,710,2048,715,2043,738,2033,751,2033,764,2018,770,2005,777,1989,783,1977,784,1960,784,1947,804,1937,812,2044,797"},"West_Virginia":{"id":215,"code":"WV","poly":"2190,692,2191,718,2191,745,2188,758,2179,740,2178,725,2170,705,2162,704,2154,709,2138,708,2127,700,2129,688,2122,676,2112,671,2104,662,2092,661,2067,664,2047,680,2045,665,2156,641,2161,653,2174,694"},"Delaware":{"id":216,"code":"DE","poly":"2165,648,2180,666,2190,680,2188,688,2176,691,2172,672,2168,660"}}}}}}}}';

        $alllocationArray = json_decode($alllocationJson);
        // dd($alllocationArray);
        foreach ($alllocationArray as $countrySlug => $country) {
            $getCountry = Country::where('slug', '=', $countrySlug)->first();
            if ($getCountry) {
                $countryId = $getCountry->id;
                foreach ($country->regions as $regionSlug => $region) {
                    //Save Region Details
                    $newRegion = new Region;
                    $newRegion->country_id = $countryId;
                    $newRegion->name = $regionSlug;
                    $newRegion->slug = $regionSlug;
                    $newRegion->is_active = 1;
                    $newRegion->save();
                    $regionId = $newRegion->id;
                    // dd($region);
                    
                    foreach ($region->divisions as $divisionSlug => $divisions) {

                        //Save Division Details
                        $division = new Devision;
                        $division->region_id = $regionId;
                        $division->name = $divisionSlug;
                        $division->slug = $divisionSlug;
                        $division->is_active = 1;
                        $division->save();
                        $divisionId = $division->id;

                        foreach ($divisions->states as $stateName => $stateDetais) {
                            $stateSlug = $stateDetais->code;
                            $statePoly = $stateDetais->poly;
                            //Save State Details
                            $state = new State;
                            $state->division_id = $divisionId;
                            $state->name = $stateName;
                            $state->slug = $stateSlug;
                            $state->poly = $statePoly;
                            $state->is_active = 1;
                            $state->save();
                        }
                    }
                }
            }
        }
    }
    
    public function get_state_list(){
        $state_list=State::where(['is_active'=>1])->get();
        return Api::make_response(1, $state_list, 'State List Found.');
    }

}
