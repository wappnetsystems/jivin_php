<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//Load Models
use App\SupportRequest;
use App\User;
use  App\SupportRequestAttachment;
 use Illuminate\Support\Facades\Input;
 use DB;
class AdminSupportRequestController extends Controller
{

    public function __construct(){
    	$this->middleware('admin');
    }
    public function listSupportyRequest(Request $request){
    	$getSupportRequestInboxCount = SupportRequest::where('is_deleted','=',0)
    	                                              ->where('is_seen','=',0)->count();
    	$getSupportRequestDeletedCount = SupportRequest::where('is_deleted','=',1)->count();
    	// if ($type =='inbox') {
   //                  $count=SupportRequest::where('is_deleted','=',0)->selectRaw('*, count(*)')->groupBy('subject');
   // dd($count);
    		$getSupportRequest = SupportRequest::where('is_deleted','=',0)->orderBy('id','DESC')->groupBy('ticket_no')->paginate(50);


    	// }
     //    else{
    	// 	$getSupportRequest = SupportRequest::where('is_deleted','=',1)->paginate(10);
    	// }
        //dd($getSupportRequest);

    	return view('admin.support_request.inbox')
    	           ->with('getSupportRequests',$getSupportRequest)
    	           ->with('getSupportRequestInboxCount',$getSupportRequestInboxCount)
    	           ->with('getSupportRequestDeletedCount',$getSupportRequestDeletedCount)
                  
                   ;
    }

     public function searchinbox(Request $request)
     {
            $getSupportRequestInboxCount = SupportRequest::where('is_deleted','=',0)
                                                      ->where('is_seen','=',0)->count();
        $getSupportRequestDeletedCount = SupportRequest::where('is_deleted','=',1)->count();
        // if ($type =='inbox') {
            $getSupportRequest = SupportRequest::where('is_deleted','=',0)->where('ticket_no','=',$request->search)->groupBy('ticket_no')->orderBy('id','DESC')->paginate(10);
            

        // }
     //    else{
        //  $getSupportRequest = SupportRequest::where('is_deleted','=',1)->paginate(10);
        // }
        //dd($getSupportRequest);

        return view('admin.support_request.searchinbox')
                   ->with('getSupportRequests',$getSupportRequest)
                   ->with('getSupportRequestInboxCount',$getSupportRequestInboxCount)
                   ->with('getSupportRequestDeletedCount',$getSupportRequestDeletedCount);
     }



  public function listSupportRequest(Request $request){
        $getSupportRequestInboxCount = SupportRequest::where('is_deleted','=',0)
                                                      ->where('is_seen','=',0)->count();

        $getSupportRequestDeletedCount = SupportRequest::where('is_deleted','=',1)->count();
        // if ($type =='inbox') {
        //     $getSupportRequest = SupportRequest::where('is_deleted','=',0)->paginate(10);
            

        // }else{
            $getSupportRequest = SupportRequest::where('is_deleted','=',1)->orderBy('id','DESC')->groupBy('ticket_no')->paginate(10);
        // }
      

        return view('admin.support_request.trash')
                   ->with('getSupportRequests',$getSupportRequest)
                   ->with('getSupportRequestInboxCount',$getSupportRequestInboxCount)
                   ->with('getSupportRequestDeletedCount',$getSupportRequestDeletedCount);
    }

  public function searchtrashbox(Request $request){
        $getSupportRequestInboxCount = SupportRequest::where('is_deleted','=',0)
                                                      ->where('is_seen','=',0)->count();

        $getSupportRequestDeletedCount = SupportRequest::where('is_deleted','=',1)->count();
        // if ($type =='inbox') {
        //     $getSupportRequest = SupportRequest::where('is_deleted','=',0)->paginate(10);
            

        // }else{
            $getSupportRequest = SupportRequest::where('is_deleted','=',1)->where('ticket_no','=',$request->search)->groupBy('ticket_no')->orderBy('id','DESC')->paginate(10);
        // }

        return view('admin.support_request.searchtrash')
                   ->with('getSupportRequests',$getSupportRequest)
                   ->with('getSupportRequestInboxCount',$getSupportRequestInboxCount)
                   ->with('getSupportRequestDeletedCount',$getSupportRequestDeletedCount);
    }


    /*public function DetailsSupportyRequest($ticket_no,$user_id,Request $request)
    {
       // $attachmentcount = SupportRequestAttachment::where('request_id','=',$id);
       //  $attachment = SupportRequestAttachment::where('request_id','=',$id)->pluck('file_name');
       //  $details=SupportRequest::find($id);
       //  $postype=$details->post_type;
       //  $username=$details->user_id;
       //  $user=User::find($username);

          $bothcomment=SupportRequest::with('attachments')->where('ticket_no','=',$ticket_no)->where('is_main','=',0)->get();
         $usercomment=SupportRequest::with('attachments')->where('ticket_no','=',$ticket_no)->where('is_main','=',1)->get();
         $imag=User::where('id','=',$user_id)->get();
         foreach($imag as $images)
         {
            $image=$images->user_image;

         }


        
         // dd($usercomment);

  //  foreach($bothcomment as $bothcomments)
  //  {
  
  //  //dd($bothcomments->attachments);
  //  foreach($bothcomments->attachments as $both)
  //  {
  //   dd($both->file_name);
  //  }
   
  // }



          foreach($usercomment as $usercomments)

          {
            $countusercomment=count($usercomments->attachments);
          }
            foreach($usercomment as $$usercomments)

          {
            $userask=$usercomments->attachments;
          }
            foreach(   $bothcomment as    $bothcomments)
            {
                foreach(   $bothcomments->attachments as $comment)
                {
                    $d[]=$comment->file_name;
                }

            }
           // dd($d);
            $replies=DB::table('support_request')->where('user_id','=',$user_id)->where('ticket_no',$ticket_no)->get();
             
               $count=DB::table('support_request')->where('user_id','=',$user_id)->where('ticket_no',$ticket_no)->where('is_main','=',1)->count();
         
               $user=User::find($user_id);
          
              // for($i=0;$i<$count;$i++)
              // { 
              //   //$d[]=$replies->id;
              //  $attachmentcount[]=SupportRequestAttachment::where('request_id','=',$replies[$i]->id)->count();  
              // $attachment[]=SupportRequestAttachment::where('request_id','=',$replies[$i]->id)->pluck('file_name');

              // }
              //$d=implode('', $attachmentcount);



//      for($i=0;$i<$attachmentcount;$i++)
// {
//              echo implode(' ',$attachment[$i]);
//       }
//       die;
              //$count=count($attachment);
         
    	       $getSupportRequestInboxCount = SupportRequest::where('is_deleted','=',0)
    	                                              ->where('is_seen','=',0)->count();
    	      $getSupportRequestDeletedCount = SupportRequest::where('is_deleted','=',1)->count(); 
        // if($request->status=='')
        // {
        // $details->post_type=  $postype;
        // }
        // else{
        // $details->post_type=$request->status;
        //     }

        // $details->save();	



       $username=SupportRequest::with('attachments')->where('ticket_no','=',$ticket_no)->where('is_main','=',1)->get();


     foreach($username as $usernames)
     {
        $attach[]=$usernames->attachments;
     }
     // dd($attach);
     // foreach($attach as $key=>$val)
     // // {
     // //    dd($val->file_name);
     // }

      $postype=$replies[0]->post_type;
            if($request->status=='')
        {
            for($i=0;$i<$count;$i++)
            {
        $replies[$i]->post_type=  $postype;
             
        }
    }
        else{
    for($i=0;$i<$count;$i++)
            {
        $replies[$i]->post_type=  $request->status;
               

        }
            }

            foreach($replies as $reply)
            {
  SupportRequest::where('user_id','=',$user_id)->where('ticket_no',$ticket_no)
->update(['post_type' => $reply->post_type]);
            }

   
        return view('admin.support_request.details')
    	           ->with('getSupportRequestInboxCount',$getSupportRequestInboxCount)
    	         -> with('getSupportRequestDeletedCount',$getSupportRequestDeletedCount)
                   ->with('user',$user)
                   //->with('attachmentcount', $attachmentcount)
                   //->with('attchment', $attachment)
                    ->with('replies',$replies)
                    //->with('count',$count)
                    ->with('ticket_no',$ticket_no)
                    ->with('image',$image)
                    ->with('user_id',$user_id)
                    ->with('username',$username)
                    ->with('ticket_no',$ticket_no)
                    ->with('bothcomment', $bothcomment)
                    ->with('userask',$userask)
                    ->with('countusercomment',$countusercomment)

                   ;
                   
    }*/

  public function DetailsSupportyRequest($ticket_no,$user_id,Request $request) {

      $bothcomment=SupportRequest::with('attachments')
                                  ->where('ticket_no','=',$ticket_no)
                                  ->where('is_main','=',0)
                                  ->get();
      $usercomment=SupportRequest::with('attachments')
                                  ->where('ticket_no','=',$ticket_no)
                                  ->where('is_main','=',1)
                                  ->get();

      $imag=User::where('id','=',$user_id)->get();

      foreach($imag as $images) {
          $image=$images->user_image;
      }

      foreach($usercomment as $usercomments) {
          $countusercomment=count($usercomments->attachments);
      }
      foreach($usercomment as $$usercomments) {
          $userask=$usercomments->attachments;
      }
      foreach($bothcomment as $bothcomments) {

          foreach($bothcomments->attachments as $comment) {
              $d[]=$comment->file_name;
          }

      }
      $replies=DB::table('support_request')->where('user_id','=',$user_id)
                                            ->where('ticket_no',$ticket_no)
                                            ->get();

      $count=DB::table('support_request')->where('user_id','=',$user_id)
                                        ->where('ticket_no',$ticket_no)
                                        ->where('is_main','=',1)->count();

      $user=User::find($user_id);
             
      $getSupportRequestInboxCount = SupportRequest::where('is_deleted','=',0)
                                                    ->where('is_seen','=',0)
                                                    ->count();
      $getSupportRequestDeletedCount = SupportRequest::where('is_deleted','=',1)->count();

      $username=SupportRequest::with('attachments')->where('ticket_no','=',$ticket_no)->where('is_main','=',1)->get();

      foreach($username as $usernames) {
          $attach[]=$usernames->attachments;
      }

      $postype=$replies[0]->post_type;

      if($request->status=='') {
        for($i=0;$i<$count;$i++) {
            $replies[$i]->post_type = $postype;

        }
      }else{
        for($i=0;$i<$count;$i++) {
            $replies[$i]->post_type = $request->status;
        }
      }

      foreach($replies as $reply) {
                  SupportRequest::where('user_id','=',$user_id)
                    ->where('ticket_no',$ticket_no)
                    ->update(['post_type' => $reply->post_type]);
      }
      return view('admin.support_request.details')
      ->with('getSupportRequestInboxCount',$getSupportRequestInboxCount)
      ->with('getSupportRequestDeletedCount',$getSupportRequestDeletedCount)
      ->with('user',$user)
      ->with('replies',$replies)
      ->with('ticket_no',$ticket_no)
      ->with('image',$image)
      ->with('user_id',$user_id)
      ->with('username',$username)
      ->with('ticket_no',$ticket_no)
      ->with('bothcomment', $bothcomment)
      ->with('userask',$userask)
      ->with('countusercomment',$countusercomment);
                       
    }


    public function addreply($ticket_no,Request $request)
    {
        $too=SupportRequest::where('ticket_no','=',$ticket_no)->get();
       foreach($too as $key=> $val)
       {
               $d=$val->user_id;
       }
    $id=User::find($d);
    // dd($id->email);

        $message=$request->inputBody;
        $files=$request->file("files");
// dd($files);
           //  $files =$request->file('file');
           
                $file_count=count($files);
                $uploadcount=0;

                foreach ($files as $one)  {

                    $extension=$one->getClientOriginalExtension();
                    $filename = rand().time().".".$extension;
                    $image=$one->move(public_path('uploads'),$filename);

                    $uploadcount++;
                    $extensionname[]=$extension;
                    $listfilenames[] = $filename;

                }

                if(!empty($listfilenames))
                {
                  $list_count=1;
                 
                }
                // $email=User::select('email')->where('email','=',$to)->get();
                 //dd($email);
                 // if($email)
                 // {
                 //    $id=DB::table('users')->where('email','=',$to)->pluck('id');
                 //   //dd($id[0]);
                 // }
                 // else
                 // {
                 //    echo "not found";
                 // }
                // dd(strval($ticketno));

        if ($uploadcount == $file_count) {

              $admin='Admin';
              $support_request= SupportRequest::create(array(
                                  'ticket_no'=>$ticket_no,
                                  'user_id'=>$d,
                                  'user_asked'=>$message,
                                  'is_attachments'=>$list_count,
                                  'user_type'=>$admin,
                                  'is_main'=>0,
                                ));    
        }

             

        $support_request->save();


//         SupportRequest::where('user_id','=',$user_id)->where('ticket_no',$ticket_no)
// ->update(['post_type' => $reply->post_type]);
                 // SupportRequest::where('ticket_no','=',$ticket_no)->where('id','=',$support_request->id)->update('user_type','=','Admin');
        $requestid=DB::table('support_request')->where('ticket_no','=',$ticket_no)->pluck('id');
              
        for($i=0;$i<$file_count;$i++)
        {
        $support=SupportRequestAttachment::create(array(
        'request_id'=>$support_request->id,
        'file_name'=> $listfilenames[$i],
        'file_type'=> $extensionname[$i]
        ));
        }


       return redirect('admin/support_request/list/inbox');

        }

    




   public function AddSupportyRequest()
    {

        $getSupportRequestInboxCount = SupportRequest::where('is_deleted','=',0)
    	                                              ->where('is_seen','=',0)->count();
        $getSupportRequestDeletedCount = SupportRequest::where('is_deleted','=',1)->count();  
        $user=User::get();

        return view('admin.support_request.add')
    	    	   ->with('getSupportRequestInboxCount',$getSupportRequestInboxCount)
    	         ->with('getSupportRequestDeletedCount',$getSupportRequestDeletedCount)
               ->with('user',$user);


    }

public function delete($ticket_no)
     {

   //      $todelete=SupportRequest::where('ticket_no','=',$ticket);
   //      if($todelete)
   //      {
   //          $todelete->is_deleted=1;
   //      }
   // $postype=$replies[0]->post_type;
   //          if($request->status=='')
   //      {
   //          for($i=0;$i<$count;$i++)
   //          {
   //      $replies[$i]->post_type=  $postype;
             
   //      }
   //  }
   //      else{
   //  for($i=0;$i<$count;$i++)
   //          {
   //      $replies[$i]->post_type=  $request->status;
               

   //      }
   //          }


 SupportRequest::with('attachments')->where('ticket_no','=',$ticket_no)->update(array('is_deleted' => 1));






         // $todelete->save();
        return redirect('admin/support_request/list/inbox');
      }



    public function addsupport(Request $request)
        {
                $to=$request->inputEmail;
                $ticketno=$request->ticket;
                $subject=$request->inputSubject;
                $message=$request->inputBody;
                $files=$request->file("files");

           //  $files =$request->file('file');
           
                $file_count=count($files);
                //dd($file_count);
                $uploadcount=0;
                if($file_count > 0) {
                  foreach ($files as $one) 
                  {
                    if($one) {
                      $filename = time()."_".$one->getClientOriginalName();
                   $extension=$one->getClientOriginalExtension();
                   $image=$one->move(public_path().'/uploads',$filename);

                   $uploadcount++;
                  $extensionname[]=$extension;
                  $listfilenames[] = $filename;
                    }
                  

                  }
                }
                

                if(!empty($listfilenames))
                {
                  $list_count=1;
                 
                }
                 $email=User::select('email')->where('email','=',$to)->get();
                 //dd($email);
                 if($email)
                 {
                    $id=DB::table('users')->where('email','=',$to)->pluck('id');
                   //dd($id[0]);
                 }
                 else
                 {
                    echo "not found";
                 }
                // dd(strval($ticketno));
          $ticketno=strval($ticketno);
          $user='User';
        if ($uploadcount == $file_count){

        $support_request=SupportRequest::create(array(
         'ticket_no'=> $ticketno,
        'user_id'=>$id[0],
        'subject'=> $subject,
         'user_asked'=>$message, 
         'user_type'=>'User',
          'is_main'=>1,
        'is_attachments'=>$list_count
        
        ));    
                 } else {
                  $support_request=SupportRequest::create(array(
         'ticket_no'=> $ticketno,
        'user_id'=>$id[0],
        'subject'=> $subject,
         'user_asked'=>$message, 
         'user_type'=>'User',
          'is_main'=>1,
        
        )); 
                 }
             
            //   $find=SupportRequest::where('ticket_no','=', $ticketno)->get();
            //  foreach($find as $f)
            //  {
            // $d=$f->id;
            //  }
             
        $support_request->save();
        $requestid=DB::table('support_request')->where('ticket_no','=',$ticketno)->pluck('id');
          if($file_count > 1) {
            for($i=0;$i<$file_count;$i++)
            {
            $support=SupportRequestAttachment::create(array(
            'request_id'=> $support_request->id,
            'file_name'=> $listfilenames[$i],
            'file_type'=> $extensionname[$i]
            ));
            }    
          }    
        


       return redirect('admin/support_request/list/inbox');

        }





}






