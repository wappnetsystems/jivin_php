<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;

use App\Http\Requests;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
//load Models
use App\Api;
use App\User;
use App\NotificationLog;

class ApiNotificationLogController extends ApiGuardController
{
    // protected $apiMethods = [
    //     'getCountryList' => [
    //         'keyAuthentication' => false
    //     ],                               
    // ];
    public function getNotificationDetails(Request $request){
	 	$data = $request->json()->all();
	 	$response =  [];
	 	if (isset($data['notification_id'])) {
			 $notificationId = $data['notification_id'];
			$fillnotificationLog = NotificationLog::find($notificationId);
			$arrayCondition = ['contact_msg','bid_win']; //in_array("Glenn", $people)
			//if($fillnotificationLog->activity_type != 'contact_msg'){
			if(!in_array($fillnotificationLog->activity_type, $arrayCondition)){
					$notificationDetails = NotificationLog::with(NotificationLog::getWithModel($notificationId))->where('id','=',$notificationId)->first();
			}else{
				$notificationDetails = $fillnotificationLog;
			}
	 		

            if ($notificationDetails->count()) {
            	$userFrmDb = User::where('id', '=', $notificationDetails['activity_by'])->first();
            	if($userFrmDb->count()) {
            		$notificationDetails->username = $userFrmDb['username'];	
            	}
            	$response =  Api::make_response(1,[$notificationDetails],'Details found');
            }else{
            	$response =  Api::make_response(0,[],'Details not found');
            }
	 	}else{
	 		$response =  Api::make_response(0,[],'Invalide Param.');
	 	}
	 	return $response;     	
    }
    public function setNotificationRead(Request $request){
	 	$data = $request->json()->all();
	 	$response =  [];
	 	if (isset($data['user_id'])) {
	 		$affected = NotificationLog::where('visible_by', '=',$data['user_id'])->update(array('is_read' => 1));
            $response =  Api::make_response(1,['unread_notification_count'=>0],'All notification read');
	 	}else{
	 		$response =  Api::make_response(0,[],'Invalide Param.');
	 	}
	 	return $response;     	
    }
    public function getNotification(Request $request){
	 	$data = $request->json()->all();
	 	$response =  [];
	 	if ($data) {
	 		if ($data['pagination']) {
	 		  $getNotification = NotificationLog::where('visible_by','=',$data['user_id'])->orderBy('id','DESC')->paginate($data['itemsPerPage']);
	 		}else{
	 		  $getNotification = NotificationLog::where('visible_by','=',$data['user_id'])->orderBy('id','DESC')->get();
	 		}
            $newNotificationCount = NotificationLog::where('visible_by','=',$data['user_id'])
                                                   ->where('is_read','=',0)
                                                   ->count();
	 		if ($getNotification->count()) {
	 			$response =  Api::make_response(1,[$getNotification],'Notification Found',['new_notification_count'=>$newNotificationCount]);
	 		}else{
	 			$response =  Api::make_response(0,[],'Notification not Found');
	 		}
            
	 	}else{
	 		$response =  Api::make_response(0,[],'Invalide Param.');
	 	}
	 	return $response;  
    } 
}
