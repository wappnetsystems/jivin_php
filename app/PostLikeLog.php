<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostLikeLog extends Model
{
    protected $table = 'post_like_log';
}
