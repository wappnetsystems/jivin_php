<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
	protected $table = 'advertisement';
	
	public function getAdRegions()
	{
	    return $this->belongsTo('App\AdRegions','ad_region_id','id'); //hasMany belongsTo
	}
	
	public function getAdpages()
	{
	    return $this->belongsTo('App\AdPages','ad_page_id','id'); //hasMany belongsTo
	}

	public function getAdBid()
	{
	    return $this->hasMany('App\AdBid','advertisement_id','id'); //hasMany belongsTo
	}
}
