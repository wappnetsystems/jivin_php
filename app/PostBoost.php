<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostBoost extends Model
{
    protected $table = 'post_boost';
    protected $hidden = [
        'created_at','deleted_at'
    ]; 

    public function getboostpost()
    {
        return $this->belongsTo('App\Posts','post_id','id');
    }
//  protected $appends = ['audio_url','video_url','media_type'];
//     public function getMediaTypeAttribute()
//     {  
//         if ($this->audio_name) {
//             return "audio";
//         }elseif ($this->video_name) {
//             return "video";
//         }else{
//             return "text";
//         }
//     }    
//     public function getAudioUrlAttribute()
//     {  
//     	if ($this->audio_name) {
//     		return env('IMG_URL').'post/'. $this->audio_name;
//     	}else{
//     		return "";
//     	}
//     }      
//     public function getVideoUrlAttribute()
//     {  
//     	if ($this->video_name) {
//     		return env('IMG_URL').'post/'. $this->video_name;
//     	}else{
//     		return "";
//     	}
//     } 

//      public function reportposts()
//    {
//          return $this->hasMany('App\ReportPost','id','post_id');
    
    
//    } 

//      public function userposts()
//    {
//          return $this->belongsTo('App\User','user_id','id');
//    } 
   
}
