<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

    protected $table = 'country';
    protected $hidden = [
        'deleted_at', 'updated_at', 'created_at'
    ];
    protected $appends = ['img_url'];

    public function regions() {
        return $this->hasMany('App\Region')->active();
    }

    public function getregions() {
        return $this->belongsTo('App\Region', 'country_id', 'id');
    }

    public function getImgUrlAttribute($value) {
        return env('IMG_URL') . 'images/map/us/' . strtolower($this->slug) . '.png';
    }

    public function getNameAttribute($value) {
        return ucwords(str_replace("_", " ", $value));
    }

    //Created By : Debasis Chakraborty
    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

}
