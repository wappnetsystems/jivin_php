<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Lib\Upload_file;

class UserImage extends Model
{
    protected $table = 'user_image';
    protected $appends = ['user_image_url'];
    
    public function getUserImageUrlAttribute()
    {
        return Upload_file::get_post_file_path('Profile', $this->user_image, 0);
        //return asset('image_file/'.$this->user_image);
        /*if($this->image_type == "Profile"){
            return env('IMG_URL').'images/users/'. $this->user_image;
        }else if($this->image_type == "Cover"){
            return env('IMG_URL').'images/users/background/'. $this->user_image;
        }*/
        
    } 
}
