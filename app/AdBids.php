<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdBids extends Model
{
	protected $table = 'ad_bids'; 

	public function state() {
		return $this->belongsTo('App\State','state_id');
	}
}
