<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCryptoAddress extends Model
{
    protected $table='user_crypto_address';
}
