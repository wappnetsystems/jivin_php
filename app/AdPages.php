<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdPages extends Model
{
	protected $table = 'ad_pages'; 
	protected $appends =['ads_image_url'];
	public function getadregion()
	{
	    return $this->belongsTo('App\AdRegions','ad_region_id','id'); //hasMany belongsTo
	}

	public function getadprice()
	{
	    return $this->hasMany('App\AdPriceMaster','ad_page_id','id'); //hasMany belongsTo
	}

	public function getAdsImageUrlAttribute()
    {  
        if($this->ad_image){
            return env('IMG_URL').'images/ads/'. $this->ad_image;
        }else{
            return "";
        }
        
    }
}
