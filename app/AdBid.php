<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdBid extends Model
{
	protected $table = 'ad_bids'; 
	protected $appends = ['state_name','page_name','page_url','ad_region_id','region_slug','division_slug','country_id'];
	
	public function state() {
		return $this->belongsTo('App\State','state_id');
	}

	public function getPageNameAttribute()
    {  
		$adpages = AdPages::where('id',$this->ad_page_id)->first();
		if(count($adpages)>0){
			return $adpages->page_name;
		}else{
			return 0;
		}
	}
	public function getAdRegionIdAttribute()
    {  
		$adpages = AdPages::where('id',$this->ad_page_id)->first();
		if(count($adpages)>0){
			return AdRegions::find($adpages->ad_region_id)->name;
		}else{
			return 0;
		}
	}	
	public function getRegionSlugAttribute()
    {  
		$division = State::where('id','=',$this->state_id)->first();
		if(count($division)>0){
			$region = Devision::where('id','=',$division->division_id)->first()->region_id;
			$region = Region::where('id','=',$region)->first()->name;
			return $region;
		}else{
			return 0;
		}
	}
	public function getCountryIdAttribute()
    {  
		$division = State::where('id','=',$this->state_id)->first();
		if(count($division)>0){
			$region = Devision::where('id','=',$division->division_id)->first()->region_id;
			$country = Region::where('id','=',$region)->first()->country_id;
			$countryname = Country::where('id','=',$country)->first()->name;
			return $countryname;
		}else{
			return 0;
		}
	}
	public function getDivisionSlugAttribute()
    {  
		$division = State::where('id','=',$this->state_id)->first();
		if(count($division)>0){
			return Devision::where('id','=',$division->division_id)->first()->name;
		}else{
			return 0;
		}
	}

	public function getPageUrlAttribute()
    {  
		$adpages = AdPages::where('id',$this->ad_page_id)->first();
		if(count($adpages)>0){
			return $adpages->page_url;
		}else{
			return 0;
		}
    }
    public function getStateNameAttribute()
    {
       return State::where('id','=',$this->state_id)->first()->name;
    }

	public function getadvertisement()
	{
		return $this->belongsTo('App\Advertisement','advertisement_id','id');
	} 

	public function creativedetails(){
		return $this->belongsTo('App\AdCreative','creative_id','id');
	}
}
