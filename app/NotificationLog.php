<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\NotificationLog;
use App\PostLogs;

class NotificationLog extends Model
{
    protected $table = 'notification_log';
    protected $hidden = ['updated_at','visible_by'];
    protected $appends = ['activity_by_img_url','activity_by_full_name','time_ago'];

    public function postDetails(){
        return $this->hasOne('App\Posts','id','related_id')->orderBy('id','DESC');
    }
    public function userDetails(){
        return $this->hasOne('App\User','id','related_id')->with('events','accountSettings')->orderBy('id','DESC');
    } 
    
    public function activity_user() {
        return $this->hasOne('App\User','id','activity_by');
    }
    
    public static function getWithModel($notificationId){
      $notificationDetails = NotificationLog::where('id','=',$notificationId)->first();
        switch ($notificationDetails->activity_type) {
            case "post_share":
                return "postDetails";
                break;            
            case "post_like":
                return "postDetails";
                break;         
            case "add_post":
                return "postDetails";
                break;            
            case "post_boost":
                return "postDetails";
                break;            
            case "wall_of_fame":
                return "userDetails";
                break;            
            case "user_follow":
                return "userDetails";
                break;            
            case "profile_update":
                return "userDetails";
                break;            
            case "leader_board":
                return "userDetails";
                break;            

        }
    }       
    public function getActivityByImgUrlAttribute(){
      $user_image = User::where('id','=',$this->activity_by)->first()->user_image;
      return env('IMG_URL').'images/users/'. $user_image;
    }
    public function getActivityByFullNameAttribute(){
      $name = User::where('id','=',$this->activity_by)->first()->name;
      return $name;
    } 
    public function getTimeAgoAttribute(){
      return $this->timeAgo($this->created_at);
    }          
    public static function addLog($activity_by,$visible_by,$activity_type,$related_id=null,$notification_text=null){

       $notificationLog = new NotificationLog;
       $notificationLog->activity_by = $activity_by;
       $notificationLog->visible_by = $visible_by;
       $notificationLog->activity_type = $activity_type;
       $notificationLog->related_id = $related_id;
       $notificationLog->notification_text = $notification_text;

       if ($notificationLog->save()) {
       	return true;
       }else{
       	return false;
       }

    }

    public static function sendNotificationOnLike($postId,$likedById){
    	$isAlreadySend = false;
      $getPostOrigin = PostLogs::where('visible_by','=',$likedById)
                                 ->where('post_id','=',$postId)
                                 ->get();
      foreach ($getPostOrigin as $key => $postOrigin) {
      	if ($postOrigin->post_type == "shared" && $postOrigin->shared_by_user_id) {
      		if ($postOrigin->created_by_user_id == $postOrigin->shared_by_user_id) {
                if (!$isAlreadySend) {
      			NotificationLog::addLog($likedById,$postOrigin->shared_by_user_id,'post_like',$postId," voted a post you added");
      			$isAlreadySend = true;
      		    }
      		}else{
      			#shared by other
      			NotificationLog::addLog($likedById,$postOrigin->shared_by_user_id,'post_like',$postId," voted a post you shared");
      		}
      	}else{
            if ($postOrigin->created_by_user_id == $postOrigin->visible_by) {
             NotificationLog::addLog($likedById,$postOrigin->created_by_user_id,'post_like',$postId," voted a post you added");
            }elseif ($likedById != $postOrigin->visible_by) {
            	NotificationLog::addLog($likedById,$postOrigin->created_by_user_id,'post_like',$postId," voted a post ".User::find($postOrigin->created_by_user_id)->name." added");
            }else{
            	if (!$isAlreadySend) {
                    NotificationLog::addLog($likedById,$postOrigin->created_by_user_id,'post_like',$postId," voted a post you added");
                    $isAlreadySend = true;
            	}
            } 
      	}
      }
    }
    public function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }    
}
