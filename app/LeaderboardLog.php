<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AccountSettings;
use App\Api;
use App\User;

class LeaderboardLog extends Model
{
    protected $table = 'leaderboard_log';
    public $timestamps = false;
    protected $hidden = [
        'month','year','leader_board_no','id'
    ]; 
    protected $appends = ['user_full_name','user_name'];
    public function userInfo()
    {
        return $this->belongsTo('App\User','user_id')->with('accountSettings');
    }  
    public function getUserFullNameAttribute(){
      $name = User::where('id','=',$this->user_id)->first()->name;
      return $name;
    }    
    public function getUserNameAttribute(){
      $username = User::where('id','=',$this->user_id)->first()->username;
      return $username;
    } 
    public static function addLog($userId){
	   $now = new \DateTime('now');
	   $current_month = $now->format('m');
	   $current_year = $now->format('Y');
	   $current_leaderboard = LeaderboardLog::getCurrentLeaderBoard($current_month);
	   $userCountryId =0;
	   $userRegionId =0;
	   $userDevisionId =0;
	   $userStateId =0;
       $getUserAccount = AccountSettings::where('user_id','=',$userId)->first();
       
       if ($getUserAccount) {
	       	 $userCountry = $getUserAccount->country;
	       	 $userRegion = $getUserAccount->region;
	       	 $userDevision = $getUserAccount->division;
	       	 $userState = $getUserAccount->state;
	       	 if ($getUserAccount->country) {
	       	 	$userCountrySlug = $userCountry['slug'];
	       	 	$userCountryId =Api::getLocationId('Country',$userCountrySlug);
	       	 }else{
	       	 	return false;
	       	 }       	 
	       	 if ($getUserAccount->region) {
	       	 	$userRegionSlug = $userRegion['slug'];
	       	 	$userRegionId =Api::getLocationId('Region',$userRegionSlug);
	       	 }else{
	       	 	return false;
	       	 }        	 
	       	 if ($getUserAccount->division) {
	       	 	$userDevisionSlug = $userDevision['slug'];
	       	 	$userDevisionId =Api::getLocationId('Devision',$userDevisionSlug);
	       	 }else{
	       	 	return false;
	       	 }        	 
	       	 if ($getUserAccount->state) {
	       	 	$userStateSlug = $userState['slug'];
	       	 	$userStateId =Api::getLocationId('State',$userStateSlug);
	       	 }else{
	       	 	return false;
	       	 } 

	       	 $findLeaderboardLog = LeaderboardLog::where('user_id','=',$userId)
						       	 ->where('country_id','=',$userCountryId)
						       	 ->where('region_id','=',$userRegionId)
						       	 ->where('devision_id','=',$userDevisionId)
						       	 ->where('state_id','=',$userStateId)
						       	 ->where('month','=',$current_month)
						       	 ->where('year','=',$current_year)
						       	 ->where('leader_board_no','=',$current_leaderboard)
						       	 ->first();

		    if ($findLeaderboardLog) {
		    	$findLeaderboardLog->increment('total_vote');
		    	return true;
		    }else{

		    	$leaderboardLog = new LeaderboardLog;
		    	$leaderboardLog->user_id = $userId;
		    	$leaderboardLog->country_id = $userCountryId;
		    	$leaderboardLog->region_id = $userRegionId;
		    	$leaderboardLog->devision_id = $userDevisionId;
		    	$leaderboardLog->state_id = $userStateId;
		    	$leaderboardLog->month = $current_month;
		    	$leaderboardLog->year = $current_year;
		    	$leaderboardLog->leader_board_no = $current_leaderboard;
		    	$leaderboardLog->total_vote = 1;
		    	$leaderboardLog->save();
                // dd($leaderboardLog);
		    	return true;
		    }
       }
    }
    public static function getCurrentLeaderBoardEndingDate($leaderboard){
        switch ($leaderboard) {
            case "1":
                $month = 4;

                $firstday = date('01-' . $month . '-Y');
                $lastday = date(date('t', strtotime($firstday)) .'-' . $month . '-Y');
                return $lastday;
                
                break;            
            case "2":
                $month = 8;

                $firstday = date('01-' . $month . '-Y');
                $lastday = date(date('t', strtotime($firstday)) .'-' . $month . '-Y');
                return $lastday;
                break;            
            case "3":
                $month = 12;

                $firstday = date('01-' . $month . '-Y');
                $lastday = date(date('t', strtotime($firstday)) .'-' . $month . '-Y');
                return $lastday;
                break;            

        }
    }    
    public static function getCurrentLeaderBoardMonth($leaderboard){
        switch ($leaderboard) {
            case "1":
                return "JANUARY - APRIL";
                break;            
            case "2":
                return "MAY - AUGUST";
                break;            
            case "3":
                return "SEPTEMBER - DECEMBER";
                break;             

        }
    }    
    public static function getCurrentLeaderBoard($month){
        switch ($month) {
            case "01":
                return 1;
                break;            
        	case "02":
                return 1;
                break;            
        	case "03":
                return 1;
                break;            
        	case "04":
                return 1;
                break;            
        	case "05":
                return 2;
                break;            
        	case "06":
                return 2;
                break;            
        	case "07":
                return 2;
                break;            
        	case "08":
                return 2;
                break;            
        	case "09":
                return 3;
                break;            
        	case "10":
                return 3;
                break;            
        	case "11":
                return 3;
                break;            
        	case "12":
                return 3;
                break;

        }
    }
    public function getCountry(){
        return $this->belongsTo('App\Country','country_id','id');
    }
    public function getRegion(){
        return $this->belongsTo('App\Region','region_id','id');
    }
    public function getState(){
        return $this->belongsTo('App\State','state_id','id');
    }
    public function getDevision(){
        return $this->belongsTo('App\Devision','devision_id','id');
    }
}
