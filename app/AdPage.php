<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdPage extends Model
{
	protected $table = 'ad_pages'; 
	protected $appends =['ads_image_url'];

	public function getAdsImageUrlAttribute()
    {  
        if($this->ad_image){
            return env('IMG_URL').'images/ads/'. $this->ad_image;
        }else{
            return "";
        }
        
    }

    // public function getStateNameAttribute()
    // {
    //    return State::where('id','=',$this->state_id)->first()->name;
    // }

	// public function getadvertisement()
	// {
	// 	return $this->belongsTo('App\Advertisement','advertisement_id','id');
	// } 

	// public function creativedetails(){
	// 	return $this->belongsTo('App\AdCreative','creative_id','id');
	// }
}
