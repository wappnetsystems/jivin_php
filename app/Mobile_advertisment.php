<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobile_advertisment extends Model
{
    protected $table = 'mobile_advertisment';
    protected $hidden = [
        'updated_at', 'created_at'
    ];

    public function userInfo()
    {
        return $this->belongsTo('App\User','user_id');
    } 
    
}
