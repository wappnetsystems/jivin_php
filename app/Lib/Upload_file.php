<?php

namespace App\Lib;
use Illuminate\Support\Facades\Storage;
class Upload_file {

    private static $s3_link = "https://jivinmedia.nyc3.cdn.digitaloceanspaces.com/";
    
    public static function upload_video_file($request) {  //this
        $video_file = $request->file('video_file');
        $extension=$video_file->getClientOriginalExtension();
        $new_file_name=time().rand(10000,99999).'.'.$extension;
        $response=Storage::disk('s3')->put('video_files/'.$new_file_name,file_get_contents($video_file),'public');
        
        //$file_path = $video_file->move('video_file',$new_file_name,'public');
        
        if ($response) {
            return ['status'=>true,'storage_path'=>$new_file_name];
        }
        else{
            return ['status'=>false];
        }
    }
    
    public static function upload_audio_file($request) {
        $audio_file = $request->file('audio_file');
        $extension=$audio_file->getClientOriginalExtension();
        $new_file_name=time().rand(1000,9999).'.'.$extension;
        $response=Storage::disk('s3')->put('audio_files/'.$new_file_name,file_get_contents($audio_file),'public');
        //$file_path = $video_file->move('audio_file',$new_file_name);
        
        if ($response) {
            return ['status'=>true,'storage_path'=>$new_file_name];
        }
        else{
            return ['status'=>false];
        }
    }

    //nishit 02/06/2020
    public static function upload_advertisment_file($request) {
        $advertisment_files = $request->file('advertisement_file');
        $extension=$advertisment_files->getClientOriginalExtension();
        $new_file_name=time().rand(1000,9999).'.'.$extension;
        $response=Storage::disk('s3')->put('advertisement_file/'.$new_file_name,file_get_contents($advertisment_files),'public');
        
        if ($response) {
            return ['status'=>true,'storage_path'=>$new_file_name];
        }
        else{
            return ['status'=>false];
        }
    }
    
    public static function upload_video_thumb($file_path,$file_name) {
        
        $response=Storage::disk('s3')->put('video_thumb_path/'.$file_name,file_get_contents($file_path),'public');
        
        if ($response) {
            return ['status'=>true,'storage_path'=>$file_name];
        }
        else{
            return ['status'=>false];
        }
    }
    
    public static function get_post_file_path($file_type,$file_name,$post_id=0) {  //this
        
        switch ($file_type) {
            case 'Audio':
                //$full_path=asset("audio_file/".$file_name);
                
                $full_path= self::$s3_link."audio_files/".$file_name;
                break;
            case 'Video':
                //$full_path=asset("video_file/".$file_name);
                $full_path= self::$s3_link."video_files/".$file_name;
                break;
            case 'Profile':
                $full_path= self::$s3_link."profile_image/".$file_name;
                break;
            case 'Advertisement':
                $full_path= self::$s3_link."advertisement_file/".$file_name;
                break;    
            default:
                $full_path="";
                break;
        }
        return $full_path;
    }
    //=====================================

    public static function get_video_thumb_path($file_name) {  //this
        
        $full_path= self::$s3_link."video_thumb_path/".$file_name;
               
        return $full_path;
    }

    //=====================================
    
    public static function upload_user_image_file($request,$field_name="image_file") {
        $image_file = $request->file($field_name);
        $extension=$image_file->getClientOriginalExtension();
        $new_file_name=time().rand(1000,9999). uniqid().'.'.$extension;
        //$file_path = $image_file->move('image_file',$new_file_name);
        $response=Storage::disk('s3')->put('profile_image/'.$new_file_name,file_get_contents($image_file),'public');
        if ($response) {
            return ['status'=>true,'storage_path'=>$new_file_name];
        }
        else{
            return ['status'=>false];
        }
    }
    
    public static function upload_event_image_file($request) {
        $event_image = $request->file('event_image');
        $extension=$event_image->getClientOriginalExtension();
        $new_file_name=time().rand(1000,9999).'.'.$extension;
        $file_path = $event_image->move('event_image',$new_file_name);
        if ($file_path) {
            return ['status'=>true,'storage_path'=>$new_file_name];
        }
        else{
            return ['status'=>false];
        }
    }
    
    public static function upload_address_proff_image_file($request) {
        $address_proff = $request->file('address_proff');
        $extension=$address_proff->getClientOriginalExtension();
        $new_file_name=time().rand(1000,9999).'.'.$extension;
        $file_path = $address_proff->move('address_proff',$new_file_name);
        if ($file_path) {
            return ['status'=>true,'storage_path'=>$new_file_name];
        }
        else{
            return ['status'=>false];
        }
    }

    //nishit 29/05/2020
    public static function upload_support_attachment_file($file) {
        $support_attch = $file;
        $extension= $support_attch->getClientOriginalExtension();
        $new_file_name= time().rand(1000,9999).'.'.$extension;
        $response= Storage::disk('s3')->put('support_attch/'.$new_file_name,file_get_contents($support_attch),'public');
        
        if ($response) {
            return ['status'=>true,'storage_path'=>$new_file_name,'file_type' => $extension];
        }
        else{
            return ['status'=>false];
        }
    }

}
