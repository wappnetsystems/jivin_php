<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $table = 'event';
    protected $hidden = [
        'updated_at', 'created_at','deleted_at'
    ];
    //protected $appends = ['state_name','division_name','country_name','region_name','time','event_start_date','event_image_url'];
    protected $appends = ['state_name','country_name','start_update_time','time','event_start_date','event_image_url'];
    public function getStateNameAttribute(){
       // $accountstateid = AccountSettings::where('user_id','=',$this->country_id)->first()->state_id;
        return State::where('id', '=',$this->state_id)->first()->name;
    }

    

    // public function getDivisionNameAttribute(){
    //     //$accountdivisionid = AccountSettings::where('user_id','=',$this->id)->first()->division_id;
    //     return Devision::where('id', '=',$this->division_id)->first()->name;
    // }
    public function getCountryNameAttribute(){
        //$accountcountryid = AccountSettings::where('user_id','=',$this->id)->first()->country_id;
        return Country::where('id', '=',$this->country_id)->first()->name;
    }
    // public function getRegionNameAttribute(){
    //     //$accountregionid = AccountSettings::where('user_id','=',$this->id)->first()->region_id;
    //     return Region::where('id', '=',$this->region_id)->first()->name;
    // }

    public function getStartUpdateTimeAttribute(){
        
        return strtotime($this->start_time);
    }

    public function userInfo()
    {
        return $this->belongsTo('App\User','user_id');
    } 
    
    public function userInfoWithEvent()
    {
        return $this->belongsTo('App\User','user_id')->with('events');
    }  

    public function getTimeAttribute()
    {  
        return date('l , F j S Y', strtotime($this->start_date)).' at '.date('h:i A', strtotime($this->start_time));
    }

    public function getEventStartDateAttribute()
    {  
         return date('Y-n-j', strtotime($this->start_date));
        //return $this->attribute['start_date'];
        //return $this->start_date;
    }

    public function getEventImageUrlAttribute() {
        if(isset($this->event_image) && !empty($this->event_image)){
            return env('IMG_URL') . 'event_image/' . $this->event_image;
        }else{
            return "";
        }
    }         
}
