<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model {

    protected $table = 'region';
    protected $hidden = [
        'updated_at', 'created_at', 'deleted_at'
    ];
    protected $appends = ['country_name', 'img_url'];

    public function country() {
        return $this->belongsTo('App\Country', 'country_id');
    }

    public function getNameAttribute($value) {
        return ucwords(str_replace("_", " ", $value));
    }

    public function getImgUrlAttribute($value) {
        return env('IMG_URL') . 'images/map/us/region/img_' . strtolower($this->slug) . '.png';
    }

    public function divisions() {
        return $this->hasMany('App\Devision')->where('is_active', 1);
    }

    public function getCountryNameAttribute() {
        $name = Country::where('id', '=', $this->country_id)->first()->name;
        return $name;
    }

    //Created By : Debasis Chakraborty
    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

}
