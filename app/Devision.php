<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Region;

class Devision extends Model {

    protected $table = 'division';
    protected $hidden = [
        'updated_at', 'created_at', 'deleted_at'
    ];
    protected $appends = ['region_name', 'img_url'];

    public function getRegionNameAttribute() {
        $name = Region::where('id', '=', $this->region_id)->first()->name;
        return $name;
    }

    public function getImgUrlAttribute($value) {
        return env('IMG_URL') . 'images/map/us/division/img_' . strtolower($this->slug) . '.png';
    }

    public function getNameAttribute($value) {
        return ucwords(str_replace("_", " ", $value));
    }

    public function region() {
        return $this->belongsTo('App\Region', 'region_id');
    }

    public function states() {
        return $this->hasMany('App\State', 'division_id')->where('is_active', 1);
    }

}
