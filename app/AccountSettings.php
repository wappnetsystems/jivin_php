<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Country;
use App\Region;
use App\Devision;
use App\State;

class AccountSettings extends Model
{
    protected $table = 'user_account_settings';
    protected $hidden = [
        'id','deleted_at','updated_at', 'created_at','user_id','country_id','region_id','division_id','state_id','background_image'
    ];  
    protected $appends =['background_image_url','country_slug','country','region','division','state','boost_country','boost_region','boost_division','boost_state'];
    private $s3_link = "https://jivinmedia.nyc3.cdn.digitaloceanspaces.com/";
    public function getBackgroundImageUrlAttribute()
    {  
        if(isset($this->background_image)){
            //return asset('image_file/'.$this->background_image);
            return $this->s3_link.'profile_image/'.$this->background_image;
        }
        else{
            return "";
        }
        
    } 
    public function getCountrySlugAttribute()
    {  
        if($country = Country::find($this->country_id)){
           return $country->slug;
        }else{
            return "";
        }
    }
    public function getCountryAttribute()
    {  
    	if($country = Country::find($this->country_id)){
           return ['name'=>str_replace('_', ' ', $country->name),'slug'=>$country->slug,'id'=>$this->country_id];
    	}else{
            return "";
    	}
    }
    public function getRegionAttribute()
    {  
    	if($region = Region::find($this->region_id)){
           return ['name'=>str_replace('_', ' ', $region->name),'slug'=>$region->slug,'id'=>$this->region_id];
    	}else{
            return "";
    	}
    }

    public function getDivisionAttribute()
    {  
    	if($division = Devision::find($this->division_id)){
           return ['name'=>str_replace('_', ' ', $division->name),'slug'=>$division->slug,'id'=>$this->division_id];
    	}else{
            return "";
    	}
    } 
    
    public function getStateAttribute()
    {  
    	if($state = State::find($this->state_id)){
           return ['name'=>str_replace('_', ' ', $state->name),'slug'=>$state->slug,'id'=>$this->state_id];
    	}else{
            return "";
    	}
    }

    /******For Boost Start*******/
    public function getBoostCountryAttribute()
    {  
        $id = ($this->boosted_country) ? $this->boosted_country : $this->country_id;
    	if($country = Country::find($id)){
           return ['name'=>str_replace('_', ' ', $country->name),'slug'=>$country->slug,'id'=>$id];
    	}else{
            return "";
    	}
    }
    public function getBoostRegionAttribute()
    {  
        $id = ($this->boosted_region) ? $this->boosted_region : $this->region_id;
    	if($region = Region::find($id)){
           return ['name'=>str_replace('_', ' ', $region->name),'slug'=>$region->slug,'id'=>$id];
    	}else{
            return "";
    	}
    }

    public function getBoostDivisionAttribute()
    {  
        $id = ($this->boosted_division) ? $this->boosted_division : $this->division_id;
    	if($division = Devision::find($id)){
           return ['name'=>str_replace('_', ' ', $division->name),'slug'=>$division->slug,'id'=>$id];
    	}else{
            return "";
    	}
    } 
    
    public function getBoostStateAttribute()
    {   
        $id = ($this->boosted_state) ? $this->boosted_state : $this->state_id;
    	if($state = State::find($id)){
           return ['name'=>str_replace('_', ' ', $state->name),'slug'=>$state->slug,'id'=>$id];
    	}else{
            return "";
    	}
    }
    /******For Boost Start*******/
    public function getUserDetails(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function getCountry(){
        return $this->belongsTo('App\Country','country_id','id');
    }
    public function getRegion(){
        return $this->belongsTo('App\Region','region_id','id');
    }
    public function getState(){
        return $this->belongsTo('App\State','state_id','id');
    }
    public function getDevision(){
        return $this->belongsTo('App\Devision','division_id','id');
    }             
}
