<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Posts;
use App\PostLogs;
use App\NotificationLog;

class PostLogs extends Model
{
    protected $logedUserId;
    protected $table = 'post_logs';
    //protected $fillable = ['logedUserId'];


    public function getpost()
    {
        return $this->belongsTo('App\Posts','post_id','id');
    }
    protected $hidden = [
         'created_at','post_type','visible_by','shared_by_user_id','deleted_at','audio_name','audio_name'
    ]; 
    protected $appends = ['time_ago','total_vote','created_by_name','created_by_user_name','created_by_img','audio_url','video_url','is_liked','is_shared','media_type'];

    public function getIsLikedAttribute()
    { 
         return false;
    }
    public function setLogedUserId($id)
    {
        $this->logedUserId = $id;
        return $this;
    }    
    public function getIsSharedAttribute()
    { 
         return false;
    }
    public function getMediaTypeAttribute(){
      if ($this->video_name) {
        return "video";
      }else if($this->audio_name){
         return "audio";
      }else{
        return "";
      }
    }        
    public function getAudioUrlAttribute()
    {  
      if ($this->audio_name) {
        return env('IMG_URL').'post/'. $this->audio_name;
      }else{
        return "";
      }
    }      
    public function getVideoUrlAttribute()
    {  
      if ($this->video_name) {
        return env('IMG_URL').'post/'. $this->audio_name;
      }else{
        return "";
      }
    }     
    public function getTimeAgoAttribute(){
      return $this->timeAgo($this->created_at);
    }  
    public function getTotalVoteAttribute(){
      $getPostLike = Posts::where('id','=',$this->post_id)->first()->total_like;
      return $getPostLike;
    }
    public function getCreatedByNameAttribute(){
      $name = User::where('id','=',$this->created_by_user_id)->first()->name;
      return $name;
    }
    public function getCreatedByImgAttribute(){
      $imgUrl = User::where('id','=',$this->created_by_user_id)->first()->user_image_url;
      return $imgUrl;
    }     
    public function getCreatedByUserNameAttribute(){
      $imgUrl = User::where('id','=',$this->created_by_user_id)->first()->username;
      return $imgUrl;
    }          
    public static function logPosts ($post_id,$user_id,$type){
    	$visibleUserIds =[];

          //************* this section for private post start**********************//
          $getPostDetails = Posts::where('id','=',$post_id)
          ->where(function($q) {
            $q->where('is_public','=',1)
            ->orwhere('is_public','=',2);
            })
          ->first();
          //dd($getPostDetails);
          if ($getPostDetails) {
          	   $getUserInfo = User::with('follows')->where('id','=',$user_id)->first()->toArray();
          	   if ($getUserInfo) {
          	    	$log = new PostLogs;
          	    	$log->post_id = $getPostDetails->id;
          	    	$log->visible_by = $user_id;
          	    	$log->created_by_user_id = $getPostDetails->user_id;
          	    	
          	    	if ($type =="self") {
          	    		$log->post_type = "self";
          	    		$log->display_text = "Added a new post";
          	    		$log->shared_by_user_id =null;

          	    	}else{
                        $log->post_type = "shared";
                        if ($getPostDetails->user_id == $user_id) {
                          $log->display_text = "Shared own post";
                        }else{
                          $log->display_text = $getUserInfo['name']." shared a post of ".User::find($getPostDetails->user_id)->name;
                        NotificationLog::addLog($user_id,$getPostDetails->user_id,'post_share',$getPostDetails->id," shared your post");                          
                        }
                        $log->shared_by_user_id =$user_id; 

          	    	}
          	    	
          	    	$log->text_details = $getPostDetails->text_details;
          	    	$log->audio_name = $getPostDetails->audio_name;
          	    	$log->video_name = $getPostDetails->video_name;
          	    	$log->is_adult = $getPostDetails->is_adult;
          	    	$log->is_active = $getPostDetails->is_active;
                    $log->is_public = $getPostDetails->is_public;
          	    	$log->boost_order = 0;
          	    	$log->save();  

          	    $follows = $getUserInfo['follows'];
                    
          	    foreach ($follows as $key => $follow) {
          	    	$visibleUserIds[] = $follow['follows_user']['id'];
          	    	$log = new PostLogs;
          	    	$log->post_id = $getPostDetails->id;
          	    	$log->post_type = "shared";
          	    	$log->visible_by = $follow['follows_user']['id'];
          	    	$log->created_by_user_id = $getPostDetails->user_id;
          	    	$log->display_text = $getUserInfo['name']." added a new post";
          	    	if ($type =="self") {
          	    		$log->display_text = $getUserInfo['name']." added a new post";
          	    		$log->shared_by_user_id =null; 
                    NotificationLog::addLog($getPostDetails->user_id,$follow['follows_user']['id'],'add_post',$getPostDetails->id," added a new post");
          	    	}else{
                        if ($getPostDetails->user_id == $user_id) {
                          $log->display_text = $getUserInfo['name']." shared own post ";
                        NotificationLog::addLog($user_id,$follow['follows_user']['id'],'post_share',$getPostDetails->id," shared own post ");                        
                        }else{
                          $log->display_text = $getUserInfo['name']." shared a post of ".User::find($getPostDetails->user_id)->name;
                        NotificationLog::addLog($user_id,$follow['follows_user']['id'],'post_share',$getPostDetails->id," shared a post of ".User::find($getPostDetails->user_id)->name);                          
                        }
                        $log->shared_by_user_id =$user_id;                        
          	    	}          	    	
          	    	$log->text_details = $getPostDetails->text_details;
          	    	$log->audio_name = $getPostDetails->audio_name;
          	    	$log->video_name = $getPostDetails->video_name;
          	    	$log->is_adult = $getPostDetails->is_adult;
          	    	$log->is_active = $getPostDetails->is_active;
          	    	$log->boost_order = 0;
                    $log->is_public = $getPostDetails->is_public;
          	    	$log->save();          	    	
          	    }
          	   }
          	   array_push($visibleUserIds,(int)$user_id);
          	   return true;
          }
          //************* this section for private post end**********************//
          else{
          //************* this section for only me post start**********************//

            $getPostDetailsPrivate = Posts::where('id','=',$post_id)->where('is_public','=',0)->first();
            if ($getPostDetailsPrivate) {

                //NotificationLog::addLog($getPostDetailsPrivate->user_id,$follow['follows_user']['id'],'add_post',$getPostDetailsPrivate->id," added a new post");

                $log = new PostLogs;
                $log->post_id = $getPostDetailsPrivate->id;
                $log->visible_by = $user_id;
                $log->created_by_user_id = $getPostDetailsPrivate->user_id;
                $log->post_type = "self";
                $log->display_text = "Added a new post";
                $log->shared_by_user_id =null;
                $log->text_details = $getPostDetailsPrivate->text_details;
                $log->audio_name = $getPostDetailsPrivate->audio_name;
                $log->video_name = $getPostDetailsPrivate->video_name;
                $log->is_adult = $getPostDetailsPrivate->is_adult;
                $log->is_active = $getPostDetailsPrivate->is_active;
                $log->boost_order = 0;
                $log->is_public = 0;
                $log->save(); 
            }
            //************* this section for only me post end**********************//
          }
    }
    public function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }
}
