<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mail;
use App\UserSubcriptionHistory;
use App\Plans;
use App\UserTransaction;

class Email extends Model
{
    public static function sendMail($id = 1){
        $user = User::findOrFail($id);

        Mail::send('email_template.registration', ['user' => $user], function ($m) use ($user) {
            $m->from('hello@app.com', 'Jivin');

            $m->to($user->email, $user->name)->subject('Welcome to Jivin!');
        });    	
    }
    public static function sendsubScriptionMail($id = 1){
        $user = User::findOrFail($id);
        $subscription = UserSubcriptionHistory::where('user_id',$id)->orderBy('created_at', 'desc')->first();
        $plan = [];
        if($subscription){
            $plan = Plans::find($subscription->package_id);
        }
        $userTransaction = UserTransaction::where('user_id',$id)->orderBy('created_at', 'desc')->first();
        Mail::send('email_template.scription', ['user' => $user,'plan' => $plan,'userTransaction' => $userTransaction], function ($m) use ($user,$plan,$userTransaction) {
            $m->from('hello@app.com', 'Jivin');

            $m->to($user->email, $user->name)->subject('Plan Subscribed!');
        });   
    }

    public static function sendForgetPasswordMail($id = 1){
        $user = User::findOrFail($id);
        Mail::send('email_template.resetpassword', ['user' => $user], function ($m) use ($user) {
            $m->from('hello@app.com', 'Jivin');

            $m->to($user->email, $user->name)->subject('Jivin : Reset Password Link');
        });     
    }
    public static function sendcontactusMail($data = array()){
        //echo json_encode($data);
        // echo $data['email'];
        // die;

        // $user = User::findOrFail($id);

        Mail::send('email_template.contactus', ['user' => $data], function ($m) use ($data) {
            $m->from('hello@app.com', 'Jivin');

            $m->to($data['email'], "")->subject('Jivin : Contact Us');
        });     
    }
}
