<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class InterestCategory extends Model
{
    protected $table = 'intereste_category';
    protected $hidden = array('created_at','updated_at','deleted_at');

	public function activeInterests()
	{
	    return $this->hasMany('App\SubInterest')->active()->orderBy('order','asc');
	}
	public function interests()
	{
	    return $this->hasMany('App\SubInterest');
	}
    //Created By : Debasis Chakraborty
	public function scopeActive($query)
	{
	    return $query->where('is_active', '=', 1);
	}    
}
