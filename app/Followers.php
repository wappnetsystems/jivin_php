<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Followers extends Model
{
    protected $table = 'followers';
    protected $hidden = [
        'updated_at', 'created_at','id','deleted_at'
    ];
    public function followingUser()
    {
        return $this->belongsTo('App\User','is_following')->with('accountSettings');
    }
    public function followsUser()
    {
        return $this->belongsTo('App\User','user_id')->with('accountSettings');
    }  
           
}
