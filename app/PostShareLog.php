<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostShareLog extends Model
{
    protected $table = 'post_share_log';
}
