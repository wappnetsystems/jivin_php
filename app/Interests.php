<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interests extends Model
{
    protected $table = 'interest';
    protected $hidden = [
        'updated_at', 'created_at'
    ];
    protected $appends = ['interest_name'];
    public function getInterestNameAttribute()
    {
      return SubInterest::where('id', '=', $this->interest_id)->first()->name;
    }    
}
