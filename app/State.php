<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'state';
    protected $hidden = [
        'updated_at', 'created_at','deleted_at'
    ];
    protected $appends = ['devision_name','img_url'];
    
    public function getDevisionNameAttribute(){
      $name = Devision::where('id','=',$this->division_id)->first()->name;
      return $name;
    }
    public function getImgUrlAttribute($value)
    {
        return env('IMG_URL').'images/map/us/states/'.strtolower($this->slug).'.png';
    }
    public function getNameAttribute($value)
    {
        return ucwords(str_replace("_", " ", $value));
    }

   public function statesub()
   {
         return $this->belongsTo('App\AdPriceMaster','state_id','id');
   } 
}
