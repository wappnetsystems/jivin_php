<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAccountSetting extends Model
{
    protected $table = 'user_account_settings';
    protected $appends = ['address_proof_url'];
    private $s3_link = "https://jivinmedia.nyc3.cdn.digitaloceanspaces.com/";
    public function getAddressProofUrlAttribute() {
        if(isset($this->address_proof) && !empty($this->address_proof)){
            return asset("address_proff/".$this->address_proof);
        }else{
            return "";
        }
    }
    
    public function getBackgroundImageUrlAttribute() {
        if(isset($this->background_image)){
            return $this->s3_link.'profile_image/'.$this->background_image;
            //return asset('image_file/'.$this->background_image);
        }
        else{
            return "";
        }
    }
    
    public function getUserDetails(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function getCountry(){
        return $this->belongsTo('App\Country','country_id','id');
    }
    public function getRegion(){
        return $this->belongsTo('App\Region','region_id','id');
    }
    public function getState(){
        return $this->belongsTo('App\State','state_id','id');
    }
    public function getDevision(){
        return $this->belongsTo('App\Devision','devision_id','id');
    }
}
