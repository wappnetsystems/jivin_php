<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportPost extends Model
{
	   protected $fillable = ['report_by', 'post_id', 'title', 'details', 'created_at', 'updated_at'];
    protected $table="report_post";


  public function posts()
  {
  	return $this->belongsTo('App\Posts','post_id','id');
  }

    public function users()
   {
    return $this->hasMany('App\User','id','report_by');
   }
}
