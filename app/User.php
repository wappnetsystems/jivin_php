<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Interests;
use App\Followers;
use App\Posts;
use App\User_category_interest;

class User extends Authenticatable {
    private $s3_link = "https://jivinmedia.nyc3.cdn.digitaloceanspaces.com/";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
    //Add extra attribute
    //protected $attributes = ['user_image_url','age'];
    //Make it available in the json response
    protected $appends = ['user_image_url', 'age', 'total_events', 'interests', 'total_video_votes', 'total_audio_votes'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'updated_at', 'created_at', 'user_image'
    ];
    
    public function getNameAttribute($value) {
        return ucfirst($value);
    }

    public function getTotalFollowsAttribute() {
        return self::selectRaw('count(*) as followers')->leftjoin('followers', 'users.id', '=', 'followers.user_id')->where('users.id', $this->id)->where('followers.user_id', $this->id)->where('followers.is_active', 1)->count();
    }

    public function getTotalFollowedByAttribute() {
        return self::selectRaw('count(*) as followers')->leftjoin('followers', 'users.id', '=', 'followers.is_following')->where('users.id', $this->id)->where('followers.is_following', $this->id)->where('followers.is_active', 1)->where('followers.is_following', '!=', 0)->count();
    }

    public function getTotalAudiosAttribute() {
        return Posts::where('user_id', '=', $this->id)->where('audio_name', '!=', '')->count();
    }

    public function getTotalVideosAttribute() {
        return Posts::where('user_id', '=', $this->id)->where('video_name', '!=', '')->count();
    }

    public function getTotalAudioVotesAttribute() {
        $getTotalAudioVote = Posts::where('user_id', '=', $this->id)
                ->where('audio_name', '!=', '')
                ->selectRaw('*, sum(total_like) as total_audio_vote')
                ->first();
        if ($getTotalAudioVote->total_audio_vote) {
            return $getTotalAudioVote->total_audio_vote;
        } else {
            return 0;
        }
    }

    public function getTotalVideoVotesAttribute() {
        $getTotalVideoVote = Posts::where('user_id', '=', $this->id)
                ->where('video_name', '!=', '')
                ->selectRaw('*, sum(total_like) as total_video_vote')
                ->first();
        if ($getTotalVideoVote->total_video_vote) {
            return $getTotalVideoVote->total_video_vote;
        } else {
            return 0;
        }
    }

    public function getTotalVotesAttribute() {
        $getTotalVote = Posts::where('user_id', '=', $this->id)
                //->where('video_name','!=','')
                //->orWhere('audio_name','!=','')
                ->selectRaw('*, sum(total_like) as total_vote')
                // ->orderBy('total_like','DESC')       
                ->first();
        if ($getTotalVote->total_vote) {
            return $getTotalVote->total_vote;
        } else {
            return 0;
        }
    }

    public function isFollowing() {
        $following = $this->hasMany('App\Followers')->where('is_following', '!=', 0)->where('is_active', '=', 1)->with('followingUser');
        return $following;
    }

    public function follows($search_keyword="") {
        
        $follows = $this->hasMany('App\Followers', 'is_following')->where('is_following', '!=', 0)->where('is_active', '=', 1)->with(['followsUser'])->groupBy('user_id');
        return $follows;
    }

    public function events() {
        return $this->hasMany('App\Events')->limit(4)->orderBy('id', 'DESC');
    }

    public function getInterestsAttribute() {
        $InterestsArray = [];
        foreach ($interests = Interests::where('user_id', '=', $this->id)->get() as $key => $interest) {
            $InterestsArray[] = $interest->interest_name;
        }
        return implode(',', $InterestsArray);
    }

    public function getTotalEventsAttribute() {
        return $this->hasMany('App\Events')->count();
    }

    public function accountSettings() {
        return $this->hasOne('App\AccountSettings');
    }
    
    public function userAddressSettings() {
        return $this->hasOne('App\UserAccountSetting');
    }

    public function designSettings() {
        return $this->hasOne('App\AccountSettings');
    }

    public static function getfollowerSuggetionsFromTempUserIds($user_interests, $limit = null) {
        $user_interests_array_ids = [];

        if ($user_interests_array = explode(',', $user_interests)) {
            if ($limit) {
                //$other_user_interests = Interests::whereIn('interest_id', $user_interests_array)->groupBy('user_id')->limit($limit)->get();
                $other_user_interests = User_category_interest::whereIn('interest_cat_id', $user_interests_array)->groupBy('user_id')->limit($limit)->get();
            } else {
                //$other_user_interests = Interests::whereIn('interest_id', $user_interests_array)->groupBy('user_id')->get();
                $other_user_interests = User_category_interest::whereIn('interest_cat_id', $user_interests_array)->groupBy('user_id')->get();
            }

            foreach ($other_user_interests as $key => $other_user_interest) {
                $user_interests_array_ids[] = $other_user_interest->user_id;
            }
        }

        return implode(',', $user_interests_array_ids);
    }

    public static function getfollowerSuggetionsIds($user_id, $limit = null) {
        //$user_interests = Interests::where('user_id', '=', $user_id)->get();
        $user_interests = User_category_interest::where('user_id', '=', $user_id)->get();
        $user_interests_array = [];
        $user_interests_array_ids = [];
        $user_follower_array = [];
        foreach ($user_interests as $key => $user_interest) {
            $user_interests_array[] = $user_interest->interest_cat_id;
        }
        if ($user_interests_array) {
            //$other_user_interests = Interests::whereIn('interest_id', $user_interests_array)->where('user_id', '!=', $user_id)->groupBy('user_id')->get();
            $other_user_interests = User_category_interest::whereIn('interest_cat_id', $user_interests_array)->where('user_id', '!=', $user_id)->groupBy('user_id')->get();
            foreach ($other_user_interests as $key => $other_user_interest) {
                $user_interests_array_ids[] = $other_user_interest->user_id;
            }
        }
        $user_followers = Followers::where('user_id', '=', $user_id)->groupBy('is_following')->get();
        foreach ($user_followers as $key => $user_follower) {
            $user_follower_array[] = $user_follower->is_following;
        }
        if ($limit) {
            $suggestedUsers = User::whereIn('id', $user_interests_array_ids)->select('id', 'name', 'email', 'user_image')->whereNotIn('id', $user_follower_array)->limit($limit)->get()->toArray();
        } else {
            $suggestedUsers = User::whereIn('id', $user_interests_array_ids)->select('id', 'name', 'email', 'user_image')->whereNotIn('id', $user_follower_array)->get()->toArray();
        }
        // dd($suggestedUsers);
        $suggestedUsersArray = [];
        foreach ($suggestedUsers as $key => $suggestedUser) {
            $suggestedUsersArray[] = $suggestedUser['id'];
        }
        return implode(',', $suggestedUsersArray);
    }

    //this 
    public static function getfollowerSuggetionsFromTempUser($user_interests, $logged_user_id, $limit = null) {
        $user_interests_array_ids = [];

        if ($user_interests_array = explode(',', $user_interests)) {
            if ($limit) {
                
                //$other_user_interests = Interests::whereIn('interest_id', $user_interests_array)->groupBy('user_id')->limit($limit)->get();
                $other_user_interests = User_category_interest::whereIn('interest_cat_id', $user_interests_array)->groupBy('user_id')->limit($limit)->get();
            } else {
                //$other_user_interests = Interests::whereIn('interest_id', $user_interests_array)->groupBy('user_id')->get();
                $other_user_interests = User_category_interest::whereIn('interest_cat_id', $user_interests_array)->groupBy('user_id')->get();
            }

            foreach ($other_user_interests as $key => $other_user_interest) {
                $user_interests_array_ids[] = $other_user_interest->user_id;
            }
        }
        
        $suggestedUsers = User::with('accountSettings')
           ->whereIn('id', $user_interests_array_ids)->select('id', 'name', 'username','email','user_type','user_image', 'birth_month', 'birth_year')->get()->toArray();
    
        if ($logged_user_id == 0) {
            return $suggestedUsers;
        }

        foreach ($suggestedUsers as $key => $value) {
            if (Followers::where('user_id',$logged_user_id)->where('is_following',$value['id'])->where('is_active',1)->first()) {
                $suggestedUsers[$key]['is_follow'] = 1;
            } else {
                $suggestedUsers[$key]['is_follow'] = 0;
            }
        }
        return $suggestedUsers;
    
    }

    public static function getfollowerSuggetions($user_id, $limit = null) {
        //$user_interests = Interests::where('user_id', '=', $user_id)->get();
        $user_interests = User_category_interest::where('user_id', '=', $user_id)->get();
        $user_interests_array = [];
        $user_interests_array_ids = [];
        $user_follower_array = [];
        foreach ($user_interests as $key => $user_interest) {
            $user_interests_array[] = $user_interest->interest_cat_id;
        }
        if ($user_interests_array) {
            //$other_user_interests = Interests::whereIn('interest_id', $user_interests_array)->where('user_id', '!=', $user_id)->groupBy('user_id')->get();
            $other_user_interests= User_category_interest::whereIn('interest_cat_id', $user_interests_array)->where('user_id', '!=', $user_id)->groupBy('user_id')->get();
            foreach ($other_user_interests as $key => $other_user_interest) {
                $user_interests_array_ids[] = $other_user_interest->user_id;
            }
        }
        $user_followers = Followers::where('user_id', '=', $user_id)->groupBy('is_following')->get();
        foreach ($user_followers as $key => $user_follower) {
            $user_follower_array[] = $user_follower->is_following;
        }
        if ($limit) {
            return $suggestedUsers = User::where('is_active', '=', 1)->whereIn('id', $user_interests_array_ids)->select('id', 'username', 'name', 'email', 'user_image', 'birth_month', 'birth_year')->whereNotIn('id', $user_follower_array)->limit($limit)->get()->toArray();
        } else {
            return $suggestedUsers = User::where('is_active', '=', 1)->whereIn('id', $user_interests_array_ids)->select('id', 'username', 'name', 'email', 'user_image', 'birth_month', 'birth_year')->whereNotIn('id', $user_follower_array)->get()->toArray();
        }
    }

    public function getUserImageUrlAttribute() {
        //return env('IMG_URL') . 'images/users/' . $this->user_image;
        if($this->registrationtype=="facebook" || $this->registrationtype=="twitter" || $this->registrationtype=="linkedin"){
            return $this->s3_link.'profile_image/'.$this->user_image;
        }
        else{
            return $this->s3_link.'profile_image/'.$this->user_image;
            //return asset('image_file/'.$this->user_image);
        }
    }

    public function getAgeAttribute() {
        $date = "";
        if ($this->birth_month && $this->birth_year) {
            $date = "1-" . $this->birth_month . "-" . $this->birth_year;
        }
        return $this->findage($date);
    }

    public function findage($dob) {
        if (!$dob) {
            return "";
        }
        $localtime = getdate();
        $today = $localtime['mday'] . "-" . $localtime['mon'] . "-" . $localtime['year'];
        $dob_a = explode("-", $dob);
        $today_a = explode("-", $today);
        $dob_d = $dob_a[0];
        $dob_m = $dob_a[1];
        $dob_y = $dob_a[2];
        $today_d = $today_a[0];
        $today_m = $today_a[1];
        $today_y = $today_a[2];
        $years = $today_y - $dob_y;
        $months = $today_m - $dob_m;
        if ($today_m . $today_d < $dob_m . $dob_d) {
            $years--;
            $months = 12 + $today_m - $dob_m;
        }

        if ($today_d < $dob_d) {
            $months--;
        }

        $firstMonths = array(1, 3, 5, 7, 8, 10, 12);
        $secondMonths = array(4, 6, 9, 11);
        $thirdMonths = array(2);

        if ($today_m - $dob_m == 1) {
            if (in_array($dob_m, $firstMonths)) {
                array_push($firstMonths, 0);
            } elseif (in_array($dob_m, $secondMonths)) {
                array_push($secondMonths, 0);
            } elseif (in_array($dob_m, $thirdMonths)) {
                array_push($thirdMonths, 0);
            }
        }
        return $years;
        //echo "<br><br> Age is $years years $months months.";
    }

    public function posts() {
        return $this->belongsTo('App\ReportPost');
    }

    public function getAccountDetails() {
        return $this->belongsTo('App\AccountSettings', 'id', 'user_id');
    }

    // public function userInfo()
    // {
    //     return $this->belongsTo('App\User','id')->with('accountSettings');
    // } 
}
