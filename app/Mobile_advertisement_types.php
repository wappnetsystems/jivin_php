<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobile_advertisement_types extends Model
{
    protected $table = 'mobile_advertisement_types';
    protected $hidden = [
        'updated_at', 'created_at'
    ];
    
}
