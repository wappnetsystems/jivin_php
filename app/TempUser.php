<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempUser extends Model
{
    protected $table = 'temp_users';
    protected $hidden = [
        'password','updated_at', 'created_at'
    ];    
}
