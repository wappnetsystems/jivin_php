<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportRequestAttachment extends Model
{
    //

    protected $table = 'support_request_attachment';
    protected $hidden = [
        'updated_at', 'created_at'
    ];
    protected $fillable=['request_id','file_name','file_type'];
    protected $appends =['file_url'];
  
    public function getFileUrlAttribute()
    {
        return env('IMG_URL').'uploads/'. $this->file_name;
    }    
}
