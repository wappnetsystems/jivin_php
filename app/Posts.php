<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PostLikeLog;
use App\Lib\Upload_file;

class Posts extends Model {

    protected $table = 'posts';
    protected $hidden = [
        'created_at', 'audio_name', 'audio_name'
    ];
    //protected $appends = ['time_ago', 'total_vote', 'audio_url', 'video_url', 'media_type'];
    protected $appends = ['time_ago', 'total_vote', 'audio_url', 'video_url', 'media_type', 'video_thumb_url'];

    public function post_comment() {
        return $this->hasMany('App\Post_comment', 'post_id','id')->where('is_deleted',0)->orderBy('id','DESC');
    }

    public function getVideoThumbUrlAttribute() {
        // if ($this->video_thumb) {
        //     return asset('video_file/video_thumb/' . $this->video_thumb);
        // } else {
        //     return "";
        // }
        if ($this->video_thumb) {
            return Upload_file::get_video_thumb_path($this->video_thumb);
        } else {
            return "";
        }

    }

    public function getVideoAudioTimeDetailAttribute() {
        return $this->video_audio_time;
    }

    public function getMediaTypeAttribute() {
        if ($this->audio_name) {
            return "audio";
        } elseif ($this->video_name) {
            return "video";
        } else {
            return "text";
        }
    }

    public function getAudioUrlAttribute() {
        /* if ($this->audio_name) {
          return env('IMG_URL') . 'post/' . $this->audio_name;
          } else {
          return "";
          } */
        if ($this->audio_name) {
            return Upload_file::get_post_file_path('Audio', $this->audio_name);
        } else {
            return "";
        }
    }

    public function getVideoUrlAttribute() {
        /* if ($this->video_name) {
          return env('IMG_URL') . 'post/' . $this->video_name;
          } else {
          return "";
          } */

        if ($this->video_name) {
            return Upload_file::get_post_file_path('Video', $this->video_name);
        } else {
            return "";
        }
    }

    public function getTimeAgoAttribute() {
        return $this->timeAgo($this->created_at);
    }

    public function getTotalVoteAttribute() {

        $post_id = ($this->attribute['org_post_id']) ? $this->attribute['org_post_id'] : $this->attribute['id'];
        $getPostLike = PostLikeLog::where('post_id', $post_id)->get()->count();
        // $getPostLike = $this->total_like;
        return $getPostLike;
    }

    public function timeAgo($time_ago) {
        $time_ago = strtotime($time_ago);
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        }
        //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        }
        //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        }
        //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        }
        //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "$months months ago";
            }
        }
        //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }

    public function reportposts() {
        return $this->hasMany('App\ReportPost', 'id', 'post_id');
    }

    public function userposts() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function post_votes() {
        return $this->hasMany('App\PostLikeLog', 'post_id', 'id');
    }

}
