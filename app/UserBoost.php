<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBoost extends Model
{
    protected $table = 'user_boost';


    public function userboostplans()
    {
        return $this->belongsTo('App\BoostPlans','boost_plan','id');
    } 

    public function userdetails()
    {
        return $this->belongsTo('App\User','user_id','id');
    } 

    /*
    protected $appends = ['user_image_url'];
    
    public function getUserImageUrlAttribute()
    {
        if($this->image_type == "Profile"){
            return env('IMG_URL').'images/users/'. $this->user_image;
        }else if($this->image_type == "Cover"){
            return env('IMG_URL').'images/users/background/'. $this->user_image;
        }
        
    }
    */
}
