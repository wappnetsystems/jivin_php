<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Country;
use App\Region;
use App\Devision;
use App\State;

class Api extends Model
{
    public static function getLocationId($model,$slug){
        
        switch ($model) {
            case "Country":
                return Country::where('slug', '=',$slug)->first()->id;
                break;
            case "Region":
                return Region::where('slug', '=',$slug)->first()->id;
                break;
            case "Devision":
                return Devision::where('slug', '=',$slug)->first()->id;
                break;
            default:
                return State::where('slug', '=',$slug)->first()->id;
                break;
        }        
    }

    public static function getLocationName($model,$slug){
        switch ($model) {
            case "Country":
                return Country::where('slug', '=',$slug)->first()->name;
                break;
            case "Region":
                return Region::where('slug', '=',$slug)->first()->name;
                break;
            case "Devision":
                return Devision::where('slug', '=',$slug)->first()->name;
                break;
            default:
                return State::where('slug', '=',$slug)->first()->name;
        }        
    }
	//Created By : Debasis Chakraborty
	public static function make_response($success,$data,$msg,$extraData=[]){
		    $response =  [];
	        $response['success'] = $success;
	        $response['msg'] =$msg;
            foreach ($extraData as $key => $value) {
                $response[$key] =$value;
            }
	        $response['data'] = $data;
	        $response['error'] = [];
	        return Api::replace_null_with_empty_string($response);
	}
	//Created By : Debasis Chakraborty
    public static function replace_null_with_empty_string($array)
    {
        foreach ($array as $key => $value) 
        {
            if(is_array($value))
                $array[$key] = Api::replace_null_with_empty_string($value);
            else
            {
                if (is_null($value))
                    $array[$key] = "";
            }
        }
        return $array;
    } 
}
