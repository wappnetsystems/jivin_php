<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubInterest extends Model {

    protected $table = 'sub_intereste_category';
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    protected $appends = ['interest_category_name'];

    public function interestCategory() {
        return $this->belongsTo('App\InterestCategory', 'interest_category_id');
    }

    public function getInterestCategoryNameAttribute() {
        $interests = InterestCategory::find($this->interest_category_id);
        if ($interests) {
            return $interests->name;
        } else {
            return "";
        }
    }

    //Created By : Debasis Chakraborty
    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

}
