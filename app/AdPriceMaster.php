<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdPriceMaster extends Model
{
	protected $table = 'ad_price_master'; 
	protected $appends = ['state_name'];
	public function getadpageid()
	{
	    return $this->belongsTo('App\AdPages','ad_page_id','id'); //hasMany belongsTo
	}
   public function substate()
   {
         return $this->belongsTo('App\State','state_id','id');
   } 
    public function getStateNameAttribute()
    {
       return State::where('id','=',$this->state_id)->first()->name;
    }
}
