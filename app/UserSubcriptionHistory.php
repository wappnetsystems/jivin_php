<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSubcriptionHistory extends Model
{
    protected $table = 'user_subcription_history';
    protected $hidden = [
        'updated_at', 'created_at','deleted_at'
    ];

   public function getplan()
   {
         return $this->hasMany('App\Plans','id','package_id');
   }
   public function getuser()
   {
         return $this->hasMany('App\User','id','user_id');
   } 
}
