<?php

return [

	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => '\\OAuth\\Common\\Storage\\Session',

	/**
	 * Consumers
	 */
	'consumers' => [

		'Facebook' => [
			'client_id'     => '',
			'client_secret' => '',
			'scope'         => [],
		],
		'Google' => [
		    'client_id'     => '325839229776-s0ucjtnue1s1bl2vcsgodcjv2i53mfqr.apps.googleusercontent.com',
		    'client_secret' => 'M42-XKfncwzkzSgpCL3oe2ko',
		    'scope'         => ['userinfo_email', 'userinfo_profile', 'https://www.google.com/m8/feeds/'],
		],

	]

];